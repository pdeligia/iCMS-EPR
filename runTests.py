#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import os
import sys
import unittest
import time
import logging

logging.basicConfig(format = '%(asctime)-15s: %(message)s', level=logging.INFO)
# logger = logging.getLogger("UnitTesting")
# logger.setLevel(logging.INFO)


try:
    import coverage
except ModuleNotFoundError:
    logging.warning('going to install coverage explicitly ... ')
    import subprocess
    subprocess.call( 'source ./venv-py3/bin/activate; pip install coverage', shell=True )
    import coverage

COV = None
if os.environ.get('ICMS_EPR_COVERAGE'):
    COV = coverage.coverage(branch=True, include='app/*')
    COV.start()
    print("Coverage started ... ")


for scrPath in ['./scripts', '..']:
    if scrPath not in sys.path:
        sys.path.append(scrPath)

import SetupDummies

from app import create_app, db

class EPRTestCase(unittest.TestCase):

    def setUp(self):
        theApp = create_app( 'testing' )
        self.app = theApp.test_client()
        with theApp.app_context():
            theApp.init_db()

    def tearDown(self):
        db.session.rollback()

def main():
    global COV
    if coverage and not os.environ.get('ICMS_EPR_COVERAGE'):
        os.environ['ICMS_EPR_COVERAGE'] = '1'
        print( "Going to run with coverage ... " )
        os.execvp(sys.executable, [sys.executable] + sys.argv)

    # prepare the DB
    theApp = create_app( 'testing' )
    with theApp.app_context( ) :
        dbUrl = str(db.engine.url)
        print( "DB URL: '%s' " % dbUrl )
        if ( not dbUrl.startswith('postgres') or
             (not dbUrl.endswith('TestDB') and not dbUrl.endswith('_test') and not dbUrl.endswith('TestDBNew'))
             ) :
            print( "\nFATAL: will not touch non-test DB %s \n" % (dbUrl[:8]+'...'+dbUrl[-8:]) )
            sys.exit(-1)
        # SetupDummies.resetDB( )
        SetupDummies.setup( )
        print("DB set up ...")

    print('going to run tests')

    # loader = unittest.TestLoader( ).discover( 'tests' ) # , pattern='*api*' )
    loader = unittest.TestLoader().loadTestsFromNames([ 'tests.BaseTestClass.BaseTestCase',
                                                        'tests.test_JsonDiffer.JsonDifferTestCase',
                                                        'tests.test_api.APITestCase',
                                                        'tests.test_basics.BasicsTestCase',
                                                        'tests.test_client.ClientTestCase',
                                                        'tests.test_admin_api.ApiAdminTestCase',
                                                        'tests.test_logicPledge.LogicTestCase',
                                                        'tests.test_logicInstRespPledges.InstRespPledgesTestCase',
                                                        # 'tests.test_logicPledge.LogicTestCase2',
                                                        'tests.test_userView.UserModelTestCase',
                                                        'tests.test_manage.ManageTestCase',
                                                        'tests.test_managerView.PledgesTestCase',
                                                        'tests.test_managerView.TaskEditTestCase',
                                                        'tests.test_managerView.MakeActivityTestCase',
                                                        'tests.test_PM_api.ApiPmTestCase',
                                                        'tests.test_adminView.AdminTestCase',
                                                        'tests.test_shiftTaskMap.ShiftTaskMapTestCase',
                                                        'tests.test_helpers.HelpersTestCase',
                                                        'tests.test_ldapAuth.LDAPAuthTestCase',
                                                        'tests.test_history.HistoryTestCase',
                                                        'tests.test_mail.MailTestCase',
                                                        # 'tests.test_adminView.AdminTaskTestCase',
                                                        ])

    # temporarily enabling only some tests, if the 'SELENIUM_FORCE_ALL' env var is NOT set:
    if 'UNIT_TEST_FORCE_ALL' not in os.environ.keys() and '--force-all' not in sys.argv:
        # COV = None
        # loader = unittest.TestLoader( ).discover( 'tests', pattern='test_history.py' )
        # loader = unittest.TestLoader( ).discover( 'tests', pattern='test_logicPledge.py' )
        # loader = unittest.TestLoader( ).discover( 'tests', pattern='test_api.py' )
        # loader = unittest.TestLoader( ).discover( 'tests', pattern='test_client.py' )
        # loader = unittest.TestLoader().loadTestsFromNames(['tests.test_managerView.TaskTestCase'])
        loader = unittest.TestLoader().loadTestsFromNames(['tests.test_logicInstRespPledges.InstRespPledgesTestCase'])
        # loader = unittest.TestLoader( ).discover( 'tests', pattern='test_shiftTaskMap.py' )
        COV = None
        pass

    ret = unittest.TextTestRunner( descriptions=True, verbosity=2 ).run( loader )

    time.sleep( 2 ) # wait a bit for output from subprocesses
    sys.stderr.flush()
    sys.stdout.flush()

    print( "\nran %s tests, errors: %s, failures %s, unexpectedSuccesses %s \n" % (ret.testsRun, len(ret.errors), len(ret.failures), len(ret.unexpectedSuccesses)) )

    retCode = int( len(ret.errors) ) + \
              int( len(ret.failures) ) + \
              int( len(ret.unexpectedSuccesses) )

    if COV:
        COV.stop()
        COV.save()
        print('Coverage Summary:')
        overallCoverage = COV.report()
        basedir = os.path.abspath(os.path.dirname(__file__))
        covdir = os.path.join(basedir, 'tmp/coverage')
        COV.html_report(directory=covdir)
        print( '\nHTML version of the results at: file://%s/index.html' % covdir )
        print( '\nOverall coverage: %2.0f%%\n' % overallCoverage )

    return retCode

if __name__ == '__main__' :
    retCode = main()
    sys.exit( -1*retCode )
