#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import threading
import flask

import subprocess

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from sqlalchemy.exc import ProgrammingError

import unittest
import os, sys
import logging

srcPaths = ['.', './scripts']
for srcPath in srcPaths:
    if srcPath not in sys.path:
        sys.path.append(srcPath)

from app import create_app, db
from app.models import Project

from testutils import URL_TEST_PORT
import SetupDummies

logging.basicConfig(format = '%(asctime)-15s: %(message)s', level=logging.DEBUG)
logger = logging.getLogger("SeleniumTesting")
logger.setLevel(logging.DEBUG)

class CentralThread(threading.Thread):

    def __init__(self, web_app, **kwargs):
        threading.Thread.__init__(self, **kwargs)
        self.web_app = web_app
        print("Central Server Thread initialised.")

    def run(self):
        print("Central Server Thread starting.")
        self.web_app.run(host='127.0.0.1', port=URL_TEST_PORT, threaded=True)

def doCmd(cmd, verbose=False, dryRun=False):

    if verbose:
        print( '\n[verbose] going to execute: "%s"' % cmd )

    out = ''
    err = ''
    returnCode = 0

    if dryRun:
        print( 'dryRun requested. No action ... ' )
    else:
        process = subprocess.Popen( cmd, shell=True, stdin=subprocess.PIPE, stderr=subprocess.STDOUT )
        out, err = process.communicate()
        returnCode = process.returncode

    msg = ''
    if verbose:
        if out: msg += '\n%s' % out
        if err: msg += '\n%s' % err

    if msg:
        print( msg )
    if returnCode != 0:
        raise subprocess.CalledProcessError(returnCode, cmd)

    return returnCode, out, err

initDB = True

def main():

    # todo: any way to avoid hard-coding the string here? -but without importing the package as it will create a webapp instance, initialize the logger etc.
    # root_path = os.path.join(os.getcwd(), 'webapp')
    web_app = create_app( 'testing' )
    web_app.logger.setLevel( logging.DEBUG )

    # a shutdown route for werkzeug
    @web_app.route('/shutdown')
    def route_shutdown():
        func = flask.request.environ.get('werkzeug.server.shutdown')
        message = 'Shutdown impossible. Try shooting down...'
        if func:
            message = 'All good, shutdown imminent.'
            func()
        return message

    @web_app.route('/rollback_db')
    def route_rollback_db():

        db.session.rollback()
        db.session.close()

        message = 'Database successfully rolled back.'
        logger.info(message)
        return message

    @web_app.route('/restore_db/<string:which>')
    def route_restore_db(which):

        logger.info( "going to reset DB ... " )

        global initDB

        try:
            prjList = db.session.query(Project).all()
        except ProgrammingError as e:
            logger.info( 'Got ProgErr: %s' % str(e) )
            if 'relation "epr.projects" does not exist' in str(e):
                prjList = []

        if initDB or prjList == []:
            initDB = False
            try :
                SetupDummies.resetDB()
            except Exception as e :
                logger.error( "ERROR re-setting DB: %s " % str( e ) )
                raise e

            logger.info("DB successfully reset.")

            try :
                SetupDummies.setup( pledges=True )
            except Exception as e :
                logger.error( "ERROR setting up DB: %s " % str( e ) )

            logger.info("Dummies successfully setup.")

            # logger.info( "\n%s\n DB initially set up \n%s\n" % ('=' * 42, '=' * 42) )
            # cmd = 'dropdb --if-exists CovSelTestDB_copy; createdb CovSelTestDB_copy; '
            # cmd += 'pg_dump CovSelTestDB | psql -q CovSelTestDB_copy '
            # doCmd( cmd, verbose=False )
            # logger.info( "\n%s\n DB copied to backup \n%s\n" % ('=' * 42, '=' * 42) )

        # db.session.rollback()
        # db.session.close()
        # db.session.close()
        # cmd = 'dropdb --if-exists CovSelTestDB; createdb CovSelTestDB; '
        # cmd += 'pg_dump CovSelTestDB_copy | psql -q CovSelTestDB'
        # doCmd( cmd, verbose=False )

        # try:
        #     SetupDummies.resetDB()
        # except Exception as e :
        #     logger.error( "ERROR re-setting DB: %s " % str( e ) )
        # try:
        #     if 'full' in which.lower():
        #         SetupDummies.setup(pledges=True)
        #     else:
        #         SetupDummies.setup(pledges=False)
        # except Exception as e:
        #     logger.error( "ERROR setting up DB: %s " % str(e) )

        message = 'Database {0} restored successfully!'.format(which)
        logger.info(message)
        return message


    app_thread = CentralThread(web_app, name='WebAppThread')
    app_thread.start()

    test_runner = unittest.TextTestRunner()
    test_loader = unittest.TestLoader()
    print( "starting from ", os.path.dirname(__file__) )

    # complete_test_suite = test_loader.discover( start_dir=os.path.dirname(__file__), pattern='selenium_test_*.py')
    complete_test_suite = test_loader.loadTestsFromNames( [ 'selenium_cases.selenium_test_InstView.InstViewTestCase',
                                                            'selenium_cases.selenium_test_basics.BasicThingsTestCase',
                                                            'selenium_cases.selenium_test_eGrpMgt.eGroupManagementTestCase',
                                                            'selenium_cases.selenium_test_pledgeManager.PledgeManagerTestCase',
                                                            'selenium_cases.selenium_test_pledgeUser.PledgeUserTestCase',
                                                            'selenium_cases.selenium_test_pledgeUser2015.PledgeUserPrevYearsTestCaseFail',
                                                            'selenium_cases.selenium_test_pledgeUser2015.PledgeUserPrevYearsTestCase',
                                                            'selenium_cases.selenium_test_pledgeUserFuture.PledgeUserFutureTestCase',
                                                            'selenium_cases.selenium_test_pledgeUserSelYear.PledgeUserSelYearTestCase',
                                                            'selenium_cases.selenium_test_showMine.ShowMineTestCase',
                                                            'selenium_cases.selenium_test_manage.ManagementTestCase',
                                                            ] )

    # temporarily enabling only some tests, if the 'SELENIUM_FORCE_ALL' env var is NOT set:
    if 'SELENIUM_FORCE_ALL' not in os.environ.keys() :
        complete_test_suite = test_loader.discover( start_dir=os.path.dirname(__file__), pattern='selenium_test_pledgeManager.py' )

    test_results = test_runner.run(complete_test_suite)

    # driver = webdriver.PhantomJS()
    driver = webdriver.Firefox()

    # options = Options()
    # options.headless = True  # older webdriver versions
    # # options.set_headless( True )  # newer webdriver versions
    # options.add_argument( '--no-sandbox' )
    # print( "option no-sandbox for chromium set ... " )
    # driver = webdriver.Chrome( options=options )

    print( 'Pre-shutdown nap' )
    print( "closing ... (port %i) " % URL_TEST_PORT )
    driver.implicitly_wait(20)
    driver.set_page_load_timeout(20)
    driver.get('http://127.0.0.1:%i/shutdown' % URL_TEST_PORT)
    driver.quit()

    print( 'Tests run: %d, \t failures: %d, \t skipped: %d, \t errors: %d, \t unexpectedSuccesses: %d' % \
          (test_results.testsRun, len(test_results.failures), len(test_results.skipped), len(test_results.errors), len(test_results.unexpectedSuccesses)) )

    retCode = int( len(test_results.errors) ) + \
              int( len(test_results.failures) ) + \
              int( len(test_results.unexpectedSuccesses) )

    app_thread.join(1.)

    return retCode

if __name__ == '__main__':
    sys.exit( main() )

