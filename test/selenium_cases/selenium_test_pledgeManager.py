
import time

from .SeleniumTestBase import SeleniumTestCase, loginUser

from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# from app.models import Pledge
from .pledges import createPledge, updatePledge, rejectPledge, clearAndSend

class PledgeManagerTestCase(SeleniumTestCase):

    def setUp( self ) :
        super(PledgeManagerTestCase, self).setUp()

        self.id = 'plM'
        self.username = 'p1m1' # manager of project 1
        # loginUser( username = self.username, theTest=self )
        # self.logger.info(" %s logged in ... " % self.username)

    def test_approveMultiPldManager(self):

        loginUser( username = self.username, theTest=self )

        self.go_to('/showProject/1')

        self.debugLogWithScreenShot('approveMulti-0' )
        ta1 = WebDriverWait( self.driver, 10 ).until( EC.visibility_of_element_located( (By.XPATH, '//table[@id="projectTable"]/tbody/tr/td[text()=" 1.10"]') ) )

        self.assertIn('iCMS - EPR Pledge Overview for pr1 ', self.driver.title)

        # # tick the checkbox for task ta1 in the rows of the table:
        nAc1 = 0
        for row in self.driver.find_elements_by_xpath('//table[@id="projectTable"]/tbody/tr'):
            # print '==> ', row.text
            cols = row.find_elements_by_tag_name( 'td' )
            if not cols: continue # ignore header rows
            # print "got %d cols: %s " % (len(cols), ' | '.join(x.text.strip() for x in cols))
            # select the rows for task 'ta1'
            if 'ta1' in cols[3].text:
                cols[0].click()
                nAc1 += 1

        self.debugLogWithScreenShot( 'approveMulti-1' )

        # the "button" to approve looks like this:
        # <a class="dt-button" tabindex="0" aria-controls="projectTable"><span>Approve selected</span></a>
        approveBtn = self.driver.find_element_by_xpath( '//a[@class="dt-button"]/span[text()="Approve selected"]' )
        WebDriverWait( self.driver, 10 ).until( EC.element_to_be_clickable( (By.XPATH, '//a[@class="dt-button"]/span[text()="Approve selected"]') ) )
        approveBtn.click( )

        # wait for some accepted pledges showing up in the table
        ta1 = WebDriverWait( self.driver, 15 ).until( EC.visibility_of_element_located( (By.XPATH, '//table[@id="projectTable"]/tbody/tr/td[text()="accepted"]') ) )

        self.debugLogWithScreenShot( 'approveMulti-2' )
        self.logger.info( "++> approved all %d pledges for 'ta1'" % nAc1 )

        # now check that the 'ta1' pledges are "accepted"
        tas = self.driver.find_elements_by_xpath( '//table[@id="projectTable"]/tbody/tr/td[text()="accepted"]/parent::tr' )
        nA1 = 0
        for t in tas:
            if 'ta1' in t.text.replace('\n', ' '): nA1 += 1

        tns = self.driver.find_elements_by_xpath( '//table[@id="projectTable"]/tbody/tr/td[text()="new"]/parent::tr' )
        nN2 = 0
        for t in tns:
            if 'ta2' in t.text.replace('\n', ' '): nN2 += 1

        self.logger.info( "++> checking: found 'ta1' %d accepted 'ta2' %d new" % (nA1, nN2) )
        self.assertEqual( 2, nA1)
        self.assertEqual( 2, nN2)

        # select all pledges for the project and bulk-approve them
        # ta1 = WebDriverWait( self.driver, 10 ).until( EC.element_to_be_clickable( (By.XPATH, '//a[@class="dt-button"]/span[text()="Select all"]') ) )
        selectAllBtn = self.driver.find_element_by_xpath( '//a[@class="dt-button"]/span[text()="Select all"]' )
        WebDriverWait( self.driver, 10 ).until( EC.element_to_be_clickable( (By.XPATH, '//a[@class="dt-button"]/span[text()="Select all"]') ) )
        selectAllBtn.click()
        # time.sleep(1)

        self.debugLogWithScreenShot( 'approveMulti-3' )

        # the "button" to approve looks like this:
        # <a class="dt-button" tabindex="0" aria-controls="projectTable"><span>Approve selected</span></a>
        approveBtn = self.driver.find_element_by_xpath( '//a[@class="dt-button"]/span[text()="Approve selected"]' )
        WebDriverWait( self.driver, 10 ).until( EC.element_to_be_clickable( (By.XPATH, '//a[@class="dt-button"]/span[text()="Approve selected"]') ) )
        approveBtn.click( )

        # wait for some done pledges showing up in the table
        td1 = WebDriverWait( self.driver, 10 ).until( EC.visibility_of_element_located( (By.XPATH, '//table[@id="projectTable"]/tbody/tr/td[text()="done"]') ) )

        self.debugLogWithScreenShot( 'approveMulti-4' )
        self.logger.info( "++> all pledges for 'ta1' now set to done"  )

        # now check that the 'ta1' pledges are "accepted"
        tas = self.driver.find_elements_by_xpath( '//table[@id="projectTable"]/tbody/tr/td[text()="accepted"]/parent::tr' )
        nA2 = 0
        for t in tas:
            if 'ta2' in t.text.replace('\n', ' '): nA2 += 1

        tns = self.driver.find_elements_by_xpath( '//table[@id="projectTable"]/tbody/tr/td[text()="done"]/parent::tr' )
        nD1 = 0
        for t in tns:
            if 'ta1' in t.text.replace('\n', ' '): nD1 += 1

        self.logger.info( "++> checking: 'ta1' %d done, 'ta2' %d accepted " % (nD1, nA2) )
        self.debugLogWithScreenShot( 'approveMulti-5' )

        self.assertEqual( 2, nD1 )
        self.assertEqual( 2, nA2 )


    def testPledgeManagerEGroup(self):

        loginUser('a4meg', self)

        self.go_to('/showProject/2')
        # wait a bit to load the data ...
        time.sleep(1)

        self.debugLogWithScreenShot( 'pledgeManagerEGroup-0' )

        wait = WebDriverWait( self.driver, 10 )
        ta1 = wait.until( EC.visibility_of_element_located( (By.XPATH, '//table[@id="projectTable"]/tbody/tr/td[text()=" 1.70"]') ) )

        # update a pledge for user 'u3'

        username = 'u3'

        # ---------- modify the pledge ----------
        # this part assumes that we only have one pledge !!!

        self.logger.info('going to update pledge for user %s ', username)

        # make sure we're working on the test DB
        self.assertIn( 'This is not the production version, all changes will be discarded',
                          self.driver.page_source )

        time.sleep( 1 )

        editPl = None
        for item in self.driver.find_elements_by_xpath( '//table[@id="projectTable"]/tbody/tr/td[text()=" 1.70"]/following-sibling::td/a' ) :
            link = item.get_attribute( 'href' )
            if '/pledge/7' in link.lower() :
                editPl = link
                break

        self.assertNotEqual( editPl, None )

        self.debugLogWithScreenShot( 'pledgeManagerEGroup-1' )

        # make sure we're working on the test DB
        self.assertIn( 'This is not the production version, all changes will be discarded',
                          self.driver.page_source )
        self.logger.info( "found link for editing pledge as: '%s'" % editPl )
        self.go_to( '/pledge/7' )

        self.debugLogWithScreenShot( 'pledgeManagerEGroup-2' )

        clearAndSend( self, 'workTimePld', '3.' )
        clearAndSend( self, 'workedSoFar', '2.' )
        self.driver.find_element_by_xpath( "//input[@type='submit']" ).click()
        time.sleep( 2 )
        self.assertNotIn( '502 Bad Gateway', self.driver.page_source )

        alertCrit = self.driver.find_elements_by_class_name( 'alert-danger' )
        self.assertListEqual( alertCrit, [ ] )

        alertWarn = self.driver.find_elements_by_class_name( 'alert-warning' )
        self.assertIn( 'Pledge info successfully updated.', alertWarn[ 0 ].text )

        self.debugLogWithScreenShot( 'pledgeManagerEGroup-3' )

        # check work time pledged got updated to 3. for pledge-7. As the row-order
        # can change, we need to look at each row until we find the one for pledge-7,
        # then look in there if we find a column with '3.00' ...
        rows = self.driver.find_elements_by_xpath( '//table[@id="projectTable"]/tbody/tr' )
        for row in rows:
            for item in row.find_elements_by_xpath( '//td/a' ):
                if '/pledge/7' in item.get_attribute( 'href' ).lower():
                    self.assertEqual( row.find_element_by_xpath( '//td[text()=" 3.00"]' ).text, '3.00')

        self.debugLogWithScreenShot( 'pledgeManagerEGroup-4' )

        alertWarn = self.driver.find_elements_by_class_name( 'alert-warning' )
        self.assertIn( 'Pledge info successfully updated.', alertWarn[ 0 ].text )


    def testPledgeManager(self):

        loginUser('p1m1', self)

        # make a pledge for user 'u1', then update and reject it

        username = 'p1m1'
        self.logger.info('going to create pledge for user %s ', username)
        createPledge( self, username=username, name='p1, m1' )

        # ---------- modify the pledge ----------
        # this part assumes that we only have one pledge !!!

        self.logger.info('going to update pledge for user %s ', username)
        updatePledge( self, username=username )

        # ---------- finally go and reject the pledge ----------
        self.logger.info('going to reject pledge for user %s ', username)
        rejectPledge( self, username=username )

        # print self.driver.page_source

    def testPledgeFutureManagerFail(self):

        username = 'p1m1'
        selYear = self.selYear+3

        loginUser(username, self)

        # now the same after setting the year to 2020.
        # I did not find a way to do this from the nav menu with Selenium (the drop-down menu was found but did not open)
        self.go_to( '/setYear/%s' % selYear)

        # try to make a pledge for user 'u1', then update and reject it
        self.logger.info('going to create pledge for user %s in %s (should FAIL)' % (username, selYear))
        createPledge( self, username=username, name='p1, m1', year=selYear )

        self.debugLogWithScreenShot( 'futurePledgeFail' )

        # print self.driver.page_source

