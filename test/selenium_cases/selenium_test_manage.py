
# -*- coding: utf-8 -*-

from .SeleniumTestBase import SeleniumTestCase, loginUser

from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from .pledges import createPledge, updatePledge, rejectPledge

class ManagementTestCase(SeleniumTestCase):

    def setUp( self ) :
        super(ManagementTestCase, self).setUp()

        self.id = 'mgt'
        self.username = 'p1m1' # manager of project 1
        loginUser( username = self.username, theTest=self )
        self.logger.info(" %s logged in ... " % self.username)

    def addField( self, idName, value) :
        field = self.driver.find_element_by_xpath( '//input[@id="%s"]' % idName )
        field.clear( )
        field.send_keys( value )

    def addTextArea( self, idName, value) :
        field = self.driver.find_element_by_xpath( '//textarea[@id="%s"]' % idName )
        field.clear( )
        field.send_keys( value )

    def selField( self, idName, optVal) :
        fieldSel = Select( self.driver.find_element_by_xpath( '//select[@id="%s"]' % idName ) )
        fieldSel.select_by_visible_text( optVal.strip() )

    def boolField( self, idName, optVal) :
        field = self.driver.find_element_by_xpath( '//input[@id="%s"]' % idName )
        if optVal: field.click()


    def test_addActFromProjPM(self):

        self.go_to( '/manage/project/p1' )
        self.assertIn( 'This is not the production version, all changes will be discarded',
                       self.driver.page_source )

        self.debugLogWithScreenShot( 'addActFromProj' )

        commitBtn = self.driver.find_element_by_xpath( '//input[@value="Add Activity"]' )
        commitBtn.click( )

        self.debugLogWithScreenShot( 'addActFromProj-0' )

        self.addField( 'name', 'new Act ' )
        self.addField( 'description', 'new act - testing only ... ' )

        self.debugLogWithScreenShot( 'addActFromProj-1' )

        commitBtn = self.driver.find_element_by_xpath( '//input[@type="submit"]' )
        commitBtn.click( )

        self.debugLogWithScreenShot( 'addActFromProj-1-clicked-' )

        self.assertIn( 'A new activity was successfully created.', self.driver.page_source )

    def test_addActFromProjAMFail( self ) :

        self.username = 'a1m1'
        loginUser( self.username, self )  # needs an admin

        self.go_to( '/manage/project/p1' )
        self.assertIn( 'This is not the production version, all changes will be discarded',
                       self.driver.page_source )

        self.debugLogWithScreenShot( 'addActFromProjAMFail' )

        self.assertIn('You are not a manager for this project, ', self.driver.page_source)

    def test_addTaskPM( self ) :

        # loginUser( 'pfeiffer', self )  # needs an admin

        self.addNewTask( self.username )

    #-ap: this should now fail, check again once updates are allowed again
    # def test_addTaskAMSelYearOK( self ) :
    #
    #     self.username = 'a1m1'
    #     loginUser( self.username, self )  # needs an admin
    #
    #     # first, set the year to the actual selYear.
    #     self.go_to('/setYear/%d' % selYear)
    #
    #     # in preparation for the selYear, activity admins are allowed to create new tasks
    #     self.addNewTask( self.username )

    def addNewTask( self, dbgId ):

        # go to the new task page and select a project and activity for the new task:
        self.go_to( '/newTask/None' )
        self.assertIn( 'This is not the production version, all changes will be discarded',
                       self.driver.page_source )

        self.debugLogWithScreenShot( 'addTask-newTask-selPrj-%s' % dbgId )

        WebDriverWait( self.driver, 10 ).until( EC.element_to_be_clickable( (By.XPATH, '//select[@id="projects"]/option[@value="p1"]') ) )
        self.selField( 'projects', 'pr1' )

        self.debugLogWithScreenShot( 'addTask-newTask-selAct-%s' % dbgId )

        WebDriverWait( self.driver, 10 ).until( EC.element_to_be_clickable( (By.XPATH, '//select[@id="activities"]/option[@value="a1"]') ) )
        self.selField( 'activities', 'ac1' )

        self.debugLogWithScreenShot( 'addTask-newTask-%s-filled' % dbgId )

        commitBtn = self.driver.find_element_by_xpath( '//input[@id="selectButton"]' )
        commitBtn.click( )

        # now fill the page for the new task
        self.debugLogWithScreenShot( 'addTask-0-%s' % dbgId )

        # this is now done with tooltips which don't show up in the form any more
        #-toDo: see how to check for this again ... :(
        # # only fill the comment area, this should trigger an error in the form:
        # self.addTextArea( 'comment', 'testing only ... ' )
        #
        # self.debugLogWithScreenShot( 'addTask-1-%s' % dbgId )
        #
        # commitBtn = self.driver.find_element_by_xpath( '//input[@type="submit"]' )
        # commitBtn.click( )
        #
        # self.debugLogWithScreenShot( 'addTask-2-%s' % dbgId )
        #
        # # this first attempt should result in a number of errors, check them:
        # for item in [ 'Work needed (person-months)',
        #               # 'Required % of presence at CERN',
        #               'Short name (required)',
        #               'Description (required)' ]:
        #     self.assertIn( 'Error in the %s field - ' % item, self.driver.page_source )

        # now fill all required fields and try again
        self.addField( 'name', 'test Task1' )
        self.addTextArea( 'description', 'desc for test Task1' )
        self.addField( 'neededWork' , '2')
        self.addField( 'pctAtCERN' , '0')
        self.addTextArea( 'comment', 'still testing only ... ' )

        self.debugLogWithScreenShot( 'addTask-2-%s' % dbgId )

        commitBtn = self.driver.find_element_by_xpath( '//input[@type="submit"]' )
        commitBtn.click( )

        self.debugLogWithScreenShot( 'addTask-2-clicked-%s' % dbgId )

        self.assertIn( 'A new task was successfully created.', self.driver.page_source )

    def testManageAllUser(self):

        loginUser('user1', self)

        self.go_to( '/manage/all' )

        self.debugLogWithScreenShot('manageAllUser')

        self.assertIn( 'This is not the production version, all changes will be discarded',
                          self.driver.page_source )

        # a normal user should not be able to see anything here, just the warning:
        self.assertTrue( self.findInAlerts( 'You are not a manager for this project, ' ) )

        # print self.driver.page_source

    def testManageAll( self ) :
        loginUser( 'p1m1', self )

        self.go_to( '/manage/all' )

        self.assertIn( 'This is not the production version, all changes will be discarded',
                       self.driver.page_source )

        self.debugLogWithScreenShot('manageAllPM')

        self.assertIn( 'Manage Projects', self.driver.page_source )
        self.assertIn( 'project 1', self.driver.page_source )

        self.assertIn( 'Manage Activities', self.driver.page_source )
        self.assertIn( 'activity 2 (p2)', self.driver.page_source )

        self.assertIn( 'Manage Tasks', self.driver.page_source )
        self.assertIn( 'task 1 (a1 p1)', self.driver.page_source )
        self.assertIn( 'task 4 (a3 p2)', self.driver.page_source )

        # print self.driver.page_source

    def testManageProjUserFail(self):

        loginUser('user1', self)

        self.go_to( '/manage/project/p1' )

        self.debugLogWithScreenShot('manageProjUserFail')

        self.assertIn( 'This is not the production version, all changes will be discarded',
                          self.driver.page_source )

        # a normal user should not be able to see anything here, just the warning:
        self.assertTrue( self.findInAlerts( 'You are not a manager for this project, ' ) )

    def testManageActUserFail(self):

        loginUser('user1', self)

        self.go_to( '/manage/activity/a1' )

        self.debugLogWithScreenShot('manageActUserFail')

        self.assertIn( 'This is not the production version, all changes will be discarded',
                          self.driver.page_source )

        self.debugLogWithScreenShot('manageActUserFail-1')

        # a normal user should not be able to see anything here, just the warning:
        self.assertTrue( self.findInAlerts( 'You are not a manager for this activity, ' ) )

    def testManageTaskUserFail(self):

        loginUser('user1', self)

        self.go_to( '/manage/task/t1' )

        self.debugLogWithScreenShot('manageTaskUserFail')

        self.assertIn( 'This is not the production version, all changes will be discarded',
                          self.driver.page_source )

        # a normal user should not be able to see anything here, just the warning:
        self.assertTrue( self.findInAlerts( 'You are not a manager for this task, ' ) )

    def testManageProjectPMFail( self ) :
        self.go_to( '/manage/project/foo' )
        self.assertIn( 'This is not the production version, all changes will be discarded',
                       self.driver.page_source )

        self.debugLogWithScreenShot( 'manageProj-fooFail' )

        self.assertTrue( self.findInAlerts( 'can not find project for code' ) )

    def testManageActivityPMFail( self ) :
        self.go_to( '/manage/activity/foo' )
        self.assertIn( 'This is not the production version, all changes will be discarded',
                       self.driver.page_source )

        self.debugLogWithScreenShot( 'manageAct-fooFail' )

        self.assertTrue( self.findInAlerts( 'can not find activity for code' ) )

    def testManageTaskPMFail( self ) :
        self.go_to( '/manage/task/foo' )
        self.assertIn( 'This is not the production version, all changes will be discarded',
                       self.driver.page_source )

        self.debugLogWithScreenShot( 'manageTask-fooFail' )

        self.assertTrue( self.findInAlerts( 'can not find task for code' ) )

    def testManageActivityPM( self ) :

        self.go_to( '/manage/activity/a5' )

        self.assertIn( 'This is not the production version, all changes will be discarded',
                       self.driver.page_source )

        self.debugLogWithScreenShot('manageAct')

        # update project name:
        selName = self.driver.find_element_by_id( 'name' )
        oldName = selName.text
        self.logger.debug( "found name: \n %s" % selName.text )
        selName.clear()
        selName.send_keys( "ac5 - newName" )

        updateBtn = self.driver.find_element_by_id( 'updateName' )
        updateBtn.click()

        self.assertIn( 'Successfully updated name for ac5', self.driver.page_source )
        self.assertIn( 'Activity: ac5 - newName', self.driver.page_source )

        self.debugLogWithScreenShot('manageAct-newName')

        # update project description:
        selDesc = self.driver.find_element_by_id( 'description' )
        oldDes = selDesc.text
        self.logger.debug( "found description: \n %s" % selDesc.text )
        selDesc.clear()
        selDesc.send_keys( "new desc for Activity 5" )

        updateBtn = self.driver.find_element_by_id( 'update' )
        updateBtn.click()

        self.debugLogWithScreenShot('manageAct-updDesc')

        self.assertIn( 'Successfully updated description for ac5', self.driver.page_source )
        self.assertIn( 'new desc for Activity 5', self.driver.page_source )

        # self.debugLogWithScreenShot('manageUpdateProjDesc-new')

        # now add a new manager:
        addMgr = self.driver.find_element_by_xpath( "//input[@id='addMgrs']" )
        addMgr.click()

        wait = WebDriverWait( self.driver, 10 )
        tableReady = wait.until( EC.visibility_of_element_located( (By.CSS_SELECTOR, "#userTable.display.dataTable" ) ) )

        self.debugLogWithScreenShot('manageAct-addMgrSel0')

        selMgrTable = self.driver.find_element_by_css_selector ( "#userTable.display.dataTable" )
        selTesterRow = selMgrTable.find_element_by_xpath( "//tr[3]" )
        ActionChains( self.driver).move_to_element( selTesterRow ).click( selTesterRow ).perform()

        self.debugLogWithScreenShot('manageAct-addMgrSel1')

        asu = self.driver.find_element_by_xpath( "//div[@class='dt-buttons']/a[@class='dt-button']" )
        asu.click()

        self.debugLogWithScreenShot('manageAct-addMgrSel2')

        confirmSelUser = wait.until( EC.element_to_be_clickable( (By.XPATH, "//form[@id='done']/input[@type='submit']") ) )

        confirmSelUserForm = self.driver.find_element_by_xpath("//form[@id='done']")
        confirmSelUser.submit()
        # ActionChains( self.driver ).move_to_element( confirmSelUserForm ).click( confirmSelUser ).perform( )

        confirmSelUser = wait.until( EC.title_contains("Manage Activity") )

        self.debugLogWithScreenShot('manageAct-addMgrClk')

        # and remove that manager again:
        mgrList = Select(self.driver.find_element_by_name('managers'))
        mgrList.select_by_visible_text("Tester, Master")
        removeMgr = self.driver.find_element_by_xpath("//input[@value='Remove selected managers']")
        removeMgr.click()

        self.debugLogWithScreenShot('manageAct-rmMgrClk')
        self.assertIn( 'Successfully removed manager(s) Tester', self.driver.page_source )

        # print self.driver.page_source

    def testManageTaskPM( self ) :

        self.go_to( '/manage/task/t1' )

        self.assertIn( 'This is not the production version, all changes will be discarded',
                       self.driver.page_source )

        self.debugLogWithScreenShot('manageTask')

        # update project name:
        selName = self.driver.find_element_by_id( 'name' )
        oldName = selName.text
        self.logger.debug( "found name: \n %s" % selName.text )
        selName.clear()
        selName.send_keys( "ta1 - newName" )

        updateBtn = self.driver.find_element_by_id( 'updateName' )
        updateBtn.click()

        self.assertIn( 'Successfully updated name for ta1', self.driver.page_source )
        self.assertIn( 'ta1 - newName', self.driver.page_source )

        self.debugLogWithScreenShot('manageTask-newName')

        # update project description:
        selDesc = self.driver.find_element_by_id( 'description' )
        oldDes = selDesc.text
        self.logger.debug( "found description: \n %s" % selDesc.text )
        selDesc.clear()
        selDesc.send_keys( "new desc for Task 1" )

        updateBtn = self.driver.find_element_by_id( 'update' )
        updateBtn.click()

        self.assertIn( 'Successfully updated description for ta1', self.driver.page_source )
        self.assertIn( 'new desc for Task 1', self.driver.page_source )

        self.logger.debug( "description updated" )

        self.debugLogWithScreenShot('manageUpdateProjDesc-new')

        # now add a new manager:
        addMgr = self.driver.find_element_by_xpath( "//input[@id='addMgrs']" )
        addMgr.click()

        selMgrTable = self.driver.find_element_by_css_selector ( "#userTable.display.dataTable" )
        selTesterRow = selMgrTable.find_element_by_xpath( "//tr[3]" )
        ActionChains( self.driver).move_to_element( selTesterRow ).click( selTesterRow ).perform()

        self.debugLogWithScreenShot('manageTask-addMgrSel0')

        asu = self.driver.find_element_by_xpath( "//div[@class='dt-buttons']/a[@class='dt-button']" )
        asu.click()

        self.debugLogWithScreenShot('manageTask-addMgrSel1')

        wait = WebDriverWait( self.driver, 10 )
        confirmSelUser = wait.until( EC.element_to_be_clickable( (By.XPATH, "//form[@id='done']/input[@type='submit']") ) )

        self.debugLogWithScreenShot('manageTask-addMgrSel2')
        confirmSelUserForm = self.driver.find_element_by_xpath("//form[@id='done']")
        confirmSelUser.submit()
        # ActionChains( self.driver ).move_to_element( confirmSelUserForm ).click( confirmSelUser ).perform( )

        confirmSelUser = wait.until( EC.title_contains("Manage Task") )

        self.debugLogWithScreenShot('manageTask-addMgrClk')

        # and remove that manager again:
        mgrList = Select(self.driver.find_element_by_name('managers'))
        mgrList.select_by_visible_text("Tester, Master")
        removeMgr = self.driver.find_element_by_xpath("//input[@value='Remove selected managers']")
        removeMgr.click()

        self.debugLogWithScreenShot('manageTask-rmMgrClk')
        self.assertIn( 'Successfully removed manager(s) Tester', self.driver.page_source )

        # now add a new observer:
        wait = WebDriverWait( self.driver, 10 )
        addObs = wait.until( EC.element_to_be_clickable( (By.XPATH, "//input[@id='addObs']") ) )
        addObs.click()

        selObsTable = self.driver.find_element_by_css_selector ( "#userTable.display.dataTable" )
        selTesterRow = selObsTable.find_element_by_xpath( "//tr[3]" )
        ActionChains( self.driver).move_to_element( selTesterRow ).click( selTesterRow ).perform()

        self.debugLogWithScreenShot('manageTask-addObsSel0')

        asu = self.driver.find_element_by_xpath( "//div[@class='dt-buttons']/a[@class='dt-button']" )
        asu.click()

        self.debugLogWithScreenShot('manageTask-addObsSel1')

        wait = WebDriverWait( self.driver, 10 )
        confirmSelUser = wait.until( EC.element_to_be_clickable( (By.XPATH, "//form[@id='done']/input[@type='submit']") ) )

        confirmSelUserForm = self.driver.find_element_by_xpath("//form[@id='done']")
        confirmSelUser.submit()
        # ActionChains( self.driver ).move_to_element( confirmSelUserForm ).click( confirmSelUser ).perform( )

        confirmSelUser = wait.until( EC.title_contains("Manage Task") )

        self.debugLogWithScreenShot('manageTask-addObsClk')

        # and remove that observer again:
        obsList = Select(self.driver.find_element_by_name('observers'))
        obsList.select_by_visible_text("Tester, Master")
        removeMgr = self.driver.find_element_by_xpath("//input[@value='Remove selected observers']")
        removeMgr.click()

        self.debugLogWithScreenShot('manageTask-rmObsClk')
        self.assertIn( 'Successfully removed observer(s) Tester, Master from ta1', self.driver.page_source )

        # print self.driver.page_source


    def test_addUser( self ) :

        loginUser('pfeiffer', self) # needs an admin

        self.go_to( '/manage/addUser' )

        self.assertIn( 'This is not the production version, all changes will be discarded',
                       self.driver.page_source )

        self.debugLogWithScreenShot('addUser')

        self.addField( 'username', 'useradded')
        self.addField( 'name', 'User, Added')
        self.addField( 'hrId', 77088 )
        self.addField( 'cmsId', 77077 )
        self.addField( 'instCode', 'CERN' )

        self.selField( 'status', 'CMS')
        self.selField( 'category', 'Physicist')

        self.boolField( 'authorReq', True )
        self.boolField( 'isSusp', False )

        self.addField( 'comment', 'testing only ... ' )

        self.debugLogWithScreenShot('addUser-filled')

        commitBtn = self.driver.find_element_by_xpath( '//input[@id="addUser"]')
        commitBtn.click()

        self.debugLogWithScreenShot('addUser-clicked')

        self.assertIn( 'User User, Added successfully added', self.driver.page_source )

    def test_addInstitute( self ) :

        loginUser('pfeiffer', self) # needs an admin

        self.go_to( '/manage/addInstitute' )

        self.debugLogWithScreenShot('addInstitute')

        self.assertIn( 'This is not the production version, all changes will be discarded',
                       self.driver.page_source )

        self.debugLogWithScreenShot('addInstitute')

        self.addField( 'name', 'New Inst')
        self.addField( 'country', 'India' )
        self.addField( 'code', 'NIINST' )
        self.addField( 'cernId', 7777 )

        self.addField( 'status', "Yes" )
        self.addField( 'comment', '' )

        self.debugLogWithScreenShot( 'addInstitute-filled' )

        commitBtn = self.driver.find_element_by_xpath( '//input[@id="addInst"]')
        commitBtn.click()

        self.debugLogWithScreenShot('addInstitute-clicked')

        self.assertIn( 'Institute New Inst successfully added', self.driver.page_source )

    def testManageMinePM( self ) :

        # catch error reported by Cristina Fernández Bedoya <cristina.fernandez@ciemat.es> Nov 26, 2016

        self.username = 'a3m1' # manager of activities a3 and a4
        loginUser( username = self.username, theTest=self )
        self.logger.info(" %s logged in ... " % self.username)

        self.go_to( '/manage/mine' )

        self.assertIn( 'This is not the production version, all changes will be discarded',
                       self.driver.page_source )

        self.debugLogWithScreenShot( 'createTask' )

    # def testCreateTaskPM( self ) :
    #     self.go_to( '/manage/activity/a1' )
    #
    #     self.assertIn( 'This is not the production version, all changes will be discarded',
    #                    self.driver.page_source )
    #
    #     self.debugLogWithScreenShot( 'createTask' )



            ###-toDo: fill here some more testing stuff, act on the pages ...

    # def test_manageInstUsers( self ) :
    #
    #     loginUser('pfeiffer', self) # needs an admin
    #
    #     self.go_to( '/manage/manageInstUsers/CERN' )
    #
    #     self.assertIn( 'This is not the production version, all changes will be discarded',
    #                    self.driver.page_source )
    #
    #     self.debugLogWithScreenShot('manageInstUsers')
    #
    # def test_changeUserInst( self ) :
    #
    #     loginUser('pfeiffer', self) # needs an admin
    #
    #     self.go_to( '/manage/changeUserInst/9900' )
    #
    #     self.assertIn( 'This is not the production version, all changes will be discarded',
    #                    self.driver.page_source )
    #
    #     self.debugLogWithScreenShot('changeUserInst')
    #
    #
    # def test_manageProjTasks( self ) :
    #
    #     self.go_to( '/manage/manageProjTasks/pr1' )
    #
    #     self.assertIn( 'This is not the production version, all changes will be discarded',
    #                    self.driver.page_source )
    #
    #     self.debugLogWithScreenShot('manageProjTasks')
    #
    # def test_manageInstUsers( self ) :
    #
    #     self.go_to( '/manage/manageInstUsers/CERN' )
    #
    #     self.assertIn( 'This is not the production version, all changes will be discarded',
    #                    self.driver.page_source )
    #
    #     self.debugLogWithScreenShot('manageInstUsers')
    #
    #

    def testMoveTaskPM( self ) :
        # this test needs a PM
        loginUser( username='p2m1', theTest=self )
        self.logger.info( " p2m1 logged in ... " )

        # move task t3 (a2 p2) to a4 (p2)
        self.go_to( '/manage/task/t3' )

        self.assertIn( 'This is not the production version, all changes will be discarded',
                       self.driver.page_source )

        self.debugLogWithScreenShot( 'moveTask-0' )

        # now move the task to another activity:
        actSel = Select( self.driver.find_element_by_xpath( "//select[@id='activities']" ) )
        actSel.select_by_visible_text('ac3')
        actSelConf = self.driver.find_element_by_xpath( "//input[@id='chgAct']" )
        actSelConf.click()

        self.debugLogWithScreenShot('moveTask-1')

        self.assertIn( 'Activity for task changed to id 3', self.driver.page_source )
        self.assertIn( 'Activity: <a href="/manage/activity/a3">ac3', self.driver.page_source )

    def testMoveTaskAMFail( self ) :
        # this test needs a PM, so as an AM we should fail here:
        loginUser( username='a2m1', theTest=self )
        self.logger.info( " a2m1 logged in ... " )

        # try move task t3 (a2 p2) to a4 (p2)
        self.go_to( '/manage/task/t3' )

        self.assertIn( 'This is not the production version, all changes will be discarded',
                       self.driver.page_source )

        self.debugLogWithScreenShot( 'moveTaskFail-0' )

        # now move the task to another activity:
        actSel = Select( self.driver.find_element_by_xpath( "//select[@id='activities']" ) )
        actSel.select_by_visible_text('ac3')
        actSelConf = self.driver.find_element_by_xpath( "//input[@id='chgAct']" )
        actSelConf.click()

        self.debugLogWithScreenShot('moveTaskFail-1')

        # as the AM of ac2 can not manage the task now that it's in ac3, we only see the "flash"
        # messages about the change, and get an error as we can't manage the task any more:
        self.assertIn( 'You are not a manager for this task, ', self.driver.page_source)
        self.assertIn( 'Activity for task changed to id 3', self.driver.page_source )

    # do this test before the next one which changes the data of the PAT
    def test_addTaskAMFail( self ) :

        self.username = 'a1m1'
        loginUser( self.username, self )  # needs an admin

        # go to the new task page and select a project and activity for the new task:
        self.go_to( '/newTask/None' )
        self.assertIn( 'This is not the production version, all changes will be discarded',
                       self.driver.page_source )

        self.debugLogWithScreenShot( 'addTask-newTask-selP' )

        WebDriverWait( self.driver, 10 ).until( EC.element_to_be_clickable( (By.XPATH, '//select[@id="projects"]/option[@value="p5"]') ) )
        self.selField( 'projects', 'Project 5 - newName' )

        self.debugLogWithScreenShot( 'addTask-newTask-selA' )

        WebDriverWait( self.driver, 10 ).until( EC.element_to_be_clickable( (By.XPATH, '//select[@id="activities"]/option[@value="a5"]') ) )
        self.selField( 'activities', 'ac5 - newName' )

        self.debugLogWithScreenShot( 'addTask-newTask-filled' )

        commitBtn = self.driver.find_element_by_xpath( '//input[@id="selectButton"]' )
        commitBtn.click( )

        # now fill the page for the new task
        self.debugLogWithScreenShot( 'addTask-0' )

        self.assertIn( 'Sorry, you (a1m1) are not allowed to create tasks for this activity.',
                       self.driver.page_source )

    def testManageProjectPM( self ) :

        self.go_to( '/manage/project/p5' )

        self.assertIn( 'This is not the production version, all changes will be discarded',
                       self.driver.page_source )

        # update project name:
        selName = self.driver.find_element_by_id( 'name' )
        oldName = selName.text
        self.logger.debug( "found name: \n %s" % selName.text )
        selName.clear()
        selName.send_keys( "Project 5 - newName" )

        updateBtn = self.driver.find_element_by_id( 'updateName' )
        updateBtn.click()

        self.assertIn( 'Successfully updated name for Project 5', self.driver.page_source )
        self.assertIn( 'Project 5 - newName', self.driver.page_source )

        # self.debugLogWithScreenShot('manageProj-new')

        self.debugLogWithScreenShot('manageProj')

        # update project description:
        selDesc = self.driver.find_element_by_id( 'description' )
        oldDes = selDesc.text
        self.logger.debug( "found description: \n %s" % selDesc.text )
        selDesc.clear()
        selDesc.send_keys( "new desc for Project 5" )

        updateBtn = self.driver.find_element_by_id( 'update' )
        updateBtn.click()

        self.assertIn( 'Successfully updated description for Project 5', self.driver.page_source )
        self.assertIn( 'new desc for Project 5', self.driver.page_source )

        # self.debugLogWithScreenShot('manageProj-new')

        # now add a new manager:
        addMgr = self.driver.find_element_by_xpath( "//input[@id='addMgrs']" )
        addMgr.click()

        selMgrTable = self.driver.find_element_by_css_selector ( "#userTable.display.dataTable" )
        selTesterRow = selMgrTable.find_element_by_xpath( "//tr[3]" )
        ActionChains( self.driver).move_to_element( selTesterRow ).click( selTesterRow ).perform()

        self.debugLogWithScreenShot('manageProj-addMgrSel0')

        asu = self.driver.find_element_by_xpath( "//div[@class='dt-buttons']/a[@class='dt-button']" )
        asu.click()

        self.debugLogWithScreenShot('manageProj-addMgrSel1')

        wait = WebDriverWait( self.driver, 10 )
        confirmSelUser = wait.until( EC.element_to_be_clickable( (By.XPATH, "//form[@id='done']/input[@type='submit']") ) )

        self.debugLogWithScreenShot('manageProj-addMgrSel2')

        confirmSelUserForm = self.driver.find_element_by_xpath("//form[@id='done']")
        confirmSelUser.submit()
        # ActionChains( self.driver ).move_to_element( confirmSelUserForm ).click( confirmSelUser ).perform( )

        confirmSelUser = wait.until( EC.title_contains("Manage Project") )

        self.debugLogWithScreenShot('manageProj-addMgrClk')

        # and remove that manager again:
        mgrList = Select(self.driver.find_element_by_name('managers'))
        mgrList.select_by_visible_text("Tester, Master")
        removeMgr = self.driver.find_element_by_xpath("//input[@value='Remove selected managers']")
        removeMgr.click()

        self.debugLogWithScreenShot('manageUpdateProjDesc-rmMgrClk')
        self.assertIn( 'Successfully removed manager(s) Tester', self.driver.page_source )

        # print self.driver.page_source

