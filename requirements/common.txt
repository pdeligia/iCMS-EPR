alchy==2.2.2
alembic==1.4.3
attrs==20.2.0
Babel==2.8.0
blinker==1.4
click==7.1.2
coverage==5.5
cx-Oracle==7.2.3
dataclasses==0.6
dominate==2.5.2
et-xmlfile==1.0.1
Flask==1.1.2
Flask-Alchy==0.5.0
Flask-Alembic==2.0.1
Flask-Babel==2.0.0
Flask-Bootstrap==3.3.7.1
Flask-Caching==1.10.1
Flask-Cors==3.0.10
Flask-DebugToolbar==0.11.0
Flask-HTTPAuth==4.4.0
Flask-Login==0.5.0
Flask-Mail==0.9.1
Flask-Migrate==3.0.1
Flask-Moment==1.0.1
Flask-PageDown==0.3.0
Flask-Script==2.0.6
Flask-SQLAlchemy==2.5.1
Flask-SSLify==0.1.5
Flask-Table==0.5.0
Flask-WTF==0.15.1
importlib-metadata==2.0.0
itsdangerous==2.0.0
jdcal==1.4.1
Jinja2==3.0.0
Mako==1.1.3
MarkupSafe==2.0.0
openpyxl==3.0.7
packaging==20.4
pluggy==0.13.1
psycopg2==2.8.6
psycopg2-binary==2.9.1
py==1.9.0
pyasn1==0.4.8
pyasn1-modules==0.2.8
pydash==4.8.0
PyMySQL==0.10.1
pyparsing==2.4.7
pytest==6.2.4
pytest-cov==2.12.1
python-dateutil==2.8.1
python-editor==1.0.4
python-ldap==3.3.1
pytz==2021.1
selenium==3.141.0
six==1.15.0
SQLAlchemy==1.3.23
SQLAlchemy-Continuum==1.3.11
SQLAlchemy-Utils==0.36.8
stringcase==1.2.0
toml==0.10.1
urllib3==1.25.10
visitor==0.1.3
Werkzeug==2.0.0
WTForms==2.3.3
zipp==3.2.0
