#!/usr/bin/env bash

source ./venv-py3/bin/activate;

# enforce the running of all tests, even if only a fraction is selected in the code:
export SELENIUM_FORCE_ALL=1
export UNIT_TEST_FORCE_ALL=1
export USE_FAKE_EGROUPS=1

export ICMS_EPR_SECRET_KEY='testing - what else ?'
# export TEST_DATABASE_URL=postgresql://ap@localhost/TestDB
# export TEST_DATABASE_URL=postgresql://ap:ap@127.0.0.1/CombTestDBNew
# export TEST_DATABASE_URL=postgresql://ap@localhost/TestDBNew

if [[ $(hostname -s) =~ ^macbook-pro.*$ ]]; then
  if [[ $(hostname -f) =~ ^macbook-pro.home$ ]]; then
    # enable this to run outside CERN (and set up a corresponding port forwarding):
    export LDAP_URI=ldaps://localhost:8636
    export LDAP_AUTH_URI=ldaps://localhost:9636
    echo "===+++===> using ssh tunnel for LDAP "
  fi
  # enable the following to get debug screenshots and html files stored in ../logs/
  export DO_DBG_SCREENSHOTS=1
  echo "===+++===> requesting screenshots in ../logs/ "
fi

# export POSTGRES_ENV_GITLAB_CI=1
# use this when running on gitlab-CI
if [ $POSTGRES_ENV_GITLAB_CI ]; then
  # ensure the DB is empty and the schemata are there
  # createuser -h postgres -U postgres --superuser runner
  dropdb -h postgres -U runner TestDB
  createdb -h postgres -U runner TestDB 
  echo 'create schema toolkit;' | psql -h postgres -U runner -d TestDB
  echo 'create schema epr;' | psql -h postgres -U runner -d TestDB
  export TEST_DATABASE_URL=postgresql://runner@postgres/TestDB
  export TEST_ICMS_DATABASE_URL=postgresql://runner@postgres/TestDB
else
  export TEST_DATABASE_URL=postgresql://epr:icms@localhost/TestDBNew
  # export TEST_ICMS_DATABASE_URL=postgresql://icms:icms@localhost/TestDBNew
  export TEST_ICMS_DATABASE_URL=postgresql://ap@localhost/TestDBNew
fi

dropdb icms_test
export ICMS_OLD_PEOPLE_SCHEMA=TEST_CMSPEOPLE
export ICMS_OLD_NOTES_SCHEMA=TEST_Portal_notes
export ICMS_OLD_WF_NOTES_SCHEMA=TEST_Portal_wf_notes
export ICMS_OLD_NEWS_SCHEMA=TEST_Portal_news
export ICMS_OLD_CADI_SCHEMA=TEST_CMSAnalysis
export ICMS_OLD_METADATA_SCHEMA=TEST_MetaData
export ICMS_COMMON_ROLE=icms_test
export ICMS_EPR_ROLE=epr_test
export ICMS_TOOLKIT_ROLE=toolkit_test
export ICMS_READER_ROLE=icms_test_reader
export ICMS_LEGACY_USER=icms_legacy_test
export POSTGRES_HOST=127.0.0.1
export MYSQL_HOST=127.0.0.1
cd ../icms-orm
# echo "Setting up MySQL ..."
#  PYTHONPATH=./ python icms_orm/configuration/db_bootstrap_script.py mysql | mysql -u root -h 127.0.0.1
echo "Setting up PostgreSQL ... "
PYTHONPATH=./ python icms_orm/configuration/db_bootstrap_script.py postgres | psql -U postgres -h 127.0.0.1
cd -

export EPR_LOGIN_USE_LOCAL=1

# LDAP_URI=ldaps://localhost:9999 
# LDAP_AUTH_URI=ldaps://localhost:9999 
export TEST_DATABASE_URL=postgresql+psycopg2://epr_test:icms_test@localhost/icms_test 
export TEST_ICMS_DATABASE_URL=postgresql+psycopg2://icms_test:icms_test@localhost/icms_test 
export ICMS_READER_ROLE=icms_test_reader 
export POSTGRES_ENV_GITLAB_CI=1 
# ./combinedCoverage.sh

export ICMS_EPR_COVERAGE=1

# activate the lines below for details on the environment in the container ...
# echo "==============================================================================="
# echo "==============================================================================="
# printenv | sed -e 's|\(.*PASSWORD=\).*|\1xxxxxxx|'
# echo "==============================================================================="
# echo "==============================================================================="

echo '---'
which python3
python3 -c 'import coverage;print("coverage module at:", coverage.__path__)'
which coverage
coverage --version
echo '---'

time python3 ./runTests.py
export ret0=$?
coverage html
/bin/mv .coverage .coverage.unittest
/bin/mv ./coverage_html_report ./coverage_unit

echo "==============================================================================="
echo "Unit testing returned "$ret0
echo "==============================================================================="

if [ $POSTGRES_ENV_GITLAB_CI ]; then
    echo "*******************************************************************************"
    echo "*******************************************************************************"
    echo ""
    echo "Selenium tests temporarily disabled for GitLab CI ... "
    echo ""
    echo "*******************************************************************************"
    echo "*******************************************************************************"
    mkdir -p ./coverage_selenium
    export ret1=0
else
    # time coverage run ./test/selenium_multithread.py
    echo " ***** ***** ***** Selenium tests temporarily disabled ...  ***** ***** ***** "
    export ret1=$?
    coverage html
    /bin/mv .coverage .coverage.seltest
    /bin/mv ./coverage_html_report ./coverage_selenium

    echo "==============================================================================="
    echo "Selenium testing returned "$ret1
    echo "==============================================================================="
fi

coverage combine
coverage html

coverage report

# open ./coverage_html_report/index.html

# report result-summary and return an error if one of the two fails:
echo "Return values: unit: "$ret0", selenium:"$ret1

# return 0 if both tests were OK:
if [ "$ret0" == 0 -a "$ret1" == 0 ]; then
exit 0
fi

# and -1 otherwise. Adding up the two ret-vals will give 0 if one is 255 and the other one is 1
exit -1
