#!/usr/bin/env bash

if [ -e  "/opt/rh/rh-postgresql95/enable" ] ; then
  source /opt/rh/rh-postgresql95/enable
fi

source ./venv-py3/bin/activate

# export TEST_DATABASE_URL=postgresql://ap@localhost/CovSelTestDB
# export TEST_DATABASE_URL=postgresql://ap@localhost/TestDB

time python -i ./manage.py shell
