#!/usr/bin/env python

#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import time
import json
import sys
import copy
import difflib
from uuid import uuid4
import traceback as tb

import logging
logging.basicConfig(format = '%(asctime)-15s - %(levelname)s: %(message)s', level=logging.INFO)

# from sqlalchemy.exc import IntegrityError

scrPath = '.'
if scrPath not in sys.path:
    sys.path.append(scrPath)

from app import db
from app.models import Project, CMSActivity, Task, TimeLineInst, TimeLineUser, Category, Level3Name
from app.models import Manager

from app.models import commitNew, CMSDbException

projCodeMap = {}
actCodeMap = {}
tskCodeMap = {}

selectedProjList = ['Trigger Coordination']

selectedProjectsOnly = True
allProjects = False

# projVetoList = ['Run Coordination']
# for testing, veto the big ones and see ... 
projVetoList = [ # 'Trigger Coordination', 
                 'Tracker', 'PPD', 
                 # 'MTD', 'L1 Trigger', 'HGCAL (CE)', 'BRIL', 'General', 
                 'Muon', 'ECAL', 'PPS', 'Offline Software And Computing', 
                 'HCAL', 'DAQ', 'Run Coordination' ]

skipList = { 'acts': [], 'tasks': [] }

assert( bool(selectedProjectsOnly) != bool(allProjects)), 'Only one can be True !! ' 

if selectedProjectsOnly:
    logging.info('going to process selected project(s) [%s] ' % ', '.join(selectedProjList) )
else:
    logging.info('going to process ALL projects, except [%s] ' % ', '.join(projVetoList) )

def copyMgrItems( codeMap, itemType, fromYear, toYear, verbose=False ):

    oldItems = db.session.query(Manager).filter_by(year=fromYear, itemType=itemType, status='active').all()
    logging.info( "\nfound %s active %s managers for %s " % (len(oldItems), itemType, fromYear) )

    nSkip = { 'project': 0, 'activity': 0, 'task': 0 }
    for oldItem in oldItems:
        try:
            if oldItem.itemCode not in codeMap: continue
            codeMapItem = codeMap[oldItem.itemCode]
            newItem = Manager(itemType = oldItem.itemType,
                            itemCode = codeMapItem.code,
                            year = toYear,
                            userId = oldItem.userId,
                            comment = oldItem.comment,
                            role = oldItem.role,
                            egroup = oldItem.egroup)
            if verbose : logging.info( '++> going to set %s manager for %s in %s to: %s ' % (itemType, codeMap[oldItem.itemCode].name, toYear, str(newItem)) )
            commitNew( newItem )
        except Exception as e :
            logging.error( "++> ERROR when copying manager for %s %s from %s to %s, got: %s" % ( itemType, oldItem.itemCode, fromYear, toYear, ''.join(tb.format_exception(None, e, e.__traceback__)) ) )

    logging.info( "%s %s managers set up for %s " % (len(oldItems), itemType, toYear) )
#     logging.info( "  --  managers skipped for %s: %s " % (itemType, nSkip[itemType]) )

def copyManagers(prjCodeMap, actCodeMap, tskCodeMap, fromYear, toYear, verbose=False):

    copyMgrItems( codeMap=prjCodeMap, itemType='project', fromYear=fromYear, toYear=toYear, verbose=verbose )
    copyMgrItems( codeMap=actCodeMap, itemType='activity', fromYear=fromYear, toYear=toYear, verbose=verbose )
    copyMgrItems( codeMap=tskCodeMap, itemType='task', fromYear=fromYear, toYear=toYear, verbose=verbose )


def copyPAT( fromYear, toYear, verbose=False ):
    copyProjs( fromYear, toYear, verbose )
    copyActs( fromYear, toYear, verbose )
    copyL3Ns( fromYear, toYear, verbose )
    copyTasks( fromYear, toYear, verbose )

def isValidProj( pName ):
    global allProjects, projVetoList, selectedProjList, selectedProjectsOnly
    if allProjects and pName in projVetoList: return False
    if selectedProjectsOnly and pName not in selectedProjList: return False
    return True

def projExists( pName, toYear ):
    entry = db.session.query(Project).filter_by(name=pName, year=toYear, status='ACTIVE').one_or_none()
    if entry:
        return True
    return False

def copyProjs( fromYear, toYear, verbose=False ):
    # proj: name, desc, code
    # act:  name, desc, proj, code
    # task: name, desc, act, needs, pctAtCERN, tType, comment, code, year=2015, shiftTypeId=-1,
    #             status='ACTIVE', locked=False, kind='CORE'
    #             parent='', earliestStart='', latestEnd='', level=1,
    #
    # use the occasion to set the descriptions to the name if not yet set ...

    # projects:
    global projCodeMap

    allProjFrom = db.session.query(Project).filter_by(year=fromYear, status='ACTIVE').all()
    if verbose: logging.info( 'found %s active projects in %s ' % (len(allProjFrom), fromYear) )

    newProjs = []
    projCodeMap = {}  # we need to keep a mapping for the codes for the further processing
    for p in allProjFrom:
        if not isValidProj(p.name): continue
        if projExists( p.name, toYear):
            if selectedProjectsOnly: # for selected projects, crash here:
                raise Exception( '\nERROR: Project %s already exists in DB for year %s -- aborting' % (p.name, toYear) )
            else: # for all processing, update projVetoList and continue
                projVetoList.append( p.name )
                logging.warning( 'Project %s already exists in DB for year %s -- skipping' % (p.name, toYear) )
        newPr = Project(name=p.name,
                        desc=p.description if p.description else p.name,
                        code=str(uuid4()))
        newPr.year = toYear
        newProjs.append( newPr )
        projCodeMap[p.code] = newPr

    if verbose : logging.info( "\nnew Projects:" )
    for np in newProjs:
        if verbose : logging.info( ' - %s' % np.name )
        try:
            commitNew( np )
        except CMSDbException as e:
            logging.warning( "ERROR when committing project %s - got: %s " % (np.name, str(e)) )

    logging.info( "%s Projects set up for %s " % (len(newProjs), toYear) )

def copyActs( fromYear, toYear, verbose=False ):
    # activities:
    global projCodeMap, actCodeMap
    allActs = db.session.query(CMSActivity).filter_by(year=fromYear, status='ACTIVE').all()
    if verbose : logging.info( 'found %s active activities in %s ' % (len(allActs), fromYear) )
    newActs = [ ]
    actCodeMap = {}  # we need to keep a mapping for the codes for the further processing
    for a in allActs :
        # skip projects in veto list or not selected ones
        if not isValidProj(a.project.name): 
            skipList['acts'].append( (a.project.name, a.name) )
            continue
        newA = CMSActivity( name=a.name,
                            desc=a.description if a.description else a.name,
                            proj=projCodeMap[a.project.code],
                            code=str( uuid4( ) ) )
        newA.year = toYear
        newActs.append( newA )
        actCodeMap[a.code] = newA

    if verbose : logging.info( "\nnew Activities:" )
    for na in newActs :
        if verbose : logging.info( ' -- %s ' % na.name )
        commitNew( na )

    logging.info( "%s Activities set up for %s (%d skipped)" % (len(newActs), toYear, len(skipList['acts'])) )

def copyL3Ns( fromYear, toYear, verbose=False ):

    # level-3s
    allLvl3s = ( db.session.query(Level3Name)
                    .filter(Level3Name.startYear <= int(fromYear) )
                    # .filter(Level3Name.endYear >= fromYear)
                    .all() )
    if verbose : 
        logging.warning( 'found %s active lvl3Names in %s ' % (len(allLvl3s), fromYear) )

    # check all existing lvl3 names and extend the one which are not open-ended
    # (in 2020 all were open-ended, so the extension needs to be written/debugged later)
    for l3n in allLvl3s :
        if l3n.endYear > 0 and l3n.endYear < toYear:
            print(' l3 name, startyear, endyear: %s %s %s' % (l3n.name, l3n.startYear, l3n.endYear) )

def copyTasks( fromYear, toYear, verbose=False ):

    # tasks
    global projCodeMap, actCodeMap, tskCodeMap
    allTasks = db.session.query(Task).filter_by( year=fromYear, status='ACTIVE')\
                         .filter(Task.kind.ilike("CORE") )\
                         .all( )
    if verbose : logging.info( 'found %s active core tasks in %s ' % (len( allTasks ), fromYear) )
    newTasks = [ ]
    tskCodeMap = {}
    for t in allTasks :
        if t.name == None :
            logging.info( "==> task with no name found (code %s) - skipping" % t.code )
            continue

        # skip tasks for projects in veto list
        if not isValidProj( t.activity.project.name ): 
            skipList['tasks'].append( (t.activity.project.name, t.name) )
            continue

        newT = Task( name=t.name,
                     desc=t.description if t.description else t.name,
                     act = actCodeMap[t.activity.code],
                     level3 = t.level3,
                     needs=t.neededWork,
                     pctAtCERN=t.pctAtCERN,
                     tType=t.tType,
                     comment = t.comment,
                     code=str( uuid4( ) ),
                     year=toYear,
                     shiftTypeId=t.shiftTypeId,
                     status='ACTIVE',
                     locked=t.locked,
                     kind='CORE',
                     )
        newT.year = toYear
        newTasks.append( newT )
        tskCodeMap[ t.code ] = newT

    if verbose : logging.info( "\nnew Tasks:" )
    for nt in newTasks :
        if verbose : logging.info( ' --- %s ' % nt.name )
        commitNew( nt )

    logging.info( "%s Tasks set up for %s (%d skipped)" % (len(newTasks), toYear, len(skipList['tasks'])) )

    copyManagers( projCodeMap, actCodeMap, tskCodeMap, fromYear, toYear, verbose )


def DoNot_copyTimeLines(fromYear, toYear, verbose):

    # get the list of all institutes with a timeline:
    tlInstCodesFrom = db.session.query( TimeLineInst.code.distinct() )\
                                .filter(TimeLineInst.year == fromYear)\
                                .all()

    if verbose: logging.info( 'found %s active timelines for institutes in %s ' % (len(tlInstCodesFrom), fromYear) )

    newTlInsts = []
    for tliCode, in tlInstCodesFrom:
        oldTli = db.session.query(TimeLineInst).filter_by(code=tliCode, year=fromYear).order_by( TimeLineInst.timestamp.desc() ).first()
        newTlI = TimeLineInst()
        newTlI.code      = tliCode
        newTlI.year      = toYear
        newTlI.cmsStatus = oldTli.cmsStatus # keep that value from last year's entry
        newTlI.comment   = oldTli.comment     # keep that value from last year's entry
        newTlInsts.append( newTlI )

    if verbose : logging.info( "\nnew TimeLines for Institutes:" )
    index = 0
    for newItem in newTlInsts :
        index += 1
        if verbose : logging.info( "%d/%d - %s" % (index, len(newTlInsts), newItem) )
        db.session.add( newItem )
    db.session.commit()

    logging.info( "%s TimeLines for Institutes set up for %s " % (len(newTlInsts), toYear) )

    # get the list of all institutes with a timeline:
    tlUserCmsIdFrom = db.session.query( TimeLineUser.cmsId.distinct( ) ) \
        .filter( TimeLineUser.year == fromYear ) \
        .all( )

    if verbose : logging.info( 'found %s active timelines for users in %s ' % (len( tlUserCmsIdFrom), fromYear) )

    newTlUsers = [ ]
    for tluID, in tlUserCmsIdFrom:
        oldTlu = db.session.query(TimeLineUser).filter_by( cmsId=tluID, year=fromYear ).order_by( TimeLineUser.timestamp.desc() ).first()
        newTlu = TimeLineUser()
        newTlu.cmsId        = tluID
        newTlu.instCode     = oldTlu.instCode
        newTlu.year         = toYear
        newTlu.mainProj     = oldTlu.mainProj
        newTlu.isAuthor     = oldTlu.isAuthor
        newTlu.isSuspended  = oldTlu.isSuspended
        newTlu.status       = oldTlu.status
        newTlu.category     = oldTlu.category
        newTlu.dueApplicant = oldTlu.dueApplicant
        newTlu.dueAuthor    = oldTlu.dueAuthor
        newTlu.yearFraction = oldTlu.yearFraction
        newTlu.comment      = oldTlu.comment  # keep that value from last year's entry
        newTlUsers.append( newTlu )

    if verbose : logging.info( "\nnew TimeLines for Users:" )
    # add all new timelines and commit them only at the end to speed up
    index =0
    for newItem in newTlUsers :
        index += 1
        if verbose : logging.info( "%d/%d - %s" % (index, len(newTlUsers), newItem) )
        db.session.add( newItem )
    db.session.commit()

    logging.info( "%s TimeLines for Users set up for %s " % (len(newTlUsers), toYear) )

def prepareFor(fromYear, toYear, verbose=False):

    copyPAT( fromYear=fromYear, toYear=toYear, verbose=verbose )

    # timelines are now created when they are needed (change of year), 
    # so we do not need to copy them over here. 
    ####  copyTimeLines( fromYear=fromYear, toYear=toYear, verbose=verbose )

if __name__ == '__main__':

    from app import create_app
    theApp = create_app('default')
    with theApp.app_context():

        # prepareFor( fromYear=2016, toYear=2017, verbose=True )
        # prepareFor( fromYear=2017, toYear=2018, verbose=True )
        # prepareFor( fromYear=2018, toYear=2019, verbose=True )
        # prepareFor( fromYear=2020, toYear=2021, verbose=False )

        print( 'going to prepare for 2022 from 2021')
        prepareFor( fromYear=2021, toYear=2022, verbose=False )
