
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import json
import os
import time
import datetime

from app import db
from app.models import EprInstitute, EprUser, Manager, Project, CMSActivity, Task, Shift, Pledge, AllInstView, EprDue
from app.ldapAuth import getLogin


def exportItems(what, fileNamePart):

    start = time.time()
    allItems = what.query.all()
    print( "retrieving data took ", time.time() - start, 'sec' )

    start = time.time()
    data = []
    for i in allItems:
        data.append( i.to_json() )
    print( "converting data took ", time.time() - start, 'sec' )

    start = time.time()
    with open('exports/iCMS-EPR_%s.json' % fileNamePart, 'w') as outFile:
        json.dump(data, outFile, sort_keys=True, indent=4, separators=(',', ': '))
    print( "writing json took ", time.time() - start, 'sec. wrote ', len(allItems), 'items to iCMS-EPR_%s.json \n' % fileNamePart )

def exportAll():
    if not os.path.exists('./exports'): os.makedirs('./exports')

    exportItems(what=EprUser, fileNamePart='users')
    exportItems(what=EprInstitute, fileNamePart='institutes')

    exportItems(what=Manager, fileNamePart='managers')
    exportItems(what=Project, fileNamePart='projects')
    exportItems(what=CMSActivity, fileNamePart='activities')
    exportItems(what=Task, fileNamePart='tasks')
    exportItems(what=Shift, fileNamePart='shifts')
    exportItems(what=Pledge, fileNamePart='pledges')
    exportItems(what=AllInstView, fileNamePart='allInstView')
    exportItems(what=EprDue, fileNamePart='eprDue')

