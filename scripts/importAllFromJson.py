
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import json
import os
import time
import datetime

from app import db
from app.models import EprInstitute, EprUser, Manager
from app.ldapAuth import getLogin

# from .updateInstUsers import createCategories

def importInstitutes( ):
    start = time.time()
    with open('exports/iCMS-EPR_institutes.json', 'r') as inFile:
        data = json.load(inFile)
    print( "reading json took ", time.time() - start, 'sec. got ', len(data), 'items from iCMS-EPR_institutes.json \n' )

    for i in data:
        inst = EprInstitute(cernId=i['cernId'], name=i['name'], country=i['country'], code=i['code'], year=i['year'], comment=i['comment'])
        db.session.add(inst)

    start = time.time()
    try:
        db.session.commit()
    except Exception as e:
        print( "ERROR when committing institutes, got: %s " % str(e) )
        db.session.rollback()
    print( 'imported %i institutes in %f sec.' % (len(data), time.time()-start) )

    return

def importUsers():
    start = time.time()
    with open('exports/iCMS-EPR_users.json', 'r') as inFile:
        data = json.load(inFile)
    print( "reading json took ", time.time() - start, 'sec. got ', len(data), 'items from iCMS-EPR_users.json \n' )

    start = time.time()
    for i in data:
        try:
            user = EprUser(username=i['username'], name=i['name'],
                    hrId=int(i['hrId']), cmsId=int(i['cmsId']),
                    instId=i['mainInst'],
                    authorReq=i['authReq'], status=i['status'], isSusp=i['suspended'],
                    categoryName=i['category'], comment=i['comment'], year=int(i['year']))
            db.session.add(user)
        except Exception as e:
            print( "ERROR creating user %s, mainInst=%i - got %s" % (i['name'], i['mainInst'], str(e)) )
            raise e

    try:
        db.session.commit()
    except Exception as e:
        print( "ERROR when committing users, got: %s " % str(e) )
        db.session.rollback()
        raise e

    print( 'imported %i users in %f sec.' % (len(data), time.time()-start) )

    return

def importManagers():
    start = time.time()
    with open('exports/iCMS-EPR_managers.json', 'r') as inFile:
        data = json.load(inFile)
    print( "reading json took ", time.time() - start, 'sec. got ', len(data), 'items from iCMS-EPR_managers.json \n' )

    for i in data:
        m = Manager(itemType=i['itemType'], itemCode=i['itemCode'], userId=i['userId'], comment=i['comment'], year=i['year'], role=i['role'])
        db.session.add(m)

    start = time.time()
    try:
        db.session.commit()
    except Exception as e:
        print( "ERROR when committing users, got: %s " % str(e) )
        db.session.rollback()
    print( 'imported %i managers in %f sec.' % (len(data), time.time()-start) )

    return



def importAll():

    # db.create_all()
    # Role.insert_roles()
    # createCategories()
    #
    # importInstitutes()
    importUsers()
    importManagers()

    # importItems(what=Project, fileNamePart='projects')
    # importItems(what=CMSActivity, fileNamePart='activities')
    # importItems(what=Task, fileNamePart='tasks')
    # importItems(what=Shift, fileNamePart='shifts')
    # importItems(what=Pledge, fileNamePart='pledges')
    # importItems(what=AllInstView, fileNamePart='allInstView')
    # importItems(what=EprDue, fileNamePart='eprDue')
    #
