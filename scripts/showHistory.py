
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import time
import datetime

import sys, os
sys.path.append(os.getcwd())

import sqlalchemy as sa

from app import create_app, db


def showAll():

    insp = sa.engine.reflection.Inspector.from_engine(db.engine)
    print( insp.get_table_names() )

    show('activity')
    show('transaction')
    show('pledges_version')

def show(tableName):
    start = time.time()

    meta = sa.MetaData()
    selTable = sa.Table(tableName, meta)

    insp = sa.engine.reflection.Inspector.from_engine(db.engine)

    colHeaders = [ x['name'] for x in insp.get_columns(table_name=tableName)]
    insp.reflecttable(selTable, None)

    sel = sa.sql.text( 'select * from %s' % tableName )
    data = db.session.execute( sel ).fetchall()

    print( '-'*80 )
    print( tableName.capitalize()+":" )
    for c in colHeaders:
        print( '%10s' % c[:10], end='')
    print('')
    for row in data:
        for d in row:
            print( "%10s" % str(d)[:10], end='')
        print ('')

    print("processing took ", time.time()-start, 'sec.' )

if __name__ == '__main__':

    app = create_app('default')
    app_context = app.app_context()
    app_context.push()

    showAll()
