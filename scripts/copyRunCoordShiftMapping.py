#!/usr/bin/env python

#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import os, sys
import time
import datetime

scrPath = '.'
if scrPath not in sys.path:
    sys.path.append(scrPath)

from app.models import Task, CMSActivity, Project, ShiftTaskMap
from app import db


class RCTaskShiftMapHandler(object):

    def __init__(self):

        pass

    def copyTaskShiftMap( self, fromYear, toYear ):

        oldSTM = db.session.query( ShiftTaskMap )\
                           .filter( ShiftTaskMap.year == fromYear )\
                           .filter( ShiftTaskMap.status == 'ACTIVE' )\
                           .all()

        print( 'found %s active stms in %s' % (len(oldSTM), fromYear) )

        stmTaskCodes = [ t.taskCode for t in oldSTM ]
        # print "stm-tcs: %s " % stmTaskCodes[:5]

        # Find the mapped tasks for Run Coordination.
        # As Task codes are unique across all years, we have to match the _name_ of
        # the task for the two years in question. So first we get a mapping of code:name
        # for the old year, then a mapping name:code for the new year

        oldRCTaskCodeNameMap = {}
        oldRCTasks = db.session.query( Task.code, Task.name )\
                             .join( CMSActivity, Task.activityId == CMSActivity.id )\
                             .join( Project, CMSActivity.projectId == Project.id )\
                             .filter( Project.name == 'Run Coordination' )\
                             .filter( Task.year == fromYear )\
                             .filter( Task.code.in_(stmTaskCodes) )\
                             .all()
        [ oldRCTaskCodeNameMap.update( { t[0] : t[1] } ) for t in oldRCTasks ]

        oldTaskNames = [ t[1] for t in oldRCTaskCodeNameMap.items() ]

        newTaskNameCodeMap = {}
        newTasks = db.session.query( Task.code, Task.name )\
                             .filter( Task.year == toYear )\
                             .filter( Task.name.in_(oldTaskNames) ) \
                             .all()
        [ newTaskNameCodeMap.update( { t[1] : t[0] } ) for t in newTasks ]

        print( 'old: %s ' % len(oldRCTaskCodeNameMap) )
        print( 'new: %s ' % len(newTaskNameCodeMap) )

        for c,n in oldRCTaskCodeNameMap.items():
            if n not in newTaskNameCodeMap.keys(): print( "%s not in new list " % n )

        newSTMTCs = [ c[0] for c in db.session.query( ShiftTaskMap.taskCode )\
                           .filter( ShiftTaskMap.year == toYear )\
                           .filter( ShiftTaskMap.status == 'ACTIVE' )\
                           .all() ]

        nNew = 0
        for stm in oldSTM:

            # ignore task codes not in the Run Coordination project:
            if stm.taskCode not in oldRCTaskCodeNameMap: continue

            # get the new code for the task and compare if it is already in the system:
            newCode = newTaskNameCodeMap[ oldRCTaskCodeNameMap[ stm.taskCode ] ]

            if newCode in newSTMTCs:
                print( 'mapping for task code %s (%s) already existing !! skipping ... ' % (stm.taskCode, oldRCTaskCodeNameMap[stm.taskCode] ) )
                continue

            # not yet in, create new item:
            newItem = ShiftTaskMap(stm.subSystem, stm.shiftType, stm.shiftTypeId, stm.flavourName, newCode, stm.status, stm.weight, stm.addedValueCredits, toYear)
            db.session.add( newItem )
            nNew += 1

        db.session.commit()
        print('created: %s new STM entries for %s' % (nNew, toYear))

if __name__ == '__main__':

    from app import create_app
    theApp = create_app('default')
    with theApp.app_context():

        db.create_all()
        h = RCTaskShiftMapHandler()
        h.copyTaskShiftMap(fromYear=2018, toYear=2019)

