
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import time
import json
import sys
import copy
import difflib
from uuid import uuid4

from sqlalchemy.exc import IntegrityError

from app import db
from app.models import Role, Permission, EprUser, EprInstitute, Manager, Project, CMSActivity, Task, Pledge, getTaskTypes

# OpenPyXL - A Python library to read/write Excel 2010 xlsx/xlsm files
#
# for info and docs see: https://openpyxl.readthedocs.org/en/default/index.html
#
from openpyxl import load_workbook

headerMap = enumerate(  [u'Project', u'Activity',
                         u'Task Name', u'Task Type', u'Shift Type', u'Task Description',
                         u'Skills Required', u'Percentage at CERN',
                         u'Earliest start date', u'Latest end date',
                         u'#Months', u'Core?'] )

def remap():
    res = {}
    for item in headerMap:
        k, v = item
        res[v] = k+1
    return res

indexMap = remap()

print( "headermap: ", str(headerMap) )
print( 'indexmap : ', indexMap )

def analyseRow(row):

    info = {}
    for cell in row:
        if cell.value: # found something, check where we are:
            info[cell.column] = cell.value

    return info

def analyseSheet(workSheet):

    projs = []
    acts  = []
    tasks = []
    pName = ''
    aName = ''
    for row in workSheet.iter_rows(row_offset=1):
        info = analyseRow(row)
        if not info: continue # ignore empty rows

        if 12 not in info.keys(): continue

        if 1 in info.keys():
            if not pName or info[1] != pName: pName = info[1]
        else:
            info[1] = pName

        if 2 in info.keys():
            if not aName or info[2] != aName: aName = info[2]
        else:
            info[2] = aName

        if 'Yes' not in info[12]: continue # only Core tasks

        if info[1] not in projs: projs.append( info[1] )
        if info[2] not in acts: acts.append( info[2] )

        # print( "got:", info
        tasks.append( info )

    return projs, acts, tasks

def convertString(inString):

    try:
        outString = inString.encode('utf-8', 'xmlcharrefreplace')
        return outString
    except UnicodeEncodeError:
        outString = inString.decode('latin-1').encode('ascii', 'xmlcharrefreplace')
        return outString
    except:
        raise

def updatePctAtCernForNewPAT( inFileName ):
    wb = load_workbook(filename=inFileName, read_only=True)

    sheets = wb.get_sheet_names()

    print( "**> found the following worksheets:", sheets, 'in ', inFileName )

    oldHeaders = []
    for sheetName in sheets:

        if 'global' in inFileName:
            print( 'found global ... ' )
            toSkip = False
            for prj in ['activities', 'Tracker', 'HCAL']:
                print( ' prj %s in %s : %s ' % (prj, sheetName, prj in sheetName) )
                if prj in sheetName: toSkip = True
            if toSkip: continue

        elif 'HCAL' in inFileName:
            if 'hcal' not in sheetName.lower():
                print( ' ... ignoring sheet', sheetName, 'for hcal ... ' )
                continue
        else:
            if 'tasks' not in sheetName:
                print( ' ... ignoring sheet', sheetName, 'for tracker ' )
                continue

        print( "-"*80 )
        print( "worksheet : ", sheetName )

        workSheet = wb.get_sheet_by_name(sheetName)

        headers = []
        for row in workSheet['A1:L1']:
            for cell in row:
                headers.append( cell.value )

        # raises if headers are different in a workSheet
        diffHeaders( headers, oldHeaders, sheetName )

        print( " ... headers OK" )

        proj, act, tasks = analyseSheet(workSheet)

        print( len(proj) , "projects  : ", proj )
        print( len(act)  , 'activities: ', act )
        print( len(tasks), 'CORE tasks ' ) # , tasks

        # create projects and tasks first, then find and hand over the activity for the task:

        nOK = 0
        for t in tasks:

            # fill in missing values
            for i in range(len(indexMap)):
                if i in t : continue
                if i == 8 : t[i] = 0 # percentage at CERN
                elif i == 11 : t[i] = -1 # needed work
                else: t[i] = ''

            projName = t[ indexMap['Project'] ]
            actName  = t[ indexMap['Activity'] ]
            taskName = convertString( t[ indexMap['Task Name'] ] )

            if 'general' in sheetName.lower():
                if 'Offline And Computing' in projName: projName = "General"

            if taskName.strip() == '' :
                print( "task w/o name found for %s %s (desc[:10]:'%s') - skipping " % (projName, actName, t[ indexMap['Task Description'] ][:10] ) )

            try:
                proj =  db.session.query(Project).filter_by(name=projName, year=2016).one()
            except Exception as e:
                if "No row was found for " in str(e):
                    print( "ERROR: no project found for ", projName, actName, taskName, 'skipping ... ' )
                    continue
                else:
                    raise e

            proj =  db.session.query(Project).filter_by(name=projName, year=2016).one()

            try: # to find the corresponding activity:
                act  =  db.session.query(CMSActivity).filter_by(name=actName, projectId=proj.id, year=2016).one()
            except Exception as e:
                if "No row was found for " in str(e):
                    print( "ERROR: no activity found for %s::%s::%s - skipping " % (projName, actName, taskName) )
                    continue
                elif "Multiple rows were found for one" in str(e):
                    print( "ERROR multiple rows found for act %s, projId %i :" % (actName, proj.id) )
                    print( '\n'.join(  db.session.query(CMSActivity).filter_by(name=actName, projectId=proj.id, year=2016).all() ) )
                    raise e
                else:
                    raise e

            act  =  db.session.query(CMSActivity).filter_by(name=actName, projectId=proj.id, year=2016).one()

            try:
                dbTask = db.session.query(Task).filter_by(year=2016,activityId=act.id,name=taskName).one()
            except Exception as e:
                if "No row was found for " in str(e):
                    print( u"ERROR: no task found for " , projName, actName, taskName, ' - skipping ...' )
                    continue

            # pctAtCern = 0.
            # try:
            #     pctAtCern = float( t[ indexMap['Percentage at CERN'] ] )
            # except:
            #     print( "ERROR converting value for pctAtCERN: ", t[ indexMap['Percentage at CERN'] ], 'skipping this one ... '
            #     continue

            # if dbTask.pctAtCERN == 0 and pctAtCern != 0:
            #     if pctAtCern < 1.5: pctAtCern = pctAtCern*100.  # fix number to percentage instead of fraction
            #     print( " ... going to update pct for %s::%s::%s:  old: %i  new: %i " % (projName, actName, taskName, dbTask.pctAtCERN, pctAtCern)
            #     dbTask.pctAtCERN = pctAtCern

            tType = ''
            if 'perennial' in t[ indexMap['Task Type'] ].lower().strip():
                tType = 'Perennial'
            elif 'one-off'   in t[ indexMap['Task Type'] ].lower().strip():
                tType = 'One-off'
            elif t[ indexMap['Task Type'] ].lower().strip() == '':
                tType = 'Perennial'
            else:
                print( 'unknown task type found: "%s" ' % t[ indexMap['Task Type'] ] )

            if tType != dbTask.tType :
                print( " ... going to update tType for %s::%s::%s: old %s new %s " % (projName, actName, dbTask.name, dbTask.tType, tType) )
                dbTask.tType = tType

                try:
                    db.session.commit()
                    nOK += 1
                except Exception as e:
                    db.session.rollback()
                    print( "session rolled back - got: %s " % str(e) )

        print( "updated %i new tasks for %s in 2016" % (nOK, sheetName) )

    db.session.rollback()


def diffHeaders( headers, oldHeaders, sheetName ) :

    if not oldHeaders :
        oldHeaders = copy.copy( headers )
        # print( 'copied headers to:', oldHeaders )
    else :
        try :
            delta = list( difflib.context_diff( oldHeaders, headers ) )
        except Exception as e :
            print( "processing %s got: %s" % (sheetName, str( e )) )
            print( "old: ", oldHeaders )
            print( 'new: ', headers )
            raise e

        if len( delta ) > 0 :
            msg = "found different headers for %s:" % (sheetName,)
            for line in delta :
                sys.stdout.write( line )
            raise Exception( msg )
        else :
            print( "headers match ... " )

def updateAll():

    for inFileName in ['global_epr_2016-v8_forAndreas.xlsx',
                       'ERP-2016-tasks-tracker.20160127.xlsx', 'HCAL-DPG-EPR-2016-v9.20160209.xlsx']:
        print( "\n *** Processing ", inFileName )
        updatePctAtCernForNewPAT('imports/'+inFileName)
