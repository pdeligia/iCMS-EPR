
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from netrc import netrc
from sqlalchemy import create_engine
from sqlalchemy.engine import reflection

(login, account, password) = netrc().authenticators('cms_icms_dev')
print( 'found', login, account )

connString = 'oracle://%s:%s@%s' % (account, password, login)
engine = create_engine(connString)
insp = reflection.Inspector.from_engine(engine)
print( insp.get_table_names() )
