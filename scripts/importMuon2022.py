#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import time
import re
import sys
import copy
import difflib
from uuid import uuid4

from sqlalchemy.exc import IntegrityError


scrPath = '.'
if scrPath not in sys.path:
    sys.path.append(scrPath)

from app import db, create_app
from app.models import Project, CMSActivity, Task, Level3Name

# OpenPyXL - A Python library to read/write Excel 2010 xlsx/xlsm files
#
# for info and docs see: https://openpyxl.readthedocs.org/en/default/index.html
#
from openpyxl import load_workbook
import datetime

headerMapFromFile = enumerate(  [ u'', u'Project/LV1', u'Activity/LV2', u'LV3', u'TaskName', u'Work Needed', 
                                  u'% of work at CERN', u'Type of task', u'Locked', u'Task description'] )

nominalHeaders = [ x[1].strip().lower() for x in headerMapFromFile ]
headerMap = enumerate( [x.strip().lower() for x in nominalHeaders] )

tgtYear = 2022

def remap():
    res = {}
    for item in headerMap:
        k, v = item
        res[v] = k+1
    return res

indexMap = remap()

print("headermap: ", str(headerMap))
print('indexmap : ', indexMap)

def analyseRow(row):

    info = {}

    try:
        if row is None or len(row) == 0 or 'task-name' in row[4].value: # ignore header row
            return info
    except TypeError as e:
        return info

    for cell in row:
        if cell.value or cell.value == 0: # found something, check where we are:
            try:
                info[cell.column] = cell.value.strip()
            except AttributeError:
                info[cell.column] = cell.value

    return info

def analyseSheet(workSheet):

    projs = []
    acts  = []
    tasks = []
    l3Names = []
    pName = ''
    aName = ''
    l3Name = 'Unknown'

    errors = { 'noInfo': 0,
               'noType': [],
               'noNeeds': [],
               'wrongType': [] }

    nItems = 0
    for row in workSheet.iter_rows():

        info = analyseRow(row)

        if info and len(info) < 5: continue
        if info and info[2].lower() == 'project/lv1': continue

        if not info:
            errors['noInfo'] += 1
            continue # ignore empty rows

        nItems += 1

        # print('==> info: ', info)

        iP = indexMap['project/lv1']
        iA = indexMap['activity/lv2']
        i3 = indexMap['lv3']
        iC = indexMap['type of task']
        iN = indexMap['work needed']

        if iC not in info.keys():
            errors['noType'].append( info )
            info[iC] = 'Perennial'

        if iP in info.keys():
            if not pName or info[iP] != pName: pName = info[iP]
        else:
            info[iP] = pName

        if iA in info.keys():
            if not aName or info[iA] != aName: aName = info[iA]
        else:
            info[iA] = aName

        if i3 in info.keys():
            if not l3Name or info[i3] != l3Name: l3Name = info[i3]
        else:
            info[i3] = 'Unknown'

        if ( 'perennial' not in info[iC].lower() and
             'instresp' not in info[iC].lower() and
             'one-off' not in info[iC].lower() and
             'core' not in info[iC].lower() ):
            errors['wrongType'].append(info)
            continue # only core tasks

        if iN in info.keys() and info[iN] == '-': info[iN] = -1
        if ( iN not in info.keys() or info[iN] < 0):
            errors['noNeeds'].append(info)

        if info[iP] not in projs: projs.append( info[iP] )
        if info[iA] not in acts: acts.append( info[iA] )
        if info[i3] not in l3Names: l3Names.append( info[i3] )

        # print "got:", info
        tasks.append( info )

    print( 'INFO: found %d entries in worksheet' % nItems)
    for k,v in errors.items():
        if v:
            if type(v) ==  type([]):
                print('ERROR: %s (%s): %s' % (k, len(v), str(v)))
            else:
                print('ERROR: %s     : %s' % (k, str(v)))

    return projs, acts, l3Names, tasks

def makeNewProject(pName):

    p = Project(name=pName, desc=pName, code=str(uuid4()), year=tgtYear)
    try:
        db.session.add(p)
    except Exception as e:
        print("ERROR committing new project %s, got %s" % (pName, str(e)))
        raise e

    return

def makeNewActivity(aName, proj):

    p = CMSActivity(name=aName, desc='', proj=proj, code=str(uuid4()), year=tgtYear)
    try:
        db.session.add(p)
    except Exception as e:
        print("ERROR committing new activity %s, got %s" % (aName, str(e)))
        raise e

    return

def makeNewLvl3Name(l3Name):

    p = Level3Name(name=l3Name, startYear=tgtYear, endYear=None)
    try:
        db.session.add(p)
    except Exception as e:
        print("ERROR committing new L3Name %s, got %s" % (l3Name, str(e)))
        raise e

    return

def convertString(inString):
    return inString.replace( '( task )( task )', '' ).strip()

def convertStringOld(inString):

    try:
        outString = inString.encode('utf-8', 'xmlcharrefreplace')
        return outString
    except UnicodeEncodeError:
        outString = inString.decode('latin-1').encode('ascii', 'xmlcharrefreplace')
        return outString
    except:
        raise

def createTask(task, lvl3, act, proj):

    # fill in missing values
    for i in range(len(indexMap)):
        if i in task : continue
        if i == 6 : task[i] = 0 # percentage at CERN
        elif i == 5 : task[i] = -1 # needed work
        else: task[i] = ''

    taskName = convertString( task[ indexMap['taskname'] ] )
    taskName = re.sub(r'\(\s*task\s*\)', r'', taskName )

    try:
        taskDesc = convertString( task[ indexMap['task description'] ] )
    except KeyError:
        taskDesc = ''
    if taskDesc.strip() == '':
        taskDesc = taskName.strip()

    taskSkills = ''

    # check also for  lvl3 here ...s
    if db.session.query(Task).filter_by(name=taskName, activityId=act.id, level3=lvl3.id, year=tgtYear).all() != []:
        msg = "Skipping already existing task %s for actId %i actName %s " % (str(task), act.id, act.name)
        print(msg)
        return False, msg

    comment = taskSkills
    pctAtCern = 0
    try:
        pctAtCern = int( task[ indexMap['% of work at cern'] ] )
    except:
        pctAtCern = 0
        comment += '; ' + task[ indexMap['% of work at cern'] ]

    kind = "CORE"

    taskType = task[ indexMap[ 'type of task' ] ]
    print('')
    if taskType.strip() == '':
        taskType = 'Perennial'

    neededWork = -1
    try:
        neededWork = float( task[indexMap['work needed']] )
    except Exception as e:
        raise e

    shiftType = -1
    if neededWork < 0 and '[automatic]' in taskName:
        shiftType = 'on-call shift'
    if 'run coord' in proj.name.lower() and 'central shifts' in act.name.lower():
        shiftType = 'on-call shift'
    if 'run coord' in proj.name.lower() and 'shift manager' in taskName.lower():
        shiftType = 'on-call shift'

    taskNew = None
    try:
        taskNew = Task( name = taskName,
                        desc = taskDesc,
                        act  = act,
                        code = str(uuid4()),
                        needs     = neededWork,
                        pctAtCERN = pctAtCern,
                        tType     = taskType,
                        comment   = comment,
                        level=1, parent='',
                        level3=lvl3.id,
                        shiftTypeId = shiftType,
                        status = 'ACTIVE',
                        locked = False,
                        kind = kind,
                        year=tgtYear,
                    )
    except ValueError as e:
        msg = "ERROR creating task %s for proj %s act %s - got %s " % (taskName, proj.name, act.name, str(e))
        print(msg)
        return False, msg

    if not taskNew:
        print("ERROR: can not create task for %s " % (taskName, ))
        raise Exception('error creating task')

    try:
        db.session.add(taskNew)
    except Exception as e:
        print("ERROR: got: ", str(e))
        raise e

    return True, 'OK'


def importNewPAT(projName):

    inFileName = 'imports/%s-EPR-2022.xlsx' % projName

    print( 'importing data for %s from %s ' % (projName, inFileName) )

    wb = load_workbook(filename=inFileName, read_only=True)

    sheets = wb.sheetnames

    print("found the following worksheets:", sheets)

    selSheet = 'sheet1'
    if selSheet not in [x.lower() for x in sheets]:
        print( '\nERROR: project name %s not found in worksheets in file !' % projName )
        print( 'Cannot import anything ... aborting.')
        return

    oldHeaders = []
    for sheetName in sheets:

        if selSheet not in sheetName.lower(): continue
        
        print( 'found worksheet for %s ' % sheetName )
        
        workSheet = wb[sheetName]

        headers = []
        for row in workSheet['A1:J1']:
            for cell in row:
                headers.append( cleanCellValue(cell) )

        # raises if headers are different in a workSheet
        diffHeaders( headers, nominalHeaders, sheetName )

        proj, act, lvl3, tasks = analyseSheet(workSheet)

        print("-"*80)
        print("worksheet : ", sheetName)
        print(len(proj) , "projects  : ", proj)
        print(len(act)  , 'activities: ', act)
        print(len(lvl3) , 'level-3   : ', lvl3)
        print(len(tasks), 'CORE tasks :')
        # [ print('   %s ' % t) for t in tasks ]

        # create projects and tasks first, then find and hand over the activity for the task:

        nOK = importTasks(tasks)

    print("imported %i new tasks for 2016" % nOK)


def cleanCellValue(cell):
    if cell.value:
        return cell.value.lower()\
                     .replace('word needed', 'work needed')\
                     .replace('\xa0', ' ') \
                     .strip()
    else:
        return ''


def importTasks(tasks):
    errors = { 'notCreated': [] }

    nOK = 0
    for t in tasks:

        projName = t[indexMap['project/lv1']]
        actName  = t[indexMap['activity/lv2']]
        lvl3Name = t[indexMap['lv3']]

        try:
            proj = db.session.query(Project).filter_by(name=projName, year=tgtYear).one()
        except Exception as e:
            if "No row was found for " in str(e):
                makeNewProject(projName)
                db.session.commit()
            else:
                raise e

        proj = db.session.query(Project).filter_by(name=projName, year=tgtYear).one()

        try:  # to find the corresponding activity:
            act = db.session.query(CMSActivity).filter_by(name=actName, projectId=proj.id, year=tgtYear).one()
        except Exception as e:
            if "No row was found for " in str(e):
                makeNewActivity(actName, proj)
            elif "Multiple rows were found for one" in str(e):
                print("ERROR multiple rows found for act %s, projId %i :" % (actName, proj.id))
                print('\n'.join(
                    db.session.query(CMSActivity).filter_by(name=actName, projectId=proj.id, year=tgtYear).all()))
                raise e
            else:
                raise e

        act = db.session.query(CMSActivity).filter_by(name=actName, projectId=proj.id, year=tgtYear).one()

        try:  # to find the corresponding activity:
            lvl3 = db.session.query(Level3Name).filter_by(name=lvl3Name).all()
            print('lvl3: ', lvl3)
            lvl3 = db.session.query(Level3Name).filter_by(name=lvl3Name).filter(Level3Name.endYear.in_([-1,tgtYear])).one()
            print('found lvl3 for name %s in DB' % lvl3Name)
        except Exception as e:
            print('lvl3 %s - got exception: %s' % (lvl3Name, str(e)) )
            if "No row was found for " in str(e):
                print('++> lvl3 %s - got exception: %s' % (lvl3Name, str(e)))
                makeNewLvl3Name(lvl3Name)
                print('lvl3 created for %s ' % lvl3Name)
            elif "Multiple rows were found for one" in str(e):
                print("ERROR multiple rows found for lvl3 %s :" % lvl3Name)
                print('\n'.join( db.session.query(Level3Name).filter(Level3Name.name==lvl3Name).filter(Level3Name.endYear.in_([-1,tgtYear])).all() ) )
                raise e
            else:
                raise e

        lvl3 = db.session.query(Level3Name).filter_by(name=lvl3Name).filter(Level3Name.endYear.in_([-1,tgtYear])).one()

        res, msg = createTask(t, lvl3, act, proj)
        if not res:
            errors['notCreated'].append( (t, lvl3, act, proj) )
            # print "ERROR creating task %s " % (t,)
            continue
        print('created task for:', t)
        nOK += 1

    for k, v in errors.items():
        for e in v:
            print( '%s: %s' % (k, str(e) ) )

    return nOK


def diffHeaders( headers, oldHeaders, sheetName ) :

    if not oldHeaders :
        oldHeaders = copy.copy( headers )
    else :
        try :
            delta = list( difflib.context_diff( oldHeaders, headers ) )
        except Exception as e :
            print("processing %s got: %s" % (sheetName, str( e )))
            print("old: ", oldHeaders)
            print('new: ', headers)
            raise e

        if len( delta ) > 0 :
            msg = "found different headers for %s:\n" % (sheetName,)
            for line in delta :
                sys.stdout.write( line )
            print("old: ", oldHeaders)
            print('new: ', headers)
            raise Exception( msg )
        else :
            print("headers match ... ")

def checkNewTasks(startTime):

    startTimeStamp = datetime.datetime.utcfromtimestamp(startTime)
    newTasks = (db.session.query( Task, Project, CMSActivity, Level3Name )
                          .join( CMSActivity, CMSActivity.id == Task.activityId )
                          .join( Project, Project.id == CMSActivity.projectId )
                          .join( Level3Name, Level3Name.id == Task.level3 )
                          .filter( Task.year == tgtYear )
                          .filter( Task.timestamp > startTimeStamp )
                          .all())

    print('\nFound %s tasks since %s (%s):' % (len(newTasks), startTimeStamp, startTime) )
    # [ print( str(x) ) for x in newTasks ]

    errors = { 'invalidNeeds' : [] }
    for task, proj, act, lvl3 in newTasks:
        if task.neededWork < 0:
            errors['invalidNeeds'].append( {'lvl2': act.name, 'lvl3': lvl3.name, 'task': task.name, 'needs': task.neededWork } )

    for k, v in errors.items():
        for e in v:
            print( '%s: %s' % (k, str(e) ) )

    print( 'Check done.')

if __name__ == '__main__':

    theApp = create_app('default')
    with theApp.app_context():

        start = time.time()

        importNewPAT('Muon')

        checkNewTasks( start )
