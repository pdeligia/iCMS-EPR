
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import json
import time

from app import db
from app.models import EprInstitute, EprUser, Role, Project, CMSActivity, Task, Pledge, Permission, Manager
from app.ldapAuth import getLogin

projMap = {}

def importProjects():
    
    start = time.time()
    with open('imports/iCMS_Project.json', 'r') as projFile:
        projects = json.load( projFile, encoding='latin-1' )
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'found %i projects' % len(projects) )

    print( projects[0] )

    start = time.time()
    nOK = 0
    nOld = 0
    for i in projects:

        if int( i['year'] ) < 2015:
            # print "skipping old project", str(i)
            nOld += 1
            continue

        print( '\nprocessing project: ', str(i) )

        iName = i['projectId']
        if not iName :
            iName = i['projectId']
        if not iName:
            print( "ERROR: Ignoring project w/o projectId : ", str(i) )
            continue

        try:
            proj = Project(name=i['name'].decode('latin-1').encode("utf-8"),
                           desc=i['name'].decode('latin-1').encode("utf-8"),
                           code=iName.decode('latin-1').encode("utf-8") )
            db.session.add(proj)
            db.session.commit()
            nOK += 1
        except Exception as e:
            print( "ERROR: got: ", str(e) )
            db.session.rollback()

    print( "imported %i (%i OK, %i old) projects in %f sec." % (len(projects), nOK, nOld, time.time()-start) )
    print( '\n' )


def getActMgt():

    start = time.time()
    with open('imports/iCMS_ActivityManagement.json', 'r') as projFile:
        actMgt = json.load( projFile, encoding='latin-1' )
    print( "loading json took ", time.time() - start, 'sec' )

    actProjMap = {}
    actMgrs = {}
    actIdMap = {}
    for i in actMgt:
        if int( i['year']) < 2014 : continue

        actIdMap[ i['id'] ] = '%s-%s' % ( i['projectId'], i['activityId'] )

        actProjMap[ i['activityId'] ] = i['projectId']
        actMgrs[ i['activityId'] ] = [ i['cmsId'] ]
        for index in ['cmsId2', 'cmsId3', 'cmsId4']:
            if i[index] != 0 :
                actMgrs[ i['activityId'] ].append(i[index])


    return actProjMap, actMgrs, actIdMap

def getActivityInfo():
    
    start = time.time()
    with open('imports/iCMS_Activity.json', 'r') as projFile:
        activities = json.load( projFile, encoding='latin-1' )
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'found %i activities' % len(activities) )

    actInfo = {}
    for i in activities:

# some activities from older times are still active. :(
#         if int( i['year'] ) < 2015: continue

        actId = i['activityId']
        if actId in actInfo.keys(): continue

        actInfo[actId] = { 'desc' : i['description'],
                           'name' : i['name'],
                           'shortName' : i['shortName'],
                           }

    return actInfo

def importActivities():

    start = time.time()
    with open('imports/iCMS_Activity.json', 'r') as projFile:
        activities = json.load( projFile, encoding='latin-1' )
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'found %i activities' % len(activities) )

    print( activities[0] )

    actProjMap, actMgrs, actIdMap = getActMgt()

    start = time.time()
    nOK = 0
    nOld = 0
    for i in activities:

        if int( i['year'] ) < 2015:
            # print "skipping old project", str(i)
            nOld += 1
            continue
        try:
            actIdString = '[%s-%s] %s' % ( actProjMap[ i['activityId'] ] ,
                                       i['activityId'],
                                       i['shortName'].decode('latin-1').encode("utf-8"))
        except KeyError:
            print( "ERROR: no project found in map for ", i['activityId'], 'skipping activity.' )
            continue

        actCode = '[%s-%s]' % ( actProjMap[ i['activityId'] ] , i['activityId'] )

        print( '\nprocessing activity named "' + actIdString + '" : ', str(i) )
        print( '        ...  code: ', actCode )

        iName = i['activityId']
        if not iName :
            iName = i['activityId']
        if not iName or iName.strip() == '':
            print( "ERROR: Ignoring activity w/o activityId : ", str(i) )
            continue

        proj = Project.query.filter_by(code = actProjMap[ i['activityId'].decode('latin-1').encode("utf-8") ] ).one()

        try:
            act = CMSActivity( name= i['shortName'].decode('latin-1').encode("utf-8"),
                               desc=i['name'].decode('latin-1').encode("utf-8"),
                               proj = proj,
                               code = actCode )
            db.session.add(act)
            db.session.commit()
            nOK += 1
        except Exception as e:
            print( "ERROR: got: ", str(e) )
            db.session.rollback()

    print( "imported %i (%i OK, %i old) activities in %f sec." % (len(activities), nOK, nOld, time.time()-start) )
    print( '\n' )


def getTaskInfo():

    start = time.time()
    with open('imports/iCMS_Task.json', 'r') as projFile:
        tasks = json.load( projFile, encoding='latin-1' )
    print( "loading json took ", time.time() - start, 'sec' )

    taskInfo = {}
    for i in tasks:
        if int( i['year']) < 2015 : continue
        # if i['code'].strip() != '':
        #     print "found code for task:", str(i)
        if i['flag'] != "ACTIVE":
            print( "ignoring non-active task ", str(i) )
            continue

        taskInfo[ i['taskId'] ] = { 'shortName' : i['shortName'],
                                    'name' : i['name'],
                                    'activityId' : i['activityId'],
                                    'description' : i['description'],
                                    'projId' : i['projectId'],
                                    'shiftType' : i['shiftType'],
                                    }

    return taskInfo

def updateShiftTasks():
    return importTasks(updateShiftTasks=True)

def importTasks(updateShiftTasks=False):
    
    start = time.time()
    with open('imports/iCMS_TaskManagement.json', 'r') as projFile:
        tasks = json.load( projFile, encoding='latin-1' )
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'found %i tasks' % len(tasks) )

    print( tasks[0] )

    actInfo = getActivityInfo()
    taskInfo = getTaskInfo()

    start = time.time()
    nActOK = 0
    nOK = 0
    nOld = 0
    for i in tasks:

        if int( i['year'] ) < 2015:
            # print "skipping old task", str(i)
            nOld += 1
            continue

        iName = i['taskId']
        if not iName or iName.strip() == '':
            print( "ERROR: Ignoring task w/o taskId : ", str(i) )
            continue

        if iName not in taskInfo.keys():
            print( "Ignoring task for %s as not found in active tasks " % iName )
            continue

        projId = i['projectId'].decode('latin-1').encode("utf-8")
        actId  = i['activityId'].decode('latin-1').encode("utf-8")

        tName = '[[%s-%s]-%s] %s' % ( projId, actId,
                                      i['taskId'],
                                      taskInfo[iName]['name'].decode('latin-1').encode("utf-8") )

        tCode = '[[%s-%s]-%s]' % ( projId, actId,
                                   i['taskId'] )

        try:
            actCode = '[%s-%s]' % (projId, actId)
            activity = CMSActivity.query.filter( CMSActivity.code.like( actCode ) ).one( )
        except Exception as e:
            if "No row was found for " in str(e):
                print( "No activity found for actCode '%s' (taskId %s) - creating activity" % (actCode+'%', iName) )
                # first get the project:
                proj = Project.query.filter_by(code = i['projectId'].decode('latin-1').encode("utf-8")).one()
                print( "got project ", proj )
                # now try to create the new activity:
                print( "going to create act for name %s desc %s code %s" % (actInfo[actId]['shortName'].decode('latin-1').encode("utf-8"),
                                                                           actInfo[actId]['name'].decode('latin-1').encode("utf-8"),
                                                                           actCode) )
                try:
                    act = CMSActivity( name = actInfo[actId]['shortName'].decode('latin-1').encode("utf-8"),
                                       desc = actInfo[actId]['name'].decode('latin-1').encode("utf-8"),
                                       proj = proj,
                                       code = actCode )
                    db.session.add(act)
                    db.session.commit()
                    nActOK += 1
                except Exception as e:
                    print( "ERROR: can not create CMSActivity code '%s' for task '%s-%s-%s' - got: '%s' " % (actCode, projId,actId,tCode, str(e)) )
                    db.session.rollback()
            else:
                print( "ERROR: DB error when retrieving CMSActivity %s for task %s-%s-%s - got: %s " % (actCode, projId,actId,tCode, str(e)) )


        # now re-retrieve it so it's available below
        activity = CMSActivity.query.filter( CMSActivity.code.like( actCode+'%' ) ).one( )

        if not updateShiftTasks:
            print( '\nprocessing task code %s name "%s":  %s', (tCode, tName, str(i)) )

            taskType = 'Perennial'
            sTypeId = taskInfo[iName]['shiftType']

            if sTypeId != '': taskType = 'central shift'
            if "on" in taskInfo[iName]['name'] and "call" in taskInfo[iName]['name']: taskType = 'on-call expert shift'

        if updateShiftTasks and sTypeId != '':
            print( "updating shift task, code: %s shiftTypeId %s " % (tCode, sTypeId) )
            task = Task.query.filter_by(code=tCode).one()
            task.tType = taskType
            task.shiftTypeId = sTypeId

            db.session.commit()
            nOK += 1
            continue

        try:
            task = Task(name = taskInfo[iName]['name'].decode('latin-1').encode("utf-8"),
                        desc = taskInfo[iName]['description'].decode('latin-1').encode("utf-8"),
                        act = activity,
                        needs = float( i['needed'] ),
                        pctAtCERN = 0,
                        tType = taskType,
                        comment = 'imported',
                        code = tCode,
                        level=1, parent='', 
                        shiftTypeId = sTypeId
                        )
            db.session.add(task)
            db.session.commit()
            nOK += 1
        except Exception as e:
            print( "ERROR: got: ", str(e) )
            db.session.rollback()

    print( "imported %i (%i OK, %i old) tasks in %f sec. Created %i new activities" % (len(tasks), nOK, nOld, time.time()-start, nActOK) )
    print( '\n' )

def importTasksOnly(updateShiftTasks=False):

    start = time.time()
    with open('imports/iCMS_TaskManagement.json', 'r') as projFile:
        tasks = json.load( projFile, encoding='latin-1' )
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'found %i tasks' % len(tasks) )

    print( tasks[0] )

    taskInfo = getTaskInfo()

    start = time.time()
    nOK = 0
    nOld = 0
    for i in tasks:

        if int( i['year'] ) < 2015:
            # print "skipping old task", str(i)
            nOld += 1
            continue

        iName = i['taskId']
        if not iName or iName.strip() == '':
            print( "ERROR: Ignoring task w/o taskId : ", str(i) )
            continue

        actCode = '[%s-%s]%%' % (i['projectId'].decode('latin-1').encode("utf-8"),
                                 i['activityId'].decode('latin-1').encode("utf-8"))
        try:
            activity = CMSActivity.query.filter( CMSActivity.code.like( actCode ) ).one( )
        except Exception as e:
            if "No row was found for " in str(e):
                print( "ERROR: no activity found for actCode %s (taskId %s) (skipping task)" % (actCode, iName) )
                continue

        if iName not in taskInfo.keys():
            print( "Ignoring task for %s as not found in active tasks " % iName )
            continue

        tName = '[[%s-%s]-%s] %s' % ( i['projectId'].decode('latin-1').encode("utf-8"),
                                      i['activityId'].decode('latin-1').encode("utf-8"),
                                      i['taskId'],
                                      taskInfo[iName]['name'].decode('latin-1').encode("utf-8") )

        tCode = '[[%s-%s]-%s]' % ( i['projectId'].decode('latin-1').encode("utf-8"),
                                   i['activityId'].decode('latin-1').encode("utf-8"),
                                   i['taskId'] )
        if not updateShiftTasks:
            print( '\nprocessing task code %s name "%s":  %s', (tCode, tName, str(i)) )

            taskType = 'Perennial'
            sTypeId = taskInfo[iName]['shiftType']

            if sTypeId != '': taskType = 'central shift'
            if "on" in taskInfo[iName]['name'] and "call" in taskInfo[iName]['name']: taskType = 'on-call expert shift'

        if updateShiftTasks and sTypeId != '':
            print( "updating shift task, code: %s shiftTypeId %s " % (tCode, sTypeId) )
            task = Task.query.filter_by(code=tCode).one()
            task.tType = taskType
            task.shiftTypeId = sTypeId

            db.session.commit()
            nOK += 1
            continue

        try:
            task = Task(name = taskInfo[iName]['name'].decode('latin-1').encode("utf-8"),
                        desc = taskInfo[iName]['description'].decode('latin-1').encode("utf-8"),
                        act = activity,
                        needs = float( i['needed'] ),
                        pctAtCERN = 0,
                        tType = taskType,
                        comment = 'imported',
                        code = tCode,
                        level=1, parent='',
                        shiftTypeId = sTypeId
                        )
            db.session.add(task)
            db.session.commit()
            nOK += 1
        except Exception as e:
            print( "ERROR: got: ", str(e) )
            db.session.rollback()

    print( "imported %i (%i OK, %i old) tasks in %f sec." % (len(tasks), nOK, nOld, time.time()-start) )
    print( '\n' )

def importPledges():
    
    start = time.time()
    with open('imports/iCMS_Pledge.json', 'r') as projFile:
        pledges = json.load( projFile, encoding='latin-1' )
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'found %i pledges' % len(pledges) )

    print( pledges[0] )

    plInDb = db.session.query(Pledge.code).all()
    print( plInDb[:10] )

    taskInfo = getTaskInfo()

    start = time.time()
    nOK = 0
    nOld = 0
    for i in pledges:

        if i['year'] < 2015:
            # print "skipping old pledge", str(i)
            nOld += 1
            continue

        print( '\nprocessing pledge: ', str(i) )

        try:
            taskCodeStr = '[[%s-%s]-%s]%%' % ( i['projectId'],
                                              i['activityId'],
                                              i['taskId'] )
            task = Task.query.filter( Task.code.like(taskCodeStr) ).one()
        except Exception as e:
            if "No row was found for " in str(e):
                print( "ERROR: no task found for taskCode %s (pledgeId %s) (skipping pledge)" % (taskCodeStr, i['id']) )
                continue
            else:
                print( "ERROR: tryring to find taskId %s for pledge %s: %s" % (i['taskId'], i['id'], str(e)) )
                continue
        try:
            inst = EprInstitute.query.filter_by(code=i['instCode']).one()
        except Exception as e:
            if "No row was found for " in str(e):
                print( "ERROR: no institute (%s) found for taskId %s (pledgeId %s) (skipping pledge)" % (i['instCode'], i['taskId'], i['id']) )
                continue
            else:
                print( "ERROR: tryring to find institute (%s) for pledge %s: %s" % (i['instCode'], i['id'], str(e)) )
                continue

        try:
            user = EprUser.query.filter_by( cmsId=int(i['cmsId']) ).one()
        except Exception as e:
            if "No row was found for " in str(e):
                print( "ERROR: no user (%s) found for taskId %s (pledgeId %s) (skipping pledge)" % (i['cmsId'], i['taskId'], i['id']) )
                continue
            else:
                print( "ERROR: tryring to find institute (%s) for pledge %s: %s" % (i['instCode'], i['id'], str(e)) )
                continue

        plCode = '+'.join([str(x) for x in [task.id, user.id, inst.id]])
        if (plCode,) in plInDb:
            print( 'ignoring already existing pledge for code : ', plCode )
            continue

        try:
            stat = 'acc'
            if  float(i['total']) > 0: stat = 'done'
            pledge = Pledge(taskId=task.id,
                            userId = user.id,
                            instId = inst.id,
                            workTimePld = float(i['amount']),
                            workTimeAcc = float(i['totalAccepted']),
                            workTimeDone = float(i['total']),
                            status = stat,
                            year = 2015,
                            workedSoFar = float(i['total']))
            db.session.add(pledge)
            db.session.commit()
            nOK += 1
        except Exception as e:
            print( "ERROR: got: ", str(e) )
            db.session.rollback()

    print( "imported %i (%i OK, %i old) pledges in %f sec." % (len(pledges), nOK, nOld, time.time()-start) )
    print( '\n' )

def doAll():
    importProjects()
    # now done from Task-import  importActivities()
    importTasks()
    importPledges()

if __name__ == '__main__':
    doAll()
