
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import xml.etree.ElementTree as ET
import os, sys
import time
import datetime

from sqlalchemy import and_
import sqlalchemy

scrPath = '.'
if scrPath not in sys.path:
    sys.path.append(scrPath)

from app import db, create_app
from app.models import EprUser, Shift, ShiftGrid

from config import config

if 'TEST_DATABASE_URL' in os.environ :
    impRetVal = True
else:
    impRetVal = False

# From Frank, Oct 18, 2017:
# New tables to manage the shift weights (from Frank Glege, 18-Oct-2017):
# cms_shiftlist.shifttypes  : contains details on which shifts belongs to which "System"
# cms_shiftlist.shiftgrid   : contains the templates which are defined for each shift-entity
# to be used when implementing "conversion scaling" ... likely together with parsing/interpreting the date/time
# to see if a shift is of "day/evening/night" and "week/weekend" type.
# Q: where are the flavour_names ???
# from the data: three possible "flavourNames": "main", "main 2nd", and "trainee" ...

# get a list of the relevant parts of the system:
# SELECT t.SHIFT_TYPE_ID, t.SUB_SYSTEM, t.SHIFT_TYPE, g.ORDER_NUM, g.START_HOUR, g.END_HOUR FROM CMS_SHIFTLIST.SHIFTGRID g JOIN CMS_SHIFTLIST.SHIFTTYPES t ON t.SHIFT_TYPE_ID=g.SHIFT_TYPE_ID ORDER BY t.SUB_SYSTEM, t.SHIFT_TYPE, g.ORDER_NUM;


class OnlineShiftGrid(db.Model):
    __tablename__ = 'SHIFTGRID'
    __bind_key__ = 'onlShiftDB'
    __table_args__ = { 'implicit_returning' : impRetVal,
                         'schema' : 'cms_shiftlist' }

    shiftTypeId = db.Column( 'shift_type_id', db.Integer , nullable=False, primary_key=True )
    subSystem   = db.Column( 'sub_system', db.String(200), primary_key=True )
    shiftType   = db.Column( 'shift_type', db.String(200), nullable=False )
    orderNum    = db.Column( 'order_num', db.Integer , nullable=False, primary_key=True )
    startHour  = db.Column( 'start_hour', db.DateTime )
    endHour    = db.Column( 'end_hour'  , db.DateTime )


#-ap: not sure what this is useful for in our context:
# class OnlineShiftType(db.Model):
#     __tablename__ = 'SHIFTTYPES'
#     __bind_key__ = 'onlShiftDB'
#     __table_args__ = {"implicit_returning":  impRetVal,
#                       'schema' : 'cms_shiftlist' }
#
#     shiftTypeId = db.Column( 'shift_type_id', db.Integer , nullable=False )
#     shiftType   = db.Column( 'shift_type', db.String(200), nullable=False )
#     subSystem   = db.Column( 'sub_system', db.String(200) )
#-ap end

class OnlineShift(db.Model):
    __tablename__ = 'SHIFTDATA'
    __bind_key__ = 'onlShiftDB'
    __table_args__ = {"implicit_returning":  impRetVal,
                      'schema' : 'cms_shiftlist' }

    #desc cms_shiftlist.shiftData;
    # Name                                      Null?    Type
    # ----------------------------------------- -------- ----------------------------
    # SHIFT_ID                                  NOT NULL NUMBER
    # SHIFTER_ID                                NOT NULL NUMBER
    # SUB_SYSTEM                                         VARCHAR2(200)
    # SHIFT_TYPE                                NOT NULL VARCHAR2(200)
    # SHIFT_TYPE_ID                             NOT NULL NUMBER
    # SHIFT_START                               NOT NULL DATE
    # SHIFT_END                                 NOT NULL DATE
    # FLAVOUR_NAME                                       VARCHAR2(64)
    # WEIGHT                                             NUMBER

    shiftId     = db.Column( 'shift_id', db.Integer, primary_key=True )
    shifterId   = db.Column( 'shifter_id', db.Integer, nullable=False )

    subSystem   = db.Column( 'sub_system', db.String(200) )
    shiftType   = db.Column( 'shift_type', db.String(200), nullable=False )
    shiftTypeId = db.Column( 'shift_type_id', db.Integer , nullable=False )

    shiftStart  = db.Column( 'shift_start', db.DateTime, nullable=False )
    shiftEnd    = db.Column( 'shift_end'  , db.DateTime, nullable=False )
    flavourName = db.Column( 'flavour_name', db.String(64) )
    weight      = db.Column( db.Float )


    def to_json(self):
        json_data = {
                'shiftId' : self.shiftId,
                'shifterId' : self.shifterId,

                'subSystem' : self.subSystem,
                'shiftType' : self.shiftType,
                'shiftTypeId' : self.shiftTypeId,

                'shiftStart' : self.shiftStart.strftime('%Y-%m-%dT%H%M%S %z'),
                'shiftEnd' : self.shiftEnd.strftime('%Y-%m-%dT%H%M%S %z'),
                'flavourName' : self.flavourName,
                'weight' : self.weight,
        }
        return json_data

    def __repr__( self ) :
        msg = '<OnlineShift '
        for k, v in self.to_json( ).items( ) :
            msg += '%s: %s, ' % (k, str( v ))
        msg += '>'
        return msg

def makeDateTime(inString):

    dString, tString = inString.split()
    mon,day,yr = [int(x) for x in dString.split('/')]
    hr, min, sec = [int(x) for x in tString.split(':')]

    return datetime.datetime(yr,mon,day, hr,min, sec)

def getWeight(shiftEntry):

    startTime = shiftEntry.shiftStart
    endTime   = shiftEntry.shiftEnd

    # determine scaling factor as a function of day/time
    # see also presentation at CB-112 (Feb 2017):
    # https://indico.cern.ch/event/605565/contributions/2445977/attachments/1406703/2149710/20170203_EPR_Shifts2017_vF.pdf
    # and the central shift twiki for 2018 at:
    # https://twiki.cern.ch/twiki/bin/viewauth/CMS/CentralShifts2018

    scalingFactor = 1.
    duration = (endTime-startTime).days*86400 + (endTime-startTime).seconds
    if startTime.hour in [7,15,23] and duration == 8*3600: # "normal" 8-hour shift
        # weekday: Monday is 0 and Sunday is 6, so Fri,Sat is 4,5
        if startTime.weekday() in [4, 5] and startTime.hour == 23:
                scalingFactor = 2.0        # w/e night shift: 2.5
        elif startTime.weekday() in [5, 6] and startTime.hour in [7, 15]:
                scalingFactor = 1.5        # w/e day/eve shift: 2.0
        else: # 'normal' weekday:
            if   startTime.hour == 23:
                scalingFactor = 1.5        # weekday night shift: 1.5
    elif duration == 86400: # full day shift:
        if startTime.weekday() in [4, 5] and startTime.hour == 00:
            scalingFactor = 1.5
    elif duration == 604800: # week shift, weight 8
        scalingFactor = 8.0

    # if float(shiftEntry.weight) > 0. and scalingFactor != float( shiftEntry.weight ):
    #     print( 'found inconsistent weights: from DB: %4.2f - calc: %4.2f - for %s (%d) shiftStart %s end %s - subsystem %s, type %s (%d)' %
    #            (shiftEntry.weight, scalingFactor, startTime.strftime('%a'), duration, startTime, endTime, shiftEntry.subSystem, shiftEntry.shiftType, shiftEntry.shiftTypeId)  )

    if shiftEntry.weight is None: 
        return None, scalingFactor

    return float( shiftEntry.weight ), scalingFactor

def calculateWeight(shiftEntry, year):

    newWeight = 0.
    scalingFactor = 1.
    if ( shiftEntry.subSystem == 'RPC' and
         shiftEntry.shiftTypeId == 12 and
         shiftEntry.weight == 0. and
         year >= 2017 ): newWeight = 1.
    elif year == 2021 and checkShift( shiftEntry ):
        newWeight, scalingFactor = getWeight( shiftEntry )
        newWeight = 1. * scalingFactor
        print("++> INFO: Weight of NULL for shift %s reset to %s %s - %s " % (shiftEntry, newWeight, scalingFactor, newWeight) )
    else:
        if shiftEntry.weight is None:
            newWeight = 0.
        else:
            newWeight, scalingFactor = getWeight( shiftEntry )

    # ignore known entries with weight=0
    if shiftEntry.weight is not None and newWeight == 0. and shiftEntry.subSystem != 'TC' and shiftEntry.shiftTypeId != 92:
        print("++> WARNING: Weight of 0. (but not NULL) found for shift %s " % shiftEntry)

    return newWeight

def checkShift( shiftEntry ):

    knownShifts = [
( "CSC",         "DOC",             37,        "main" ),
( "Central",     "DCS",             2,         "main" ),
( "Central",     "DCS",             2,         "trainee" ),
( "Central",     "Shift_leader",    8,         "main" ),
( "HCAL",        "DOC",             26,        "main" ),
( "HCAL",        "DOC",             26,        "main 2nd" ),
( "HCAL",        "DOC",             26,        "trainee" ),
( "TRK",         "DOC",             32,        "main" ),
( "TRK",         "DOC",             32,        "main 2nd" ),
( "TRK",         "DOC",             32,        "trainee" ),
]
    if (shiftEntry.subSystem, shiftEntry.shiftType, shiftEntry.shiftTypeId, shiftEntry.flavourName) in knownShifts:
        return True
    
    return False


def importShiftsFromDB(year):

    # select * from  cms_shiftlist.shiftData where to_char(shift_start,'YYYY') = '2016';

    onlineShifts = (db.session.query(OnlineShift)
                              .filter( and_( OnlineShift.shiftStart > datetime.date(year,1,1),
                                             OnlineShift.shiftEnd < datetime.date(year+1,1,1) ) )
                              .all())

    print("\nfound %s shifts for %s in online DB" % (len(onlineShifts), year))

    print(onlineShifts[:10])

    userList = db.session.query(EprUser.hrId).all()
    existingShiftIds = db.session.query(Shift.shiftId).filter(Shift.year==year).all()

    start = time.time()
    nNew = 0
    nUpdated = 0
    nNullShifts = 0
    mappedIDs = []
    for shift in onlineShifts:

        # Note: since 2017 it is possible to define "free schedule" shifts,
        # which may have times different from the standard shift times. These
        # shifts have their weight set to "null" in the online DB and are
        # ignored when importing.

        if shift.weight is None :
            if not checkShift( shift ):
                print("++> INFO: skipping unknown shift with weight NULL ('free schedule shift') found for shift %s " % shift)
                nNullShifts += 1
                continue

        if ( (shift.shifterId,) not in userList ) :
            if shift.shifterId != 0:
                print("skipping shift %s as user %s not found " % (shift.shiftId, shift.shifterId))
            continue

        if ( (shift.shiftId,) in existingShiftIds ) : # update existing shift

            eprShift = db.session.query(Shift).filter_by(shiftId=shift.shiftId).one()
            # check and update the existing values
            updateNeeded = False
            if (shift.shifterId != eprShift.userId) :
               updateNeeded = True
               eprShift.userId = shift.shifterId
            if (shift.subSystem != eprShift.subSystem) :
               updateNeeded = True
               eprShift.subSystem = shift.subSystem
            if (shift.shiftType != eprShift.shiftType) :
               updateNeeded = True
               eprShift.shiftType = shift.shiftType
            if (shift.shiftTypeId != eprShift.shiftTypeId) :
               updateNeeded = True
               eprShift.shiftTypeId = shift.shiftTypeId
            if (shift.shiftStart != eprShift.shiftStart) :
               updateNeeded = True
               eprShift.shiftStart = shift.shiftStart
            if (shift.shiftEnd != eprShift.shiftEnd) :
               updateNeeded = True
               eprShift.shiftEnd = shift.shiftEnd
            if (shift.flavourName != eprShift.flavourName) :
               updateNeeded = True
               eprShift.flavourName = shift.flavourName
            newWeight = calculateWeight( shift, year )
            if newWeight is None: newWeight = 0.
            if ( newWeight != eprShift.weight) :
               updateNeeded = True
               eprShift.weight = newWeight

            if updateNeeded:
                nUpdated += 1
                db.session.add( eprShift )
        else:

            newWeight = calculateWeight( shift, year )
            if newWeight is None: newWeight = 0.
            s = Shift( shiftId = shift.shiftId,
                       hrId = shift.shifterId,
                       subSys = shift.subSystem,
                       sType = shift.shiftType,
                       sTypeId = shift.shiftTypeId,
                       sStart = shift.shiftStart,
                       sEnd = shift.shiftEnd,
                       flavourName = shift.flavourName,
                       weight = newWeight,
                       year = year,
                 )
            db.session.add(s)

        mappedIDs.append( shift.shiftId )  # store the shifts found ...
    try:
       db.session.commit()
       nNew += 1
    except sqlalchemy.exc.IntegrityError as e:
       print("ERROR, user with id %i not found for shift_id %i (%s)" % \
             (int(shift.find('SHIFTER_ID').text), int(shift.find('SHIFT_ID').text), str(e)))
       db.session.rollback()

    deletedShifts = list( set([ x[0] for x in existingShiftIds] ) - set(mappedIDs) )
    print("found %s deleted shifts - from %d existing, %s mapped" % (len(deletedShifts), len(existingShiftIds), len(mappedIDs)))
    print(deletedShifts)

    delShifts = db.session.query(Shift).filter( Shift.shiftId.in_( deletedShifts) ).all()
    for delShift in delShifts:
        print("id: %s - %s " % ( delShift.shiftId, str(delShift) ))
        db.session.delete( delShift )
    db.session.commit()

    print("found %d 'free-schedule' shifts with weight null" % nNullShifts)

    print("processing %d shifts (%i new, %i updated) took %f sec" % ( len(onlineShifts), nNew, nUpdated, time.time()-start ))


def importShiftsFromXML(year):

    # the data file is from:
    # http://cmsonline.cern.ch/shiftlist_info/DBGetData?period=2016

    start = time.time()
    tree = ET.parse('imports/shifts-%d.xml' % year)
    root = tree.getroot()
    print("reading and parsing file took %f sec" % (time.time()-start))

    userList = db.session.query(EprUser.hrId).all()
    existingShifts = db.session.query(Shift.shiftId).all()

    start = time.time()
    nNew = 0
    for shift in root.findall('SHIFT'):

        userId = int(shift.find('SHIFTER_ID').text)
        if ( (userId,) not in userList ) :
            print("skipping shift %s as user %s not found " % (shift.find('SHIFT_ID').text, shift.find('SHIFTER_ID').text))
            continue

        shiftId = int(shift.find('SHIFT_ID').text)
        if ( (shiftId,) in existingShifts ) :
           # print "skipping shift %s - already in DB " % (shift.find('SHIFT_ID').text, )
           continue

        s = Shift( shiftId= int(shift.find('SHIFT_ID').text),
                   hrId = int(shift.find('SHIFTER_ID').text),
                   subSys = shift.find('SUB_SYSTEM').text,
                   sType = shift.find('SHIFT_TYPE').text,
                   sTypeId = int(shift.find('SHIFT_TYPE_ID').text),
                   sStart = makeDateTime( shift.find('SHIFT_START').text ),
                   sEnd = makeDateTime( shift.find('SHIFT_END').text ),
                   flavourName = shift.find('FLAVOUR_NAME').text,
                   weight = float( shift.find('WEIGHT').text ),
                   year = year,
             )
        db.session.add(s)
    try:
       db.session.commit()
       nNew += 1
    except sqlalchemy.exc.IntegrityError as e:
       print("ERROR, user with id %i not found for shift_id %i (%s)" % \
             (int(shift.find('SHIFTER_ID').text), int(shift.find('SHIFT_ID').text), str(e)))
       db.session.rollback()

    print("processing %d shifts (%i new) took %f sec" % ( len(root.findall('SHIFT')), nNew, time.time()-start ))


def importShiftGridFromDB():

    shiftGridEntries = db.session.query(OnlineShiftGrid).all()

    if not shiftGridEntries:
        print("ERROR: no entries found in table ShiftGrid in online DB ... nothing done.")
        return

    # first delete the content of the table in postgres:
    db.session.query(ShiftGrid).delete()
    db.session.commit()

    # then add them back one by one
    sgList = []
    for item in shiftGridEntries:
        # print( item )
        sgList.append( ShiftGrid(subSys=item.subSystem,
                                   sType=item.shiftType,
                                   sTypeId=item.shiftTypeId,
                                   startHour=item.startHour,
                                   endHour=item.endHour,
                                   orderNum=item.orderNum,
                                   )
                       )
    db.session.add(sgList)
    db.session.commit()


def importShiftGridFromJsonFile():

    import json
    with open('shiftGrid.json', 'r') as fp:
        shiftGridEntries = json.load(fp)

    if not shiftGridEntries:
        print("ERROR: no entries found in table ShiftGrid in online DB ... nothing done.")
        return

    # first delete the content of the table in postgres:
    db.session.query(ShiftGrid).delete()
    db.session.commit()

    # then add them back one by one
    for item in shiftGridEntries:
        # print( item )
        sg = ShiftGrid( subSys=item['subSystem'],
                        sType=item['shiftType'],
                        sTypeId=item['shiftTypeId'],
                        startHour=datetime.datetime.strptime( '000%s' % item['startHour'].strip(), '%Y-%m-%dT%H%M%S'),
                        endHour=datetime.datetime.strptime( '000%s' % item['endHour'].strip(), '%Y-%m-%dT%H%M%S'),
                        orderNum=item['orderNum'],
                       )
        db.session.add(sg)
    db.session.commit()



def importShifts(year):

    importShiftsFromDB(year=year)
#    importShiftGridFromDB()
#    importShiftGridFromJsonFile()

if __name__ == '__main__':

    theApp = create_app('default')
    with theApp.app_context():

        # print "onlShiftDB from: ", theApp.config['SQLALCHEMY_ONLSHIFTDB_URI']

        # Extensions like Flask-SQLAlchemy now know what the "current" app
        # is while within this block. Therefore, you can now run........
        # db.create_all(bind=None)
        # importShifts(year=2015)
        # importShifts(year=2016)
        # importShifts(year=2017)
        # importShifts(year=2018)
        # importShifts(year=2019)
        # importShifts(year=2020)
        importShifts(year=2021)
        importShifts(year=2022)
