
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from datetime import datetime
import sys
import logging
import subprocess

logging.basicConfig(format = '%(asctime)-15s: %(message)s')
logger = logging.getLogger("SetupDummies")
logger.setLevel(logging.INFO)

from instance import config

from sqlalchemy.exc import ProgrammingError, IntegrityError
from sqlalchemy import event, DDL

from icms_orm import epr_schema_name
from icms_orm.common import ApplicationAsset, AppNameValues
from icms_orm.common import Institute, Country
# from icms_orm.common import UserPreferences, Person, PersonStatus, Institute, InstituteStatus

import sqlalchemy
import flask
import icms_orm
from icms_orm.orm_interface.orm_manager import OrmManager

from app import db
from app.models import Role, Category, Permission, EprUser, EprInstitute, Manager,  TimeLineInst, TimeLineUser, Project, CMSActivity, Task, Pledge, Shift, ShiftTaskMap, EprCeiling, Level3Name, getTaskShiftTypeIds, getTaskTypes, commitNew

if '.' not in sys.path: sys.path.append('.')
if './scripts' not in sys.path: sys.path.append('./scripts')

from updateInstView import updateAllInstView
from prepareForNextYear import prepareFor
from icms_orm.orm_interface import OrmManager
import flask
import icms_orm
import sqlalchemy


def doCmd(cmd, verbose=False, dryRun=False):

    if verbose:
        print( '\n[verbose] going to execute: "%s"' % cmd )

    out = ''
    err = ''
    returnCode = 0

    if dryRun:
        print( 'dryRun requested. No action ... ' )
    else:
        process = subprocess.Popen( cmd, shell=True, stdin=subprocess.PIPE, stderr=subprocess.STDOUT )
        out, err = process.communicate()
        returnCode = process.returncode

    msg = ''
    if verbose:
        if out: msg += '\n%s' % out
        if err: msg += '\n%s' % err

    if msg:
        print( msg )
    if returnCode != 0:
        raise subprocess.CalledProcessError(returnCode, cmd)

    return returnCode, out, err

selYear = 2022

def addAndCommit(o):
    if type(o) == type([]):
        for item in o:
            commitNew( item )
    else:
        commitNew( o )


def setupAssets():
    assets = {
        "name"        : "epr_year_params",
        "application" : AppNameValues.EPR,
        "data"        : {
            "pledgeYear"   : selYear,
            "reqWork"      : { "2017" : 3.0,
                               "2018" : 4.0,
                               "2019" : 4.0,
                               "2020" : 4.0,
                               "2021" : 4.0,
                               "2022" : 4.0,
                               "2023" : 4.0,
                               },
            "cspPerAuthor" : {
                "2017" : 8.9,
                "2018" : 9.572632623,
                "2019" : 9.572632623, # 3.4,
                "2020" : 2.92,
                "2021" : 2.92,
                "2022" : 9.0,
                "2023" : 9.0,
            }
        }
    }

    ApplicationAsset.store( name=assets['name'], value=assets['data'], application=assets['application'] )

def createCategories():

    catInfo = { "Administrative" : False,
                "Doctoral Student" : True,
                "Engineer" : True,
                "Engineer Electronics" : True,
                "Engineer Mechanical" : True,
                "Engineer Software" : True,
                "Non-Doctoral Student" : False,
                "Other" : False,
                "Physicist" : True,
                "Technician" : False,
                "Theoretical Physicist" : True,
              }
    for cat, ok in catInfo.items():
        cat = Category(name=cat, ok=ok )
        try:
            db.session.add(cat)
            db.session.commit()
        except IntegrityError as e:
            db.session.rollback()
            print('=+=+=+>>> str(e)=', str(e))
            if 'duplicate key value violates unique constraint' in str(e):
                pass
            else:
                logging.error( "got error when creating categories: %s " % str(e) )
                raise e

def resetDB():
    db.session.close()
    doCmd('./resetDB.sh')

def foo():
    for role in ['epr', 'icms', 'icms_reader']:
        try:
            db.engine.execute('alter role %s with SUPERUSER' % role)
        except ProgrammingError as e:
            if 'role "%s" does not exist' %role in str(e):
                db.engine.execute('create role %s with SUPERUSER' % role)

    # # # these have enums, so drop them explicitly:
    # for tbl in [ 'event', 'user_preferences',
    #              'person', 'person_status', 'categories', 'roles', 'insts', 'users'
    #              'institute', 'institute_status',
    #              'announcement', 'application_asset',
    #              'mo', 'request', 'request_step', 'cont_transaction',
    #              'epr_aggregate', 'career_event', 'tasks', 'level3_values_enum',
    #              'shiftasks'
    #              ] :
    #     db.engine.execute('DROP TABLE IF EXISTS %s CASCADE' % tbl)
    #     db.engine.execute('DROP SEQUENCE if exists id_seq_cats cascade')

    logging.info( "\n%s\n setting up DB ... \n%s\n" % ('=' * 42, '=' * 42) )

    # # these have enums, so drop them explicitly:

    for v in ['epr.shiftasks', 'view_people_summary', 'view_person_managed_unit_ids']:
        db.engine.execute('DROP VIEW IF EXISTS %s CASCADE' % v)
    db.engine.execute('DROP TABLE IF EXISTS %s CASCADE' % 'shift_task_map')
    # ensure table IDs re-start at 1
    db.engine.execute('DROP SEQUENCE if exists id_seq_stm cascade')

    # ensure that no mails are in the table already
    db.engine.execute('DROP TABLE IF EXISTS public.email_message CASCADE')

    class SessionNonProliferationManager(OrmManager):
        def create_scoped_session(self, options=None):
                return db.session

    ormMgr = SessionNonProliferationManager( flask.current_app.config )

    # ormMgr.drop_all(icms_orm.cms_common_bind_key())
    ormMgr.drop_all(icms_orm.epr_bind_key())
    # ormMgr.drop_all(icms_orm.online_shift_bind_key())

    # this is to remove the *_version tables, as they are not properly bound. :(
    md = sqlalchemy.MetaData()
    md.bind = ormMgr.get_engine(bind=icms_orm.epr_bind_key())
    md.reflect()
    md.drop_all()

    epr_metadata = EprUser.__table__.metadata
    epr_metadata.bind = ormMgr.get_engine(bind=icms_orm.epr_bind_key())
    epr_metadata.create_all()

    ormMgr.create_all(icms_orm.cms_common_bind_key())
    ormMgr.create_all(icms_orm.epr_bind_key())
    # ormMgr.create_all(icms_orm.online_shift_bind_key())

    # event.listen( DeclarativeBase.metadata, 'before_create', DDL( "CREATE SCHEMA IF NOT EXISTS %s" % epr_schema_name() ) )
    # DDL( "CREATE SCHEMA IF NOT EXISTS %s" % epr_schema_name() )

    # db.create_all()

    logging.info( "\n%s\n DB initially set up \n%s\n" % ('=' * 42, '=' * 42) )

def setupInstMap():

    cList = [ ('CH', 'Switzerland'), ('US', 'USA'), ('DE', 'Germany') ]

    instList = [ ('CERN', 'CERN', 'CH'),
                 ('ETHZ', 'ETH Zurich', 'CH'),
                 ('FNAL', 'FNAL', 'US'),
                 ('DESY', 'DESY', 'DE'),
                 ]

    if db.session.query(Country).all() == []:
        for (code, name) in cList:
            c = Country()
            c.name = name
            c.code = code
            addAndCommit(c)

    if db.session.query(Institute).all() == []:
        for (code, name, countryCode) in instList :
            inst = Institute()
            inst.code = code
            inst.name = name
            inst.country_code = countryCode
            inst.official_joining_date = None
            addAndCommit(inst)
    else:  # add 'ETHZ':
        if not db.session.query(Institute).filter(Institute.code=='ETHZ').one():
            inst = Institute()
            inst.code = 'ETHZ'
            inst.name = 'ETH Zurich'
            inst.country_code = 'CH'
            inst.official_joining_date = None
            addAndCommit(inst)

def mkTimeLineInst(code, year, cmsStatus, timestamp):
    tli = TimeLineInst()
    tli.code = code
    tli.year = year
    tli.cmsStatus = cmsStatus
    tli.timestamp = timestamp
    return tli

def mkTimeLineUser(cmsId, instCode, mainProj, isAuthor, isSuspended, status, category, dueApplicant, dueAuthor, yearFraction, year, timestamp) :
    tlu = TimeLineUser()
    tlu.cmsId = cmsId
    tlu.instCode = instCode
    tlu.mainProj = mainProj
    tlu.isAuthor = isAuthor
    tlu.isSuspended = isSuspended
    tlu.status = status
    tlu.category = category
    tlu.dueApplicant = dueApplicant
    tlu.dueAuthor = dueAuthor
    tlu.yearFraction = yearFraction
    tlu.year = year
    tlu.timestamp = timestamp
    return tlu


def setupUserInst( ) :

    setupAssets()

    createCategories()

    Role.insert_roles( )

    setupInstMap()

    # create some institutes
    cern = EprInstitute(year=selYear, cmsStatus='Yes', name='CERN', cernId=99, country='Switzerland', code='CERN')
    addAndCommit( cern )
    fnal = EprInstitute(year=selYear, cmsStatus='Yes', name='FNAL', cernId=98, country='USA', code='FNAL')
    addAndCommit( fnal )
    ethz = EprInstitute(year=selYear, cmsStatus='Yes', name='ETH Zurich', cernId=97, country='Switzerland', code='ETHZ')
    addAndCommit( ethz )
    desy = EprInstitute(year=selYear, cmsStatus='Yes', name='DESY', cernId=96, country='Germany', code='DESY')
    addAndCommit( desy )

    # add some users:
    # wizard user
    roleWiz = db.session().query(Role).filter_by(permissions=Permission.ADMINISTER).first()
    logging.info( "\nRole %s for wizard set up at id: %s " % (str(roleWiz), roleWiz.id) )
    ap = EprUser( username='pfeiffer', name='Pfeiffer, Andreas', hrId=422405, cmsId=999, instId=cern.cernId, role=roleWiz,
               authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=cern.id)
    addAndCommit( ap )

    # Engagement Office for task mails
    roleEng = db.session().query(Role).filter_by(name='EngagementOffice').first()
    logging.info( "\nRole %s for ENGOFF_MGT set up at id: %s " % (str(roleEng), roleEng.id) )
    kb = EprUser( username='engoffice', name='Engagement, Officer', hrId=425998, cmsId=3771, instId=cern.cernId, role=roleEng,
               authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=cern.id )
    addAndCommit( kb )
    logging.info( "\nEngagement officer set up: %s " % str(kb) )

    # test user
    tstUser = EprUser( username='tester', name='Tester, Master', hrId=42042, cmsId=9042, instId=cern.cernId,
               authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=cern.id )
    addAndCommit( tstUser )

    # shift user with only one shift for the tests
    shiftUser = EprUser( username='shifter', name='Tester, Shifter', hrId=42043, cmsId=9043, instId=cern.cernId,
               authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=cern.id )
    addAndCommit( shiftUser)

    # standard users. Note that the names need to be at least 3 char for auto-search to work ...
    u1 = EprUser( username='user1', name='user, one', hrId=99000, cmsId=9900, instId=cern.cernId,
               authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=cern.id )
    u2 = EprUser( username='user2', name='user, two', hrId=99001, cmsId=9901, instId=fnal.cernId,
               authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=fnal.id )
    u3 = EprUser( username='user3', name='user, three', hrId=99002, cmsId=9902, instId=ethz.cernId,
               authorReq=False, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=ethz.id )
    u4 = EprUser( username='user4', name='user, four', hrId=99003, cmsId=9903, instId=cern.cernId,
               authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=cern.id )
    # u4a = User( username='user4', name='user, four', hrId=99003, cmsId=9903, instId=cern.cernId,
    #            authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear-1, mainInst=cern.id )
    addAndCommit( [u1, u2, u3, u4] )

    # some ETHZ users for the instResp pledge (including the contact/observer):
    roleObs = db.session().query(Role).filter_by(permissions=Permission.OBSERVE).first()
    ethUsers = []
    ethUsers.append( EprUser( username='userEth0', name='ethUser, zero' , hrId=97000, cmsId=9700, instId=ethz.cernId, authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=ethz.id ) )
    ethUsers.append( EprUser( username='userEth1', name='ethUser, one'  , hrId=97001, cmsId=9701, instId=ethz.cernId, authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=ethz.id ) )
    ethUsers.append( EprUser( username='userEth2', name='ethUser, two'  , hrId=97002, cmsId=9702, instId=ethz.cernId, authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=ethz.id ) )
    ethUsers.append( EprUser( username='userEth3', name='ethUser, three', hrId=97003, cmsId=9703, instId=ethz.cernId, authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=ethz.id ) )
    ethUsers.append( EprUser( username='userEth4', name='ethUser, four' , hrId=97004, cmsId=9704, instId=ethz.cernId, authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=ethz.id ) )
    ethUsers.append( EprUser( username='userEth5', name='ethUser, five' , hrId=97005, cmsId=9705, instId=ethz.cernId, authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=ethz.id, role=roleObs ) )
    addAndCommit( ethUsers )

    # member of the "a4MgrEgroup" egroup
    a4mgtEgMem = EprUser( username='a4meg', name='a4Mgr, eGroup', hrId=42044, cmsId=9044, instId=fnal.cernId,
               authorReq=False, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=fnal.id )
    addAndCommit( a4mgtEgMem)

    # applicant user
    applicant = EprUser( username='appl', name='appl, icant', hrId=42054, cmsId=9054, instId=cern.cernId,
               authorReq=False, status='CMS', isSusp=False, categoryName='Doctoral Student', year=selYear, mainInst=cern.id )
    addAndCommit( applicant )


    # ... and the corresponding TimeLines:
    # make sure the timelines start on Jan 1st ...
    addAndCommit( mkTimeLineInst(code=cern.code, year=selYear, cmsStatus='Yes', timestamp=datetime(selYear,1,1,0,0,0)) )
    addAndCommit( mkTimeLineInst(code=fnal.code, year=selYear, cmsStatus='Yes', timestamp=datetime(selYear,1,1,0,0,0)) )
    addAndCommit( mkTimeLineInst(code=ethz.code, year=selYear, cmsStatus='Yes', timestamp=datetime(selYear,1,1,0,0,0)) )

    addAndCommit( mkTimeLineUser(cmsId=ap.cmsId, instCode=cern.code, mainProj='PPD', isAuthor=True, isSuspended=False, status='CMS', category=7, dueApplicant=0, dueAuthor=3, yearFraction = 1., year=selYear, timestamp=datetime(selYear,1,1,0,0,0)) )
    addAndCommit( mkTimeLineUser(cmsId=u1.cmsId, instCode=cern.code, mainProj='PPD', isAuthor=True, isSuspended=False, status='CMS', category=7, dueApplicant=0, dueAuthor=3, yearFraction = 1., year=selYear, timestamp=datetime(selYear,1,1,0,0,0)) )

    addAndCommit( mkTimeLineUser(cmsId=u2.cmsId, instCode=fnal.code, mainProj='pr1', isAuthor=True, isSuspended=False, status='CMS', category=7, dueApplicant=0, dueAuthor=3, yearFraction = .5, year=selYear, timestamp=datetime(selYear,1,1,0,0,0)) )

    addAndCommit( mkTimeLineUser(cmsId=u3.cmsId, instCode=ethz.code, mainProj='pr1', isAuthor=False, isSuspended=False, status='CMS', category=7, dueApplicant=0, dueAuthor=3, yearFraction = 1., year=selYear, timestamp=datetime(selYear,1,1,0,0,0)) )
    addAndCommit( mkTimeLineUser(cmsId=u4.cmsId, instCode=cern.code, mainProj='HCAL', isAuthor=True, isSuspended=False, status='CMS', category=7, dueApplicant=0, dueAuthor=3, yearFraction = 1., year=selYear, timestamp=datetime(selYear,1,1,0,0,0)) )

    # add a second timeline for the fnal user:
    addAndCommit( mkTimeLineUser(cmsId=u2.cmsId, instCode=fnal.code, mainProj='pr2', isAuthor=True, isSuspended=False, status='CMS', category=7, dueApplicant=0, dueAuthor=3, yearFraction = .5, year=selYear, timestamp=datetime(selYear,6,1,0,0,0) ) )

    # add timeLines for the specific (test) users:
    addAndCommit( mkTimeLineUser(cmsId=kb.cmsId, instCode=cern.code, mainProj='PPD', isAuthor=True, isSuspended=False, status='CMS', category=7, dueApplicant=0, dueAuthor=3, yearFraction = 1., year=selYear, timestamp=datetime(selYear,1,1,0,0,0)) )
    addAndCommit( mkTimeLineUser(cmsId=tstUser.cmsId, instCode=cern.code, mainProj='PPD', isAuthor=True, isSuspended=False, status='CMS', category=7, dueApplicant=0, dueAuthor=3, yearFraction = 1., year=selYear, timestamp=datetime(selYear,1,1,0,0,0)) )
    addAndCommit( mkTimeLineUser(cmsId=shiftUser.cmsId, instCode=cern.code, mainProj='PPD', isAuthor=True, isSuspended=False, status='CMS', category=7, dueApplicant=0, dueAuthor=3, yearFraction = 1., year=selYear, timestamp=datetime(selYear,1,1,0,0,0)) )

    addAndCommit( mkTimeLineUser(cmsId=a4mgtEgMem.cmsId, instCode=fnal.code, mainProj='PPD', isAuthor=False, isSuspended=False, status='CMS', category=7, dueApplicant=0, dueAuthor=0, yearFraction = 1., year=selYear, timestamp=datetime(selYear,1,1,0,0,0)) )

    addAndCommit( mkTimeLineUser(cmsId=applicant.cmsId, instCode=cern.code, mainProj='PPD', isAuthor=False, isSuspended=False, status='CMS', category=7, dueApplicant=3, dueAuthor=0, yearFraction = 1., year=selYear, timestamp=datetime(selYear,1,1,0,0,0)) )

    ethAuthors = [9701, 9702, 9704]
    for ethUser in ethUsers:
        addAndCommit( mkTimeLineUser(cmsId=ethUser.cmsId, instCode=ethz.code, mainProj='PPD',
                                   isAuthor=ethUser.cmsId in ethAuthors,
                                   isSuspended=False, status='CMS', category=7,
                                   dueApplicant=4.2 if ethUser.cmsId==9700 else 0,
                                   dueAuthor=4 if ethUser.cmsId in ethAuthors else 0,
                                   yearFraction = 1., year=selYear, timestamp=datetime(selYear,1,1,0,0,0)) )

    # managers of the projects
    p1m1 = EprUser( username='p1m1', name='p1, m1', hrId=98001, cmsId=9801, instId=cern.cernId,
                 authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=cern.id )
    p2m1 = EprUser( username='p2m1', name='p2, m1', hrId=98002, cmsId=9802, instId=fnal.cernId,
                 authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=fnal.id )
    p1m2 = EprUser( username='p1m2', name='p1, m2', hrId=98003, cmsId=9803, instId=cern.cernId,
                 authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=cern.id )
    egPM = EprUser( username='egPM', name='PM, eGroup', hrId=98004, cmsId=9804, instId=cern.cernId,
                 authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=cern.id )
    addAndCommit([p1m1, p2m1, p1m2, egPM])

    # activity managers
    a1m1 = EprUser( username='a1m1', name='a1, m1', hrId=98011, cmsId=9811, instId=ethz.cernId,
                 authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=ethz.id )
    a2m1 = EprUser( username='a2m1', name='a2, m1', hrId=98012, cmsId=9812, instId=fnal.cernId,
                 authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=fnal.id )
    a3m1 = EprUser( username='a3m1', name='a3, m1', hrId=98013, cmsId=9813, instId=cern.cernId,
                 authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=cern.id )
    a4m1 = EprUser( username='a4m1', name='a4, m1', hrId=98014, cmsId=9814, instId=cern.cernId,
                 authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear-1, mainInst=cern.id )
    addAndCommit([p1m1, p2m1, a1m1, a2m1, a3m1, a4m1])

    # task managers
    t1m1 = EprUser( username='t1m1', name='t1, m1', hrId=98021, cmsId=9821, instId=cern.cernId,
                 authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=cern.id )
    addAndCommit([t1m1])

    # institute leaders
    ethLdr = EprUser( username='ethLdr', name='ETH, Leader', hrId=97021, cmsId=9721, instId=ethz.cernId,
                 authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=ethz.id )
    cernLdr = EprUser( username='cernLdr', name='CERN, Leader', hrId=97022, cmsId=9722, instId=cern.cernId,
                 authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=cern.id )
    fnalLdr = EprUser( username='fnalLdr', name='FNAL, Leader', hrId=97023, cmsId=9723, instId=fnal.cernId,
                 authorReq=True, status='CMS', isSusp=False, categoryName='Physicist', year=selYear, mainInst=fnal.id )
    addAndCommit([ethLdr, cernLdr, fnalLdr])

    # make sure the timelines start on Jan 1st ...
    for u in [p1m1, p2m1, p1m2, a1m1, a2m1, a3m1, t1m1, egPM]:
        try:
            inst = db.session.query(EprInstitute).filter_by(id=u.mainInst).one()
        except Exception as e:
            print( 'ERROR searching for inst with id: ', u.mainInst)
            raise e
        addAndCommit( mkTimeLineUser( cmsId=u.cmsId, instCode=inst.code, mainProj='COMP', isAuthor=True, isSuspended=False, status='CMS', category=7, dueApplicant=0, dueAuthor=3, yearFraction=1., year=selYear, timestamp=datetime(selYear,1,1,0,0,0) ) )

    # observers - not authors to make things easier
    obs1 = EprUser( username='t1o1', name='t1, obs1', hrId=96001, cmsId=9601, instId=cern.cernId,
                 authorReq=False, status='CMS', isSusp=False, categoryName='Engineer', year=selYear, mainInst=cern.id )
    addAndCommit([obs1])
    addAndCommit( mkTimeLineUser( cmsId=obs1.cmsId, instCode=cern.code, mainProj='COMP', isAuthor=False, isSuspended=False,
                                status='CMS', category=7, dueApplicant=0, dueAuthor=3, yearFraction=1., year=selYear,
                                timestamp=datetime( selYear, 1, 1, 0, 0, 0 ) ) )

    logging.info( 'Users and Institutes set up' )

    return ap, p1m1, p1m2, p2m1, a1m1, a2m1, a3m1, a4m1, t1m1, u1, u2, u3, obs1, ethLdr, cernLdr, fnalLdr, egPM, a4mgtEgMem, applicant

def setupPAT( ap, p1m1, p1m2, p2m1, a1m1, a2m1, a3m1, a4m1, t1m1, t1o1, ethLdr, cernLdr, fnalLdr, egPM, a4mgtEgMem, applicant) :

    # add projects to db so we can find them when creating the activities
    p1 = Project(name='pr1', desc='project 1', code='p1', year=selYear)
    p2 = Project(name='pr2', desc='project 2', code='p2', year=selYear)
    p3 = Project(name='pr3', desc='project 3', code='p3', year=selYear)
    p4 = Project(name='pr1', desc='project 1 last year', code='p1a', year=selYear-1)
    p5 = Project(name='pr5', desc='project 5', code='p5', year=selYear)
    addAndCommit([p1, p2, p3, p4, p5])

    a1 = CMSActivity( name='ac1', desc='activity 1 (p1)', proj=p1, code='a1', year=selYear)
    a2 = CMSActivity( name='ac2', desc='activity 2 (p2)', proj=p2, code='a2', year=selYear)
    a3 = CMSActivity( name='ac3', desc='activity 3 (p2)', proj=p2, code='a3', year=selYear)
    a4 = CMSActivity( name='ac4', desc='activity 4 (p2)', proj=p2, code='a4', year=selYear)
    ad0 = CMSActivity( name='adel0', desc='activity to delete-0 (p1)', proj=p1, code='ad0', year=selYear)
    ad1 = CMSActivity( name='adel1', desc='activity to delete-1 (p1)', proj=p1, code='ad1', year=selYear)
    a4a = CMSActivity( name='ac4a', desc='activity 4a (p1)', proj=p1, code='a4a', year=selYear-1)
    a5  = CMSActivity( name='ac5', desc='activity 5 (p5)', proj=p5, code='a5', year=selYear)
    addAndCommit([a1, a2, a3, a4, a4a, ad0, ad1, a5])

    # set up managers
    m = [ Manager( itemType='project', itemCode='p1', userId=p1m1.id, role=Permission.MANAGEPROJ, year=selYear ),
          Manager( itemType='project', itemCode='p1', userId=p1m2.id, role=Permission.MANAGEPROJ, year=selYear ),
          Manager( itemType='project', itemCode='p2', userId=p2m1.id, role=Permission.MANAGEPROJ, year=selYear ),

          Manager( itemType='project', itemCode='p2', userId=  ap.id, role=Permission.MANAGEPROJ, year=selYear, egroup='p2MgrEgroup'.lower() ),
          Manager( itemType='project', itemCode='p3', userId=  ap.id, role=Permission.MANAGEPROJ, year=selYear ),

          # p1m1 can also manage the project in the last year
          Manager( itemType='project', itemCode='p1', userId=p1m1.id, role=Permission.MANAGEPROJ, year=selYear-1 ),
          # ... and proj 5
          Manager( itemType='project', itemCode='p5', userId=p1m1.id, role=Permission.MANAGEPROJ, year=selYear ),

          Manager( itemType='activity', itemCode='a1', userId=a1m1.id, role=Permission.MANAGEACT, year=selYear ),
          Manager( itemType='activity', itemCode='a2', userId=a2m1.id, role=Permission.MANAGEACT, year=selYear ),
          Manager( itemType='activity', itemCode='a3', userId=a3m1.id, role=Permission.MANAGEACT, year=selYear ),
          Manager( itemType='activity', itemCode='a4', userId=a3m1.id, role=Permission.MANAGEACT, year=selYear ),
          Manager( itemType='activity', itemCode='a4a', userId=a4m1.id, role=Permission.MANAGEACT, year=selYear-1 ),

          Manager( itemType='activity', itemCode='a4', userId=p1m2.id, role=Permission.MANAGEACT, year=selYear ), # allow this PM also to manage that activity

          Manager( itemType='institute', itemCode='ETHZ', userId=ethLdr.id, role=Permission.MANAGEINST, year=selYear ),
          Manager( itemType='institute', itemCode='CERN', userId=cernLdr.id, role=Permission.MANAGEINST, year=selYear ),
          Manager( itemType='institute', itemCode='FNAL', userId=fnalLdr.id, role=Permission.MANAGEINST, year=selYear ),
          Manager( itemType='institute', itemCode='DESY', userId=cernLdr.id, role=Permission.MANAGEINST, year=selYear, egroup='desyInstMgrEgroup'.lower() ),

          Manager( itemType='activity', itemCode='a3', userId=ap.id, role=Permission.MANAGEACT, year=selYear, egroup='a4MgrEgroup'.lower() ),

          ]
    addAndCommit( m )

    # set up observers
    o = [ Manager( itemType='task', itemCode='t1', userId=t1o1.id, role=Permission.OBSERVE, year=selYear ),
          Manager( itemType='task', itemCode='t2', userId=t1o1.id, role=Permission.OBSERVE, year=selYear, egroup='t1ObserverEgroup'.lower() ),
        ]
    addAndCommit( o )

    # set up level3names for tasks
    lvl3List = [ Level3Name('Unknown', startYear=2000, comment='default for older tasks'),
                 Level3Name('lvl3-one', startYear=selYear),
                 Level3Name('qtLvl3', startYear=selYear)
               ]
    addAndCommit( lvl3List )

    # set up tasks - add new ones only to the end as some tests use hard-coded ids
    tList = [ Task(name='ta1', code='t1', desc='task 1 (a1 p1)', act=a1, needs=12, pctAtCERN=10, tType=getTaskTypes()[0], level3=1, comment='', year=selYear),
              Task(name='ta2', code='t2', desc='task 2 (a1 p1)', act=a1, needs= 9, pctAtCERN=10, tType=getTaskTypes()[1], level3=2, comment='', year=selYear),
              Task(name='ta3', code='t3', desc='task 3 (a2 p2)', act=a2, needs= 4, pctAtCERN=10, tType=getTaskTypes()[1], comment='to finish before Jun16', year=selYear),
              Task(name='ta4', code='t4', desc='task 4 (a3 p2)', act=a3, needs= 8, pctAtCERN=10, tType=getTaskTypes()[1], comment='', year=selYear),
              Task( name='td0', code='del0', desc='task to delete   (OK)   (a3 p2)', act=a3, needs=0, pctAtCERN=10, tType=getTaskTypes( )[ 1 ], comment='', year=selYear ),
              Task( name='td1', code='del1', desc='task to delete (fail-1) (a3 p2)', act=a3, needs=8, pctAtCERN=10, tType=getTaskTypes( )[ 1 ], comment='', year=selYear ),
              Task( name='td2', code='del2', desc='task to delete (fail-2) (a3 p2)', act=a3, needs=0, pctAtCERN=10, tType=getTaskTypes( )[ 1 ], comment='', year=selYear ),
              Task( name='tda0', code='tda0', desc='task to delete (OK) (ad0 p1)', act=ad0, needs=0, pctAtCERN=10, tType=getTaskTypes( )[ 1 ], comment='', year=selYear ),
              Task( name='ta4a', code='t4a', desc='task 4a (a4a p1)', act=a4a, needs= 8, pctAtCERN=10, tType=getTaskTypes()[1], comment='', year=selYear-1 ),
              Task(name='ta5', code='t5', desc='task 4 (a3 p2)', act=a3, needs= 8, pctAtCERN=10, tType=getTaskTypes()[1], comment='', year=selYear),
              Task(name='ta6l', code='t6l', desc='locked task 6 (a3 p2)', act=a3, needs= 1, pctAtCERN=42, tType=getTaskTypes()[1], comment='', year=selYear, locked=True),
              Task(name='ta5p5', code='ta5p5', desc='standard task (a5 p5)', act=a5, needs= 20, pctAtCERN=0, tType=getTaskTypes()[1], comment='', year=selYear),
              Task(name='qta5p5', code='qta5p5', desc='qualification task (a5 p5)', act=a5, needs= 1, pctAtCERN=0, tType=getTaskTypes()[3], comment='', year=selYear),
              ]
    addAndCommit( tList )

    # set up manager for a task
    tm0 = Manager( itemType='task', itemCode='t1', userId=t1m1.id, role=Permission.MANAGETASK, year=selYear )
    addAndCommit( tm0 )

    # add a shift-task
    tShift = Task( name='dbShift', code='ts1', desc='DB expert shifts', act=a3, needs=12, pctAtCERN=100,
                   tType=getTaskTypes()[0], comment='', shiftTypeId=getTaskShiftTypeIds()[ 1 ], year=selYear )
    addAndCommit( tShift )

    logging.info( 'Projects, Activities and Tasks set up' )

    return tList

def setupPledges( tList , u1, u2, u3 ):
    # set up pledges (code is: taskId+userId+instId)
    pList = [ Pledge(taskId=tList[0].id, userId=u1.id, instId=u1.mainInst, workTimePld=1.1, workTimeAcc=0, workTimeDone=0, status='new', year=selYear, workedSoFar=0),
              Pledge(taskId=tList[1].id, userId=u1.id, instId=u1.mainInst, workTimePld=1.2, workTimeAcc=0, workTimeDone=0, status='new', year=selYear, workedSoFar=0),
              Pledge(taskId=tList[2].id, userId=u1.id, instId=u1.mainInst, workTimePld=1.3, workTimeAcc=0, workTimeDone=0, status='new', year=selYear, workedSoFar=0),

              Pledge(taskId=tList[0].id, userId=u2.id, instId=u2.mainInst, workTimePld=1.4, workTimeAcc=0, workTimeDone=0, status='new', year=selYear, workedSoFar=0),
              Pledge(taskId=tList[1].id, userId=u2.id, instId=u2.mainInst, workTimePld=1.5, workTimeAcc=0, workTimeDone=0, status='new', year=selYear, workedSoFar=0),

              Pledge(taskId=tList[2].id, userId=u3.id, instId=u3.mainInst, workTimePld=1.6, workTimeAcc=0, workTimeDone=0, status='new', year=selYear, workedSoFar=0),
              Pledge(taskId=tList[3].id, userId=u3.id, instId=u3.mainInst, workTimePld=1.7, workTimeAcc=0, workTimeDone=0, status='new', year=selYear, workedSoFar=0),

              # create a pledge for a task-to-delete to make sure we can't delete it
              Pledge( taskId=tList[ 6 ].id, userId=u3.id, instId=u3.mainInst, workTimePld=1, workTimeAcc=0, workTimeDone=0, status='new', year=selYear, workedSoFar=0 ),

              # create a guest pledge
              Pledge( taskId=tList[ 3 ].id, userId=u3.id, instId=u2.mainInst, workTimePld=1., workTimeAcc=0, workTimeDone=0, status='new', year=selYear, workedSoFar=0, isGuest=True ),

              # another pledge to check the summary infos ...
              Pledge(taskId=tList[0].id, userId=u3.id, instId=u3.mainInst, workTimePld=0.4, workTimeAcc=0.4, workTimeDone=0.4, status='done', year=selYear, workedSoFar=0),
              ]

    addAndCommit( pList )

    logging.info( ' %s pledges set up' % len(pList) )

    print( '' )

def setupShifts( ) :

    # add a few shifts
    sList = []
    flavList = ['main', 'main 2nd', 'trainee', 'main'] # give "main" a slightly higher weight ...
    subSysList = ['pr1', 'pr2', 'pr3']

    sTypeList = [ 'sType0', 'sType1', 'sType2', 'sType3', 'sType4' ]
    hrIdList = [ 422405, # pfeiffer
                 99000, 99001, 99002, 99003, # standard users
                 98001, 98011, 98021, # P,A,T managers
                 ]
    # set some defaults
    (hrId, subSys, sType, sTypeId, flavourName, weight, year) = (422405, 'subSys1', 'sType1', 1, 'flav1', 1.0, selYear)

    # create one shift for each day in Jan of selYear, so that they show all up when running test early in the year
    # selecting random values from the lists above:
    for sId in range(0,30): # make sure we have a valid day for the month of shifts (May) ...
        sStart, sEnd = ( datetime(selYear, 1, sId+1, 0,0,0), datetime(selYear, 1, sId+1, 8,0,0) )
        # choose from the lists above
        sTypeIndex = sId % len(sTypeList)
        sList.append( Shift (sId,
                             hrIdList[ sId % len(hrIdList) ],
                             subSysList[ sId % len(subSysList) ],
                             sTypeList[sTypeIndex], sTypeIndex,
                             sStart, sEnd,
                             flavList[ sId % len(flavList) ],
                             weight, year) )
    for s in sList:
        db.session.add( s )

    # add two more shifts (in early Feb, see above) so we check better on mappings:
    # make sure the date here is before you actually _run_ the tests,
    # otherwise (e.g. before Feb 1st in this case) you'll miss these shifts
    sStart, sEnd = (datetime( selYear, 2, 1, 0, 0, 0 ), datetime( selYear, 2, 1, 8, 0, 0 ))
    s0 = Shift (31, 422405, 'pr2', 'sType1', 1, sStart, sEnd, 'main', 1., selYear)
    s1 = Shift (32,  99000, 'pr2', 'sType1', 1, sStart, sEnd, 'main', 1., selYear)
    db.session.add( s0 )
    db.session.add( s1 )

    # now add two shift-task-map, one for the shifts of Pfeiffer, Andreas one for Shifter, Tester:
    stm = ShiftTaskMap(subSys='pr2', sType='sType1', sTypeId=1, flavourName='main', taskCode='ts1', year=selYear)
    db.session.add( stm )
    stm = ShiftTaskMap(subSys='pr1', sType='sType0', sTypeId=1, flavourName='main', taskCode='ts1', year=selYear)
    db.session.add( stm )

    # add a shifts for the shifter to test early in the year:
    sStart, sEnd = (datetime( selYear, 1, 2, 0, 0, 0 ), datetime( selYear, 1, 2, 8, 0, 0 ))
    st = Shift (33, 42043, 'pr1', 'sType0', 0, sStart, sEnd, 'main', 1., selYear) # 42043 is the tester-shifter
    db.session.add( st )

    # add a shifts for the shifter to test:
    sStart, sEnd = (datetime( selYear, 1, 3, 0, 0, 0 ), datetime( selYear, 1, 3, 8, 0, 0 ))
    st = Shift (34, 42043, 'pr1', 'sType0', 0, sStart, sEnd, 'main', 1., selYear) # 42043 is the tester-shifter
    db.session.add( st )

    db.session.commit( )

    # make sure this is filled as well ...
    updateAllInstView( selYear )

    logging.info( 'Shifts set up - %d shifts and 2 shift-task-maps created ' % len(sList) )

def setup(pledges=True):
    resetDB()
    (ap, p1m1, p1m2, p2m1, a1m1, a2m1, a3m1, a4m1, t1m1, u1, u2, u3, t1o1, ethLdr, cernLdr, fnalLdr, egPM, a4MgtEgMem, applicant) = setupUserInst( )
    tList = setupPAT( ap, p1m1, p1m2, p2m1, a1m1, a2m1, a3m1, a4m1, t1m1, t1o1, ethLdr, cernLdr, fnalLdr, egPM, a4MgtEgMem, applicant )
    if pledges: setupPledges(tList, u1, u2, u3 )
    setupShifts()

    prepareFor( fromYear=selYear, toYear=selYear+1, verbose=False )

    # also prepare some tasks for some time in the future
    prepareFor( fromYear=selYear, toYear=selYear+3, verbose=False )

    # create a project which only exists in "yesteryear"
    p1Old= Project(name='pr1old', desc='project 1 old', code='p1old', year=selYear-1)
    # have this proj also in this year
    p1a = Project(name='pr1a', desc='project 1a', code='p1a0', year=selYear)
    addAndCommit( [p1Old, p1a] )

    # make sure this is filled as well ...
    updateAllInstView( selYear )



if __name__ == '__main__':

    from app import create_app
    theApp = create_app('testing')
    with theApp.app_context():
        print( "using db from: ", db.engine.url )
        setup()
