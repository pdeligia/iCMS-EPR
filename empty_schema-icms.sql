--
-- PostgreSQL database dump
--

-- Dumped from database version 12.3
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: cadi; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA cadi;


--
-- Name: epr; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA epr;


--
-- Name: toolkit; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA toolkit;


--
-- Name: mem_role_types; Type: TYPE; Schema: cadi; Owner: -
--

CREATE TYPE cadi.mem_role_types AS ENUM (
    'Member',
    'Chair',
    'LE'
);


--
-- Name: rev_com_types; Type: TYPE; Schema: cadi; Owner: -
--

CREATE TYPE cadi.rev_com_types AS ENUM (
    'ARC',
    'IRC'
);


--
-- Name: accepted_enum; Type: TYPE; Schema: epr; Owner: -
--

CREATE TYPE epr.accepted_enum AS ENUM (
    'NO',
    'PENDING',
    'YES',
    'REJECTED',
    'PARTIAL'
);


--
-- Name: app_names; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.app_names AS ENUM (
    'epr',
    'tools',
    'notes',
    'common',
    'cadi'
);


--
-- Name: app_names_enum; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.app_names_enum AS ENUM (
    'epr',
    'tools',
    'notes',
    'common',
    'cadi'
);


--
-- Name: career_event_type_enum; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.career_event_type_enum AS ENUM (
    'phd start',
    'phd completion',
    'cms arrival',
    'cms departure'
);


--
-- Name: email_issue; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.email_issue AS ENUM (
    'DONOTSEND',
    'NOEMAIL',
    'BADEMAIL'
);


--
-- Name: epr_aggregate_units_enum; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.epr_aggregate_units_enum AS ENUM (
    'PLEDGES DONE',
    'SHIFTS DONE',
    'INST PLEDGES DONE'
);


--
-- Name: gender; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.gender AS ENUM (
    'F',
    'M'
);


--
-- Name: gender_enum; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.gender_enum AS ENUM (
    'FEMALE',
    'MALE'
);


--
-- Name: history_event_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.history_event_type AS ENUM (
    'PERSONAL',
    'INSTITUTIONAL'
);


--
-- Name: inst_status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.inst_status AS ENUM (
    'Yes',
    'No',
    'ForReference',
    'Associated',
    'Leaving',
    'Cooperating'
);


--
-- Name: inst_status_enum; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.inst_status_enum AS ENUM (
    'Yes',
    'ForReference',
    'No',
    'Cooperating',
    'Associated',
    'Leaving'
);


--
-- Name: mail_where; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.mail_where AS ENUM (
    'CERN',
    'institute'
);


--
-- Name: member_status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.member_status AS ENUM (
    'CMS',
    'EXMEMBER',
    'NOSHOW',
    'NOTCMS',
    'NOTNAME',
    'RELATED',
    'CMSEXTENDED',
    'CMSEMERITUS',
    'CMSVO',
    'CMSASSOC',
    'DECEASED',
    'CMSAFFILIATE'
);


--
-- Name: mo_status_enum; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.mo_status_enum AS ENUM (
    'APPROVED_FREE_GENERIC',
    'APPROVED_FREE_EXTENDED_RIGHTS',
    'PROPOSED',
    'APPROVED_SWAPPED_IN',
    'APPROVED_FREE_LATECOMER',
    'REJECTED',
    'SWAPPED_OUT',
    'CANCELLED',
    'APPROVED_LATE',
    'APPROVED_FREE_NEW_INST',
    'APPROVED',
    'APPROVED_FREE_TOTEM',
    'APPROVED_FREE_PHD_GRAD'
);


--
-- Name: person_activity_enum; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.person_activity_enum AS ENUM (
    'Administrative',
    'Doctoral Student',
    'Engineer',
    'Engineer Electronics',
    'Engineer Mechanical',
    'Engineer Software',
    'Non-Doctoral Student',
    'Other',
    'Physicist',
    'Technician',
    'Theoretical Physicist'
);


--
-- Name: person_status_enum; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.person_status_enum AS ENUM (
    'CMS',
    'EXMEMBER',
    'NOSHOW',
    'NOTCMS',
    'NOTNAME',
    'RELATED',
    'CMSEXTENDED',
    'CMSEMERITUS',
    'CMSVO',
    'CMSASSOC',
    'DECEASED',
    'CMSAFFILIATE'
);


--
-- Name: person_title; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.person_title AS ENUM (
    'Ms.',
    'Mr.',
    'Dr.',
    'Prof.',
    'Eng.'
);


--
-- Name: photo_public; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.photo_public AS ENUM (
    'never',
    'public',
    'restricted'
);


--
-- Name: request_status_enum; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.request_status_enum AS ENUM (
    'FAILED_TO_EXECUTE',
    'APPROVED_SCHEDULED',
    'EXECUTED',
    'READY_FOR_EXECUTION',
    'PENDING_APPROVAL',
    'REJECTED'
);


--
-- Name: request_step_status_enum; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.request_step_status_enum AS ENUM (
    'PENDING',
    'APPROVED',
    'REJECTED'
);


--
-- Name: yesno; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.yesno AS ENUM (
    'YES',
    'NO'
);


--
-- Name: restricted_action_type_enum; Type: TYPE; Schema: toolkit; Owner: -
--

CREATE TYPE toolkit.restricted_action_type_enum AS ENUM (
    'read',
    'write',
    'delete'
);


--
-- Name: restricted_resource_type_enum; Type: TYPE; Schema: toolkit; Owner: -
--

CREATE TYPE toolkit.restricted_resource_type_enum AS ENUM (
    'file',
    'database row',
    'database table',
    'endpoint'
);


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: review_committee; Type: TABLE; Schema: cadi; Owner: -
--

CREATE TABLE cadi.review_committee (
    id integer NOT NULL,
    rc_type cadi.rev_com_types,
    members_needed integer NOT NULL,
    cadi_line character varying(100) NOT NULL,
    creator_cms_id integer NOT NULL,
    comment character varying(1000),
    status character varying(100),
    start timestamp without time zone,
    "end" timestamp without time zone,
    last_update timestamp without time zone,
    updater_cms_id integer NOT NULL,
    annex json
);


--
-- Name: review_committee_id_seq; Type: SEQUENCE; Schema: cadi; Owner: -
--

CREATE SEQUENCE cadi.review_committee_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: review_committee_id_seq; Type: SEQUENCE OWNED BY; Schema: cadi; Owner: -
--

ALTER SEQUENCE cadi.review_committee_id_seq OWNED BY cadi.review_committee.id;


--
-- Name: review_committee_member; Type: TABLE; Schema: cadi; Owner: -
--

CREATE TABLE cadi.review_committee_member (
    id integer NOT NULL,
    rc_id integer,
    cms_id integer,
    inst_code character varying,
    role cadi.mem_role_types,
    status character varying(128) NOT NULL,
    comment character varying(1000),
    start timestamp without time zone,
    "end" timestamp without time zone,
    last_update timestamp without time zone
);


--
-- Name: review_committee_member_id_seq; Type: SEQUENCE; Schema: cadi; Owner: -
--

CREATE SEQUENCE cadi.review_committee_member_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: review_committee_member_id_seq; Type: SEQUENCE OWNED BY; Schema: cadi; Owner: -
--

ALTER SEQUENCE cadi.review_committee_member_id_seq OWNED BY cadi.review_committee_member.id;


--
-- Name: activity; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.activity (
    id bigint NOT NULL,
    verb character varying(255),
    transaction_id bigint NOT NULL,
    data json,
    object_type character varying(255),
    object_id bigint,
    object_tx_id bigint,
    target_type character varying(255),
    target_id bigint,
    target_tx_id bigint
);


--
-- Name: activity_base_id_seq; Type: SEQUENCE; Schema: epr; Owner: -
--

CREATE SEQUENCE epr.activity_base_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: activity_base_id_seq; Type: SEQUENCE OWNED BY; Schema: epr; Owner: -
--

ALTER SEQUENCE epr.activity_base_id_seq OWNED BY epr.activity.id;


--
-- Name: alembic_version; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.alembic_version (
    version_num character varying(32) NOT NULL
);


--
-- Name: categories; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.categories (
    id integer NOT NULL,
    name character varying(80),
    author_ok boolean,
    "timestamp" timestamp without time zone
);


--
-- Name: ceilings; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.ceilings (
    id integer NOT NULL,
    code character varying(80),
    year integer,
    ceiling double precision,
    "timestamp" timestamp without time zone
);


--
-- Name: ceilings_id_seq; Type: SEQUENCE; Schema: epr; Owner: -
--

CREATE SEQUENCE epr.ceilings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ceilings_id_seq; Type: SEQUENCE OWNED BY; Schema: epr; Owner: -
--

ALTER SEQUENCE epr.ceilings_id_seq OWNED BY epr.ceilings.id;


--
-- Name: ceilings_version; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.ceilings_version (
    id integer NOT NULL,
    code character varying(80),
    year integer,
    ceiling double precision,
    "timestamp" timestamp without time zone,
    transaction_id bigint NOT NULL,
    end_transaction_id bigint,
    operation_type smallint NOT NULL,
    code_mod boolean DEFAULT false NOT NULL,
    year_mod boolean DEFAULT false NOT NULL,
    ceiling_mod boolean DEFAULT false NOT NULL,
    timestamp_mod boolean DEFAULT false NOT NULL
);


--
-- Name: cms_activities; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.cms_activities (
    id integer NOT NULL,
    code character varying(80),
    year integer,
    name character varying(200),
    description character varying(1000),
    project_id integer,
    "timestamp" timestamp without time zone,
    status character varying(20) NOT NULL
);


--
-- Name: cms_activities_version; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.cms_activities_version (
    id integer NOT NULL,
    code character varying(80),
    year integer,
    name character varying(200),
    description character varying(1000),
    project_id integer,
    "timestamp" timestamp without time zone,
    transaction_id bigint NOT NULL,
    end_transaction_id bigint,
    operation_type smallint NOT NULL,
    code_mod boolean DEFAULT false NOT NULL,
    year_mod boolean DEFAULT false NOT NULL,
    name_mod boolean DEFAULT false NOT NULL,
    description_mod boolean DEFAULT false NOT NULL,
    project_id_mod boolean DEFAULT false NOT NULL,
    timestamp_mod boolean DEFAULT false NOT NULL,
    status character varying(20),
    status_mod boolean NOT NULL
);


--
-- Name: epr_due; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.epr_due (
    id integer NOT NULL,
    user_id integer,
    inst_id integer,
    work_due double precision,
    year integer,
    "timestamp" timestamp without time zone
);


--
-- Name: id_seq_act; Type: SEQUENCE; Schema: epr; Owner: -
--

CREATE SEQUENCE epr.id_seq_act
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: id_seq_act; Type: SEQUENCE OWNED BY; Schema: epr; Owner: -
--

ALTER SEQUENCE epr.id_seq_act OWNED BY epr.cms_activities.id;


--
-- Name: v_all_inst; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.v_all_inst (
    id integer NOT NULL,
    code character varying(100),
    name character varying(200),
    expected double precision,
    pledged double precision,
    pledged_fract double precision,
    accepted double precision,
    done double precision,
    done_fract double precision,
    authors double precision,
    phys_users double precision,
    sum_epr_due double precision,
    shifts_pld double precision,
    epr_pledged double precision,
    epr_pld_fract double precision,
    shifts_done double precision,
    epr_accounted double precision,
    epr_acc_fract double precision,
    year integer,
    "timestamp" timestamp without time zone
);


--
-- Name: id_seq_allinstview; Type: SEQUENCE; Schema: epr; Owner: -
--

CREATE SEQUENCE epr.id_seq_allinstview
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: id_seq_allinstview; Type: SEQUENCE OWNED BY; Schema: epr; Owner: -
--

ALTER SEQUENCE epr.id_seq_allinstview OWNED BY epr.v_all_inst.id;


--
-- Name: id_seq_cats; Type: SEQUENCE; Schema: epr; Owner: -
--

CREATE SEQUENCE epr.id_seq_cats
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: id_seq_cats; Type: SEQUENCE OWNED BY; Schema: epr; Owner: -
--

ALTER SEQUENCE epr.id_seq_cats OWNED BY epr.categories.id;


--
-- Name: id_seq_ceilings; Type: SEQUENCE; Schema: epr; Owner: -
--

CREATE SEQUENCE epr.id_seq_ceilings
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: id_seq_eprdue; Type: SEQUENCE; Schema: epr; Owner: -
--

CREATE SEQUENCE epr.id_seq_eprdue
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: id_seq_eprdue; Type: SEQUENCE OWNED BY; Schema: epr; Owner: -
--

ALTER SEQUENCE epr.id_seq_eprdue OWNED BY epr.epr_due.id;


--
-- Name: insts; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.insts (
    id integer NOT NULL,
    code character varying(80),
    cernid integer,
    name character varying(200),
    country character varying(150),
    cms_status character varying(40) NOT NULL,
    comment character varying(1000),
    year integer,
    "timestamp" timestamp without time zone
);


--
-- Name: id_seq_inst; Type: SEQUENCE; Schema: epr; Owner: -
--

CREATE SEQUENCE epr.id_seq_inst
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: id_seq_inst; Type: SEQUENCE OWNED BY; Schema: epr; Owner: -
--

ALTER SEQUENCE epr.id_seq_inst OWNED BY epr.insts.id;


--
-- Name: managers; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.managers (
    id integer NOT NULL,
    code character varying(80),
    item_type character varying(80),
    item_code character varying(80),
    userid integer,
    role integer,
    year integer,
    comment character varying(1000),
    status character varying(20),
    "timestamp" timestamp without time zone,
    egroup character varying(80)
);


--
-- Name: id_seq_mgr; Type: SEQUENCE; Schema: epr; Owner: -
--

CREATE SEQUENCE epr.id_seq_mgr
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: id_seq_mgr; Type: SEQUENCE OWNED BY; Schema: epr; Owner: -
--

ALTER SEQUENCE epr.id_seq_mgr OWNED BY epr.managers.id;


--
-- Name: pledges; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.pledges (
    id integer NOT NULL,
    code character varying(80),
    user_id integer,
    inst_id integer,
    task_id integer,
    work_pld double precision,
    work_acc double precision,
    work_done double precision,
    work_so_far double precision,
    status character varying(20),
    year integer,
    "timestamp" timestamp without time zone,
    is_guest boolean NOT NULL
);


--
-- Name: id_seq_pledge; Type: SEQUENCE; Schema: epr; Owner: -
--

CREATE SEQUENCE epr.id_seq_pledge
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: id_seq_pledge; Type: SEQUENCE OWNED BY; Schema: epr; Owner: -
--

ALTER SEQUENCE epr.id_seq_pledge OWNED BY epr.pledges.id;


--
-- Name: projects; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.projects (
    id integer NOT NULL,
    code character varying(80),
    year integer,
    name character varying(80),
    description character varying(1000),
    max_task_level integer,
    "timestamp" timestamp without time zone,
    status character varying(20) NOT NULL
);


--
-- Name: id_seq_proj; Type: SEQUENCE; Schema: epr; Owner: -
--

CREATE SEQUENCE epr.id_seq_proj
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: id_seq_proj; Type: SEQUENCE OWNED BY; Schema: epr; Owner: -
--

ALTER SEQUENCE epr.id_seq_proj OWNED BY epr.projects.id;


--
-- Name: roles; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.roles (
    id integer NOT NULL,
    name character varying(64),
    "default" boolean,
    permissions integer
);


--
-- Name: id_seq_role; Type: SEQUENCE; Schema: epr; Owner: -
--

CREATE SEQUENCE epr.id_seq_role
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: id_seq_role; Type: SEQUENCE OWNED BY; Schema: epr; Owner: -
--

ALTER SEQUENCE epr.id_seq_role OWNED BY epr.roles.id;


--
-- Name: shifts; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.shifts (
    id integer NOT NULL,
    shift_id integer,
    shifter_id integer,
    sub_system character varying(100),
    shift_type character varying(100),
    shift_type_id integer,
    shift_start timestamp without time zone,
    shift_end timestamp without time zone,
    flavour_name character varying(20),
    weight double precision,
    year integer,
    "timestamp" timestamp without time zone
);


--
-- Name: id_seq_shifts; Type: SEQUENCE; Schema: epr; Owner: -
--

CREATE SEQUENCE epr.id_seq_shifts
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: id_seq_shifts; Type: SEQUENCE OWNED BY; Schema: epr; Owner: -
--

ALTER SEQUENCE epr.id_seq_shifts OWNED BY epr.shifts.id;


--
-- Name: shift_task_map; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.shift_task_map (
    id integer NOT NULL,
    sub_system character varying(100),
    shift_type character varying(100),
    shift_type_id integer,
    flavour_name character varying(20),
    task_code character varying(80),
    status character varying(100),
    weight double precision,
    year integer,
    "timestamp" timestamp without time zone,
    added_value_cred double precision DEFAULT 0
);


--
-- Name: id_seq_stm; Type: SEQUENCE; Schema: epr; Owner: -
--

CREATE SEQUENCE epr.id_seq_stm
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: id_seq_stm; Type: SEQUENCE OWNED BY; Schema: epr; Owner: -
--

ALTER SEQUENCE epr.id_seq_stm OWNED BY epr.shift_task_map.id;


--
-- Name: tasks; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.tasks (
    id integer NOT NULL,
    code character varying(80),
    year integer,
    name character varying(200),
    description character varying(2000),
    activity_id integer,
    needed_work double precision,
    pct_at_cern integer,
    t_type character varying(80),
    shift_type_id character varying(80),
    comment character varying(2000),
    earliest_start character varying(80),
    latest_end character varying(80),
    level integer,
    parent character varying(80),
    "timestamp" timestamp without time zone,
    status character varying(20) NOT NULL,
    kind character varying(20) NOT NULL,
    locked boolean NOT NULL,
    level3_id integer
);


--
-- Name: id_seq_task; Type: SEQUENCE; Schema: epr; Owner: -
--

CREATE SEQUENCE epr.id_seq_task
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: id_seq_task; Type: SEQUENCE OWNED BY; Schema: epr; Owner: -
--

ALTER SEQUENCE epr.id_seq_task OWNED BY epr.tasks.id;


--
-- Name: time_line_inst; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.time_line_inst (
    id integer NOT NULL,
    inst_code character varying(80),
    year integer,
    cms_status character varying(40) NOT NULL,
    comment character varying(1000),
    "timestamp" timestamp without time zone
);


--
-- Name: id_seq_timlininst; Type: SEQUENCE; Schema: epr; Owner: -
--

CREATE SEQUENCE epr.id_seq_timlininst
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: id_seq_timlininst; Type: SEQUENCE OWNED BY; Schema: epr; Owner: -
--

ALTER SEQUENCE epr.id_seq_timlininst OWNED BY epr.time_line_inst.id;


--
-- Name: time_line_user; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.time_line_user (
    id integer NOT NULL,
    user_cms_id integer,
    inst_code character varying(80),
    year integer,
    main_project character varying(80),
    is_author boolean,
    is_suspended boolean,
    status character varying(40),
    category integer,
    due_applicant double precision,
    due_author double precision,
    year_fraction double precision,
    comment character varying(1000),
    "timestamp" timestamp without time zone
);


--
-- Name: id_seq_timlinuser; Type: SEQUENCE; Schema: epr; Owner: -
--

CREATE SEQUENCE epr.id_seq_timlinuser
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: id_seq_timlinuser; Type: SEQUENCE OWNED BY; Schema: epr; Owner: -
--

ALTER SEQUENCE epr.id_seq_timlinuser OWNED BY epr.time_line_user.id;


--
-- Name: users; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.users (
    id integer NOT NULL,
    username character varying(80),
    name character varying(200),
    hrid integer,
    cmsid integer,
    main_inst integer,
    role_id integer,
    auth_req boolean,
    is_suspended boolean,
    status character varying(40),
    category integer,
    year integer,
    comment character varying(1000),
    code character varying(80),
    "timestamp" timestamp without time zone,
    creation_time timestamp without time zone
);


--
-- Name: id_seq_user; Type: SEQUENCE; Schema: epr; Owner: -
--

CREATE SEQUENCE epr.id_seq_user
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: id_seq_user; Type: SEQUENCE OWNED BY; Schema: epr; Owner: -
--

ALTER SEQUENCE epr.id_seq_user OWNED BY epr.users.id;


--
-- Name: userinst; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.userinst (
    id integer NOT NULL,
    user_id integer,
    inst_id integer
);


--
-- Name: id_seq_userinst; Type: SEQUENCE; Schema: epr; Owner: -
--

CREATE SEQUENCE epr.id_seq_userinst
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: id_seq_userinst; Type: SEQUENCE OWNED BY; Schema: epr; Owner: -
--

ALTER SEQUENCE epr.id_seq_userinst OWNED BY epr.userinst.id;


--
-- Name: insts_version; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.insts_version (
    id integer NOT NULL,
    code character varying(80),
    cernid integer,
    name character varying(200),
    country character varying(150),
    cms_status character varying(40),
    comment character varying(1000),
    year integer,
    "timestamp" timestamp without time zone,
    transaction_id bigint NOT NULL,
    end_transaction_id bigint,
    operation_type smallint NOT NULL,
    code_mod boolean DEFAULT false NOT NULL,
    cernid_mod boolean DEFAULT false NOT NULL,
    name_mod boolean DEFAULT false NOT NULL,
    country_mod boolean DEFAULT false NOT NULL,
    cms_status_mod boolean DEFAULT false NOT NULL,
    comment_mod boolean DEFAULT false NOT NULL,
    year_mod boolean DEFAULT false NOT NULL,
    timestamp_mod boolean DEFAULT false NOT NULL
);


--
-- Name: level3names; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.level3names (
    id integer NOT NULL,
    name character varying(80),
    "startYear" integer NOT NULL,
    "endYear" integer NOT NULL,
    comment character varying(1000),
    "timestamp" timestamp without time zone
);


--
-- Name: level3names_id_seq; Type: SEQUENCE; Schema: epr; Owner: -
--

CREATE SEQUENCE epr.level3names_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: level3names_id_seq; Type: SEQUENCE OWNED BY; Schema: epr; Owner: -
--

ALTER SEQUENCE epr.level3names_id_seq OWNED BY epr.level3names.id;


--
-- Name: managers_version; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.managers_version (
    id integer NOT NULL,
    code character varying(80),
    item_type character varying(80),
    item_code character varying(80),
    userid integer,
    role integer,
    year integer,
    comment character varying(1000),
    status character varying(20),
    "timestamp" timestamp without time zone,
    transaction_id bigint NOT NULL,
    end_transaction_id bigint,
    operation_type smallint NOT NULL,
    code_mod boolean DEFAULT false NOT NULL,
    item_type_mod boolean DEFAULT false NOT NULL,
    item_code_mod boolean DEFAULT false NOT NULL,
    userid_mod boolean DEFAULT false NOT NULL,
    role_mod boolean DEFAULT false NOT NULL,
    year_mod boolean DEFAULT false NOT NULL,
    comment_mod boolean DEFAULT false NOT NULL,
    status_mod boolean DEFAULT false NOT NULL,
    timestamp_mod boolean DEFAULT false NOT NULL,
    egroup character varying(80),
    egroup_mod boolean NOT NULL
);


--
-- Name: pledges_version; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.pledges_version (
    id integer NOT NULL,
    code character varying(80),
    user_id integer,
    inst_id integer,
    task_id integer,
    work_pld double precision,
    work_acc double precision,
    work_done double precision,
    work_so_far double precision,
    status character varying(20),
    year integer,
    "timestamp" timestamp without time zone,
    transaction_id bigint NOT NULL,
    end_transaction_id bigint,
    operation_type smallint NOT NULL,
    code_mod boolean DEFAULT false NOT NULL,
    user_id_mod boolean DEFAULT false NOT NULL,
    inst_id_mod boolean DEFAULT false NOT NULL,
    task_id_mod boolean DEFAULT false NOT NULL,
    work_pld_mod boolean DEFAULT false NOT NULL,
    work_acc_mod boolean DEFAULT false NOT NULL,
    work_done_mod boolean DEFAULT false NOT NULL,
    work_so_far_mod boolean DEFAULT false NOT NULL,
    status_mod boolean DEFAULT false NOT NULL,
    year_mod boolean DEFAULT false NOT NULL,
    timestamp_mod boolean DEFAULT false NOT NULL,
    is_guest boolean,
    is_guest_mod boolean NOT NULL
);


--
-- Name: projects_version; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.projects_version (
    id integer NOT NULL,
    code character varying(80),
    year integer,
    name character varying(80),
    description character varying(1000),
    max_task_level integer,
    "timestamp" timestamp without time zone,
    transaction_id bigint NOT NULL,
    end_transaction_id bigint,
    operation_type smallint NOT NULL,
    code_mod boolean DEFAULT false NOT NULL,
    year_mod boolean DEFAULT false NOT NULL,
    name_mod boolean DEFAULT false NOT NULL,
    description_mod boolean DEFAULT false NOT NULL,
    max_task_level_mod boolean DEFAULT false NOT NULL,
    timestamp_mod boolean DEFAULT false NOT NULL,
    status character varying(20),
    status_mod boolean NOT NULL
);


--
-- Name: shift_grid; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.shift_grid (
    shift_type_id integer NOT NULL,
    shift_type character varying(200) NOT NULL,
    sub_system character varying(200) NOT NULL,
    order_num integer NOT NULL,
    start_hour timestamp without time zone,
    end_hour timestamp without time zone
);


--
-- Name: shift_task_map_version; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.shift_task_map_version (
    id integer NOT NULL,
    sub_system character varying(100),
    shift_type character varying(100),
    shift_type_id integer,
    flavour_name character varying(20),
    task_code character varying(80),
    status character varying(100),
    weight double precision,
    year integer,
    "timestamp" timestamp without time zone,
    transaction_id bigint NOT NULL,
    end_transaction_id bigint,
    operation_type smallint NOT NULL,
    sub_system_mod boolean DEFAULT false NOT NULL,
    shift_type_mod boolean DEFAULT false NOT NULL,
    shift_type_id_mod boolean DEFAULT false NOT NULL,
    flavour_name_mod boolean DEFAULT false NOT NULL,
    task_code_mod boolean DEFAULT false NOT NULL,
    status_mod boolean DEFAULT false NOT NULL,
    weight_mod boolean DEFAULT false NOT NULL,
    year_mod boolean DEFAULT false NOT NULL,
    timestamp_mod boolean DEFAULT false NOT NULL,
    added_value_cred double precision DEFAULT 0,
    added_value_cred_mod boolean DEFAULT false NOT NULL
);


--
-- Name: shiftasks; Type: VIEW; Schema: epr; Owner: -
--

CREATE VIEW epr.shiftasks AS
 SELECT s.name,
    s.sum
   FROM ( SELECT tasks.name,
            sum((((shifts.weight * stm.weight) * ((1)::double precision + (stm.added_value_cred / (7)::double precision))) / (21.7)::double precision)) AS sum
           FROM ((epr.shifts
             LEFT JOIN epr.shift_task_map stm USING (shift_type_id, sub_system, flavour_name, year))
             LEFT JOIN epr.tasks ON ((((stm.task_code)::text = (tasks.code)::text) AND (stm.year = tasks.year))))
          WHERE (shifts.year = 2018)
          GROUP BY tasks.name) s
  ORDER BY s.sum;


--
-- Name: tasks_version; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.tasks_version (
    id integer NOT NULL,
    code character varying(80),
    year integer,
    name character varying(200),
    description character varying(2000),
    activity_id integer,
    needed_work double precision,
    pct_at_cern integer,
    t_type character varying(80),
    shift_type_id character varying(80),
    comment character varying(2000),
    earliest_start character varying(80),
    latest_end character varying(80),
    level integer,
    parent character varying(80),
    "timestamp" timestamp without time zone,
    transaction_id bigint NOT NULL,
    end_transaction_id bigint,
    operation_type smallint NOT NULL,
    code_mod boolean DEFAULT false NOT NULL,
    year_mod boolean DEFAULT false NOT NULL,
    name_mod boolean DEFAULT false NOT NULL,
    description_mod boolean DEFAULT false NOT NULL,
    activity_id_mod boolean DEFAULT false NOT NULL,
    needed_work_mod boolean DEFAULT false NOT NULL,
    pct_at_cern_mod boolean DEFAULT false NOT NULL,
    t_type_mod boolean DEFAULT false NOT NULL,
    shift_type_id_mod boolean DEFAULT false NOT NULL,
    comment_mod boolean DEFAULT false NOT NULL,
    earliest_start_mod boolean DEFAULT false NOT NULL,
    latest_end_mod boolean DEFAULT false NOT NULL,
    level_mod boolean DEFAULT false NOT NULL,
    parent_mod boolean DEFAULT false NOT NULL,
    timestamp_mod boolean DEFAULT false NOT NULL,
    status character varying(20),
    status_mod boolean NOT NULL,
    kind character varying(20),
    kind_mod boolean NOT NULL,
    locked boolean,
    locked_mod boolean NOT NULL,
    level3_id integer,
    level3_id_mod boolean DEFAULT false NOT NULL
);


--
-- Name: time_line_inst_version; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.time_line_inst_version (
    id integer NOT NULL,
    inst_code character varying(80),
    year integer,
    cms_status character varying(40),
    comment character varying(1000),
    "timestamp" timestamp without time zone,
    transaction_id bigint NOT NULL,
    end_transaction_id bigint,
    operation_type smallint NOT NULL,
    inst_code_mod boolean DEFAULT false NOT NULL,
    year_mod boolean DEFAULT false NOT NULL,
    cms_status_mod boolean DEFAULT false NOT NULL,
    comment_mod boolean DEFAULT false NOT NULL,
    timestamp_mod boolean DEFAULT false NOT NULL
);


--
-- Name: time_line_user_version; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.time_line_user_version (
    id integer NOT NULL,
    user_cms_id integer,
    inst_code character varying(80),
    year integer,
    main_project character varying(80),
    is_author boolean,
    is_suspended boolean,
    status character varying(40),
    category integer,
    due_applicant double precision,
    due_author double precision,
    year_fraction double precision,
    comment character varying(1000),
    "timestamp" timestamp without time zone,
    transaction_id bigint NOT NULL,
    end_transaction_id bigint,
    operation_type smallint NOT NULL,
    user_cms_id_mod boolean DEFAULT false NOT NULL,
    inst_code_mod boolean DEFAULT false NOT NULL,
    year_mod boolean DEFAULT false NOT NULL,
    main_project_mod boolean DEFAULT false NOT NULL,
    is_author_mod boolean DEFAULT false NOT NULL,
    is_suspended_mod boolean DEFAULT false NOT NULL,
    status_mod boolean DEFAULT false NOT NULL,
    category_mod boolean DEFAULT false NOT NULL,
    due_applicant_mod boolean DEFAULT false NOT NULL,
    due_author_mod boolean DEFAULT false NOT NULL,
    year_fraction_mod boolean DEFAULT false NOT NULL,
    comment_mod boolean DEFAULT false NOT NULL,
    timestamp_mod boolean DEFAULT false NOT NULL
);


--
-- Name: transaction; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.transaction (
    issued_at timestamp without time zone,
    id bigint NOT NULL,
    remote_addr character varying(50),
    user_id integer
);


--
-- Name: transaction_id_seq; Type: SEQUENCE; Schema: epr; Owner: -
--

CREATE SEQUENCE epr.transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: transaction_id_seq; Type: SEQUENCE OWNED BY; Schema: epr; Owner: -
--

ALTER SEQUENCE epr.transaction_id_seq OWNED BY epr.transaction.id;


--
-- Name: userinst_version; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.userinst_version (
    id integer NOT NULL,
    user_id integer,
    inst_id integer,
    transaction_id bigint NOT NULL,
    end_transaction_id bigint,
    operation_type smallint NOT NULL
);


--
-- Name: users_version; Type: TABLE; Schema: epr; Owner: -
--

CREATE TABLE epr.users_version (
    id integer NOT NULL,
    username character varying(80),
    name character varying(200),
    hrid integer,
    cmsid integer,
    main_inst integer,
    role_id integer,
    auth_req boolean,
    is_suspended boolean,
    status character varying(40),
    category integer,
    year integer,
    comment character varying(1000),
    code character varying(80),
    "timestamp" timestamp without time zone,
    transaction_id bigint NOT NULL,
    end_transaction_id bigint,
    operation_type smallint NOT NULL,
    username_mod boolean DEFAULT false NOT NULL,
    name_mod boolean DEFAULT false NOT NULL,
    hrid_mod boolean DEFAULT false NOT NULL,
    cmsid_mod boolean DEFAULT false NOT NULL,
    main_inst_mod boolean DEFAULT false NOT NULL,
    role_id_mod boolean DEFAULT false NOT NULL,
    auth_req_mod boolean DEFAULT false NOT NULL,
    is_suspended_mod boolean DEFAULT false NOT NULL,
    status_mod boolean DEFAULT false NOT NULL,
    category_mod boolean DEFAULT false NOT NULL,
    year_mod boolean DEFAULT false NOT NULL,
    comment_mod boolean DEFAULT false NOT NULL,
    code_mod boolean DEFAULT false NOT NULL,
    timestamp_mod boolean DEFAULT false NOT NULL,
    creation_time timestamp without time zone,
    creation_time_mod boolean NOT NULL
);


--
-- Name: affiliation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.affiliation (
    id integer NOT NULL,
    cms_id integer NOT NULL,
    inst_code character varying(32) NOT NULL,
    is_primary boolean NOT NULL,
    start_date date NOT NULL,
    end_date date
);


--
-- Name: affiliation_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.affiliation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: affiliation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.affiliation_id_seq OWNED BY public.affiliation.id;


--
-- Name: alembic_version; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.alembic_version (
    version_num character varying(32) NOT NULL
);


--
-- Name: announcement; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.announcement (
    id integer NOT NULL,
    type character varying(8),
    subject text NOT NULL,
    content text NOT NULL,
    start_datetime timestamp without time zone NOT NULL,
    end_datetime timestamp without time zone,
    application public.app_names_enum
);


--
-- Name: announcement_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.announcement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: announcement_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.announcement_id_seq OWNED BY public.announcement.id;


--
-- Name: application_asset; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.application_asset (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    application public.app_names_enum,
    data json
);


--
-- Name: application_asset_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.application_asset_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: application_asset_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.application_asset_id_seq OWNED BY public.application_asset.id;


--
-- Name: assignment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.assignment (
    id integer NOT NULL,
    cms_id integer NOT NULL,
    project_code character varying(32) NOT NULL,
    fraction double precision NOT NULL,
    start_date date NOT NULL,
    end_date date
);


--
-- Name: assignment_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.assignment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: assignment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.assignment_id_seq OWNED BY public.assignment.id;


--
-- Name: career_event; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.career_event (
    id integer NOT NULL,
    cms_id integer NOT NULL,
    date date NOT NULL,
    event_type public.career_event_type_enum NOT NULL
);


--
-- Name: career_event_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.career_event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: career_event_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.career_event_id_seq OWNED BY public.career_event.id;


--
-- Name: country; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.country (
    code character varying(2) NOT NULL,
    name character varying(128) NOT NULL
);


--
-- Name: email_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_log (
    id integer NOT NULL,
    email_id integer,
    "timestamp" timestamp without time zone NOT NULL,
    action character varying(32) NOT NULL,
    status character varying(32) NOT NULL,
    details text
);


--
-- Name: email_log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.email_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: email_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.email_log_id_seq OWNED BY public.email_log.id;


--
-- Name: email_message; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_message (
    id integer NOT NULL,
    sender character varying(256),
    reply_to character varying(256),
    "to" text NOT NULL,
    cc text,
    bcc text,
    subject text NOT NULL,
    body text NOT NULL,
    hash character varying(32) NOT NULL,
    source_application character varying(32),
    remarks text,
    last_modified timestamp without time zone NOT NULL
);


--
-- Name: email_message_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.email_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: email_message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.email_message_id_seq OWNED BY public.email_message.id;


--
-- Name: epr_aggregate; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.epr_aggregate (
    id integer NOT NULL,
    cms_id integer,
    inst_code character varying(32) NOT NULL,
    project_code character varying(32) NOT NULL,
    year integer NOT NULL,
    aggregate_value double precision DEFAULT '0'::double precision NOT NULL,
    aggregate_unit public.epr_aggregate_units_enum NOT NULL
);


--
-- Name: epr_aggregate_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.epr_aggregate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: epr_aggregate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.epr_aggregate_id_seq OWNED BY public.epr_aggregate.id;


--
-- Name: ex_officio_mandate; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ex_officio_mandate (
    id integer NOT NULL,
    last_modified timestamp without time zone NOT NULL,
    start_date date NOT NULL,
    end_date date,
    src_unit_id smallint NOT NULL,
    dst_unit_id smallint NOT NULL,
    src_position_level smallint NOT NULL,
    dst_position_level smallint NOT NULL,
    src_position_name character varying(64),
    dst_position_name character varying(64)
);


--
-- Name: ex_officio_mandate_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.ex_officio_mandate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ex_officio_mandate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.ex_officio_mandate_id_seq OWNED BY public.ex_officio_mandate.id;


--
-- Name: funding_agency; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.funding_agency (
    id integer NOT NULL,
    country_code character varying(2),
    name character varying(64)
);


--
-- Name: funding_agency_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.funding_agency_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: funding_agency_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.funding_agency_id_seq OWNED BY public.funding_agency.id;


--
-- Name: image_upload; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.image_upload (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    data bytea,
    url text
);


--
-- Name: image_upload_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.image_upload_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: image_upload_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.image_upload_id_seq OWNED BY public.image_upload.id;


--
-- Name: institute; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.institute (
    id integer NOT NULL,
    code character varying(32) NOT NULL,
    name character varying(256) NOT NULL,
    official_joining_date date,
    country_code character varying(2)
);


--
-- Name: institute_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.institute_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: institute_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.institute_id_seq OWNED BY public.institute.id;


--
-- Name: institute_leader; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.institute_leader (
    id integer NOT NULL,
    cms_id integer NOT NULL,
    inst_code character varying(32) NOT NULL,
    is_primary boolean NOT NULL,
    start_date date NOT NULL,
    end_date date
);


--
-- Name: institute_leader_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.institute_leader_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: institute_leader_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.institute_leader_id_seq OWNED BY public.institute_leader.id;


--
-- Name: institute_status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.institute_status (
    id integer NOT NULL,
    code character varying(32) NOT NULL,
    status public.inst_status_enum NOT NULL,
    start_date date NOT NULL,
    end_date date
);


--
-- Name: institute_status_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.institute_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: institute_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.institute_status_id_seq OWNED BY public.institute_status.id;


--
-- Name: legacy_flag; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.legacy_flag (
    id integer NOT NULL,
    flag_code character varying(32) NOT NULL,
    cms_id integer NOT NULL,
    remote_id integer NOT NULL,
    start_date date NOT NULL,
    end_date date
);


--
-- Name: legacy_flag_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.legacy_flag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: legacy_flag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.legacy_flag_id_seq OWNED BY public.legacy_flag.id;


--
-- Name: manager_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.manager_history (
    id integer NOT NULL,
    cms_id integer NOT NULL,
    inst_code character varying(64) NOT NULL,
    year integer NOT NULL,
    username character varying(64) NOT NULL,
    level smallint,
    role character varying(200) NOT NULL,
    name character varying(200) NOT NULL,
    "timestamp" timestamp without time zone NOT NULL
);


--
-- Name: manager_history_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.manager_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: manager_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.manager_history_id_seq OWNED BY public.manager_history.id;


--
-- Name: markdown_note; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.markdown_note (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    body text
);


--
-- Name: markdown_note_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.markdown_note_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: markdown_note_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.markdown_note_id_seq OWNED BY public.markdown_note.id;


--
-- Name: mo; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mo (
    id integer NOT NULL,
    year smallint NOT NULL,
    cms_id integer NOT NULL,
    fa_id integer,
    inst_code character varying(32),
    set_by integer,
    status public.mo_status_enum NOT NULL,
    "timestamp" timestamp without time zone NOT NULL
);


--
-- Name: mo_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.mo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.mo_id_seq OWNED BY public.mo.id;


--
-- Name: org_unit; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.org_unit (
    id smallint NOT NULL,
    last_modified timestamp without time zone NOT NULL,
    type_id smallint NOT NULL,
    domain character varying(64),
    superior_unit_id smallint,
    enclosing_unit_id smallint,
    outermost_unit_id smallint,
    is_active boolean DEFAULT true NOT NULL
);


--
-- Name: org_unit_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.org_unit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: org_unit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.org_unit_id_seq OWNED BY public.org_unit.id;


--
-- Name: org_unit_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.org_unit_type (
    id smallint NOT NULL,
    last_modified timestamp without time zone NOT NULL,
    name character varying(64) NOT NULL,
    level smallint,
    is_enclosed boolean DEFAULT false NOT NULL,
    is_active boolean DEFAULT true NOT NULL
);


--
-- Name: org_unit_type_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.org_unit_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: org_unit_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.org_unit_type_id_seq OWNED BY public.org_unit_type.id;


--
-- Name: person; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.person (
    id integer NOT NULL,
    login character varying(128),
    hr_id integer,
    cms_id integer NOT NULL,
    first_name character varying(128) NOT NULL,
    last_name character varying(128) NOT NULL,
    email character varying(128),
    gender public.gender_enum,
    nationality character varying(2)
);


--
-- Name: person_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.person_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: person_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.person_id_seq OWNED BY public.person.id;


--
-- Name: person_status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.person_status (
    id integer NOT NULL,
    cms_id integer NOT NULL,
    status public.person_status_enum,
    start_date date NOT NULL,
    end_date date,
    activity public.person_activity_enum,
    is_author boolean,
    author_block boolean,
    epr_suspension boolean
);


--
-- Name: person_status_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.person_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: person_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.person_status_id_seq OWNED BY public.person_status.id;


--
-- Name: position; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."position" (
    id smallint NOT NULL,
    last_modified timestamp without time zone NOT NULL,
    name character varying(64) NOT NULL,
    level smallint,
    unit_type_id smallint NOT NULL,
    is_active boolean DEFAULT true NOT NULL
);


--
-- Name: position_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.position_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: position_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.position_id_seq OWNED BY public."position".id;


--
-- Name: prehistory_timeline; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.prehistory_timeline (
    id integer NOT NULL,
    cms_id integer NOT NULL,
    start_date date NOT NULL,
    status_cms character varying(64),
    inst_code character varying(64),
    activity_cms character varying(64),
    end_date date
);


--
-- Name: prehistory_timeline_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.prehistory_timeline_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: prehistory_timeline_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.prehistory_timeline_id_seq OWNED BY public.prehistory_timeline.id;


--
-- Name: project; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.project (
    id integer NOT NULL,
    code character varying(32) NOT NULL,
    name character varying(64) NOT NULL
);


--
-- Name: project_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.project_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: project_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.project_id_seq OWNED BY public.project.id;


--
-- Name: project_lifespan; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.project_lifespan (
    id integer NOT NULL,
    code character varying(32) NOT NULL,
    start_year integer NOT NULL,
    end_year integer
);


--
-- Name: project_lifespan_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.project_lifespan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: project_lifespan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.project_lifespan_id_seq OWNED BY public.project_lifespan.id;


--
-- Name: request; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.request (
    id integer NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    last_modified timestamp without time zone NOT NULL,
    creator_cms_id integer NOT NULL,
    processing_data jsonb,
    type character varying(64) NOT NULL,
    status public.request_status_enum NOT NULL,
    scheduled_execution_date timestamp without time zone,
    remarks text
);


--
-- Name: request_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: request_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.request_id_seq OWNED BY public.request.id;


--
-- Name: request_step; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.request_step (
    id integer NOT NULL,
    request_id integer,
    creation_date timestamp without time zone NOT NULL,
    creator_cms_id integer,
    last_modified timestamp without time zone NOT NULL,
    updater_cms_id integer,
    status public.request_step_status_enum NOT NULL,
    deadline timestamp without time zone,
    title text,
    remarks text
);


--
-- Name: request_step_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.request_step_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: request_step_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.request_step_id_seq OWNED BY public.request_step.id;


--
-- Name: row_change; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.row_change (
    id integer NOT NULL,
    schema_name character varying(32) NOT NULL,
    table_name character varying(64) NOT NULL,
    primary_key integer,
    composite_key jsonb,
    former_values jsonb,
    new_values jsonb,
    operation character varying(6) NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    cms_id integer,
    CONSTRAINT ck_row_change_row_operation_type CHECK (((operation)::text = ANY (ARRAY[('insert'::character varying)::text, ('update'::character varying)::text, ('delete'::character varying)::text])))
);


--
-- Name: row_change_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.row_change_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: row_change_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.row_change_id_seq OWNED BY public.row_change.id;


--
-- Name: tenure; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tenure (
    id integer NOT NULL,
    last_modified timestamp without time zone NOT NULL,
    cms_id integer,
    position_id smallint NOT NULL,
    unit_id smallint NOT NULL,
    start_date date NOT NULL,
    end_date date
);


--
-- Name: tenure_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tenure_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tenure_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tenure_id_seq OWNED BY public.tenure.id;


--
-- Name: user_author_data; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_author_data (
    id integer NOT NULL,
    hrid integer NOT NULL,
    cmsid integer NOT NULL,
    inspireid character varying,
    orcid character varying,
    lastupdate timestamp without time zone NOT NULL
);


--
-- Name: user_author_data_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_author_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_author_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_author_data_id_seq OWNED BY public.user_author_data.id;


--
-- Name: user_preferences; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_preferences (
    id integer NOT NULL,
    hrid integer NOT NULL,
    app_name public.app_names,
    prefs json,
    lastupdate timestamp without time zone NOT NULL
);


--
-- Name: user_preferences_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_preferences_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_preferences_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_preferences_id_seq OWNED BY public.user_preferences.id;


--
-- Name: view_people_summary; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.view_people_summary AS
 SELECT stats.ttl AS total,
    stats.year,
    c.name AS activity_name
   FROM (( SELECT sum(tlu.year_fraction) AS ttl,
            tlu.year,
            tlu.category
           FROM epr.time_line_user tlu
          WHERE ((tlu.status)::text = 'CMS'::text)
          GROUP BY tlu.year, tlu.category
          ORDER BY tlu.year DESC, tlu.category) stats
     LEFT JOIN epr.categories c ON ((stats.category = c.id)));


--
-- Name: view_person_affiliations; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.view_person_affiliations AS
 SELECT affiliation.cms_id,
    json_object_agg(affiliation.inst_code,
        CASE
            WHEN affiliation.is_primary THEN 'primary'::text
            ELSE 'secondary'::text
        END) AS affiliations
   FROM public.affiliation
  WHERE ((now() >= affiliation.start_date) AND (now() <= COALESCE((affiliation.end_date)::timestamp with time zone, now())))
  GROUP BY affiliation.cms_id;


--
-- Name: view_person_flags; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.view_person_flags AS
 SELECT legacy_flag.cms_id,
    json_agg(legacy_flag.flag_code) AS flags
   FROM public.legacy_flag
  WHERE ((now() >= legacy_flag.start_date) AND (now() <= COALESCE((legacy_flag.end_date)::timestamp with time zone, now())))
  GROUP BY legacy_flag.cms_id;


--
-- Name: view_person_managed_unit_ids; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.view_person_managed_unit_ids AS
 SELECT t.cms_id,
    json_agg(ou.id) AS managed_unit_ids
   FROM ((public.tenure t
     JOIN public.org_unit ou ON (((t.unit_id = ou.id) OR (t.unit_id = ou.superior_unit_id))))
     JOIN public."position" p ON (((p.id = t.position_id) AND (p.level <= 1))))
  WHERE ((now() >= t.start_date) AND (now() <= COALESCE((t.end_date)::timestamp with time zone, now())))
  GROUP BY t.cms_id;


--
-- Name: view_person_projects; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.view_person_projects AS
 SELECT assignment.cms_id,
    json_object_agg(assignment.project_code, assignment.fraction) AS projects
   FROM public.assignment
  WHERE ((now() >= assignment.start_date) AND (now() <= COALESCE((assignment.end_date)::timestamp with time zone, now())))
  GROUP BY assignment.cms_id;


--
-- Name: view_person_status; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.view_person_status AS
 SELECT ps.cms_id,
    ps.status,
    ps.activity,
    ps.author_block,
    ps.is_author,
    ps.epr_suspension
   FROM public.person_status ps
  WHERE ((now() >= ps.start_date) AND (now() <= COALESCE((ps.end_date)::timestamp with time zone, now())));


--
-- Name: view_person_team_duties; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.view_person_team_duties AS
 SELECT institute_leader.cms_id,
    json_object_agg(institute_leader.inst_code,
        CASE
            WHEN (institute_leader.is_primary = true) THEN 'leader'::text
            ELSE 'deputy'::text
        END) AS team_duties
   FROM public.institute_leader
  WHERE ((now() >= institute_leader.start_date) AND (now() <= COALESCE((institute_leader.end_date)::timestamp with time zone, now())))
  GROUP BY institute_leader.cms_id;


--
-- Name: view_person; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.view_person AS
 SELECT person.cms_id,
    person.first_name,
    person.last_name,
    person.hr_id,
    person.login,
    view_person_status.status,
    view_person_status.activity,
    view_person_status.is_author,
    view_person_status.author_block,
    view_person_status.epr_suspension,
    view_person_affiliations.affiliations,
    view_person_projects.projects,
    view_person_team_duties.team_duties,
    view_person_flags.flags,
    view_person_managed_unit_ids.managed_unit_ids
   FROM ((((((public.person
     LEFT JOIN public.view_person_status USING (cms_id))
     LEFT JOIN public.view_person_affiliations USING (cms_id))
     LEFT JOIN public.view_person_projects USING (cms_id))
     LEFT JOIN public.view_person_team_duties USING (cms_id))
     LEFT JOIN public.view_person_flags USING (cms_id))
     LEFT JOIN public.view_person_managed_unit_ids USING (cms_id));


--
-- Name: view_person_inst_code; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.view_person_inst_code AS
 SELECT affiliation.cms_id,
    affiliation.inst_code
   FROM public.affiliation
  WHERE ((affiliation.is_primary = true) AND ((now() >= affiliation.start_date) AND (now() <= COALESCE((affiliation.end_date)::timestamp with time zone, now()))));


--
-- Name: access_class; Type: TABLE; Schema: toolkit; Owner: -
--

CREATE TABLE toolkit.access_class (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    rules jsonb
);


--
-- Name: access_class_id_seq; Type: SEQUENCE; Schema: toolkit; Owner: -
--

CREATE SEQUENCE toolkit.access_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: access_class_id_seq; Type: SEQUENCE OWNED BY; Schema: toolkit; Owner: -
--

ALTER SEQUENCE toolkit.access_class_id_seq OWNED BY toolkit.access_class.id;


--
-- Name: activity_check; Type: TABLE; Schema: toolkit; Owner: -
--

CREATE TABLE toolkit.activity_check (
    id integer NOT NULL,
    for_cms_id smallint,
    by_cms_id smallint,
    for_activity smallint,
    "timestamp" timestamp without time zone,
    confirmed boolean
);


--
-- Name: activity_check_id_seq; Type: SEQUENCE; Schema: toolkit; Owner: -
--

CREATE SEQUENCE toolkit.activity_check_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: activity_check_id_seq; Type: SEQUENCE OWNED BY; Schema: toolkit; Owner: -
--

ALTER SEQUENCE toolkit.activity_check_id_seq OWNED BY toolkit.activity_check.id;


--
-- Name: alembic_version; Type: TABLE; Schema: toolkit; Owner: -
--

CREATE TABLE toolkit.alembic_version (
    version_num character varying(32) NOT NULL
);


--
-- Name: author_list_files; Type: TABLE; Schema: toolkit; Owner: -
--

CREATE TABLE toolkit.author_list_files (
    id integer NOT NULL,
    paper_code character varying(64) NOT NULL,
    status character varying(64) NOT NULL,
    location character varying(64) NOT NULL,
    last_update date NOT NULL
);


--
-- Name: author_list_files_id_seq; Type: SEQUENCE; Schema: toolkit; Owner: -
--

CREATE SEQUENCE toolkit.author_list_files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: author_list_files_id_seq; Type: SEQUENCE OWNED BY; Schema: toolkit; Owner: -
--

ALTER SEQUENCE toolkit.author_list_files_id_seq OWNED BY toolkit.author_list_files.id;


--
-- Name: authorship_application_check; Type: TABLE; Schema: toolkit; Owner: -
--

CREATE TABLE toolkit.authorship_application_check (
    id integer NOT NULL,
    run_number integer NOT NULL,
    datetime timestamp without time zone DEFAULT now() NOT NULL,
    cms_id integer NOT NULL,
    inst_code character varying(32) NOT NULL,
    activity_id integer NOT NULL,
    passed boolean NOT NULL,
    failed boolean NOT NULL,
    reason character varying(128),
    days_count integer NOT NULL,
    worked_self double precision NOT NULL,
    worked_inst double precision NOT NULL,
    needed_inst double precision NOT NULL,
    possible_slipthrough boolean NOT NULL,
    mo_status boolean NOT NULL,
    notification_id integer
);


--
-- Name: authorship_application_check_id_seq; Type: SEQUENCE; Schema: toolkit; Owner: -
--

CREATE SEQUENCE toolkit.authorship_application_check_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: authorship_application_check_id_seq; Type: SEQUENCE OWNED BY; Schema: toolkit; Owner: -
--

ALTER SEQUENCE toolkit.authorship_application_check_id_seq OWNED BY toolkit.authorship_application_check.id;


--
-- Name: cern_country; Type: TABLE; Schema: toolkit; Owner: -
--

CREATE TABLE toolkit.cern_country (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    status_id smallint
);


--
-- Name: cern_country_id_seq; Type: SEQUENCE; Schema: toolkit; Owner: -
--

CREATE SEQUENCE toolkit.cern_country_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cern_country_id_seq; Type: SEQUENCE OWNED BY; Schema: toolkit; Owner: -
--

ALTER SEQUENCE toolkit.cern_country_id_seq OWNED BY toolkit.cern_country.id;


--
-- Name: cern_country_status; Type: TABLE; Schema: toolkit; Owner: -
--

CREATE TABLE toolkit.cern_country_status (
    id smallint NOT NULL,
    name character varying(64) NOT NULL
);


--
-- Name: cern_country_status_id_seq; Type: SEQUENCE; Schema: toolkit; Owner: -
--

CREATE SEQUENCE toolkit.cern_country_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cern_country_status_id_seq; Type: SEQUENCE OWNED BY; Schema: toolkit; Owner: -
--

ALTER SEQUENCE toolkit.cern_country_status_id_seq OWNED BY toolkit.cern_country_status.id;


--
-- Name: cms_project; Type: TABLE; Schema: toolkit; Owner: -
--

CREATE TABLE toolkit.cms_project (
    code character varying(32) NOT NULL,
    name character varying(128) NOT NULL,
    official boolean NOT NULL,
    status character varying(32)
);


--
-- Name: cms_week; Type: TABLE; Schema: toolkit; Owner: -
--

CREATE TABLE toolkit.cms_week (
    id integer NOT NULL,
    title character varying(80) NOT NULL,
    date date NOT NULL,
    date_end date NOT NULL,
    is_external boolean NOT NULL
);


--
-- Name: cms_week_id_seq; Type: SEQUENCE; Schema: toolkit; Owner: -
--

CREATE SEQUENCE toolkit.cms_week_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cms_week_id_seq; Type: SEQUENCE OWNED BY; Schema: toolkit; Owner: -
--

ALTER SEQUENCE toolkit.cms_week_id_seq OWNED BY toolkit.cms_week.id;


--
-- Name: ext_author; Type: TABLE; Schema: toolkit; Owner: -
--

CREATE TABLE toolkit.ext_author (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    cms_id smallint,
    hr_id integer,
    inst_code character varying(40) NOT NULL,
    inst_code_also character varying(40),
    inst_code_now character varying(40),
    reason text,
    date_work_start date,
    date_work_end date,
    date_sign_end date,
    project_id integer NOT NULL,
    spires_id character varying(40)
);


--
-- Name: ext_author_flag; Type: TABLE; Schema: toolkit; Owner: -
--

CREATE TABLE toolkit.ext_author_flag (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    project_id integer NOT NULL,
    active boolean NOT NULL
);


--
-- Name: ext_author_flag_id_seq; Type: SEQUENCE; Schema: toolkit; Owner: -
--

CREATE SEQUENCE toolkit.ext_author_flag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ext_author_flag_id_seq; Type: SEQUENCE OWNED BY; Schema: toolkit; Owner: -
--

ALTER SEQUENCE toolkit.ext_author_flag_id_seq OWNED BY toolkit.ext_author_flag.id;


--
-- Name: ext_author_id_seq; Type: SEQUENCE; Schema: toolkit; Owner: -
--

CREATE SEQUENCE toolkit.ext_author_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ext_author_id_seq; Type: SEQUENCE OWNED BY; Schema: toolkit; Owner: -
--

ALTER SEQUENCE toolkit.ext_author_id_seq OWNED BY toolkit.ext_author.id;


--
-- Name: ext_author_project; Type: TABLE; Schema: toolkit; Owner: -
--

CREATE TABLE toolkit.ext_author_project (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    active boolean NOT NULL
);


--
-- Name: ext_author_project_id_seq; Type: SEQUENCE; Schema: toolkit; Owner: -
--

CREATE SEQUENCE toolkit.ext_author_project_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ext_author_project_id_seq; Type: SEQUENCE OWNED BY; Schema: toolkit; Owner: -
--

ALTER SEQUENCE toolkit.ext_author_project_id_seq OWNED BY toolkit.ext_author_project.id;


--
-- Name: ext_authors_flags_rel; Type: TABLE; Schema: toolkit; Owner: -
--

CREATE TABLE toolkit.ext_authors_flags_rel (
    author_id integer NOT NULL,
    flag_id integer NOT NULL
);


--
-- Name: permission; Type: TABLE; Schema: toolkit; Owner: -
--

CREATE TABLE toolkit.permission (
    id integer NOT NULL,
    resource_id integer,
    access_class_id integer,
    action toolkit.restricted_action_type_enum NOT NULL
);


--
-- Name: permission_id_seq; Type: SEQUENCE; Schema: toolkit; Owner: -
--

CREATE SEQUENCE toolkit.permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: permission_id_seq; Type: SEQUENCE OWNED BY; Schema: toolkit; Owner: -
--

ALTER SEQUENCE toolkit.permission_id_seq OWNED BY toolkit.permission.id;


--
-- Name: restricted_resource; Type: TABLE; Schema: toolkit; Owner: -
--

CREATE TABLE toolkit.restricted_resource (
    id integer NOT NULL,
    type toolkit.restricted_resource_type_enum NOT NULL,
    key character varying(128) NOT NULL,
    filters jsonb
);


--
-- Name: restricted_resource_id_seq; Type: SEQUENCE; Schema: toolkit; Owner: -
--

CREATE SEQUENCE toolkit.restricted_resource_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: restricted_resource_id_seq; Type: SEQUENCE OWNED BY; Schema: toolkit; Owner: -
--

ALTER SEQUENCE toolkit.restricted_resource_id_seq OWNED BY toolkit.restricted_resource.id;


--
-- Name: room; Type: TABLE; Schema: toolkit; Owner: -
--

CREATE TABLE toolkit.room (
    id smallint NOT NULL,
    indico_id smallint,
    building character varying(8) NOT NULL,
    floor character varying(4) NOT NULL,
    room_nr character varying(8) NOT NULL,
    custom_name character varying(128),
    at_cern boolean NOT NULL,
    capacity smallint
);


--
-- Name: room_id_seq; Type: SEQUENCE; Schema: toolkit; Owner: -
--

CREATE SEQUENCE toolkit.room_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: room_id_seq; Type: SEQUENCE OWNED BY; Schema: toolkit; Owner: -
--

ALTER SEQUENCE toolkit.room_id_seq OWNED BY toolkit.room.id;


--
-- Name: room_request; Type: TABLE; Schema: toolkit; Owner: -
--

CREATE TABLE toolkit.room_request (
    id integer NOT NULL,
    cms_id_by smallint NOT NULL,
    cms_id_for smallint NOT NULL,
    title character varying(256) NOT NULL,
    project character varying(32) NOT NULL,
    webcast boolean NOT NULL,
    time_start timestamp without time zone NOT NULL,
    duration smallint NOT NULL,
    cms_week_id integer,
    room_id_preferred smallint NOT NULL,
    room_id smallint,
    capacity smallint NOT NULL,
    password character varying(16),
    official boolean NOT NULL,
    remarks text,
    reason text,
    status character varying(32),
    time_created timestamp without time zone,
    time_updated timestamp without time zone,
    agenda_url character varying(256)
);


--
-- Name: room_request_convener; Type: TABLE; Schema: toolkit; Owner: -
--

CREATE TABLE toolkit.room_request_convener (
    id integer NOT NULL,
    cms_id smallint NOT NULL,
    request_id integer NOT NULL,
    active boolean NOT NULL
);


--
-- Name: room_request_convener_id_seq; Type: SEQUENCE; Schema: toolkit; Owner: -
--

CREATE SEQUENCE toolkit.room_request_convener_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: room_request_convener_id_seq; Type: SEQUENCE OWNED BY; Schema: toolkit; Owner: -
--

ALTER SEQUENCE toolkit.room_request_convener_id_seq OWNED BY toolkit.room_request_convener.id;


--
-- Name: room_request_id_seq; Type: SEQUENCE; Schema: toolkit; Owner: -
--

CREATE SEQUENCE toolkit.room_request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: room_request_id_seq; Type: SEQUENCE OWNED BY; Schema: toolkit; Owner: -
--

ALTER SEQUENCE toolkit.room_request_id_seq OWNED BY toolkit.room_request.id;


--
-- Name: room_week_rel; Type: TABLE; Schema: toolkit; Owner: -
--

CREATE TABLE toolkit.room_week_rel (
    id integer NOT NULL,
    room_id smallint NOT NULL,
    week_id integer NOT NULL
);


--
-- Name: room_week_rel_id_seq; Type: SEQUENCE; Schema: toolkit; Owner: -
--

CREATE SEQUENCE toolkit.room_week_rel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: room_week_rel_id_seq; Type: SEQUENCE OWNED BY; Schema: toolkit; Owner: -
--

ALTER SEQUENCE toolkit.room_week_rel_id_seq OWNED BY toolkit.room_week_rel.id;


--
-- Name: status; Type: TABLE; Schema: toolkit; Owner: -
--

CREATE TABLE toolkit.status (
    code character varying(32) NOT NULL
);


--
-- Name: vote_delegation; Type: TABLE; Schema: toolkit; Owner: -
--

CREATE TABLE toolkit.vote_delegation (
    id integer NOT NULL,
    voting_code character varying(16),
    cms_id_from integer NOT NULL,
    cms_id_to integer NOT NULL,
    status character varying(32) NOT NULL,
    is_long_term boolean NOT NULL,
    cms_id_creator integer NOT NULL,
    time_created timestamp without time zone,
    cms_id_updater integer,
    time_updated timestamp without time zone,
    specific_inst_code character varying(32)
);


--
-- Name: vote_delegation_id_seq; Type: SEQUENCE; Schema: toolkit; Owner: -
--

CREATE SEQUENCE toolkit.vote_delegation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: vote_delegation_id_seq; Type: SEQUENCE OWNED BY; Schema: toolkit; Owner: -
--

ALTER SEQUENCE toolkit.vote_delegation_id_seq OWNED BY toolkit.vote_delegation.id;


--
-- Name: voting_id_seq; Type: SEQUENCE; Schema: toolkit; Owner: -
--

CREATE SEQUENCE toolkit.voting_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: voting; Type: TABLE; Schema: toolkit; Owner: -
--

CREATE TABLE toolkit.voting (
    code character varying(16) NOT NULL,
    title character varying(128) NOT NULL,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone,
    delegation_deadline timestamp without time zone,
    type character varying(20) NOT NULL,
    applicable_mo_year integer,
    list_closing_date date,
    id integer DEFAULT nextval('toolkit.voting_id_seq'::regclass) NOT NULL
);


--
-- Name: voting_list_entry; Type: TABLE; Schema: toolkit; Owner: -
--

CREATE TABLE toolkit.voting_list_entry (
    id integer NOT NULL,
    code character varying(16) NOT NULL,
    cms_id integer NOT NULL,
    weight double precision NOT NULL,
    represented_inst_code character varying(32),
    represented_merger_id integer,
    delegated_by_cms_id integer,
    remarks text,
    "timestamp" timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: voting_list_entry_id_seq; Type: SEQUENCE; Schema: toolkit; Owner: -
--

CREATE SEQUENCE toolkit.voting_list_entry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: voting_list_entry_id_seq; Type: SEQUENCE OWNED BY; Schema: toolkit; Owner: -
--

ALTER SEQUENCE toolkit.voting_list_entry_id_seq OWNED BY toolkit.voting_list_entry.id;


--
-- Name: voting_merger; Type: TABLE; Schema: toolkit; Owner: -
--

CREATE TABLE toolkit.voting_merger (
    id integer NOT NULL,
    country character varying(64) NOT NULL,
    valid_from date NOT NULL,
    valid_till date,
    representative_cms_id integer NOT NULL
);


--
-- Name: voting_merger_id_seq; Type: SEQUENCE; Schema: toolkit; Owner: -
--

CREATE SEQUENCE toolkit.voting_merger_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: voting_merger_id_seq; Type: SEQUENCE OWNED BY; Schema: toolkit; Owner: -
--

ALTER SEQUENCE toolkit.voting_merger_id_seq OWNED BY toolkit.voting_merger.id;


--
-- Name: voting_merger_member; Type: TABLE; Schema: toolkit; Owner: -
--

CREATE TABLE toolkit.voting_merger_member (
    merger_id integer NOT NULL,
    inst_code character varying(64) NOT NULL
);


--
-- Name: review_committee id; Type: DEFAULT; Schema: cadi; Owner: -
--

ALTER TABLE ONLY cadi.review_committee ALTER COLUMN id SET DEFAULT nextval('cadi.review_committee_id_seq'::regclass);


--
-- Name: review_committee_member id; Type: DEFAULT; Schema: cadi; Owner: -
--

ALTER TABLE ONLY cadi.review_committee_member ALTER COLUMN id SET DEFAULT nextval('cadi.review_committee_member_id_seq'::regclass);


--
-- Name: activity id; Type: DEFAULT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.activity ALTER COLUMN id SET DEFAULT nextval('epr.activity_base_id_seq'::regclass);


--
-- Name: categories id; Type: DEFAULT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.categories ALTER COLUMN id SET DEFAULT nextval('epr.id_seq_cats'::regclass);


--
-- Name: ceilings id; Type: DEFAULT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.ceilings ALTER COLUMN id SET DEFAULT nextval('epr.ceilings_id_seq'::regclass);


--
-- Name: cms_activities id; Type: DEFAULT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.cms_activities ALTER COLUMN id SET DEFAULT nextval('epr.id_seq_act'::regclass);


--
-- Name: epr_due id; Type: DEFAULT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.epr_due ALTER COLUMN id SET DEFAULT nextval('epr.id_seq_eprdue'::regclass);


--
-- Name: insts id; Type: DEFAULT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.insts ALTER COLUMN id SET DEFAULT nextval('epr.id_seq_inst'::regclass);


--
-- Name: level3names id; Type: DEFAULT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.level3names ALTER COLUMN id SET DEFAULT nextval('epr.level3names_id_seq'::regclass);


--
-- Name: managers id; Type: DEFAULT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.managers ALTER COLUMN id SET DEFAULT nextval('epr.id_seq_mgr'::regclass);


--
-- Name: pledges id; Type: DEFAULT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.pledges ALTER COLUMN id SET DEFAULT nextval('epr.id_seq_pledge'::regclass);


--
-- Name: projects id; Type: DEFAULT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.projects ALTER COLUMN id SET DEFAULT nextval('epr.id_seq_proj'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.roles ALTER COLUMN id SET DEFAULT nextval('epr.id_seq_role'::regclass);


--
-- Name: shift_task_map id; Type: DEFAULT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.shift_task_map ALTER COLUMN id SET DEFAULT nextval('epr.id_seq_stm'::regclass);


--
-- Name: shifts id; Type: DEFAULT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.shifts ALTER COLUMN id SET DEFAULT nextval('epr.id_seq_shifts'::regclass);


--
-- Name: tasks id; Type: DEFAULT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.tasks ALTER COLUMN id SET DEFAULT nextval('epr.id_seq_task'::regclass);


--
-- Name: time_line_inst id; Type: DEFAULT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.time_line_inst ALTER COLUMN id SET DEFAULT nextval('epr.id_seq_timlininst'::regclass);


--
-- Name: time_line_user id; Type: DEFAULT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.time_line_user ALTER COLUMN id SET DEFAULT nextval('epr.id_seq_timlinuser'::regclass);


--
-- Name: transaction id; Type: DEFAULT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.transaction ALTER COLUMN id SET DEFAULT nextval('epr.transaction_id_seq'::regclass);


--
-- Name: userinst id; Type: DEFAULT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.userinst ALTER COLUMN id SET DEFAULT nextval('epr.id_seq_userinst'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.users ALTER COLUMN id SET DEFAULT nextval('epr.id_seq_user'::regclass);


--
-- Name: v_all_inst id; Type: DEFAULT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.v_all_inst ALTER COLUMN id SET DEFAULT nextval('epr.id_seq_allinstview'::regclass);


--
-- Name: affiliation id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.affiliation ALTER COLUMN id SET DEFAULT nextval('public.affiliation_id_seq'::regclass);


--
-- Name: announcement id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.announcement ALTER COLUMN id SET DEFAULT nextval('public.announcement_id_seq'::regclass);


--
-- Name: application_asset id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.application_asset ALTER COLUMN id SET DEFAULT nextval('public.application_asset_id_seq'::regclass);


--
-- Name: assignment id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.assignment ALTER COLUMN id SET DEFAULT nextval('public.assignment_id_seq'::regclass);


--
-- Name: career_event id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.career_event ALTER COLUMN id SET DEFAULT nextval('public.career_event_id_seq'::regclass);


--
-- Name: email_log id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_log ALTER COLUMN id SET DEFAULT nextval('public.email_log_id_seq'::regclass);


--
-- Name: email_message id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_message ALTER COLUMN id SET DEFAULT nextval('public.email_message_id_seq'::regclass);


--
-- Name: epr_aggregate id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.epr_aggregate ALTER COLUMN id SET DEFAULT nextval('public.epr_aggregate_id_seq'::regclass);


--
-- Name: ex_officio_mandate id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ex_officio_mandate ALTER COLUMN id SET DEFAULT nextval('public.ex_officio_mandate_id_seq'::regclass);


--
-- Name: funding_agency id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.funding_agency ALTER COLUMN id SET DEFAULT nextval('public.funding_agency_id_seq'::regclass);


--
-- Name: image_upload id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.image_upload ALTER COLUMN id SET DEFAULT nextval('public.image_upload_id_seq'::regclass);


--
-- Name: institute id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.institute ALTER COLUMN id SET DEFAULT nextval('public.institute_id_seq'::regclass);


--
-- Name: institute_leader id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.institute_leader ALTER COLUMN id SET DEFAULT nextval('public.institute_leader_id_seq'::regclass);


--
-- Name: institute_status id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.institute_status ALTER COLUMN id SET DEFAULT nextval('public.institute_status_id_seq'::regclass);


--
-- Name: legacy_flag id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.legacy_flag ALTER COLUMN id SET DEFAULT nextval('public.legacy_flag_id_seq'::regclass);


--
-- Name: manager_history id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.manager_history ALTER COLUMN id SET DEFAULT nextval('public.manager_history_id_seq'::regclass);


--
-- Name: markdown_note id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.markdown_note ALTER COLUMN id SET DEFAULT nextval('public.markdown_note_id_seq'::regclass);


--
-- Name: mo id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mo ALTER COLUMN id SET DEFAULT nextval('public.mo_id_seq'::regclass);


--
-- Name: org_unit id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.org_unit ALTER COLUMN id SET DEFAULT nextval('public.org_unit_id_seq'::regclass);


--
-- Name: org_unit_type id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.org_unit_type ALTER COLUMN id SET DEFAULT nextval('public.org_unit_type_id_seq'::regclass);


--
-- Name: person id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.person ALTER COLUMN id SET DEFAULT nextval('public.person_id_seq'::regclass);


--
-- Name: person_status id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.person_status ALTER COLUMN id SET DEFAULT nextval('public.person_status_id_seq'::regclass);


--
-- Name: position id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."position" ALTER COLUMN id SET DEFAULT nextval('public.position_id_seq'::regclass);


--
-- Name: prehistory_timeline id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.prehistory_timeline ALTER COLUMN id SET DEFAULT nextval('public.prehistory_timeline_id_seq'::regclass);


--
-- Name: project id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.project ALTER COLUMN id SET DEFAULT nextval('public.project_id_seq'::regclass);


--
-- Name: project_lifespan id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.project_lifespan ALTER COLUMN id SET DEFAULT nextval('public.project_lifespan_id_seq'::regclass);


--
-- Name: request id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.request ALTER COLUMN id SET DEFAULT nextval('public.request_id_seq'::regclass);


--
-- Name: request_step id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.request_step ALTER COLUMN id SET DEFAULT nextval('public.request_step_id_seq'::regclass);


--
-- Name: row_change id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.row_change ALTER COLUMN id SET DEFAULT nextval('public.row_change_id_seq'::regclass);


--
-- Name: tenure id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tenure ALTER COLUMN id SET DEFAULT nextval('public.tenure_id_seq'::regclass);


--
-- Name: user_author_data id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_author_data ALTER COLUMN id SET DEFAULT nextval('public.user_author_data_id_seq'::regclass);


--
-- Name: user_preferences id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_preferences ALTER COLUMN id SET DEFAULT nextval('public.user_preferences_id_seq'::regclass);


--
-- Name: access_class id; Type: DEFAULT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.access_class ALTER COLUMN id SET DEFAULT nextval('toolkit.access_class_id_seq'::regclass);


--
-- Name: activity_check id; Type: DEFAULT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.activity_check ALTER COLUMN id SET DEFAULT nextval('toolkit.activity_check_id_seq'::regclass);


--
-- Name: author_list_files id; Type: DEFAULT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.author_list_files ALTER COLUMN id SET DEFAULT nextval('toolkit.author_list_files_id_seq'::regclass);


--
-- Name: authorship_application_check id; Type: DEFAULT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.authorship_application_check ALTER COLUMN id SET DEFAULT nextval('toolkit.authorship_application_check_id_seq'::regclass);


--
-- Name: cern_country id; Type: DEFAULT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.cern_country ALTER COLUMN id SET DEFAULT nextval('toolkit.cern_country_id_seq'::regclass);


--
-- Name: cern_country_status id; Type: DEFAULT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.cern_country_status ALTER COLUMN id SET DEFAULT nextval('toolkit.cern_country_status_id_seq'::regclass);


--
-- Name: cms_week id; Type: DEFAULT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.cms_week ALTER COLUMN id SET DEFAULT nextval('toolkit.cms_week_id_seq'::regclass);


--
-- Name: ext_author id; Type: DEFAULT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.ext_author ALTER COLUMN id SET DEFAULT nextval('toolkit.ext_author_id_seq'::regclass);


--
-- Name: ext_author_flag id; Type: DEFAULT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.ext_author_flag ALTER COLUMN id SET DEFAULT nextval('toolkit.ext_author_flag_id_seq'::regclass);


--
-- Name: ext_author_project id; Type: DEFAULT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.ext_author_project ALTER COLUMN id SET DEFAULT nextval('toolkit.ext_author_project_id_seq'::regclass);


--
-- Name: permission id; Type: DEFAULT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.permission ALTER COLUMN id SET DEFAULT nextval('toolkit.permission_id_seq'::regclass);


--
-- Name: restricted_resource id; Type: DEFAULT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.restricted_resource ALTER COLUMN id SET DEFAULT nextval('toolkit.restricted_resource_id_seq'::regclass);


--
-- Name: room id; Type: DEFAULT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.room ALTER COLUMN id SET DEFAULT nextval('toolkit.room_id_seq'::regclass);


--
-- Name: room_request id; Type: DEFAULT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.room_request ALTER COLUMN id SET DEFAULT nextval('toolkit.room_request_id_seq'::regclass);


--
-- Name: room_request_convener id; Type: DEFAULT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.room_request_convener ALTER COLUMN id SET DEFAULT nextval('toolkit.room_request_convener_id_seq'::regclass);


--
-- Name: room_week_rel id; Type: DEFAULT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.room_week_rel ALTER COLUMN id SET DEFAULT nextval('toolkit.room_week_rel_id_seq'::regclass);


--
-- Name: vote_delegation id; Type: DEFAULT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.vote_delegation ALTER COLUMN id SET DEFAULT nextval('toolkit.vote_delegation_id_seq'::regclass);


--
-- Name: voting_list_entry id; Type: DEFAULT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.voting_list_entry ALTER COLUMN id SET DEFAULT nextval('toolkit.voting_list_entry_id_seq'::regclass);


--
-- Name: voting_merger id; Type: DEFAULT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.voting_merger ALTER COLUMN id SET DEFAULT nextval('toolkit.voting_merger_id_seq'::regclass);


--
-- Name: review_committee pk_review_committee; Type: CONSTRAINT; Schema: cadi; Owner: -
--

ALTER TABLE ONLY cadi.review_committee
    ADD CONSTRAINT pk_review_committee PRIMARY KEY (id);


--
-- Name: review_committee_member pk_review_committee_member; Type: CONSTRAINT; Schema: cadi; Owner: -
--

ALTER TABLE ONLY cadi.review_committee_member
    ADD CONSTRAINT pk_review_committee_member PRIMARY KEY (id);


--
-- Name: activity activity_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.activity
    ADD CONSTRAINT activity_pkey PRIMARY KEY (id);


--
-- Name: categories categories_name_key; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.categories
    ADD CONSTRAINT categories_name_key UNIQUE (name);


--
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: cms_activities cms_activities_code_key; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.cms_activities
    ADD CONSTRAINT cms_activities_code_key UNIQUE (code);


--
-- Name: cms_activities cms_activities_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.cms_activities
    ADD CONSTRAINT cms_activities_pkey PRIMARY KEY (id);


--
-- Name: cms_activities_version cms_activities_version_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.cms_activities_version
    ADD CONSTRAINT cms_activities_version_pkey PRIMARY KEY (id, transaction_id);


--
-- Name: epr_due epr_due_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.epr_due
    ADD CONSTRAINT epr_due_pkey PRIMARY KEY (id);


--
-- Name: insts insts_cernid_key; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.insts
    ADD CONSTRAINT insts_cernid_key UNIQUE (cernid);


--
-- Name: insts insts_code_key; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.insts
    ADD CONSTRAINT insts_code_key UNIQUE (code);


--
-- Name: insts insts_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.insts
    ADD CONSTRAINT insts_pkey PRIMARY KEY (id);


--
-- Name: insts_version insts_version_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.insts_version
    ADD CONSTRAINT insts_version_pkey PRIMARY KEY (id, transaction_id);


--
-- Name: managers managers_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.managers
    ADD CONSTRAINT managers_pkey PRIMARY KEY (id);


--
-- Name: managers_version managers_version_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.managers_version
    ADD CONSTRAINT managers_version_pkey PRIMARY KEY (id, transaction_id);


--
-- Name: ceilings pk_ceilings; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.ceilings
    ADD CONSTRAINT pk_ceilings PRIMARY KEY (id);


--
-- Name: ceilings_version pk_ceilings_version; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.ceilings_version
    ADD CONSTRAINT pk_ceilings_version PRIMARY KEY (id, transaction_id);


--
-- Name: level3names pk_level3names; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.level3names
    ADD CONSTRAINT pk_level3names PRIMARY KEY (id);


--
-- Name: shift_grid pk_shift_grid; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.shift_grid
    ADD CONSTRAINT pk_shift_grid PRIMARY KEY (shift_type_id, sub_system, order_num);


--
-- Name: pledges pledges_code_key; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.pledges
    ADD CONSTRAINT pledges_code_key UNIQUE (code);


--
-- Name: pledges pledges_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.pledges
    ADD CONSTRAINT pledges_pkey PRIMARY KEY (id);


--
-- Name: pledges_version pledges_version_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.pledges_version
    ADD CONSTRAINT pledges_version_pkey PRIMARY KEY (id, transaction_id);


--
-- Name: projects projects_code_key; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.projects
    ADD CONSTRAINT projects_code_key UNIQUE (code);


--
-- Name: projects projects_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.projects
    ADD CONSTRAINT projects_pkey PRIMARY KEY (id);


--
-- Name: projects_version projects_version_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.projects_version
    ADD CONSTRAINT projects_version_pkey PRIMARY KEY (id, transaction_id);


--
-- Name: roles roles_name_key; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.roles
    ADD CONSTRAINT roles_name_key UNIQUE (name);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: shift_task_map shift_task_map_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.shift_task_map
    ADD CONSTRAINT shift_task_map_pkey PRIMARY KEY (id);


--
-- Name: shift_task_map_version shift_task_map_version_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.shift_task_map_version
    ADD CONSTRAINT shift_task_map_version_pkey PRIMARY KEY (id, transaction_id);


--
-- Name: shifts shifts_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.shifts
    ADD CONSTRAINT shifts_pkey PRIMARY KEY (id);


--
-- Name: shifts shifts_shift_id_key; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.shifts
    ADD CONSTRAINT shifts_shift_id_key UNIQUE (shift_id);


--
-- Name: tasks tasks_code_key; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.tasks
    ADD CONSTRAINT tasks_code_key UNIQUE (code);


--
-- Name: tasks tasks_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.tasks
    ADD CONSTRAINT tasks_pkey PRIMARY KEY (id);


--
-- Name: tasks_version tasks_version_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.tasks_version
    ADD CONSTRAINT tasks_version_pkey PRIMARY KEY (id, transaction_id);


--
-- Name: time_line_inst time_line_inst_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.time_line_inst
    ADD CONSTRAINT time_line_inst_pkey PRIMARY KEY (id);


--
-- Name: time_line_inst_version time_line_inst_version_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.time_line_inst_version
    ADD CONSTRAINT time_line_inst_version_pkey PRIMARY KEY (id, transaction_id);


--
-- Name: time_line_user time_line_user_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.time_line_user
    ADD CONSTRAINT time_line_user_pkey PRIMARY KEY (id);


--
-- Name: time_line_user_version time_line_user_version_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.time_line_user_version
    ADD CONSTRAINT time_line_user_version_pkey PRIMARY KEY (id, transaction_id);


--
-- Name: transaction transaction_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.transaction
    ADD CONSTRAINT transaction_pkey PRIMARY KEY (id);


--
-- Name: ceilings uq_ceilings_code; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.ceilings
    ADD CONSTRAINT uq_ceilings_code UNIQUE (code);


--
-- Name: level3names uq_level3names_name; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.level3names
    ADD CONSTRAINT uq_level3names_name UNIQUE (name);


--
-- Name: userinst userinst_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.userinst
    ADD CONSTRAINT userinst_pkey PRIMARY KEY (id);


--
-- Name: userinst_version userinst_version_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.userinst_version
    ADD CONSTRAINT userinst_version_pkey PRIMARY KEY (id, transaction_id);


--
-- Name: users users_cmsid_key; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.users
    ADD CONSTRAINT users_cmsid_key UNIQUE (cmsid);


--
-- Name: users users_code_key; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.users
    ADD CONSTRAINT users_code_key UNIQUE (code);


--
-- Name: users users_hrid_key; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.users
    ADD CONSTRAINT users_hrid_key UNIQUE (hrid);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users_version users_version_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.users_version
    ADD CONSTRAINT users_version_pkey PRIMARY KEY (id, transaction_id);


--
-- Name: v_all_inst v_all_inst_pkey; Type: CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.v_all_inst
    ADD CONSTRAINT v_all_inst_pkey PRIMARY KEY (id);


--
-- Name: epr_aggregate epr_aggregate_coordinates; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.epr_aggregate
    ADD CONSTRAINT epr_aggregate_coordinates UNIQUE (cms_id, inst_code, project_code, year, aggregate_unit);


--
-- Name: affiliation pk_affiliation; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.affiliation
    ADD CONSTRAINT pk_affiliation PRIMARY KEY (id);


--
-- Name: announcement pk_announcement; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.announcement
    ADD CONSTRAINT pk_announcement PRIMARY KEY (id);


--
-- Name: application_asset pk_application_asset; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.application_asset
    ADD CONSTRAINT pk_application_asset PRIMARY KEY (id);


--
-- Name: assignment pk_assignment; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.assignment
    ADD CONSTRAINT pk_assignment PRIMARY KEY (id);


--
-- Name: career_event pk_career_event; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.career_event
    ADD CONSTRAINT pk_career_event PRIMARY KEY (id);


--
-- Name: country pk_country; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.country
    ADD CONSTRAINT pk_country PRIMARY KEY (code);


--
-- Name: email_log pk_email_log; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_log
    ADD CONSTRAINT pk_email_log PRIMARY KEY (id);


--
-- Name: email_message pk_email_message; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_message
    ADD CONSTRAINT pk_email_message PRIMARY KEY (id);


--
-- Name: epr_aggregate pk_epr_aggregate; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.epr_aggregate
    ADD CONSTRAINT pk_epr_aggregate PRIMARY KEY (id);


--
-- Name: ex_officio_mandate pk_ex_officio_mandate; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ex_officio_mandate
    ADD CONSTRAINT pk_ex_officio_mandate PRIMARY KEY (id);


--
-- Name: funding_agency pk_funding_agency; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.funding_agency
    ADD CONSTRAINT pk_funding_agency PRIMARY KEY (id);


--
-- Name: image_upload pk_image_upload; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.image_upload
    ADD CONSTRAINT pk_image_upload PRIMARY KEY (id);


--
-- Name: institute pk_institute; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.institute
    ADD CONSTRAINT pk_institute PRIMARY KEY (id);


--
-- Name: institute_leader pk_institute_leader; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.institute_leader
    ADD CONSTRAINT pk_institute_leader PRIMARY KEY (id);


--
-- Name: institute_status pk_institute_status; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.institute_status
    ADD CONSTRAINT pk_institute_status PRIMARY KEY (id);


--
-- Name: legacy_flag pk_legacy_flag; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.legacy_flag
    ADD CONSTRAINT pk_legacy_flag PRIMARY KEY (id);


--
-- Name: manager_history pk_manager_history; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.manager_history
    ADD CONSTRAINT pk_manager_history PRIMARY KEY (id);


--
-- Name: markdown_note pk_markdown_note; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.markdown_note
    ADD CONSTRAINT pk_markdown_note PRIMARY KEY (id);


--
-- Name: mo pk_mo; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mo
    ADD CONSTRAINT pk_mo PRIMARY KEY (id);


--
-- Name: org_unit pk_org_unit; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.org_unit
    ADD CONSTRAINT pk_org_unit PRIMARY KEY (id);


--
-- Name: org_unit_type pk_org_unit_type; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.org_unit_type
    ADD CONSTRAINT pk_org_unit_type PRIMARY KEY (id);


--
-- Name: person pk_person; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT pk_person PRIMARY KEY (id);


--
-- Name: person_status pk_person_status; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.person_status
    ADD CONSTRAINT pk_person_status PRIMARY KEY (id);


--
-- Name: position pk_position; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."position"
    ADD CONSTRAINT pk_position PRIMARY KEY (id);


--
-- Name: prehistory_timeline pk_prehistory_timeline; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.prehistory_timeline
    ADD CONSTRAINT pk_prehistory_timeline PRIMARY KEY (id);


--
-- Name: project pk_project; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT pk_project PRIMARY KEY (id);


--
-- Name: project_lifespan pk_project_lifespan; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.project_lifespan
    ADD CONSTRAINT pk_project_lifespan PRIMARY KEY (id);


--
-- Name: request pk_request; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.request
    ADD CONSTRAINT pk_request PRIMARY KEY (id);


--
-- Name: request_step pk_request_step; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.request_step
    ADD CONSTRAINT pk_request_step PRIMARY KEY (id);


--
-- Name: row_change pk_row_change; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.row_change
    ADD CONSTRAINT pk_row_change PRIMARY KEY (id);


--
-- Name: tenure pk_tenure; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tenure
    ADD CONSTRAINT pk_tenure PRIMARY KEY (id);


--
-- Name: user_author_data pk_user_author_data; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_author_data
    ADD CONSTRAINT pk_user_author_data PRIMARY KEY (id);


--
-- Name: user_preferences pk_user_preferences; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_preferences
    ADD CONSTRAINT pk_user_preferences PRIMARY KEY (id);


--
-- Name: position position_name_unit_type_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."position"
    ADD CONSTRAINT position_name_unit_type_id UNIQUE (name, unit_type_id);


--
-- Name: org_unit unit_type_domain_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.org_unit
    ADD CONSTRAINT unit_type_domain_key UNIQUE (type_id, domain);


--
-- Name: application_asset uq_application_asset_name; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.application_asset
    ADD CONSTRAINT uq_application_asset_name UNIQUE (name);


--
-- Name: country uq_country_name; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.country
    ADD CONSTRAINT uq_country_name UNIQUE (name);


--
-- Name: image_upload uq_image_upload_name; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.image_upload
    ADD CONSTRAINT uq_image_upload_name UNIQUE (name);


--
-- Name: institute uq_institute_code; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.institute
    ADD CONSTRAINT uq_institute_code UNIQUE (code);


--
-- Name: legacy_flag uq_legacy_flag_remote_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.legacy_flag
    ADD CONSTRAINT uq_legacy_flag_remote_id UNIQUE (remote_id);


--
-- Name: markdown_note uq_markdown_note_name; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.markdown_note
    ADD CONSTRAINT uq_markdown_note_name UNIQUE (name);


--
-- Name: person uq_person_cms_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT uq_person_cms_id UNIQUE (cms_id);


--
-- Name: project uq_project_code; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT uq_project_code UNIQUE (code);


--
-- Name: alembic_version alembic_version_pkc; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.alembic_version
    ADD CONSTRAINT alembic_version_pkc PRIMARY KEY (version_num);


--
-- Name: access_class pk_access_class; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.access_class
    ADD CONSTRAINT pk_access_class PRIMARY KEY (id);


--
-- Name: activity_check pk_activity_check; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.activity_check
    ADD CONSTRAINT pk_activity_check PRIMARY KEY (id);


--
-- Name: author_list_files pk_author_list_files; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.author_list_files
    ADD CONSTRAINT pk_author_list_files PRIMARY KEY (id);


--
-- Name: authorship_application_check pk_authorship_application_check; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.authorship_application_check
    ADD CONSTRAINT pk_authorship_application_check PRIMARY KEY (id);


--
-- Name: cern_country pk_cern_country; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.cern_country
    ADD CONSTRAINT pk_cern_country PRIMARY KEY (id);


--
-- Name: cern_country_status pk_cern_country_status; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.cern_country_status
    ADD CONSTRAINT pk_cern_country_status PRIMARY KEY (id);


--
-- Name: cms_project pk_cms_project; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.cms_project
    ADD CONSTRAINT pk_cms_project PRIMARY KEY (code);


--
-- Name: cms_week pk_cms_week; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.cms_week
    ADD CONSTRAINT pk_cms_week PRIMARY KEY (id);


--
-- Name: ext_author pk_ext_author; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.ext_author
    ADD CONSTRAINT pk_ext_author PRIMARY KEY (id);


--
-- Name: ext_author_flag pk_ext_author_flag; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.ext_author_flag
    ADD CONSTRAINT pk_ext_author_flag PRIMARY KEY (id);


--
-- Name: ext_author_project pk_ext_author_project; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.ext_author_project
    ADD CONSTRAINT pk_ext_author_project PRIMARY KEY (id);


--
-- Name: ext_authors_flags_rel pk_ext_authors_flags_rel; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.ext_authors_flags_rel
    ADD CONSTRAINT pk_ext_authors_flags_rel PRIMARY KEY (author_id, flag_id);


--
-- Name: room_week_rel pk_external_week_room_choice; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.room_week_rel
    ADD CONSTRAINT pk_external_week_room_choice PRIMARY KEY (id);


--
-- Name: permission pk_permission; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.permission
    ADD CONSTRAINT pk_permission PRIMARY KEY (id);


--
-- Name: restricted_resource pk_restricted_resource; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.restricted_resource
    ADD CONSTRAINT pk_restricted_resource PRIMARY KEY (id);


--
-- Name: room pk_room; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.room
    ADD CONSTRAINT pk_room PRIMARY KEY (id);


--
-- Name: room_request pk_room_request; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.room_request
    ADD CONSTRAINT pk_room_request PRIMARY KEY (id);


--
-- Name: room_request_convener pk_room_request_convener; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.room_request_convener
    ADD CONSTRAINT pk_room_request_convener PRIMARY KEY (id);


--
-- Name: status pk_status; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.status
    ADD CONSTRAINT pk_status PRIMARY KEY (code);


--
-- Name: vote_delegation pk_vote_delegation; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.vote_delegation
    ADD CONSTRAINT pk_vote_delegation PRIMARY KEY (id);


--
-- Name: voting pk_voting; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.voting
    ADD CONSTRAINT pk_voting PRIMARY KEY (code);


--
-- Name: voting_list_entry pk_voting_list_entry; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.voting_list_entry
    ADD CONSTRAINT pk_voting_list_entry PRIMARY KEY (id);


--
-- Name: voting_merger pk_voting_merger; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.voting_merger
    ADD CONSTRAINT pk_voting_merger PRIMARY KEY (id);


--
-- Name: voting_merger_member pk_voting_merger_member; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.voting_merger_member
    ADD CONSTRAINT pk_voting_merger_member PRIMARY KEY (merger_id, inst_code);


--
-- Name: access_class uq_access_class_name; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.access_class
    ADD CONSTRAINT uq_access_class_name UNIQUE (name);


--
-- Name: cms_project uq_cms_project_name; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.cms_project
    ADD CONSTRAINT uq_cms_project_name UNIQUE (name);


--
-- Name: restricted_resource uq_restricted_resource_key; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.restricted_resource
    ADD CONSTRAINT uq_restricted_resource_key UNIQUE (key);


--
-- Name: room uq_room_indico_id; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.room
    ADD CONSTRAINT uq_room_indico_id UNIQUE (indico_id);


--
-- Name: voting uq_voting_code; Type: CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.voting
    ADD CONSTRAINT uq_voting_code UNIQUE (code);


--
-- Name: idx_tlu_inst_code; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX idx_tlu_inst_code ON epr.time_line_user USING btree (inst_code);


--
-- Name: idx_tlu_year_inst_code; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX idx_tlu_year_inst_code ON epr.time_line_user USING btree (year, inst_code);


--
-- Name: ix_activity_transaction_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_activity_transaction_id ON epr.activity USING btree (transaction_id);


--
-- Name: ix_ceilings_version_end_transaction_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_ceilings_version_end_transaction_id ON epr.ceilings_version USING btree (end_transaction_id);


--
-- Name: ix_ceilings_version_operation_type; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_ceilings_version_operation_type ON epr.ceilings_version USING btree (operation_type);


--
-- Name: ix_ceilings_version_transaction_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_ceilings_version_transaction_id ON epr.ceilings_version USING btree (transaction_id);


--
-- Name: ix_cms_activities_version_end_transaction_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_cms_activities_version_end_transaction_id ON epr.cms_activities_version USING btree (end_transaction_id);


--
-- Name: ix_cms_activities_version_operation_type; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_cms_activities_version_operation_type ON epr.cms_activities_version USING btree (operation_type);


--
-- Name: ix_cms_activities_version_transaction_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_cms_activities_version_transaction_id ON epr.cms_activities_version USING btree (transaction_id);


--
-- Name: ix_insts_version_end_transaction_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_insts_version_end_transaction_id ON epr.insts_version USING btree (end_transaction_id);


--
-- Name: ix_insts_version_operation_type; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_insts_version_operation_type ON epr.insts_version USING btree (operation_type);


--
-- Name: ix_insts_version_transaction_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_insts_version_transaction_id ON epr.insts_version USING btree (transaction_id);


--
-- Name: ix_managers_version_end_transaction_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_managers_version_end_transaction_id ON epr.managers_version USING btree (end_transaction_id);


--
-- Name: ix_managers_version_operation_type; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_managers_version_operation_type ON epr.managers_version USING btree (operation_type);


--
-- Name: ix_managers_version_transaction_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_managers_version_transaction_id ON epr.managers_version USING btree (transaction_id);


--
-- Name: ix_pledges_version_end_transaction_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_pledges_version_end_transaction_id ON epr.pledges_version USING btree (end_transaction_id);


--
-- Name: ix_pledges_version_operation_type; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_pledges_version_operation_type ON epr.pledges_version USING btree (operation_type);


--
-- Name: ix_pledges_version_transaction_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_pledges_version_transaction_id ON epr.pledges_version USING btree (transaction_id);


--
-- Name: ix_projects_version_end_transaction_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_projects_version_end_transaction_id ON epr.projects_version USING btree (end_transaction_id);


--
-- Name: ix_projects_version_operation_type; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_projects_version_operation_type ON epr.projects_version USING btree (operation_type);


--
-- Name: ix_projects_version_transaction_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_projects_version_transaction_id ON epr.projects_version USING btree (transaction_id);


--
-- Name: ix_roles_default; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_roles_default ON epr.roles USING btree ("default");


--
-- Name: ix_shift_task_map_version_end_transaction_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_shift_task_map_version_end_transaction_id ON epr.shift_task_map_version USING btree (end_transaction_id);


--
-- Name: ix_shift_task_map_version_operation_type; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_shift_task_map_version_operation_type ON epr.shift_task_map_version USING btree (operation_type);


--
-- Name: ix_shift_task_map_version_transaction_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_shift_task_map_version_transaction_id ON epr.shift_task_map_version USING btree (transaction_id);


--
-- Name: ix_tasks_version_end_transaction_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_tasks_version_end_transaction_id ON epr.tasks_version USING btree (end_transaction_id);


--
-- Name: ix_tasks_version_operation_type; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_tasks_version_operation_type ON epr.tasks_version USING btree (operation_type);


--
-- Name: ix_tasks_version_transaction_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_tasks_version_transaction_id ON epr.tasks_version USING btree (transaction_id);


--
-- Name: ix_time_line_inst_version_end_transaction_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_time_line_inst_version_end_transaction_id ON epr.time_line_inst_version USING btree (end_transaction_id);


--
-- Name: ix_time_line_inst_version_operation_type; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_time_line_inst_version_operation_type ON epr.time_line_inst_version USING btree (operation_type);


--
-- Name: ix_time_line_inst_version_transaction_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_time_line_inst_version_transaction_id ON epr.time_line_inst_version USING btree (transaction_id);


--
-- Name: ix_time_line_user_version_end_transaction_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_time_line_user_version_end_transaction_id ON epr.time_line_user_version USING btree (end_transaction_id);


--
-- Name: ix_time_line_user_version_operation_type; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_time_line_user_version_operation_type ON epr.time_line_user_version USING btree (operation_type);


--
-- Name: ix_time_line_user_version_transaction_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_time_line_user_version_transaction_id ON epr.time_line_user_version USING btree (transaction_id);


--
-- Name: ix_transaction_user_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_transaction_user_id ON epr.transaction USING btree (user_id);


--
-- Name: ix_userinst_version_end_transaction_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_userinst_version_end_transaction_id ON epr.userinst_version USING btree (end_transaction_id);


--
-- Name: ix_userinst_version_operation_type; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_userinst_version_operation_type ON epr.userinst_version USING btree (operation_type);


--
-- Name: ix_userinst_version_transaction_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_userinst_version_transaction_id ON epr.userinst_version USING btree (transaction_id);


--
-- Name: ix_users_version_end_transaction_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_users_version_end_transaction_id ON epr.users_version USING btree (end_transaction_id);


--
-- Name: ix_users_version_operation_type; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_users_version_operation_type ON epr.users_version USING btree (operation_type);


--
-- Name: ix_users_version_transaction_id; Type: INDEX; Schema: epr; Owner: -
--

CREATE INDEX ix_users_version_transaction_id ON epr.users_version USING btree (transaction_id);


--
-- Name: ix_public_prehistory_timeline_cms_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX ix_public_prehistory_timeline_cms_id ON public.prehistory_timeline USING btree (cms_id);


--
-- Name: review_committee_member fk_review_committee_member_rc_id_review_committee; Type: FK CONSTRAINT; Schema: cadi; Owner: -
--

ALTER TABLE ONLY cadi.review_committee_member
    ADD CONSTRAINT fk_review_committee_member_rc_id_review_committee FOREIGN KEY (rc_id) REFERENCES cadi.review_committee(id);


--
-- Name: cms_activities cms_activities_project_id_fkey; Type: FK CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.cms_activities
    ADD CONSTRAINT cms_activities_project_id_fkey FOREIGN KEY (project_id) REFERENCES epr.projects(id);


--
-- Name: epr_due epr_due_inst_id_fkey; Type: FK CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.epr_due
    ADD CONSTRAINT epr_due_inst_id_fkey FOREIGN KEY (inst_id) REFERENCES epr.insts(id);


--
-- Name: epr_due epr_due_user_id_fkey; Type: FK CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.epr_due
    ADD CONSTRAINT epr_due_user_id_fkey FOREIGN KEY (user_id) REFERENCES epr.users(id);


--
-- Name: tasks fk_tasks_level3_id_level3names; Type: FK CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.tasks
    ADD CONSTRAINT fk_tasks_level3_id_level3names FOREIGN KEY (level3_id) REFERENCES epr.level3names(id);


--
-- Name: managers managers_userid_fkey; Type: FK CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.managers
    ADD CONSTRAINT managers_userid_fkey FOREIGN KEY (userid) REFERENCES epr.users(id);


--
-- Name: pledges pledges_inst_id_fkey; Type: FK CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.pledges
    ADD CONSTRAINT pledges_inst_id_fkey FOREIGN KEY (inst_id) REFERENCES epr.insts(id);


--
-- Name: pledges pledges_task_id_fkey; Type: FK CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.pledges
    ADD CONSTRAINT pledges_task_id_fkey FOREIGN KEY (task_id) REFERENCES epr.tasks(id);


--
-- Name: pledges pledges_user_id_fkey; Type: FK CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.pledges
    ADD CONSTRAINT pledges_user_id_fkey FOREIGN KEY (user_id) REFERENCES epr.users(id);


--
-- Name: shift_task_map shift_task_map_task_code_fkey; Type: FK CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.shift_task_map
    ADD CONSTRAINT shift_task_map_task_code_fkey FOREIGN KEY (task_code) REFERENCES epr.tasks(code);


--
-- Name: shifts shifts_shifter_id_fkey; Type: FK CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.shifts
    ADD CONSTRAINT shifts_shifter_id_fkey FOREIGN KEY (shifter_id) REFERENCES epr.users(hrid);


--
-- Name: tasks tasks_activity_id_fkey; Type: FK CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.tasks
    ADD CONSTRAINT tasks_activity_id_fkey FOREIGN KEY (activity_id) REFERENCES epr.cms_activities(id);


--
-- Name: time_line_inst time_line_inst_inst_code_fkey; Type: FK CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.time_line_inst
    ADD CONSTRAINT time_line_inst_inst_code_fkey FOREIGN KEY (inst_code) REFERENCES epr.insts(code) ON UPDATE CASCADE;


--
-- Name: time_line_user time_line_user_category_fkey; Type: FK CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.time_line_user
    ADD CONSTRAINT time_line_user_category_fkey FOREIGN KEY (category) REFERENCES epr.categories(id);


--
-- Name: time_line_user time_line_user_inst_code_fkey; Type: FK CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.time_line_user
    ADD CONSTRAINT time_line_user_inst_code_fkey FOREIGN KEY (inst_code) REFERENCES epr.insts(code);


--
-- Name: time_line_user time_line_user_user_cms_id_fkey; Type: FK CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.time_line_user
    ADD CONSTRAINT time_line_user_user_cms_id_fkey FOREIGN KEY (user_cms_id) REFERENCES epr.users(cmsid);


--
-- Name: transaction transaction_user_id_fkey; Type: FK CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.transaction
    ADD CONSTRAINT transaction_user_id_fkey FOREIGN KEY (user_id) REFERENCES epr.users(id);


--
-- Name: userinst userinst_inst_id_fkey; Type: FK CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.userinst
    ADD CONSTRAINT userinst_inst_id_fkey FOREIGN KEY (inst_id) REFERENCES epr.insts(id);


--
-- Name: userinst userinst_user_id_fkey; Type: FK CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.userinst
    ADD CONSTRAINT userinst_user_id_fkey FOREIGN KEY (user_id) REFERENCES epr.users(id);


--
-- Name: users users_category_fkey; Type: FK CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.users
    ADD CONSTRAINT users_category_fkey FOREIGN KEY (category) REFERENCES epr.categories(id);


--
-- Name: users users_main_inst_fkey; Type: FK CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.users
    ADD CONSTRAINT users_main_inst_fkey FOREIGN KEY (main_inst) REFERENCES epr.insts(id);


--
-- Name: users users_role_id_fkey; Type: FK CONSTRAINT; Schema: epr; Owner: -
--

ALTER TABLE ONLY epr.users
    ADD CONSTRAINT users_role_id_fkey FOREIGN KEY (role_id) REFERENCES epr.roles(id);


--
-- Name: affiliation fk_affiliation_cms_id_person; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.affiliation
    ADD CONSTRAINT fk_affiliation_cms_id_person FOREIGN KEY (cms_id) REFERENCES public.person(cms_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: affiliation fk_affiliation_inst_code_institute; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.affiliation
    ADD CONSTRAINT fk_affiliation_inst_code_institute FOREIGN KEY (inst_code) REFERENCES public.institute(code) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: assignment fk_assignment_cms_id_person; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.assignment
    ADD CONSTRAINT fk_assignment_cms_id_person FOREIGN KEY (cms_id) REFERENCES public.person(cms_id);


--
-- Name: assignment fk_assignment_project_code_project; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.assignment
    ADD CONSTRAINT fk_assignment_project_code_project FOREIGN KEY (project_code) REFERENCES public.project(code);


--
-- Name: career_event fk_career_event_cms_id_person; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.career_event
    ADD CONSTRAINT fk_career_event_cms_id_person FOREIGN KEY (cms_id) REFERENCES public.person(cms_id);


--
-- Name: email_log fk_email_log_email_id_email_message; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_log
    ADD CONSTRAINT fk_email_log_email_id_email_message FOREIGN KEY (email_id) REFERENCES public.email_message(id) ON DELETE CASCADE;


--
-- Name: epr_aggregate fk_epr_aggregate_cms_id_person; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.epr_aggregate
    ADD CONSTRAINT fk_epr_aggregate_cms_id_person FOREIGN KEY (cms_id) REFERENCES public.person(cms_id);


--
-- Name: epr_aggregate fk_epr_aggregate_inst_code_institute; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.epr_aggregate
    ADD CONSTRAINT fk_epr_aggregate_inst_code_institute FOREIGN KEY (inst_code) REFERENCES public.institute(code);


--
-- Name: ex_officio_mandate fk_ex_officio_mandate_dst_unit_id_org_unit; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ex_officio_mandate
    ADD CONSTRAINT fk_ex_officio_mandate_dst_unit_id_org_unit FOREIGN KEY (dst_unit_id) REFERENCES public.org_unit(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: ex_officio_mandate fk_ex_officio_mandate_src_unit_id_org_unit; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ex_officio_mandate
    ADD CONSTRAINT fk_ex_officio_mandate_src_unit_id_org_unit FOREIGN KEY (src_unit_id) REFERENCES public.org_unit(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: funding_agency fk_funding_agency_country_code_country; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.funding_agency
    ADD CONSTRAINT fk_funding_agency_country_code_country FOREIGN KEY (country_code) REFERENCES public.country(code) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: institute_leader fk_institute_leader_cms_id_person; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.institute_leader
    ADD CONSTRAINT fk_institute_leader_cms_id_person FOREIGN KEY (cms_id) REFERENCES public.person(cms_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: institute_leader fk_institute_leader_inst_code_institute; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.institute_leader
    ADD CONSTRAINT fk_institute_leader_inst_code_institute FOREIGN KEY (inst_code) REFERENCES public.institute(code) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: institute_status fk_institute_status_code_institute; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.institute_status
    ADD CONSTRAINT fk_institute_status_code_institute FOREIGN KEY (code) REFERENCES public.institute(code) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: legacy_flag fk_legacy_flag_cms_id_person; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.legacy_flag
    ADD CONSTRAINT fk_legacy_flag_cms_id_person FOREIGN KEY (cms_id) REFERENCES public.person(cms_id);


--
-- Name: manager_history fk_manager_history_cms_id_person; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.manager_history
    ADD CONSTRAINT fk_manager_history_cms_id_person FOREIGN KEY (cms_id) REFERENCES public.person(cms_id);


--
-- Name: manager_history fk_manager_history_inst_code_institute; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.manager_history
    ADD CONSTRAINT fk_manager_history_inst_code_institute FOREIGN KEY (inst_code) REFERENCES public.institute(code);


--
-- Name: mo fk_mo_cms_id_person; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mo
    ADD CONSTRAINT fk_mo_cms_id_person FOREIGN KEY (cms_id) REFERENCES public.person(cms_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: mo fk_mo_fa_id_funding_agency; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mo
    ADD CONSTRAINT fk_mo_fa_id_funding_agency FOREIGN KEY (fa_id) REFERENCES public.funding_agency(id) ON UPDATE CASCADE;


--
-- Name: mo fk_mo_inst_code_institute; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mo
    ADD CONSTRAINT fk_mo_inst_code_institute FOREIGN KEY (inst_code) REFERENCES public.institute(code) ON UPDATE CASCADE;


--
-- Name: org_unit fk_org_unit_enclosing_unit_id_org_unit; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.org_unit
    ADD CONSTRAINT fk_org_unit_enclosing_unit_id_org_unit FOREIGN KEY (enclosing_unit_id) REFERENCES public.org_unit(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: org_unit fk_org_unit_outermost_unit_id_org_unit; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.org_unit
    ADD CONSTRAINT fk_org_unit_outermost_unit_id_org_unit FOREIGN KEY (outermost_unit_id) REFERENCES public.org_unit(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: org_unit fk_org_unit_superior_unit_id_org_unit; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.org_unit
    ADD CONSTRAINT fk_org_unit_superior_unit_id_org_unit FOREIGN KEY (superior_unit_id) REFERENCES public.org_unit(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: org_unit fk_org_unit_type_id_org_unit_type; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.org_unit
    ADD CONSTRAINT fk_org_unit_type_id_org_unit_type FOREIGN KEY (type_id) REFERENCES public.org_unit_type(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: person fk_person_nationality_country; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT fk_person_nationality_country FOREIGN KEY (nationality) REFERENCES public.country(code) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: person_status fk_person_status_cms_id_person; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.person_status
    ADD CONSTRAINT fk_person_status_cms_id_person FOREIGN KEY (cms_id) REFERENCES public.person(cms_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: position fk_position_unit_type_id_org_unit_type; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."position"
    ADD CONSTRAINT fk_position_unit_type_id_org_unit_type FOREIGN KEY (unit_type_id) REFERENCES public.org_unit_type(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: prehistory_timeline fk_prehistory_timeline_cms_id_person; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.prehistory_timeline
    ADD CONSTRAINT fk_prehistory_timeline_cms_id_person FOREIGN KEY (cms_id) REFERENCES public.person(cms_id);


--
-- Name: prehistory_timeline fk_prehistory_timeline_inst_code_institute; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.prehistory_timeline
    ADD CONSTRAINT fk_prehistory_timeline_inst_code_institute FOREIGN KEY (inst_code) REFERENCES public.institute(code);


--
-- Name: project_lifespan fk_project_lifespan_code_project; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.project_lifespan
    ADD CONSTRAINT fk_project_lifespan_code_project FOREIGN KEY (code) REFERENCES public.project(code);


--
-- Name: request fk_request_creator_cms_id_person; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.request
    ADD CONSTRAINT fk_request_creator_cms_id_person FOREIGN KEY (creator_cms_id) REFERENCES public.person(cms_id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: request_step fk_request_step_creator_cms_id_person; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.request_step
    ADD CONSTRAINT fk_request_step_creator_cms_id_person FOREIGN KEY (creator_cms_id) REFERENCES public.person(cms_id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: request_step fk_request_step_request_id_request; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.request_step
    ADD CONSTRAINT fk_request_step_request_id_request FOREIGN KEY (request_id) REFERENCES public.request(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: request_step fk_request_step_updater_cms_id_person; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.request_step
    ADD CONSTRAINT fk_request_step_updater_cms_id_person FOREIGN KEY (updater_cms_id) REFERENCES public.person(cms_id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tenure fk_tenure_cms_id_person; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tenure
    ADD CONSTRAINT fk_tenure_cms_id_person FOREIGN KEY (cms_id) REFERENCES public.person(cms_id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: tenure fk_tenure_position_id_position; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tenure
    ADD CONSTRAINT fk_tenure_position_id_position FOREIGN KEY (position_id) REFERENCES public."position"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tenure fk_tenure_unit_id_org_unit; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tenure
    ADD CONSTRAINT fk_tenure_unit_id_org_unit FOREIGN KEY (unit_id) REFERENCES public.org_unit(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: activity_check fk_activity_check_by_cms_id_person; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.activity_check
    ADD CONSTRAINT fk_activity_check_by_cms_id_person FOREIGN KEY (by_cms_id) REFERENCES public.person(cms_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: activity_check fk_activity_check_for_cms_id_person; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.activity_check
    ADD CONSTRAINT fk_activity_check_for_cms_id_person FOREIGN KEY (for_cms_id) REFERENCES public.person(cms_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: authorship_application_check fk_authorship_application_check_cms_id_person; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.authorship_application_check
    ADD CONSTRAINT fk_authorship_application_check_cms_id_person FOREIGN KEY (cms_id) REFERENCES public.person(cms_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: authorship_application_check fk_authorship_application_check_notification_id_email_message; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.authorship_application_check
    ADD CONSTRAINT fk_authorship_application_check_notification_id_email_message FOREIGN KEY (notification_id) REFERENCES public.email_message(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: cern_country fk_cern_country_status_id_cern_country_status; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.cern_country
    ADD CONSTRAINT fk_cern_country_status_id_cern_country_status FOREIGN KEY (status_id) REFERENCES toolkit.cern_country_status(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: cms_project fk_cms_project_status_status; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.cms_project
    ADD CONSTRAINT fk_cms_project_status_status FOREIGN KEY (status) REFERENCES toolkit.status(code) ON UPDATE CASCADE;


--
-- Name: ext_author fk_ext_author_cms_id_person; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.ext_author
    ADD CONSTRAINT fk_ext_author_cms_id_person FOREIGN KEY (cms_id) REFERENCES public.person(cms_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: ext_author_flag fk_ext_author_flag_project_id_ext_author_project; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.ext_author_flag
    ADD CONSTRAINT fk_ext_author_flag_project_id_ext_author_project FOREIGN KEY (project_id) REFERENCES toolkit.ext_author_project(id) ON DELETE CASCADE;


--
-- Name: ext_author fk_ext_author_inst_code_also_institute; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.ext_author
    ADD CONSTRAINT fk_ext_author_inst_code_also_institute FOREIGN KEY (inst_code_also) REFERENCES public.institute(code) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: ext_author fk_ext_author_inst_code_institute; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.ext_author
    ADD CONSTRAINT fk_ext_author_inst_code_institute FOREIGN KEY (inst_code) REFERENCES public.institute(code) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: ext_author fk_ext_author_inst_code_now_institute; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.ext_author
    ADD CONSTRAINT fk_ext_author_inst_code_now_institute FOREIGN KEY (inst_code_now) REFERENCES public.institute(code) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: ext_author fk_ext_author_project_id_ext_author_project; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.ext_author
    ADD CONSTRAINT fk_ext_author_project_id_ext_author_project FOREIGN KEY (project_id) REFERENCES toolkit.ext_author_project(id) ON DELETE CASCADE;


--
-- Name: ext_authors_flags_rel fk_ext_authors_flags_rel_author_id_ext_author; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.ext_authors_flags_rel
    ADD CONSTRAINT fk_ext_authors_flags_rel_author_id_ext_author FOREIGN KEY (author_id) REFERENCES toolkit.ext_author(id) ON DELETE CASCADE;


--
-- Name: ext_authors_flags_rel fk_ext_authors_flags_rel_flag_id_ext_author_flag; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.ext_authors_flags_rel
    ADD CONSTRAINT fk_ext_authors_flags_rel_flag_id_ext_author_flag FOREIGN KEY (flag_id) REFERENCES toolkit.ext_author_flag(id) ON DELETE CASCADE;


--
-- Name: permission fk_permission_access_class_id_access_class; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.permission
    ADD CONSTRAINT fk_permission_access_class_id_access_class FOREIGN KEY (access_class_id) REFERENCES toolkit.access_class(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: permission fk_permission_resource_id_restricted_resource; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.permission
    ADD CONSTRAINT fk_permission_resource_id_restricted_resource FOREIGN KEY (resource_id) REFERENCES toolkit.restricted_resource(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: room_request fk_room_request_cms_id_by_person; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.room_request
    ADD CONSTRAINT fk_room_request_cms_id_by_person FOREIGN KEY (cms_id_by) REFERENCES public.person(cms_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: room_request fk_room_request_cms_id_for_person; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.room_request
    ADD CONSTRAINT fk_room_request_cms_id_for_person FOREIGN KEY (cms_id_for) REFERENCES public.person(cms_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: room_request fk_room_request_cms_week_id_cms_week; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.room_request
    ADD CONSTRAINT fk_room_request_cms_week_id_cms_week FOREIGN KEY (cms_week_id) REFERENCES toolkit.cms_week(id) ON DELETE CASCADE;


--
-- Name: room_request_convener fk_room_request_convener_cms_id_person; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.room_request_convener
    ADD CONSTRAINT fk_room_request_convener_cms_id_person FOREIGN KEY (cms_id) REFERENCES public.person(cms_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: room_request_convener fk_room_request_convener_request_id_room_request; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.room_request_convener
    ADD CONSTRAINT fk_room_request_convener_request_id_room_request FOREIGN KEY (request_id) REFERENCES toolkit.room_request(id) ON DELETE CASCADE;


--
-- Name: room_request fk_room_request_project_cms_project; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.room_request
    ADD CONSTRAINT fk_room_request_project_cms_project FOREIGN KEY (project) REFERENCES toolkit.cms_project(code) ON UPDATE CASCADE;


--
-- Name: room_request fk_room_request_room_id_preferred_room; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.room_request
    ADD CONSTRAINT fk_room_request_room_id_preferred_room FOREIGN KEY (room_id_preferred) REFERENCES toolkit.room(id);


--
-- Name: room_request fk_room_request_room_id_room; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.room_request
    ADD CONSTRAINT fk_room_request_room_id_room FOREIGN KEY (room_id) REFERENCES toolkit.room(id);


--
-- Name: room_request fk_room_request_status_status; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.room_request
    ADD CONSTRAINT fk_room_request_status_status FOREIGN KEY (status) REFERENCES toolkit.status(code) ON UPDATE CASCADE;


--
-- Name: room_week_rel fk_room_week_rel_room_id_room; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.room_week_rel
    ADD CONSTRAINT fk_room_week_rel_room_id_room FOREIGN KEY (room_id) REFERENCES toolkit.room(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: room_week_rel fk_room_week_rel_week_id_cms_week; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.room_week_rel
    ADD CONSTRAINT fk_room_week_rel_week_id_cms_week FOREIGN KEY (week_id) REFERENCES toolkit.cms_week(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: vote_delegation fk_vote_delegation_cms_id_from_person; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.vote_delegation
    ADD CONSTRAINT fk_vote_delegation_cms_id_from_person FOREIGN KEY (cms_id_from) REFERENCES public.person(cms_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: vote_delegation fk_vote_delegation_cms_id_to_person; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.vote_delegation
    ADD CONSTRAINT fk_vote_delegation_cms_id_to_person FOREIGN KEY (cms_id_to) REFERENCES public.person(cms_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: vote_delegation fk_vote_delegation_specific_inst_code_institute; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.vote_delegation
    ADD CONSTRAINT fk_vote_delegation_specific_inst_code_institute FOREIGN KEY (specific_inst_code) REFERENCES public.institute(code);


--
-- Name: vote_delegation fk_vote_delegation_status_status; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.vote_delegation
    ADD CONSTRAINT fk_vote_delegation_status_status FOREIGN KEY (status) REFERENCES toolkit.status(code) ON UPDATE CASCADE;


--
-- Name: vote_delegation fk_vote_delegation_voting_code_voting; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.vote_delegation
    ADD CONSTRAINT fk_vote_delegation_voting_code_voting FOREIGN KEY (voting_code) REFERENCES toolkit.voting(code) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: voting_list_entry fk_voting_list_entry_cms_id_person; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.voting_list_entry
    ADD CONSTRAINT fk_voting_list_entry_cms_id_person FOREIGN KEY (cms_id) REFERENCES public.person(cms_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: voting_list_entry fk_voting_list_entry_code_voting; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.voting_list_entry
    ADD CONSTRAINT fk_voting_list_entry_code_voting FOREIGN KEY (code) REFERENCES toolkit.voting(code) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: voting_list_entry fk_voting_list_entry_delegated_by_cms_id_person; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.voting_list_entry
    ADD CONSTRAINT fk_voting_list_entry_delegated_by_cms_id_person FOREIGN KEY (delegated_by_cms_id) REFERENCES public.person(cms_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: voting_list_entry fk_voting_list_entry_represented_inst_code_institute; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.voting_list_entry
    ADD CONSTRAINT fk_voting_list_entry_represented_inst_code_institute FOREIGN KEY (represented_inst_code) REFERENCES public.institute(code) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: voting_list_entry fk_voting_list_entry_represented_merger_id_voting_merger; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.voting_list_entry
    ADD CONSTRAINT fk_voting_list_entry_represented_merger_id_voting_merger FOREIGN KEY (represented_merger_id) REFERENCES toolkit.voting_merger(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: voting_merger_member fk_voting_merger_member_inst_code_institute; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.voting_merger_member
    ADD CONSTRAINT fk_voting_merger_member_inst_code_institute FOREIGN KEY (inst_code) REFERENCES public.institute(code) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: voting_merger_member fk_voting_merger_member_merger_id_voting_merger; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.voting_merger_member
    ADD CONSTRAINT fk_voting_merger_member_merger_id_voting_merger FOREIGN KEY (merger_id) REFERENCES toolkit.voting_merger(id);


--
-- Name: voting_merger fk_voting_merger_representative_cms_id_person; Type: FK CONSTRAINT; Schema: toolkit; Owner: -
--

ALTER TABLE ONLY toolkit.voting_merger
    ADD CONSTRAINT fk_voting_merger_representative_cms_id_person FOREIGN KEY (representative_cms_id) REFERENCES public.person(cms_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: SCHEMA cadi; Type: ACL; Schema: -; Owner: -
--

GRANT ALL ON SCHEMA cadi TO icms_reader;


--
-- Name: SCHEMA epr; Type: ACL; Schema: -; Owner: -
--

GRANT USAGE ON SCHEMA epr TO icms_reader;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: -
--

GRANT ALL ON SCHEMA public TO icms_reader;


--
-- Name: SCHEMA toolkit; Type: ACL; Schema: -; Owner: -
--

GRANT USAGE ON SCHEMA toolkit TO icms_reader;


--
-- Name: TABLE activity; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.activity TO icms_reader;


--
-- Name: TABLE alembic_version; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.alembic_version TO icms_reader;


--
-- Name: TABLE categories; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.categories TO icms_reader;


--
-- Name: TABLE ceilings; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.ceilings TO icms_reader;


--
-- Name: TABLE ceilings_version; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.ceilings_version TO icms_reader;


--
-- Name: TABLE cms_activities; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.cms_activities TO icms_reader;


--
-- Name: TABLE cms_activities_version; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.cms_activities_version TO icms_reader;


--
-- Name: TABLE epr_due; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.epr_due TO icms_reader;


--
-- Name: TABLE v_all_inst; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.v_all_inst TO icms_reader;


--
-- Name: TABLE insts; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.insts TO icms_reader;


--
-- Name: TABLE managers; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.managers TO icms_reader;


--
-- Name: TABLE pledges; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.pledges TO icms_reader;


--
-- Name: TABLE projects; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.projects TO icms_reader;


--
-- Name: TABLE roles; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.roles TO icms_reader;


--
-- Name: TABLE shifts; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.shifts TO icms_reader;


--
-- Name: TABLE shift_task_map; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.shift_task_map TO icms_reader;


--
-- Name: TABLE tasks; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.tasks TO icms_reader;


--
-- Name: TABLE time_line_inst; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.time_line_inst TO icms_reader;
GRANT INSERT,UPDATE ON TABLE epr.time_line_inst TO toolkit;


--
-- Name: TABLE time_line_user; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT,DELETE ON TABLE epr.time_line_user TO icms_reader;
GRANT INSERT,UPDATE ON TABLE epr.time_line_user TO toolkit;
GRANT DELETE ON TABLE epr.time_line_user TO icms;


--
-- Name: TABLE users; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.users TO icms_reader;


--
-- Name: TABLE userinst; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.userinst TO icms_reader;


--
-- Name: TABLE insts_version; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.insts_version TO icms_reader;


--
-- Name: TABLE level3names; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.level3names TO icms_reader;


--
-- Name: TABLE managers_version; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.managers_version TO icms_reader;


--
-- Name: TABLE pledges_version; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.pledges_version TO icms_reader;


--
-- Name: TABLE projects_version; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.projects_version TO icms_reader;


--
-- Name: TABLE shift_grid; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.shift_grid TO icms_reader;


--
-- Name: TABLE shift_task_map_version; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.shift_task_map_version TO icms_reader;


--
-- Name: TABLE shiftasks; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.shiftasks TO icms_reader;


--
-- Name: TABLE tasks_version; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.tasks_version TO icms_reader;


--
-- Name: TABLE time_line_inst_version; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.time_line_inst_version TO icms_reader;


--
-- Name: TABLE time_line_user_version; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.time_line_user_version TO icms_reader;


--
-- Name: TABLE transaction; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.transaction TO icms_reader;


--
-- Name: TABLE userinst_version; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.userinst_version TO icms_reader;


--
-- Name: TABLE users_version; Type: ACL; Schema: epr; Owner: -
--

GRANT SELECT ON TABLE epr.users_version TO icms_reader;


--
-- Name: TABLE affiliation; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.affiliation TO icms_reader;


--
-- Name: SEQUENCE affiliation_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.affiliation_id_seq TO icms_reader;


--
-- Name: TABLE alembic_version; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.alembic_version TO icms_reader;


--
-- Name: TABLE announcement; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.announcement TO icms_reader;


--
-- Name: SEQUENCE announcement_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.announcement_id_seq TO icms_reader;


--
-- Name: TABLE application_asset; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.application_asset TO icms_reader;


--
-- Name: SEQUENCE application_asset_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.application_asset_id_seq TO icms_reader;


--
-- Name: TABLE assignment; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.assignment TO icms_reader;


--
-- Name: SEQUENCE assignment_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.assignment_id_seq TO icms_reader;


--
-- Name: TABLE career_event; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.career_event TO icms_reader;


--
-- Name: SEQUENCE career_event_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.career_event_id_seq TO icms_reader;


--
-- Name: TABLE country; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.country TO icms_reader;


--
-- Name: TABLE email_log; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.email_log TO icms_reader;


--
-- Name: SEQUENCE email_log_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.email_log_id_seq TO icms_reader;


--
-- Name: TABLE email_message; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.email_message TO icms_reader;


--
-- Name: SEQUENCE email_message_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.email_message_id_seq TO icms_reader;


--
-- Name: TABLE epr_aggregate; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.epr_aggregate TO icms_reader;


--
-- Name: SEQUENCE epr_aggregate_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.epr_aggregate_id_seq TO icms_reader;


--
-- Name: TABLE ex_officio_mandate; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.ex_officio_mandate TO icms_reader;


--
-- Name: SEQUENCE ex_officio_mandate_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.ex_officio_mandate_id_seq TO icms_reader;


--
-- Name: TABLE funding_agency; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.funding_agency TO icms_reader;


--
-- Name: SEQUENCE funding_agency_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.funding_agency_id_seq TO icms_reader;


--
-- Name: TABLE image_upload; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.image_upload TO icms_reader;


--
-- Name: SEQUENCE image_upload_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.image_upload_id_seq TO icms_reader;


--
-- Name: TABLE institute; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.institute TO icms_reader;


--
-- Name: SEQUENCE institute_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.institute_id_seq TO icms_reader;


--
-- Name: TABLE institute_leader; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.institute_leader TO icms_reader;


--
-- Name: SEQUENCE institute_leader_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.institute_leader_id_seq TO icms_reader;


--
-- Name: TABLE institute_status; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.institute_status TO icms_reader;


--
-- Name: SEQUENCE institute_status_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.institute_status_id_seq TO icms_reader;


--
-- Name: TABLE legacy_flag; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.legacy_flag TO icms_reader;


--
-- Name: SEQUENCE legacy_flag_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.legacy_flag_id_seq TO icms_reader;


--
-- Name: TABLE manager_history; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE public.manager_history TO icms_reader;


--
-- Name: SEQUENCE manager_history_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.manager_history_id_seq TO icms_reader;


--
-- Name: TABLE markdown_note; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.markdown_note TO icms_reader;


--
-- Name: SEQUENCE markdown_note_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.markdown_note_id_seq TO icms_reader;


--
-- Name: TABLE mo; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.mo TO icms_reader;


--
-- Name: SEQUENCE mo_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.mo_id_seq TO icms_reader;


--
-- Name: TABLE org_unit; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.org_unit TO icms_reader;


--
-- Name: SEQUENCE org_unit_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.org_unit_id_seq TO icms_reader;


--
-- Name: TABLE org_unit_type; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.org_unit_type TO icms_reader;


--
-- Name: SEQUENCE org_unit_type_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.org_unit_type_id_seq TO icms_reader;


--
-- Name: TABLE person; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.person TO icms_reader;


--
-- Name: SEQUENCE person_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.person_id_seq TO icms_reader;


--
-- Name: TABLE person_status; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.person_status TO icms_reader;


--
-- Name: SEQUENCE person_status_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.person_status_id_seq TO icms_reader;


--
-- Name: TABLE "position"; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public."position" TO icms_reader;


--
-- Name: SEQUENCE position_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.position_id_seq TO icms_reader;


--
-- Name: TABLE prehistory_timeline; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.prehistory_timeline TO icms_reader;


--
-- Name: SEQUENCE prehistory_timeline_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.prehistory_timeline_id_seq TO icms_reader;


--
-- Name: TABLE project; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.project TO icms_reader;


--
-- Name: SEQUENCE project_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.project_id_seq TO icms_reader;


--
-- Name: TABLE project_lifespan; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.project_lifespan TO icms_reader;


--
-- Name: SEQUENCE project_lifespan_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.project_lifespan_id_seq TO icms_reader;


--
-- Name: TABLE request; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.request TO icms_reader;


--
-- Name: SEQUENCE request_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.request_id_seq TO icms_reader;


--
-- Name: TABLE request_step; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.request_step TO icms_reader;


--
-- Name: SEQUENCE request_step_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.request_step_id_seq TO icms_reader;


--
-- Name: TABLE row_change; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.row_change TO icms_reader;


--
-- Name: SEQUENCE row_change_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.row_change_id_seq TO icms_reader;


--
-- Name: TABLE tenure; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.tenure TO icms_reader;


--
-- Name: SEQUENCE tenure_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.tenure_id_seq TO icms_reader;


--
-- Name: TABLE user_author_data; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.user_author_data TO icms_reader;


--
-- Name: SEQUENCE user_author_data_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.user_author_data_id_seq TO icms_reader;


--
-- Name: TABLE user_preferences; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.user_preferences TO icms_reader;


--
-- Name: SEQUENCE user_preferences_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.user_preferences_id_seq TO icms_reader;


--
-- Name: TABLE view_people_summary; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.view_people_summary TO icms_reader;


--
-- Name: TABLE view_person_affiliations; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.view_person_affiliations TO icms_reader;


--
-- Name: TABLE view_person_flags; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.view_person_flags TO icms_reader;


--
-- Name: TABLE view_person_managed_unit_ids; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.view_person_managed_unit_ids TO icms_reader;


--
-- Name: TABLE view_person_projects; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.view_person_projects TO icms_reader;


--
-- Name: TABLE view_person_status; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.view_person_status TO icms_reader;


--
-- Name: TABLE view_person_team_duties; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.view_person_team_duties TO icms_reader;


--
-- Name: TABLE view_person; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.view_person TO icms_reader;


--
-- Name: TABLE view_person_inst_code; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE public.view_person_inst_code TO icms_reader;


--
-- Name: TABLE access_class; Type: ACL; Schema: toolkit; Owner: -
--

GRANT SELECT ON TABLE toolkit.access_class TO icms_reader;


--
-- Name: TABLE activity_check; Type: ACL; Schema: toolkit; Owner: -
--

GRANT SELECT ON TABLE toolkit.activity_check TO icms_reader;


--
-- Name: TABLE alembic_version; Type: ACL; Schema: toolkit; Owner: -
--

GRANT SELECT ON TABLE toolkit.alembic_version TO icms_reader;


--
-- Name: TABLE author_list_files; Type: ACL; Schema: toolkit; Owner: -
--

GRANT SELECT ON TABLE toolkit.author_list_files TO icms_reader;


--
-- Name: TABLE authorship_application_check; Type: ACL; Schema: toolkit; Owner: -
--

GRANT SELECT ON TABLE toolkit.authorship_application_check TO icms_reader;


--
-- Name: TABLE cern_country; Type: ACL; Schema: toolkit; Owner: -
--

GRANT SELECT ON TABLE toolkit.cern_country TO icms_reader;


--
-- Name: TABLE cern_country_status; Type: ACL; Schema: toolkit; Owner: -
--

GRANT SELECT ON TABLE toolkit.cern_country_status TO icms_reader;


--
-- Name: TABLE cms_project; Type: ACL; Schema: toolkit; Owner: -
--

GRANT SELECT ON TABLE toolkit.cms_project TO icms_reader;


--
-- Name: TABLE cms_week; Type: ACL; Schema: toolkit; Owner: -
--

GRANT SELECT ON TABLE toolkit.cms_week TO icms_reader;


--
-- Name: TABLE ext_author; Type: ACL; Schema: toolkit; Owner: -
--

GRANT SELECT ON TABLE toolkit.ext_author TO icms_reader;


--
-- Name: TABLE ext_author_flag; Type: ACL; Schema: toolkit; Owner: -
--

GRANT SELECT ON TABLE toolkit.ext_author_flag TO icms_reader;


--
-- Name: TABLE ext_author_project; Type: ACL; Schema: toolkit; Owner: -
--

GRANT SELECT ON TABLE toolkit.ext_author_project TO icms_reader;


--
-- Name: TABLE ext_authors_flags_rel; Type: ACL; Schema: toolkit; Owner: -
--

GRANT SELECT ON TABLE toolkit.ext_authors_flags_rel TO icms_reader;


--
-- Name: TABLE permission; Type: ACL; Schema: toolkit; Owner: -
--

GRANT SELECT ON TABLE toolkit.permission TO icms_reader;


--
-- Name: TABLE restricted_resource; Type: ACL; Schema: toolkit; Owner: -
--

GRANT SELECT ON TABLE toolkit.restricted_resource TO icms_reader;


--
-- Name: TABLE room; Type: ACL; Schema: toolkit; Owner: -
--

GRANT SELECT ON TABLE toolkit.room TO icms_reader;


--
-- Name: TABLE room_request; Type: ACL; Schema: toolkit; Owner: -
--

GRANT SELECT ON TABLE toolkit.room_request TO icms_reader;


--
-- Name: TABLE room_request_convener; Type: ACL; Schema: toolkit; Owner: -
--

GRANT SELECT ON TABLE toolkit.room_request_convener TO icms_reader;


--
-- Name: TABLE room_week_rel; Type: ACL; Schema: toolkit; Owner: -
--

GRANT SELECT ON TABLE toolkit.room_week_rel TO icms_reader;


--
-- Name: TABLE status; Type: ACL; Schema: toolkit; Owner: -
--

GRANT SELECT ON TABLE toolkit.status TO icms_reader;


--
-- Name: TABLE vote_delegation; Type: ACL; Schema: toolkit; Owner: -
--

GRANT SELECT ON TABLE toolkit.vote_delegation TO icms_reader;


--
-- Name: TABLE voting; Type: ACL; Schema: toolkit; Owner: -
--

GRANT SELECT ON TABLE toolkit.voting TO icms_reader;


--
-- Name: TABLE voting_list_entry; Type: ACL; Schema: toolkit; Owner: -
--

GRANT SELECT ON TABLE toolkit.voting_list_entry TO icms_reader;


--
-- Name: TABLE voting_merger; Type: ACL; Schema: toolkit; Owner: -
--

GRANT SELECT ON TABLE toolkit.voting_merger TO icms_reader;


--
-- Name: TABLE voting_merger_member; Type: ACL; Schema: toolkit; Owner: -
--

GRANT SELECT ON TABLE toolkit.voting_merger_member TO icms_reader;


--
-- PostgreSQL database dump complete
--

