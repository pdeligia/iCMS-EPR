# -*- coding: utf-8 -*-

#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from sqlalchemy.orm.exc import NoResultFound

__author__ = 'andreas.pfeiffer@cern.ch'

import os
import time
from traceback import format_exc

from flask import current_app
from flask_login import UserMixin, AnonymousUserMixin
from . import db, login_manager, cache


from .ldapAuth import userInGroup
from icmsutils.ldaputils import LdapProxy

from flask_sqlalchemy import BaseQuery

from sqlalchemy import orm, and_, join
from sqlalchemy.orm import relationship

from sqlalchemy_continuum import make_versioned, versioning_manager, plugins

import icms_orm
from icms_orm import epr_bind_key

versioning_manager.declarative_base = icms_orm.IcmsDeclarativeBasesFactory.get_for_bind_key( icms_orm.epr_bind_key() )

# CRITICAL: call BEFORE defining (importing) the models.
# user_cls parameter doesn't matter anymore (whole Transaction class is defined below)
make_versioned(plugins=[plugins.PropertyModTrackerPlugin()])

# Providing a custom transaction class to avoid "Foreign key associated with column 'transaction.user_id'
# could not find table 'users' with which to generate a foreign key to target column 'id'"
from icms_orm.epr import ContinuumTransaction as Transaction
versioning_manager.transaction_cls = Transaction

from icms_orm.epr import EprUser, EprInstitute, Manager, Role, TimeLineUser, TimeLineInst, EprDue, \
    Pledge, Task, Shift, ShiftGrid, ShiftTaskMap, Project, CMSActivity, AllInstView, Category, EprCeiling, \
    Level3Name, UsersInstitutes, Permission, DeclarativeBase, EprBaseMixin

for cls in [EprUser, Manager, TimeLineUser, TimeLineInst, EprDue, Pledge, Task, ShiftTaskMap, Shift, ShiftGrid, EprInstitute, Project,
            CMSActivity, AllInstView, Category, EprCeiling, Level3Name]:
    cls.query_class = BaseQuery

EprUser.__bases__ = tuple(list(EprUser.__bases__) + [UserMixin])

orm.configure_mappers()

# Now that the mappers for continuum are set, add this
EprInstitute.users = relationship(EprUser,
                               secondary=join(UsersInstitutes, EprUser, UsersInstitutes.user_id == EprUser.id),
                               primaryjoin=and_(EprInstitute.id == UsersInstitutes.inst_id),
                               backref='institutes')

# "translate" these to the modules which need them
from icms_orm.epr import get_task_shift_type_ids as getTaskShiftTypeIds, get_task_types as getTaskTypes

permsMap = { 'project'   : Permission.MANAGEPROJ,
             'activity'  : Permission.MANAGEACT,
             'task'      : Permission.MANAGETASK,
             'institute' : Permission.MANAGEINST,
            }

class CMSDbException(Exception):
    def __init__(self, msg):
        self.msg = msg
    def __repr__(self):
        return '<CMSDbException: '+str(self.msg)+'>'


def commitNew(item):
    db.session.add(item)
    # Activity = actPlugin.activity_cls
    # try:
    #     db.session.flush()
    # except Exception as e:
    #     msg = "Can not flush item %s, got \n     %s \n    - rolling back." % (str(item), str(e))
    #     current_app.logger.error(msg)
    #     db.session.rollback()
    #     raise CMSDbException(msg)
    #
    # thisActivity = Activity(verb=u'create', object = item)
    # db.session.add(thisActivity)
    try:
        db.session.commit()
    except Exception as e:
        # msg = "Can not commit item %s (with activity %s), got %s - rolling back." % (str(item), str(thisActivity), str(e))
        msg = "commitNew> Can not commit item %s (with activity %s), got %s - rolling back." % (str(item), 'None', str(e))
        current_app.logger.error(msg)
        # current_app.logger.error(format_exc())
        db.session.rollback()
        raise CMSDbException(msg)

    return


def commitUpdated(item, target=None):
    # Activity = actPlugin.activity_cls
    # db.session.add(item)
    # try:
    #     db.session.flush()
    # except Exception as e:
    #     msg = "Can not flush item %s, got %s - rolling back." % (str(item), str(e))
    #     current_app.logger.error(msg)
    #     current_app.logger.error(format_exc())
    #     db.session.rollback()
    #     raise CMSDbException(msg)
    #
    # thisActivity = Activity(verb=u'update', object = item, target=target)
    # db.session.add(thisActivity)
    try:
        db.session.commit()
    except Exception as e:
        # msg = "Can not commit item %s (with activity %s), got %s - rolling back." % (str(item), str(thisActivity), str(e))
        msg = "Can not commit item %s (with activity %s), got %s - rolling back." % (str(item), '', str(e))
        current_app.logger.error(msg)
        db.session.rollback()
        raise CMSDbException(msg)

    return


class AnonymousUser(AnonymousUserMixin):
    def can(self, permissions):
        return False
    def is_administrator(self, *args, **kwargs):
        return False
    def is_wizard(self, *args, **kwargs):
        return False
    def isManager(self, *args, **kwargs):
        return False

login_manager.anonymous_user = AnonymousUser


@login_manager.user_loader
def load_user(user_id):
    # return db.session.query(EprUser).get(int(user_id))
    # return db.session.query(EprUser).filter(EprUser.id == int(user_id)).one()
    return db.session.query(EprUser).get(int(user_id))

def __insert_roles():
    #  name : (permission(s), isDefault)
    roles = {
            'User'            : (Permission.READ, True),
            'ProjectMgr'      : (Permission.READ | Permission.MANAGEPROJ | Permission.MANAGEACT | Permission.MANAGETASK, False),
            'ActivityMgr'     : (Permission.READ | Permission.MANAGEACT | Permission.MANAGETASK, False),
            'TaskMgr'         : (Permission.READ | Permission.MANAGETASK, False),
            'InstMgr'         : (Permission.READ | Permission.MANAGEINST, False),
            'Observer'        : (Permission.READ | Permission.OBSERVE, False),
            'Secretariat'     : (Permission.READ | Permission.MANAGEUSER | Permission.MANAGEINST, False),
            'EngagementOffice': (Permission.READ | Permission.MANAGEPROJ | Permission.MANAGEACT | Permission.MANAGETASK | Permission.ENGOFF_MGT, False),
            'Administrator'   : (Permission.ADMINISTER, False)
        }
    db_roles = {x[0] for x in db.session.query(Role.name).all()}
    for r in [rr for rr in roles.keys() if rr not in db_roles]:
        role = Role()
        role.name = r
        role.permissions = roles[r][0]
        role.default = roles[r][1]
        db.session.add(role)
    if len(db_roles) < len(roles):
        db.session.commit()

Role.insert_roles = staticmethod(__insert_roles)


def __is_manager(self, manager, actRole):

    # check if the role is the requested one:
    if manager.role == actRole :
        current_app.logger.debug(
            'user.is_manager> user %s is manager for %s %s with Role %s' % (self.username, manager.itemType, manager.itemCode, actRole ) )
        return True  # found The One

    # check if there is an eGroup set, if so, see if the user is in that eGroup:
    if manager.egroup is not None :
        if userInGroup( self.username, manager.egroup ) :
            current_app.logger.debug(
                'user.is_manager> user %s is in mgt eGroup %s for %s %s ' % (
                self.username, manager.egroup, manager.itemType, manager.itemCode) )

            return True
    return False

def __canManageItem( self, patType, patCode, year, perm, status ):

    manager = (db.session.query(Manager).filter( Manager.itemType == patType)
                                        .filter( Manager.itemCode == patCode)
                                        .filter( Manager.year == year)
                                        .filter( Manager.role == perm)
                                        .filter( Manager.status.in_( [status, 'allowed'] ) )
                                        .filter( Manager.userId == self.id )
                                        .first()
               )

    if manager and self.is_manager(manager, permsMap[patType]):
        current_app.logger.debug( 'user.canManageItem> user %s is direct manager for %s code %s' % ( self.username, patType, patCode) )
        return True

    # Get the list of all eGroups the user is member of (from his LDAP entry),
    # then match them against the eGroups stored as management eGroups. Add these
    # PAT items to the list before returning
    # if no eGroups are (yet?) available, try again
    if not hasattr(self, 'eGroups') or not self.eGroups:
        eGroupList = getEgroupsForUser( self.username )
        self.eGroups = eGroupList
        current_app.logger.info( f'user.canManageItem> user {self.username} refreshed eGroups, found {len(self.eGroups)}/{len(eGroupList)}' )
    else:
        eGroupList = self.eGroups
        current_app.logger.info( f'user.canManageItem> user {self.username} using cached eGroups, found {len(eGroupList)}/{len(self.eGroups)}' )

    mgtEgs = (db.session.query(Manager).filter( Manager.itemType == patType)
                                       .filter( Manager.itemCode == patCode)
                                       .filter( Manager.year == year)
                                       .filter( Manager.role == perm)
                                       .filter( Manager.status.in_( [status, 'allowed'] ) )
                                       .filter( Manager.egroup.in_( eGroupList ) )
                                       .first()
            )
    current_app.logger.debug( 'user.canManageItem> user %s is NOT direct manager for %s code %s - eGroups: %s' %
                              ( self.username, patType, patCode, ','.join(eGroupList) ) )
    if mgtEgs:
        current_app.logger.debug( 'user.canManageItem> user %s is NOT direct manager for %s code %s - eGroups: %s, mgtEgs: %s ' %
                              ( self.username, patType, patCode, ','.join(eGroupList), ','.join([str(x) for x in mgtEgs]) ) )
    else:
        current_app.logger.debug( 'user.canManageItem> no mgt egroups found for %s code %s ' % (patType, patCode) )

    if mgtEgs and self.is_manager(mgtEgs, permsMap[patType]):
        current_app.logger.debug(
            'user.canManageItem> user %s is in manager eGroup for %s code %s' % (self.username, patType, patCode) )
        return True

    current_app.logger.debug( 'user.canManageItem> user %s is NOT found as manager for %s code %s - eGroups: %s ' % ( self.username, patType, patCode, ','.join(eGroupList) ) )
    return False


def __can_manage(self, patType, patCode, year):

    if self.canManageItem( patType, patCode, year, permsMap[patType], 'active') :
        current_app.logger.debug( 'user.canManage> user %s can directly manage %s code %s' % (self.username, patType, patCode) )
        return True

    # ... not found directly, check in the hierarchy up:
    if 'activity' in patType.lower():  # check the activities' project
        try:
            act = db.session.query(CMSActivity).filter_by(code=patCode).one()
        except NoResultFound:
            current_app.logger.warning( 'user.canManage> no task found for user %s with code %s' % (self.username, patCode) )
            return False
        current_app.logger.debug( 'user.canManage> checking for proj mgt of user %s for act %s' % (self.username, act.code if act else 'n/a'))
        if self.canManageItem( 'project', act.project.code, year, Permission.MANAGEPROJ, 'active') :
            current_app.logger.debug( 'user.canManage> user %s can manage activity for %s code %s as ProjMgr' % (self.username, patType, patCode) )
            return True

    if 'task' in patType.lower():  # check if we're the activity manager
        try:
            task = db.session.query(Task).filter_by(code=patCode).one()
        except NoResultFound:
            current_app.logger.warning( 'user.canManage> no task found for user %s with code %s' % (self.username, patCode) )
            return False

        act = task.activity
        proj = act.project
        current_app.logger.debug('user.canManage> checking for act mgt of user %s for task/act/proj %s/%s/%s' % (self.username, task.code, act.code, proj.code))

        if self.canManageItem( 'activity', act.code, year, Permission.MANAGEACT, 'active') :
            current_app.logger.debug( 'user.canManage> user %s can manage activity for %s code %s as ActMgr' % (self.username, patType, patCode) )
            return True
        if self.canManageItem( 'project', proj.code, year, Permission.MANAGEPROJ, 'active') :
            current_app.logger.debug( 'user.canManage> user %s can manage project for  %s code %s as ProjMgr' % (self.username, patType, patCode) )
            return True

    # default: nothing found, so no permission to manage ...
    return False

# @cache.memoize(timeout=60)
def getEgroupsForUser( username ):

    # return the CMS related eGroups for a user, lower-case and w/o the "@cern.ch" suffix
    # in dev/test environments, return something based on the usename
    startTime = time.time()
    if 'USE_FAKE_EGROUPS' in os.environ:
        eGroupList = []
        if 'egPM' in username: eGroupList = [ 'p2MgrEgroup', 'fooGroup-invalid@cern.ch' ]
        if 'a4meg' in username: eGroupList = [ 'a4MgrEgroup' ]
        if eGroupList: current_app.logger.warning( 'forcing use of FAKE eGroups: %s for user %s' % (eGroupList, username) )
        current_app.logger.info( 'TimeInfo++getEgroupsForUser> %s sec to get (fake) egroups for %s' % (time.time()-startTime, username) )
        return [ x.replace('@cern.ch', '').lower() for x in eGroupList ]

    if os.environ.get( 'USE_LDAP') != 'None' and os.environ.get( 'USE_LDAP') == 'False':
        current_app.logger.info( "getEgroupsForUser> USE_LDAP is '%s' -- not using it." % os.environ.get( 'USE_LDAP') )
        return []

    ldapUser = LdapProxy(wrapper_reconnect_attempts=3).get_person_by_login( username )
    current_app.logger.info('getEgroupsForUser> found user with %d CMS eGroups' % len(ldapUser.cmsEgroups) )
    # sometimes ldap doesn't return eGroups, in that case, try again:
    if len(ldapUser.cmsEgroups) < 1:
        ldapUser = LdapProxy(wrapper_reconnect_attempts=3).get_person_by_login( username )
        current_app.logger.info('getEgroupsForUser> found user with %d CMS eGroups on 2nd try ...' % len(ldapUser.cmsEgroups) )

    eGroupList = [ ]
    if ldapUser:
        eGroupList = [ x.replace('@cern.ch','').lower() for x in ldapUser.cmsEgroups ]
        current_app.logger.debug( 'getEgroupsForUser> found %d CMS eGroups for user %s : %s ' % (len(eGroupList), username, eGroupList) )
    else:
        current_app.logger.info( 'getEgroupsForUser> no LDAP entry found for user %s ' % (username, ) )

    current_app.logger.info( 'TimeInfo++getEgroupsForUser> %s sec to get %d egroups for %s' % (time.time() - startTime, len(eGroupList), username) )

    return eGroupList


@cache.memoize(timeout=60)
def __isTeamLeader(self, team, year):

    # if the user is a wizard or admin, they are also a team leader
    if self.is_wizard(): return True
    if self.is_administrator(): return True

    subQuery = (db.session.query(Manager).filter( Manager.itemType == 'institute')
                                        .filter( Manager.year == year)
                                        .filter( Manager.status.in_( ['active', 'allowed'] ) )
                                        .filter( Manager.userId == self.id )
               )
    if team != 'any':
        subQuery = subQuery.filter(Manager.itemCode == team)

    manager = subQuery.all()

    current_app.logger.debug( "__isTeamLeader> found %d items to manage for %s (in %s)" % (len(manager), self.name, year) )
    return (len(manager) > 0)


@cache.memoize(timeout=60)
def __isManager(self, mgtType, year):

    try:
        if mgtType == 'all':
            manager = db.session.query(Manager).filter_by( userId=self.id, status='active').all()
            manager += db.session.query(Manager).filter( Manager.egroup.in_( getEgroupsForUser(self.username) ) )\
                                    .filter( Manager.status == 'active')\
                                    .filter( Manager.year == year)\
                                    .all()
            current_app.logger.debug( "__isManager> found %d items to manage for %s (in %s)" % (len(manager), self.name, year) )
            return (len(manager) > 0)
        else:
            manager = db.session.query(Manager).filter_by(userId=self.id, itemType=mgtType, status='active').all()
            manager += db.session.query(Manager).filter( Manager.egroup.in_( getEgroupsForUser(self.username) ) )\
                                    .filter_by( itemType=mgtType, status='active')\
                                    .filter( Manager.year == year)\
                                    .all()
            current_app.logger.debug( "__isManager> found %d items of type %s to manage for %s (in %s)" % (len(manager), mgtType, self.name, year) )
            return (len(manager) > 0)

    except Exception as e:
        current_app.logger.error( "could not find manager info for %s (cmsId: %s) - got %s" % (self.name, self.cmsId, str(e)))

    return False

# use to override the corresponding entry in the icms_orm/epr.py
@cache.memoize(timeout=60)
def __is_wizard(self):
    return self.role is not None and \
           (self.role.permissions & Permission.ADMINISTER) == Permission.ADMINISTER

@cache.memoize(timeout=60)
def __is_administrator(self):

    # if the user is a wizard, she is also an administrator ...
    if self.is_wizard(): return True

    # better check by name or explicit one permission like here ?
    return self.role is not None and \
           (self.role.permissions & Permission.ENGOFF_MGT) == Permission.ENGOFF_MGT

@cache.memoize(timeout=60)
def __is_engOfficer(self):

    # if the user is a wizard, she is also an administrator ...
    if self.is_wizard(): return True

    # better check by name or explicit one permission like here ?
    return self.role is not None and \
           (self.role.permissions & Permission.ENGOFF_MGT) == Permission.ENGOFF_MGT

def __canBeManagedBy(self, mgrUser, year):

    # check if the user can be managed by another user "mgrUser"
    # (i.e. if the mgrUser is (deputy) institute/team leader of the user

    # get the actual parameters for the user and the selected year, 
    # ensure the order in the DB is "desc" to get the most recent item:
    try:
        tlUser = db.session.query(TimeLineUser).filter_by(year=year, cmsId=self.cmsId).order_by(
            TimeLineUser.timestamp.desc()).first()
    except Exception as e:
        if "No row was found for one" in str(e):
            return False  # no valid user for check
        else:
            msg = "Error from DB when trying to find user %s - got %s " % (self.username, str(e))
            print("__canBeManagedBy> ", msg)
            return False

    # current_app.logger.debug( "__canBeManagedBy> tlUser %s (%s) inst %s year %s " % (tlUser.cmsId, self.username, tlUser.instCode, year) )

    manager = (db.session.query(Manager).filter_by(itemType='institute',
                                      itemCode=tlUser.instCode,
                                      userId=mgrUser.id,
                                      year=year)
                                      .filter( Manager.status.in_( ['active', 'allowed'] ) )
                                      .first() )

    # current_app.logger.debug( "__canBeManagedBy> manager %s in year %s " % (manager, year) )

    if manager and manager.role == Permission.MANAGEINST: return True  # found The One

    return False

def __canManageUser(self, member, year):
    return member.canBeManagedBy(self, year)

EprUser.canManageItem = __canManageItem
EprUser.canManage = __can_manage
EprUser.is_manager = __is_manager
EprUser.isManager = __isManager
EprUser.is_administrator = __is_administrator
EprUser.is_engOfficer = __is_engOfficer
EprUser.is_wizard = __is_wizard
EprUser.isTeamLeader = __isTeamLeader
EprUser.canBeManagedBy = __canBeManagedBy
EprUser.canManageUser = __canManageUser
