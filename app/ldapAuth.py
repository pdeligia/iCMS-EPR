
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import logging
import netrc
import os
import socket
import ldap

class LdapTool(object):

    def getPrimUserByDN(self, dn):

        searchFilter = "distinguishedName=%s" % dn

        what = ['*']

        status, resultRaw = self.search(searchFilter, what, verbose=False)

        if not status:
            logging.debug('getPrimUserByDN> ERROR: ldap search failed for %s' % dn)
            return None

        result = resultRaw[ 0 ][ 0 ]
        if 'seeAlso' in  str(result[1]) and len(result[1]['seeAlso'])>0:
            # secondary/service account found, check main one:
            distinguishedName = result[1]['seeAlso'][0].decode('utf-8')
            sf = 'distinguishedName=%s' % distinguishedName
            logging.debug( "getPrimUserByDN> seeAlso found, re-searching for dn=%s " % distinguishedName )
            status, resultRaw = search( sf, what, verbose=False)
            if not status:
                logging.debug( "getPrimUserByDN> ERROR: No result for re-search with DN: %s" % distinguishedName )
                return None
            result = resultRaw[ 0 ][ 0 ]
            logging.debug( "getPrimUserByDN> result for re-search is: %s " % str(result) )
            return result[1]

        return result[1]

    def getPrimUserByHrId(self, employeeId, accountType='Primary'):

        searchFilter = "(&(employeeID=%s)(employeeType=%s))" % (employeeId, accountType)

        what = ['*']

        status, resultRaw = self.search(searchFilter, what, verbose=False)

        if not status:
            return None

        result = resultRaw[ 0 ][ 0 ]
        return result[1]

    def getLogin(self, employeeId, accountType='Primary'):

        searchFilter = "(&(employeeID=%s)(employeeType=%s))" % (employeeId, accountType)

        what = ['cn']

        status, resultRaw = self.search(searchFilter, what)

        result = resultRaw[ 0 ][ 0 ]
        if status:
            return result[1]['cn'][0].decode('utf-8')
        else:
            return None


    def authenticate(self, login, pwd):

        # if 'cern.ch' not in socket.getfqdn():
        #     logging.warning("can not authenticate via ldap from outside CERN domain")
        #     return False, None

        # username = 'cn=%s,ou=users,o=cern,c=ch' % (login.strip(),)
        username = 'CN=%s,OU=Users,O=cern,C=ch' % (login.strip(),)
        password = pwd

        logging.debug('request to authenticate request for login %s' % login)

        ldapURI = os.environ.get( 'LDAP_AUTH_URI' , 'ldaps://ldap.cern.ch:636')
        msg = 'ldapAuth::authenticate> using ldap server at "%s" (from %s)' % (ldapURI, socket.getfqdn())
        logging.debug(msg)
        # print( msg )

        if 'cern.ch' not in socket.getfqdn():
            ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)
        else:
            caCertFile = '/etc/openldap/cacerts/CERN_Root_Certification_Authority_2.pem'
            if os.path.exists( caCertFile ) :
                ldap.set_option( ldap.OPT_X_TLS_CACERTFILE, caCertFile )

        logging.debug('options set ... ' )

        ldap.set_option( ldap.OPT_NETWORK_TIMEOUT, 10.0 )
        l = ldap.initialize(ldapURI)
        logging.debug('secure LDAP connection initialised ... ' )

        try:
            l.protocol_version = ldap.VERSION3
            logging.debug("ldap setup, binding ... ")
            l.simple_bind_s(username, password)
        except ldap.INVALID_CREDENTIALS:
            logging.error("authenticate> ldap bind for %s returned invalid_credentials " % (username,))
            return False
        except Exception as error:
            logging.error("authenticate> ldap bind for %s returned error: %s " % (username, str(error)))
            raise error

        logging.debug("user %s authenticated... " % login )
        return True

    def userInGroup(self, login, groupName):

        searchFilter = "cn=%s" % login
        # searchAttribute = ["mail","sAMAccountName"]
        what = ['memberOf']

        status, resultRaw = self.search(searchFilter, what)

        if not status:
            logging.error("ldap search for %s/%s returned false " % (searchFilter, what))
            return False

        result = resultRaw[ 0 ][ 0 ]
        grpList = [ x.decode('utf-8').split(',')[0].replace('CN=', '') for x in result[1]['memberOf'] ]
        logging.debug("checking for %s in group %s is %s " % (login, groupName, str(groupName in grpList)) )
        return  groupName in grpList

    def getUserEmail(self, login):

        if login.strip() == '' or login.startswith( 'username_unknown_' ):
            logging.info("getUserEmail> not going to do ldap search for empty/unknown login ('%s') " % login )
            return ''

        searchFilter = "cn=%s" % login

        what = ['mail']

        status, resultRaw = self.search(searchFilter, what)

        if not status:
            logging.error("getUserEmail> ldap search for %s/%s returned false (%s) " % (searchFilter, what, status))
            if 'TEST_DATABASE_URL' in os.environ:
                logging.debug( "getUserEmail> testing mode found for user '%s' returning fake email '%s' " % (login, '%s@example.com' % login) )
                return '%s@example.com' % login
            else:
                return ''

        result = resultRaw[ 0 ][ 0 ]
        try :
            email = result[ 1 ][ 'mail' ][ 0 ].decode('utf-8')
        except TypeError :
            return ''
        except :
            raise

        logging.info( "getUserEmail> user '%s' got result: '%s' " % (login, str(result[1]) ) )
        logging.debug("getUserEmail> checking for email of %s returned %s " % (login, email) )

        # if we're in testing mode, and the user was not found in ldap, return a fake one:
        if not email and 'TEST_DATABASE_URL' in os.environ:
            fakeEMail = '%s@example.com' % login  # use example.com domain to avoid accidental spamming ...
            logging.info( "getUserEmail> testing mode found for user '%s' returning fake email '%s' " % (login, fakeEMail) )
            return fakeEMail

        return  str(email)

    def getUserFromEmail(self, email):

        searchFilter = "mail=%s" % email
        # searchAttribute = ["mail","sAMAccountName"]
        what = ['cn']

        status, resultRaw = self.search(searchFilter, what)

        if not status:
            if 'TEST_DATABASE_URL' not in os.environ:
                if 'andreas.pfeiffer@cern.ch' in email: return 'pfeiffer'
                if '@example.com' in email: return email.replace('@example.com', '')
            logging.error("ldap search for %s/%s returned false " % (searchFilter, what))
            return False

        result = resultRaw[ 0 ][ 0 ]
        userCN = result[1]['cn'][0]
        logging.debug("checking for email of %s returned %s " % (email, userCN) )
        return  userCN.decode('utf-8')

    def checkContactServer( self, searchFilter ):

        if 'cern.ch' not in socket.getfqdn( socket.gethostname() ):
            return False

        if 'runner-' in socket.getfqdn( ) and '-project-' in socket.getfqdn( ) :
            if searchFilter.startswith( 'mail=user' ) :
                # silently return nothing for our private users who don't exist in ldap ...
                return True, searchFilter.replace( 'mail=', '' ) + '@example.com'
            # running in CI testing container ... and still getting:
            # "Can't contact LDAP server" .... strange ...
            msg = "going to contact ldap server for info from CI container net: %s " % socket.getfqdn( )
            logging.debug( msg )

        return True

    def search(self, searchFilter, searchAttribute, verbose=False):

        # status, res = checkContactServer( searchFilter )
        # if status:
        #     return status, res

        if 'cern.ch' not in socket.getfqdn():
            ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)
        else:
            caCertFile = '/etc/openldap/cacerts/CERN_Root_Certification_Authority_2.pem'
            if os.path.exists( caCertFile ) :
                ldap.set_option( ldap.OPT_X_TLS_CACERTFILE, caCertFile )

        ldapURI = os.environ.get( 'LDAP_URI' , 'ldaps://xldap.cern.ch:636')
        logging.info( 'ldapAuth::search> using ldap server at "%s"' % ldapURI )

        ldap.set_option( ldap.OPT_NETWORK_TIMEOUT, 10.0 )
        l = ldap.initialize( ldapURI )

        basedn = "dc=cern,dc=ch"

        #this will scope the entire subtree under UserUnits
        searchScope = ldap.SCOPE_SUBTREE

        try:
            ldap_result_id = l.search(basedn, searchScope, searchFilter, searchAttribute)
            result_set = []
            while 1:
                result_type, result_data = l.result(ldap_result_id, 0)
                if (result_data == []):
                    break
                else:
                    ## if you are expecting multiple results you can append them
                    ## otherwise you can just wait until the initial result and break out
                    if result_type == ldap.RES_SEARCH_ENTRY:
                        result_set.append(result_data)
            # logging.info ('search> got: %s ' % str(result_set) )
        except ldap.LDAPError as e:
            logging.error("ldapAuth::search> ldap search for %s %s returned error: %s " % (searchFilter, searchAttribute, str(e)) )
            l.unbind_s()
            return False, None

        l.unbind_s()

        # make sure we get the result from the subtree of the "Users" OU, not the one for "Computers"
        userRes = []
        for item in result_set :
            if 'OU=Users,OU=Organic Units' not in item[ 0 ][ 0 ] : continue
            userRes.append( item )

        logging.debug( "ldapAuth::search> result for [%s] [%s] is: '%s'... " % (searchFilter, searchAttribute, str( userRes )[:500]) )

        if not userRes:
            return False, userRes

        return True, userRes

    def searchForUser(self, whatFor, accountType='Primary'):

        allRes = []

        # search for these:
        filters = [ 'employeeID', 'sn', 'givenName', 'cn' ]

        for f in filters:
            searchFilter = "(&(%s=%s)(employeeType=%s))" % (f, whatFor, accountType)
            what = ['distinguishedName','employeeID']
            status, resultRaw = self.search(searchFilter, what, verbose=False)
            if status:
                result = resultRaw[ 0 ][ 0 ]
                # print( "RESULT: ", result )
                allRes += ['%s -- %s' % (result[1]['distinguishedName'][0].decode('utf-8'), result[1]['employeeID'][0].decode('utf-8'))]

        return allRes

    def check(self, login, pwd, groupList):

        if not authenticate(login, pwd):
            logging.error( "authentication FAILed for %s " % login )
            return False
        else:
            logging.info( "authentication OK for %s " % login )

        res = True # assume success 
        for group in groupList:
            if userInGroup(login, group):
                logging.info( "user %s is in group %s " % (login, group) )
            else:
                logging.info( "user %s is NOT in group %s " % (login, group) )
                res = False
        
        return res


class LdapDummyTool(object):
    def __init__(self):
        self.dummyResult = True, [[ '', {'distinguishedName': ['unknown'], 'employeeID': ['-1']} ]]
    def getPrimUserByDN(self, dn):
        return None
    def getPrimUserByHrId(self, employeeId, accountType='Primary'):
        return None
    def getLogin(self, employeeId, accountType='Primary'):
        return 'unknown'
    def authenticate(self, login, pwd):
        return True
    def userInGroup(self, login, groupName):
        return False
    def getUserEmail(self, login):
        return 'unknown@no.domain'
    def getUserFromEmail(self, email):
        return 'unknown'
    def checkContactServer(self,  searchFilter ):
        return True
    def search(self, searchFilter, searchAttribute, verbose=False):
        return self.dummyResult
    def searchForUser(self, whatFor, accountType='Primary'):
        return self.dummyResult
    def check(self, login, pwd, groupList):
        return True

logging.info( "ldapAuth> USE_LDAP is '%s' (%s)" % (os.environ.get( 'USE_LDAP'), os.environ.get( 'USE_LDAP') != 'None')  )

if os.environ.get( 'USE_LDAP') != 'None' and os.environ.get( 'USE_LDAP') == 'False':
    logging.debug( "ldapAuth> using DUMMY LDAP" )
    ldapTool = LdapDummyTool()
else:
    logging.debug( "ldapAuth> using LDAP" )
    ldapTool = LdapTool()


def getPrimUserByDN(dn):
    return ldapTool.getPrimUserByDN(dn)
		
def getPrimUserByHrId(employeeId, accountType='Primary'):
    return ldapTool.getPrimUserByHrId(employeeId, accountType='Primary')
		
def getLogin(employeeId, accountType='Primary'):
    return ldapTool.getLogin(employeeId, accountType='Primary')
		
def authenticate(login, pwd):
    return ldapTool.authenticate(login, pwd)
		
def userInGroup(login, groupName):
    return ldapTool.userInGroup(login, groupName)
		
def getUserEmail(login):
    return ldapTool.getUserEmail(login)
		
def getUserFromEmail(email):
    return ldapTool.getUserFromEmail(email)
		
def checkContactServer( searchFilter ):
    return ldapTool.checkContactServer( searchFilter )
		
def search(searchFilter, searchAttribute, verbose=False):
    return ldapTool.search(searchFilter, searchAttribute, verbose=False)
		
def searchForUser(whatFor, accountType='Primary'):
    return ldapTool.searchForUser(whatFor, accountType='Primary')
		
def check(login, pwd, groupList):
    return ldapTool.check(login, pwd, groupList)
		

def test():

    import pprint

    logging.basicConfig( format='%(asctime)-15s - %(levelname)s: %(message)s', level=logging.INFO )

    netRcFile = netrc.netrc()
    (login, account, pwd) = netRcFile.authenticators('ldap.cern.ch')

    print( "\nshould work:" )
    res = check(login, pwd, ['zh'])
    print( "--> res:", res )
    
    # assert(res == True)

    print( "\nshould NOT work:" )
    res = check(login, pwd+'ff', ['zh', 'zp'])
    assert(res == False)

    id = 422405
    print( "\nfound login %s for %i" % (getLogin(id), id) )

    print( "\nfound email for %s to be: %s " % (login, getUserEmail(login)) )

    for email in ['andreas.pfeiffer@cern.ch', 'andreasp@cern.ch']:
        print( "\nfound user %s for email %s " % (getUserFromEmail(email), email) )

    # this user has an entry with the "Computers" subtree as well:
    print( "\nmail for Hengne Li: ", getUserEmail('heli') )

    id = 422405
    result = getPrimUserByHrId(id)
    if result:
        resOut = {}
        for k,v in result.items():
            if 'memberOf' in k or 'managedObjects' in k:
                resOut[k] = ['%d members in list' % len(v)]
            else:
                resOut[k] = v

        print( "\nfound user info for %i :  " % ( id, ) )
        # pprint.pprint( resOut )
        print( "\nOK - found %d items for hrId %d" % ( len(resOut.keys()), id ) )
    else:
        print( "\nERROR: no result found for %d" % id )

    ret = authenticate(login, pwd)
    print( '\nauthenticate (OK)> result = ', ret)
    assert(ret)

    ret = authenticate(login, pwd+'fff')
    print( 'authenticate (fail)> result = ', ret)
    assert( not ret )

    dn = 'CN=pfeiffer,OU=Users,OU=Organic Units,DC=cern,DC=ch'
    ret = getPrimUserByDN(dn)
    if ret is not None:
        print( '\ngetPrimUserByDN> result: ')
        for k, v in ret.items():
            # if k not in ['memberOf', 'directReports'] and 'managedObjects' not in k:
            if k in ['distinguishedName', 'displayName', 'cn'] :
                print( '\t %s : %s' % (k, v) )

    ret = userInGroup(login, 'zh')
    print( '\nuserInGroup> %s zh result = ' % login, ret)
    if 'pfeiffer' == login:
        assert(ret)
    else: 
        assert( not ret )

    ret = userInGroup(login, 'zp')
    print( '\nuserInGroup> zp result = ', ret)
    assert( not ret )

    ret = checkContactServer( 'mail=user-0' )
    print( '\ncheckContactServer> result = ', ret)
    assert( not ret )

    ret = searchForUser( '422405', accountType='Primary')
    print( '\nsearchForUser> result = ', ret)
    assert( ret == ['CN=pfeiffer,OU=Users,OU=Organic Units,DC=cern,DC=ch -- 422405'] )

if __name__ == '__main__':

    print ( "ldapAuth> USE_LDAP is '%s'" % os.environ.get( 'USE_LDAP') )
    print ( "ldapAuth> USE_LDAP != None: '%s'" % os.environ.get( 'USE_LDAP') != 'None' )

    if os.environ.get( 'USE_LDAP') != 'None' and os.environ.get( 'USE_LDAP') == 'False':
        print( "ldapAuth> using DUMMY LDAP" )
    else:
        print( "ldapAuth> using LDAP" )

    test()
