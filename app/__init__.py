#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import os
import sys
import time
import logging
import datetime
import re

import icms_orm
from alchy.model import extend_declarative_base
from flask import Flask, request
from flask_alchy import Alchy

from flask_bootstrap import Bootstrap
from flask_mail import Mail
from flask_moment import Moment

from flask_login import LoginManager
from flask_pagedown import PageDown

from flask_wtf.csrf import CSRFProtect

from flask_cors import CORS, cross_origin

from sqlalchemy import event
from sqlalchemy.engine import Engine

from flask_debugtoolbar import DebugToolbarExtension

from flask_caching import Cache
cache = Cache( config={ 'CACHE_TYPE' : 'simple' } )

from .ReverseProxied import ReverseProxied

class RequestFormatter(logging.Formatter):
    def format(self, record):
        if request:
            record.url = request.url
            record.remote_addr = request.remote_addr
            record.username = request.remote_user
        else:
            record.url = "N/A"
            record.remote_addr = "N/A"
            record.username = "N/A"

        return super(RequestFormatter, self).format(record)

formatter = RequestFormatter(
    '[iCMS-EPR] %(remote_addr)s - %(username)s [%(asctime)s] url: %(url)s | %(levelname)s in %(module)s: %(message)s'
)

logLvl = logging.INFO
logger = logging.getLogger("[iCMS-EPR]")
# create a console handler
ch = logging.StreamHandler()
ch.setLevel( logLvl )
ch.setFormatter( formatter )
# logger.addHandler( ch )

from config import config
from instance import config as instConfig

EPR_assets = None
defaultYear = -1

bootstrap = Bootstrap()
mail = Mail()
moment = Moment()

from flask_sqlalchemy import BaseQuery

from .utils.orm_interface import WebappOrmManager
db = WebappOrmManager()

# class AlchyPy3(Alchy):
#     def __init__(self, config=None, session_options=None, Model=None, session_class=None):
#         super().__init__(
#             session_options=session_options,
#             Model=Model
#         )
#         # Assembling the base classes
#         for _cls in icms_orm.IcmsDeclarativeBasesFactory.base_classes(): # self.config.get('SQLALCHEMY_BINDS').keys():
#             extend_declarative_base(
#                 _cls, # icms_orm.orm_interface.IcmsDeclarativeBasesFactory.get_for_bind_key(_bk),
#                 self.session
#             )
#     def make_declarative_base(self, model=None, metadata=None):
#         """
#         SQLAlchemy expects 3 parameters, Alchy lives under a rock with just 2.
#         """
#         if metadata is None and model is not None:
#             # if only 2 params were given, positionally, probably the client code lived in Alchy world and meant meta
#             metadata = model
#             model = None
#         if model is not None:
#             # if model is still not None, let's store it in the place where Alchy picks it up from to fwd the call
#             self.model = model
#         return super(AlchyPy3, self).make_declarative_base(metadata)
#
# # todo: setting query_cls here seemed insufficient so the app.models sets it also for each imported class
# from icms_orm import epr_bind_key
# db = AlchyPy3(Model=icms_orm.orm_interface.IcmsDeclarativeBasesFactory.get_for_bind_key( epr_bind_key() ),
#            session_options={'autoflush': False,
#                             'autocommit': False,
#                             'query_cls': BaseQuery,
#                             })

pagedown = PageDown()
csrf = CSRFProtect()

cors = CORS()

login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'auth.login'

@event.listens_for(Engine, "before_cursor_execute")
def before_cursor_execute(conn, cursor, statement,
                        parameters, context, executemany):
    conn.info.setdefault('query_start_time', []).append(time.time())
    # logger.debug("Start Query: %s", statement)

@event.listens_for(Engine, "after_cursor_execute")
def after_cursor_execute(conn, cursor, statement,
                        parameters, context, executemany):
    total = time.time() - conn.info['query_start_time'].pop(-1)
    # logger.debug("Query Complete!")
    # logger.debug("Total Time: %f", total)


def create_app(config_name):
    # todo: not sure if that's desirable for all possible scenarios but it remedies the db sessions proliferation issue while speeding up unit tests too
    if hasattr(create_app, str(config_name)):
        return getattr(create_app, str(config_name))
    else:
        setattr(create_app, config_name, Flask(__name__, instance_relative_config=True))

    app = getattr(create_app, config_name)
    app.config.from_object(config[config_name])

    app.logger.addHandler( ch )

    app.logger.debug('Using instance-specific config override: %s on top of %s' % ('config.py', config_name) )
    app.config.from_pyfile( 'config.py' )

    bootstrap.init_app(app)
    mail.init_app(app)
    moment.init_app(app)
    db.init_app(app)
    login_manager.init_app(app)
    pagedown.init_app(app)
    cache.init_app( app=app )

    useSSL = 'disabled'
    if not app.debug and not app.testing and not app.config['SSL_DISABLE']:
        from flask_sslify import SSLify
        sslify = SSLify(app)
        useSSL = 'enabled'

    # following https://flask-wtf.readthedocs.io/en/latest/csrf.html#ajax
    # we ignore all API methods from csrf checking for now.
    # see also: https://stackoverflow.com/questions/31888316/how-to-use-flask-wtforms-csrf-protection-with-ajax#31888868
    # check the "Ajax" section of that page to see how the blueprint api is protected
    csrf.init_app(app)

    # these hosts are allowed to access the methods we want to have accessible outside the deployment host itself:
    corsAllowedHosts = ['http://localhost:.*', 'http://localhost.*',  'http://localhost.localdomain:5010/', 
                        'https://icms.cern.ch', 'https://icms-dev.cern.ch',
                        'https://cdnjs.cloudflare.com'
                        ]
    cors.init_app(app,  origins=corsAllowedHosts )

    if 'TEST_DATABASE_URL' not in os.environ :
        app.wsgi_app = ReverseProxied( app.wsgi_app )

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint, url_prefix='/auth')

    from .manage import manage as manage_blueprint
    app.register_blueprint(manage_blueprint, url_prefix='/manage')

    from .api_1_0 import api as api_1_0_blueprint
    app.register_blueprint(api_1_0_blueprint, url_prefix='/api/v1.0', decorators=[csrf.exempt])

    # this is still needed, despite code in templates/base.html to set/init CSRF token for AJAX !?!?!?
    csrf.exempt(api_1_0_blueprint)

    if app.debug:
        toolbar = DebugToolbarExtension(app)

    from sqlalchemy_extensions import QueryFactory
    QueryFactory.init_with_session(db.session)

    # print "created app from %s config - ssl %s " % (config_name, useSSL)

    return app
