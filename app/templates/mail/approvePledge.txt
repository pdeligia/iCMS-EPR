Dear all,

   a pledge for user {{ plUserName }} ({{ instCode }})
   {% if instCode != plInst.code %}for institute {{ plInst.code }} {% endif %}
   was approved (by {{ curUserName }}):

    Project  :  {{ pledge.projName }}
    Activity :  {{ pledge.actName }}
    Task     :  {{ pledge.taskName }} (type: {{ pledge.task.tType }})
    Year     :  {{ pledge.year }}

    Work pledged : {{ pledge.workPledged }} month(s).
    Work accepted: {{ pledge.workAccepted }} month(s).
    Work done    : {{ pledge.workDone }} month(s).


Sincerely,
The iCMS-EPR bot.

PS: for reference, the code of the pledge is: '{{ pledge.code }}'
https://icms.cern.ch/epr/showPledge/{{ pledge.code }}
