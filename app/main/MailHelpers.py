import os
import time
import datetime
import traceback
import codecs

from flask import session, current_app

from flask_login import current_user

from config import getProdLevel

from ..sendEmails import send_email
from ..models import Permission, permsMap, EprUser, EprInstitute, Task, Manager, TimeLineInst, TimeLineUser, Level3Name

from ..ldapAuth import getUserEmail
from .. import db, cache

from .JsonDiffer import Comparator


def getMgrEmails( iType, iCode, year=None ):

    return getObsMgrEmails( 'manager', permsMap[iType], iCode, iType, year )

def getObserverEmails( iType, iCode, year=None ):

    return getObsMgrEmails( 'observer', Permission.OBSERVE, iCode, iType, year )

def getObsMgrEmails( what, permission, iCode, iType, year ) :
    if year is None :
        year = session[ 'year' ]

    current_app.logger.info( 'getObsMgrEmails> going to get %s emails for %s %s %s ' % (what, iType, iCode, year) )

    q0 = (db.session.query( Manager, EprUser.username )
          .filter( Manager.itemType == iType )
          .filter( Manager.itemCode == iCode )
          .filter( Manager.status == 'active' )
          .filter( Manager.role == permission )
          .filter( Manager.userId == EprUser.id )
          )
    if year != -1 :
        q0 = q0.filter( Manager.year == session[ 'year' ] )

    obsMails = q0.all()
    current_app.logger.debug(
        'getObsMgrEmails> found %s %s emails for %s %s in %s' % (len( obsMails ), what, iType, iCode, year) )

    startTime = time.time()
    observerEmails = [ ]
    for m, username in obsMails :
        try :
            email = getUserEmail( username )
        except Exception as e :
            current_app.logger.error(
                'getObsMgrEmails> could not find %s email for %s - got %s ' % (what, username, str( e )) )
            email = None

        if email and not m.egroup:
            observerEmails.append( email )

        # check if the manager has an eGroup set, if so, add this to the list as well
        if m.egroup is not None :
            eGroupName = '%s@cern.ch' % m.egroup.replace( '@cern.ch', '' )
            current_app.logger.info( "getObsMgrEmails> found eGroup %s for %s %ss of %s continuing ... " % (
            eGroupName, what, m.itemType, m.itemCode) )
            observerEmails.append( eGroupName )

    current_app.logger.info( 'TimeInfo++getObsMgrEmails> %s sec to get %s observerEmails for %s %s %s %s' % (time.time() - startTime, len(observerEmails), what, iType, iCode, year) )

    return observerEmails

@cache.memoize(timeout=60)
def getEmails(user, project, activity, task, pInfo, inst, what=None):

    startTime = time.time()
    mailAddresses = set()
    try:
        email = getUserEmail(user.username)
        if email: mailAddresses.add(email)
    except Exception as e:
        current_app.logger.error( "getEmails> ERROR retrieving email for username %s - continuing with managers ... " % user.username )

    pMs = db.session.query(Manager).filter_by(itemCode=project.code,itemType='project',year=session['year'], role=Permission.MANAGEPROJ, status='active').all()
    if what and 'task' in what.lower():
        # add Pigi Paolucci for EngagementOffice for task related mails,
        # update also the cmsid of the "Engagement, Officer" in scripts/SetupDummies.py for testing.
        infoUser = db.session.query(EprUser).filter_by(cmsId=3771).one()
        mailAddresses.add( getUserEmail( infoUser.username ) )

        aMs = []
        tMs = []
        iLs = []
    else:
        aMs = db.session.query(Manager).filter_by(itemCode=activity.code,itemType='activity',year=session['year'], role=Permission.MANAGEACT, status='active').all()
        tMs = db.session.query(Manager).filter_by(itemCode=task.code,itemType='task',year=session['year'], role=Permission.MANAGETASK, status='active').all()
        iLs = db.session.query(Manager).filter_by(itemCode=inst.code,itemType='institute',year=session['year'], role=Permission.MANAGEINST, status='active').all()
    current_app.logger.info( "getEmails> got : %s project %s activity %s task %s inst managers" % (len(pMs), len(aMs), len(tMs), len(iLs)) )

    obs = []
    try: # we need manager objects below, not email addresses
        obs += db.session.query(Manager).filter_by(itemCode=project.code,itemType='project',year=session['year'], role=Permission.OBSERVE, status='active').all()
        obs += db.session.query(Manager).filter_by(itemCode=activity.code,itemType='activity',year=session['year'], role=Permission.OBSERVE, status='active').all()
        obs += db.session.query(Manager).filter_by(itemCode=task.code,itemType='task',year=session['year'], role=Permission.OBSERVE, status='active').all()
    except Exception as e:
        current_app.logger.error( "getEmails> ERROR retrieving observer emails for %s/%s/%s - got: %s " % (project.code, activity.code, task.code, str(e)) )

    # add institute obsservers ???

    for m in pMs+aMs+tMs+iLs+obs:

        # if a managed entity has an eGroup set, use this to send mails to, ignore the individual users
        # (the individual users are, for now, still used for authentication.
        #-toBeDecided: should the mails be sent to the eGroup _AND_ the individuals ??

        if m.egroup is not None:
            eGroupName = '%s@cern.ch' % m.egroup.replace('@cern.ch', '')
            mailAddresses.add( str(eGroupName) )
            current_app.logger.info("getEmails> found eGroup %s for %s managers of %s continuing ... " % (eGroupName, m.itemType, m.itemCode) )
        else:
            try:
                mgr = db.session.query(EprUser).filter_by(id=m.userId).one_or_none()
                if mgr is None:
                    current_app.logger.error( "getEmails> ERROR retrieving manager for id %s - skipping ... " % m.userId )
                else:
                    email = getUserEmail( mgr.username )
                    if email: mailAddresses.add( email )
            except Exception as e:
                current_app.logger.error( "getEmails> ERROR retrieving email for %s manager id %s - continuing ... " % (m.itemType, m.userId) )

    current_app.logger.info( "getEmails> found list of addresses: %s " % ','.join(mailAddresses) )

    current_app.logger.info( 'TimeInfo++getEmails> %s sec to get %s mailAddresses for %s %s/%s/%s' % (time.time() - startTime, len(mailAddresses), what, project.code, activity.code, task.code) )

    return mailAddresses


class PledgeInfo:
    pass

def getPledgeInfo(pledgeJson):

    user = db.session.query(EprUser).filter_by(id=pledgeJson['userId']).one()
    plInst = db.session.query(EprInstitute).filter_by(id=pledgeJson['instId']).one()

    # get the most recent info for the user/inst in this year:
    tlUser = db.session.query(TimeLineUser).filter_by(cmsId=user.cmsId, year=pledgeJson['year']).order_by(TimeLineUser.timestamp.desc()).first()
    tlInst = db.session.query(TimeLineInst).filter_by(code=tlUser.instCode, year=pledgeJson['year']).order_by(TimeLineInst.timestamp.desc()).first()

    task = db.session.query(Task).filter_by(id=pledgeJson['taskId']).one()

    pInfo = PledgeInfo()
    pInfo.taskName = task.name
    activity = task.activity
    pInfo.actName = activity.name
    project = activity.project
    pInfo.projName = project.name

    pInfo.workPledged  = pledgeJson['workTimePld']
    pInfo.workAccepted = pledgeJson['workTimeAcc']
    pInfo.workDone     = pledgeJson['workTimeDone']
    pInfo.workedSoFar  = pledgeJson['workedSoFar']
    pInfo.status       = pledgeJson['status']
    pInfo.code         = pledgeJson['code']
    pInfo.id           = pledgeJson['id']
    pInfo.task         = task

    return user, project, activity, task, pInfo, tlInst, plInst

def sendMailForPledge(pledgeJson, subject, template, reason=None):

    try:
        user, project, activity, task, pInfo, inst, plInst = getPledgeInfo(pledgeJson)
        pInfo.reason = reason
        pInfo.year = pledgeJson['year']
    except Exception as e :
        msgErr = "ERROR when preparing eMail for %s (user %s, task %s)- got %s \n %s " % (
        subject, pledgeJson[ 'userId' ], pledgeJson[ 'taskId' ], str( e ), str(traceback.format_exc()))
        current_app.logger.error( 'sendMailForPledge> ' + msgErr )
        return False

    try:
        toList = getEmails(user, project, activity, task, pInfo, inst)
    except Exception as e:
        msgErr = "ERROR when preparing list of addressees for eMail for %s (user %s, task %s)- got %s \n %s " % (subject, pledgeJson['userId'], pledgeJson['taskId'], str(e), str(traceback.format_exc()))
        current_app.logger.error( 'sendMailForPledge> ' + msgErr )
        return False

    if not toList:
        # # check if we're testing, then just return an empty set
        # if 'TEST_DATABASE_URL' in os.environ:
        #     current_app.logger.warning( 'sendMailForPledge> found TESTING mode, no mails sent' )
        #     return True
        # else:
            current_app.logger.warning( 'sendMailForPledge> no emails found at all, can not send mail' )
            return False

    webHost = 'icms-dev.cern.ch'
    if 'prod' in getProdLevel():
        webHost = 'icms.cern.ch'

    plUserName  = codecs.encode( user.name, 'utf-8').decode('utf-8')
    curUserName = codecs.encode( current_user.name, 'utf-8').decode('utf-8')
    subject = '%s for user %s' % (subject, plUserName)
    if user.username != current_user.username:
        subject += ' by %s' % curUserName
    try:
        status, msg = send_email(toList, subject, template,
                   plUserName=plUserName, curUserName=curUserName,
                   instCode=inst.code, plInst=plInst,
                   pledge=pInfo, webHost=webHost)

        if status:
            msg = "Sucessfully sent email for %s to %s " % (subject, str(toList))
            current_app.logger.info( 'sendMailForPledge> '+msg )
        else:
            current_app.logger.info( 'sendMailForPledge> ERROR from send_mail: ' + msg )
            return False

    except Exception as e:
        msgErr = "ERROR when trying to send eMail for %s (user %s, task %s)- got %s " % (subject, pledgeJson['userId'], pledgeJson['taskId'], str(e))
        current_app.logger.error( 'sendMailForPledge> ' + msgErr )
        return False

    return True

def sendMailPledgeCreated(pledgeJson):

    current_app.logger.info( "sendMailPledgeCreated> pledge: %s " % pledgeJson )
    return sendMailForPledge(pledgeJson, subject='New Pledge created', template='mail/newPledge')

def sendMailPledgeRejected(pledgeJson, reason):

    current_app.logger.info( "sendMailPledgeRejected> pledge: %s reason %s " % (pledgeJson,reason) )
    return sendMailForPledge(pledgeJson, subject='Pledge rejected', template='mail/rejectPledge', reason=reason)

def sendMailPledgeApproved(pledgeJson):

    current_app.logger.info( "sendMailPledgeApproved> pledge: %s " % pledgeJson)
    return sendMailForPledge(pledgeJson, subject='Pledge approved', template='mail/approvePledge')

def sendMailPlegdeUpdated(plOld, plNew):

    plComp = Comparator()
    plDiff = plComp.compare_dicts(plOld, plNew)

    current_app.logger.info( "sendMailPlegdeUpdated> old : %s " %  plOld )
    current_app.logger.info( "sendMailPlegdeUpdated> new : %s " %  plNew )
    current_app.logger.info( "sendMailPlegdeUpdated> diff: %s " %  plDiff)
    
    # add a timestamp so updates back to a previous value will send a new mail
    subject = 'Pledge updated (%s)' % str(datetime.datetime.now() )
    template = 'mail/updatePledge'

    try:
        user, project, activity, task, pInfo, inst, plInst = getPledgeInfo(plOld)
        pInfo.year = session['year']

        if '_update' in plDiff:
            pInfo.diff = plDiff['_update']
        else:
            pInfo.diff = None

        toList = getEmails(user, project, activity, task, pInfo, inst)

        if not toList :
            # check if we're testing, then just return an empty set
            if 'TEST_DATABASE_URL' in os.environ :
                current_app.logger.warning( 'sendMailPlegdeUpdated> found TESTING mode, no mails sent' )
                return True
            else :
                current_app.logger.warning( 'sendMailPlegdeUpdated> no emails found at all, can not send mail' )
                return False

        plUserName = codecs.encode(user.name, 'utf-8').decode('utf-8')
        curUserName = codecs.encode(current_user.name, 'utf-8').decode('utf-8')

        newInst = None
        if pInfo.diff and 'instId' in pInfo.diff:
            newInst = db.session.query(EprInstitute).filter_by(id=pInfo.diff['instId']).one()

        webHost = 'icms-dev.cern.ch'
        if 'prod' in getProdLevel( ) :
            webHost = 'icms.cern.ch'

        subject = '%s for user %s' % (subject, plUserName)
        if user.username != current_user.username:
            subject += ' by %s' % curUserName
        status, msg = send_email(toList, subject, template,
                                 plUserName=str(plUserName),
                                 curUserName=str(curUserName),
                                 instCode=inst.code, plInst = plInst, newInst=newInst,
                                 pledge=pInfo, webHost=webHost)

        if status:
            msg = "Sucessfully sent email for %s to %s " % (subject, str(toList))
            current_app.logger.info( 'sendMailPledgeUpdated> ' + msg )
        else:
            current_app.logger.info( 'sendMailPledgeUpdated> ERROR from send_mail: ' + msg )
            return False

    except Exception as e:
        msg = "ERROR when trying to send eMail for %s (userId %s, taskId %s)- got %s " % (subject, plOld['userId'], plOld['taskId'], str(e))

        current_app.logger.error( 'sendMailForPledge> ' + msg )
        return False

    return True



class TaskInfo:
    pass

def getTaskInfo(taskJson):

    user = current_user
    plInst = db.session.query(EprInstitute).filter_by(id=user.mainInst).one()

    # get the most recent info for the user/inst in this year:
    tlUser = db.session.query(TimeLineUser).filter_by(cmsId=user.cmsId, year=taskJson['year']).order_by(TimeLineUser.timestamp.desc()).first()
    tlInst = db.session.query(TimeLineInst).filter_by(code=tlUser.instCode, year=taskJson['year']).order_by(TimeLineInst.timestamp.desc()).first()

    task = db.session.query(Task).filter_by(code=taskJson['code']).one()
    lvl3 = db.session.query(Level3Name).filter( Level3Name.id == task.level3 ).one_or_none()
    if not lvl3 or lvl3 is None:
        lvl3 = db.session.query(Level3Name).filter_by(name='Unknown').one()

    pInfo = TaskInfo()
    pInfo.taskName = task.name
    pInfo.actName  = task.activity.name
    pInfo.projName = task.activity.project.name

    pInfo.code       = taskJson['code']
    pInfo.neededWork = taskJson['neededWork']
    pInfo.pctAtCERN  = taskJson['pctAtCERN']
    pInfo.taskType   = taskJson['taskType']
    pInfo.kind       = taskJson['kind']
    pInfo.comment    = taskJson['comment']
    pInfo.earliestStart = taskJson['earliestStart']
    pInfo.latestEnd  = taskJson['latestEnd']
    pInfo.status     = taskJson['status']
    pInfo.locked     = taskJson['locked']

    pInfo.lvl3Name   = lvl3.name
    pInfo.task       = task

    return user, task, pInfo, tlInst, plInst

def sendMailTaskCreated( taskJson ) :

    subject='New Task created'
    template='mail/newTask'

    return sendMail(taskJson, subject, template, getTaskInfo, what='TaskCreated')

def sendMail( taskJson, subject, template, infoFunc, what ) :

    current_app.logger.info( "sendMail-%s> task: %s " % (what, taskJson) )

    try:
        user, task, pInfo, inst, plInst = infoFunc(taskJson)
        pInfo.reason = None
        pInfo.year = session['year']

        toList = getEmails(user, project=task.activity.project, activity=task.activity, task=task,
                           pInfo=pInfo, inst=inst, what='task')
    except Exception as e:
        msgErr = "ERROR when preparing eMail for %s, task '%s' - got %s " % (subject, taskJson['name'], str(e))
        current_app.logger.error( "sendMail-%s> %s " % (what, msgErr), exc_info=True )
        return False

    if not toList:
        # check if we're testing, then just return an empty set
        if 'TEST_DATABASE_URL' in os.environ :
            current_app.logger.warning( 'sendMail-%s> %s ' % (what, 'found TESTING mode, no mails sent' ) )
            return True
        else:
            current_app.logger.warning( 'sendMail-%s> %s ' % (what, 'no emails found at all, can not send mail' ) )
            return False

    curUserName = codecs.encode( current_user.name, 'utf-8').decode('utf-8')
    subject = '%s by %s' % (subject, curUserName)
    try:
        status, msg = send_email( toList, subject, template,
                    curUserName=curUserName,
                    task=pInfo )
        if status:
            msg = "Sucessfully sent email for %s to %s " % (subject, str(toList))
            current_app.logger.info( 'sendMail-%s> %s ' % (what, msg) )
        else:
            current_app.logger.info( 'sendMail-%s> ERROR from send_mail: %s ' % (what, msg) )
            return False

    except Exception as e:
        msgErr = "ERROR when trying to send eMail for %s, task '%s' - got %s " % (subject, taskJson['name'], str(e))
        current_app.logger.error( "sendMail-%s> %s " % (what, msgErr) )
        return False

    return True

def sendMailTaskUpdated(plOld, plNew):

    subject = 'Task updated'
    template = 'mail/updateTask'
    return sendMailUpdated( plOld, plNew, subject, template, infoFunc=getTaskInfo, what='Task')

def sendMailUpdated( plOld, plNew, subject, template, infoFunc, what ) :

    plComp = Comparator()
    plDiff = plComp.compare_dicts(plOld, plNew)

    current_app.logger.info( "sendMail%sUpdated> old : %s " %  (what, plOld) )
    current_app.logger.info( "sendMail%sUpdated> new : %s " %  (what, plNew ) )
    current_app.logger.info( "sendMail%sUpdated> diff: %s " %  (what, plDiff) )

    res = ''
    try:
        user, task, pInfo, inst, plInst = infoFunc( plOld )
        pInfo.year = session['year']

        # add year to subject so it can be filtered on ...
        subject = '[%s] %s' % (pInfo.year, subject)

        if '_update' in plDiff:
            pInfo.diff = plDiff['_update']
        else:
            pInfo.diff = {}

        toList = getEmails(user, task.activity.project, task.activity, task, pInfo, inst, what='task')

        if not toList :
            # check if we're testing, then just return an empty set
            if 'TEST_DATABASE_URL' in os.environ :
                current_app.logger.warning( 'sendMail%sUpdated> found TESTING mode, no mails sent' % what)
                return True
            else :
                current_app.logger.warning( 'sendMail%sUpdated> no emails found at all, can not send mail' % what)
                return False

        plUserName = codecs.encode(user.name, 'utf-8').decode('utf-8')
        curUserName = codecs.encode(current_user.name, 'utf-8').decode('utf-8')

        newInst = None
        if pInfo.diff and 'instId' in pInfo.diff:
            newInst = db.session.query(EprInstitute).filter_by(id=pInfo.diff['instId']).one()

        if 'task' in what.lower():
            status, res = send_email(toList, subject, template,
                   curUserName=curUserName,
                   task=pInfo)
        else:
            status, res = send_email(toList, subject, template,
                   plUserName=plUserName, curUserName=curUserName,
                   instCode=inst.code, plInst = plInst, newInst=newInst,
                   pledge=pInfo)
        if status:
            msg = "Sucessfully sent email for %s to %s " % (subject, str(toList))
            current_app.logger.info( 'sendMail%sUpdated> %s ' % (what, msg) )
        else:
            current_app.logger.info( 'sendMail%sUpdated> ERROR from send_mail: %s ' % (what, res) )
            return False

    except Exception as e:
        msg = "ERROR when trying to send eMail for %s, task '%s' (res: %s) - got %s " % (subject, plOld['name'].encode('ascii', 'xmlcharrefreplace').decode('utf-8'), res, str(e))

        current_app.logger.error( 'sendMail%sUpdated> %s ' % (what, msg), exc_info=True )
        return False

    return True

