import datetime
import time
from uuid import uuid4
import re
import json

import sqlalchemy as sa
from sqlalchemy.orm import exc as sa_exc

from flask import jsonify, render_template, redirect, url_for, abort, flash, request,\
    current_app, make_response, session, g

from flask_login import login_required, current_user
from flask_sqlalchemy import get_debug_queries
from ..api_1_0.decorators import crossdomain, year_required

from . import main
from .. import db

from ..models import Permission, EprUser, EprInstitute, Project, CMSActivity, Task, Pledge, TimeLineUser, Manager, Level3Name
from ..models import getTaskTypes, getTaskShiftTypeIds

from .forms import ProjectForm, ActivityForm, TaskForm, TaskAdminForm, PledgeUserForm, PledgeAdminForm, \
                   RejectPledgeForm, YearSelectorForm, SelectProjectForm, PledgeUpdateWorkForm, PledgeSplitForm, \
                   InstRespPledgeForm, InstRespPledgeEditForm, InstRespPledgeUpdateWorkForm, InstRespPledgeRejectForm

from sqlalchemy_continuum.utils import is_modified
from sqlalchemy_continuum.plugins import ActivityPlugin

from .errors import forbidden

from ..models import commitNew, commitUpdated, CMSDbException
from icms_orm.common import UserPreferences

from .Helpers import getTaskPctDone, getInstituteInfo, getTaskAccepted, getTaskDone
from .Helpers import countrySummary, getNavInfo, flash_errors, getInstAuthForProj, getLevel3NameMap
from .Helpers import HelperError, getProjectInstCountryInfo
from .Helpers import getPledgeSum, getGuestPledgeInstSummary, getShiftInfoUser
from .Helpers import getAPTManagers, getFavTaskCodes, getFavPledgeCodes, getFavIRTPledgeCodes, doPledgeSplit
from .Helpers import getManagedPATItems, canManageSelectedInst, isApplicant, canManageProject

from .Helpers import getPledgeYear, checkCeiling, logAndRedirect, get_redirect_target, is_safe_url, getManagers, \
                     getInstRespPledgeSummary, getInstRespPledges, getLevel3Names, getLevel3NamesForProj, isInstManager

from .MailHelpers import sendMailPledgeApproved, sendMailPledgeCreated, sendMailPledgeRejected, sendMailPlegdeUpdated, getMgrEmails
from .MailHelpers import sendMailTaskCreated, sendMailTaskUpdated, getUserEmail, getObserverEmails

from functools import wraps

@main.after_app_request
def after_request(response):
    for query in get_debug_queries():
        if query.duration >= current_app.config['ICMS_SLOW_DB_QUERY_TIME']:
            current_app.logger.warning(
                'Slow query: %s\nParameters: %s\nDuration: %fs\nContext: %s\n'
                % (query.statement, query.parameters, query.duration,
                   query.context))
    return response

# make sure we redirect back to the referrer in a safe way ...
# from http://flask.pocoo.org/snippets/63/

def setupYearForm( ) :
    form = YearSelectorForm( year = 1 )
    if form.year.data and form.year.data < len( form.year.choices ) :
        selYear = form.year.choices[ form.year.data ][ 1 ]
    else : # set the default year
        selYear = 2017
    return form, selYear


def checkPledgeApproval(workTimePld, workTimeAcc, workedSoFar, workTimeDone, task, plPrevJson, totalTaskOvershoot = 1.):

    if workedSoFar is None: workedSoFar = 0.

    if workTimePld == 0:
        msg = "No work pledged: 0 months"
        return False, msg

    # check if user or task is overcommitted:
    if (workTimePld < 0 or workTimePld > 12.) or (workTimePld > task.neededWork) :
        msg = "[1] Illegal amount of work pledged: "
        if workTimePld < 0 or workTimePld > 12. :
            msg += " %2.2f months is not between 0 and 12 months." % (workTimePld,)
        if workTimePld > task.neededWork :
            msg += " %2.2f months is larger than work needed by task (code %s): %2.2f." % (
            workTimePld, task.code, task.neededWork)
        return False, msg

    if workTimeAcc > workTimePld :
        msg = "Work time accepted %2.2f can not be larger than pledged %2.2f " % (workTimeAcc, workTimePld)
        return False, msg

    sumTaskAccepted = getTaskAccepted( task )
    totAccWork = sumTaskAccepted + float( workTimeAcc )
    if plPrevJson and plPrevJson['workTimeAcc'] is not None:
        totAccWork -= plPrevJson['workTimeAcc'] # substract previous value to avoid double counting

    if totAccWork > (float( task.neededWork ) + totalTaskOvershoot):
        msg = "Total work time accepted would be %2.2f (%2.2f + %2.2f), but can not be larger than needed for task %2.2f + %2.1f month (task name: %s) " % (
        totAccWork, sumTaskAccepted, float( workTimeAcc ), task.neededWork, totalTaskOvershoot, task.name)
        return False, msg

    if workedSoFar is not None and workTimePld is not None and workedSoFar > workTimePld :
        msg = "Time worked so far %2.2f can not be larger than pledged %2.2f" % (workedSoFar, workTimePld)
        return False, msg

    if workTimeDone > workTimePld :
        msg = "Work done %2.2f can not be larger than pledged %2.2f " % (workTimeDone, workTimePld)
        return False, msg

    # check is OK, no message needed:
    return True, ''


def checkPledgeDone(workTimePld, workTimeAcc, workedSoFar, workTimeDone, task, plPrevJson, totalTaskOvershoot = 1.):

    if workedSoFar is None: workedSoFar = 0.

    # check if user or task is overcommitted:
    if (workTimePld < 0 or workTimePld > 12.) or (workTimePld > task.neededWork) :
        msg = "[1] Illegal amount of work pledged: "
        if workTimePld < 0 or workTimePld > 12. :
            msg += " %2.2f months is not between 0 and 12 months." % (workTimePld,)
        if workTimePld > task.neededWork :
            msg += " %2.2f months is larger than work needed by task (code %s): %2.2f." % (
            workTimePld, task.code, task.neededWork)
        return False, msg

    if workTimeDone > workTimePld :
        msg = "Work time done %2.2f can not be larger than pledged %2.2f " % (workTimeDone, workTimePld)
        return False, msg

    sumTaskDone = getTaskDone( task )
    totDoneWork = sumTaskDone + float( workTimeDone )
    if plPrevJson and plPrevJson['workTimeDone'] is not None:
        totDoneWork -= plPrevJson['workTimeDone'] # substract previous value to avoid double counting
    current_app.logger.debug("sumTaskDone %f totDoneWork %f needed %f" % (sumTaskDone, totDoneWork, task.neededWork) )
    if totDoneWork > (float( task.neededWork ) + totalTaskOvershoot) :
        msg = "Total work time done would be %2.2f (%2.2f + %2.2f), but can not be larger than needed for task %2.2f + %2.1f month (task name: %s) " % (
        totDoneWork, sumTaskDone, float( workTimeAcc ), task.neededWork, totalTaskOvershoot, task.name)
        return False, msg

    if workedSoFar is not None and workTimePld is not None and workedSoFar > workTimePld :
        msg = "Time worked so far %2.2f can not be larger than pledged %2.2f " % (workedSoFar, workTimePld)
        return False, msg

    # check is OK, no message needed:
    return True, ''

# =============================================================================

@main.route('/', methods=['GET', 'POST'])
def index():

    if 'year' not in session:
        session['year'] = getPledgeYear()
        session.modified = True
    if 'helpButton' not in session:
        session['helpButton'] = "Show Help"
        session.modified = True
    if 'showDetails' not in session:
        session['showDetails'] = "False"
        session.modified = True

    if current_user.is_anonymous:
        # current_app.logger.debug( "index called for anonymous user !?!? : %s " % current_user )
        return redirect( url_for('auth.login'))

    return showMine()

@main.route('/faq', methods=['GET'])
@login_required
def faq():

    return render_template('faq.html', navInfo = getNavInfo( session['year'] ))


@main.route('/toggleHelp/<string:state>', methods=['GET'])
@login_required
def toggleHelp(state):

    current_app.logger.info( "toggleHelp> session keys: %s " % str(session.keys()) )

    if 'helpButton' not in session.keys():
        session['helpButton'] = "Hide Help"
        session.modified = True

    session['helpButton'] = state
    session.modified = True

    current_app.logger.info( "toggleHelp> state %s, session %s " % (state, session['helpButton']) )

    target = get_redirect_target()
    return redirect( target or url_for('main.index') )

@main.route('/setYear/<int:selYear>', methods=['GET'])
@login_required
def setYear(selYear):

    session['year'] = selYear
    session.modified = True

    current_app.logger.info( "selYear> got: %i next=%s " % (selYear, request.args.get('next') ) )
    current_app.logger.info( "selYear> ... req.ref: %s " % str(request.referrer) )
    current_app.logger.info( "selYear> ... ses.ref: %s " % str(session.get('referrer')) )

    target = get_redirect_target()
    current_app.logger.info( "selYear> ... target: %s " % target )

    return redirect( target or url_for('main.index') )

@main.route('/check', methods=['GET', 'POST'])
@crossdomain(origin='*')
@login_required
def check():

    req = request._get_current_object()
    ADFSUser = req.headers.get('User')

    # print "\n==> req: "
    # print 'script_root: ', request.script_root
    # print 'url_root   : ', request.url_root

    selYear = session.get( 'year', datetime.datetime.utcnow( ).year )

    return render_template( 'check.html', adfsUser=ADFSUser,
                             navInfo = getNavInfo( selYear) )

@main.route('/show', methods=['GET'])
@login_required
def show():

    selYear = session.get('year')
    session['referrer'] = url_for('main.show')
    return render_template( 'showPledges.html', year=selYear,
                             metaInfo = { 'user' : None, 'inst' : None, 'projId' : -1,
                                          'prefs': False,
                                          },
                             navInfo = getNavInfo( session['year'] ) )

def getProject(id, selYear):
    try:
        proj = db.session.query(Project).filter_by( id=id ).one( )
    except sa_exc.NoResultFound:
        msg =  'could not find a project with id %s for %s in DB to start with ... giving up.' % (id, selYear)
        flash(msg)
        current_app.logger.error( msg )
        return None

    if proj.year != selYear :  # find the right project-ID for the selected year and transparently switch:
        try:
            newProj = db.session.query(Project).filter_by( name=proj.name, year=selYear ).one( )
        except sa_exc.NoResultFound:
            current_app.logger.error('could not find a project with this name %s for year %d, trying with actual pledge year: %s' % (proj.name, selYear, getPledgeYear()) )
            return None
            # try :
            #     newProj = db.session.query(Project).filter_by( name=proj.name, year=getPledgeYear() ).one( )
            #     session[ 'year' ] = getPledgeYear()
            # except sa_exc.NoResultFound:
            #     current_app.logger.error( 'could not find a project with id %s for default year %d either - giving up' % (id, getPledgeYear()) )
            #     return None

        current_app.logger.info(
            "proj id %d not availabe for %s in %d, switching to id %d" % (id, proj.name, selYear, newProj.id) )
        return newProj

    return proj

@main.route('/showProject/<int:id>', methods=['GET', 'POST'])
@login_required
def showProject(id):

    selYear = session.get('year')
    proj = None
    if id > 0:
        proj = getProject(id, selYear)
        current_app.logger.info( "showProject> found project %s for id %s in %s" % (proj.id if proj else None, id, selYear) )

        if proj is None:
            msg = 'could not find a project with same name (as id %s) for %s in DB to start with ... giving up.' % (id, selYear,)
            flash( 'ERROR: %s' % msg, 'error' )
            current_app.logger.error( 'showProject> %s ' % msg )
            return redirect( url_for('main.index') )

        if id != proj.id: return redirect( url_for('main.showProject', id=proj.id) )

    session['referrer'] = url_for('main.showProject', id=id)
    return render_template( 'showProjectPledges.html', year=selYear,
                             metaInfo = { 'user' : None, 'inst' : None, 'proj' : proj,
                                          'prefs': False,
                                          },
                             navInfo = getNavInfo( session['year'] ) )

@main.route('/showProjectTasks/<int:id>', methods=['GET', 'POST'])
@login_required
def showProjectTasks(id):

    selYear = session.get('year')

    proj = None
    pmEmails = []
    if id > 0:
        proj = getProject( id, selYear )
        current_app.logger.info( "showProjTasks> found project %s for id %s in %s" % (proj.id if proj else None, id, selYear) )

        if proj is None:
            msg = 'could not find a project with same name (as id %s) for %s in DB to start with ... giving up.' % (id, selYear,)
            flash( 'ERROR: %s' % msg, 'error' )
            current_app.logger.error( '%s - original project id: %s' % (msg, id) )
            return redirect( url_for('main.index') )

        if id != proj.id : return redirect( url_for( 'main.showProjectTasks', id=proj.id ) )

        pmEmails = ','.join( getMgrEmails('project', proj.code) )

    session['referrer'] = url_for('main.showProjectTasks', id=id)
    current_app.logger.info( "showProjTasks> ses.ref: %s " % session['referrer'])
    return render_template( 'showProjectTasks.html', projId=id,
                            pmEmails=pmEmails,
                            form=None, year=selYear,
                            user=current_user, project=proj,
                            navInfo = getNavInfo( session['year'] ) )

@main.route('/showProjectInstCountry/<int:projId>', methods=['GET', 'POST'])
@login_required
def showProjectInstCountry(projId):

    selYear = session.get('year')
    try:
        data = getProjectInstCountryInfo(projId, year=selYear)
    except HelperError as e:
        current_app.logger.error( 'showProjectInstCountry> got : %s ' % (e.msg,) )
        flash(e.msg, 'error')
        return redirect( url_for('main.index') )

    # session['referrer'] = url_for('main.showProjectInstCountry', projId=projId)

    return render_template( 'showProjectInstCountry.html',
                            year=selYear, user=current_user,
                            project=data['projReq'],
                            projInstMap=data['projInstMap'],
                            projCountryMap=data['projCountryMap'],
                            projActInstMap=data['projActInstMap'],
                            projActCountryMap=data['projActCountryMap'],
                            instSums=data['instSums'],
                            countrySums=data['countrySums'],
                            aNames=data['aNames'],
                            tNames=data['tNames'],
                            instCodes=data['instCodes'],
                            countryNames=data['countryNames'],
                            authInstRatMap = data['authInstRatMap'],
                            nAuthInst = data['nAuthInst'],
                            authExpInst = data['authExpInst'],
                            navInfo = getNavInfo( session['year'] ) )

@main.route('/showProjectInstAuthors', methods=['GET', 'POST'])
@login_required
def showProjectInstAuthors():

    selYear = session.get('year')
    projCode = 'all'
    try:
        projList, instList, authInstMap = getInstAuthForProj( projCode, selYear )
    except HelperError as e:
        current_app.logger.error( 'showProjectInstAuthors> got : %s ' % (e.msg,) )
        flash(e.msg, 'error')
        return redirect( url_for('main.index') )

    projInstSum = {}
    for proj in projList:
        sum = 0.
        for inst in instList:
            if inst not in authInstMap[proj]: continue
            sum += authInstMap[proj][inst]
        projInstSum[proj] = sum

    # print "got: ", projList
    # print "   > ", instList
    # print "   > ", authInstMap

    return render_template( 'showProjectInstAuthors.html',
                            year = selYear, user = current_user,
                            projInstMap = authInstMap,
                            projInstSum = projInstSum,
                            projNames = sorted( list(projList) ),
                            instCodes = sorted( list(instList) ),
                            navInfo = getNavInfo( session['year'] ) )

@main.route('/showInstProjectAuthors', methods=['GET', 'POST'])
@login_required
def showInstProjectAuthors():

    selYear = session.get('year')
    projCode = 'all'
    try:
        projList, instList, authInstMap = getInstAuthForProj( projCode, selYear )
    except HelperError as e:
        current_app.logger.error( 'showInstProjectAuthors> got : %s ' % (e.msg,) )
        flash(e.msg, 'error')
        return redirect( url_for('main.index') )

    projInstSum = {}
    for proj in projList:
        sum = 0.
        for inst in instList:
            if inst not in authInstMap[proj]: continue
            sum += authInstMap[proj][inst]
        projInstSum[proj] = sum

    # print "got: ", projList
    # print "   > ", instList
    # print "   > ", authInstMap

    return render_template( 'showInstProjectAuthors.html',
                            year = selYear, user = current_user,
                            projInstMap = authInstMap,
                            projInstSum = projInstSum,
                            projNames = sorted( list(projList) ),
                            instCodes = sorted( list(instList) ),
                            navInfo = getNavInfo( session['year'] ) )


@main.route('/showInstitute', methods=['GET', 'POST'])
@main.route('/showInstitute/', methods=['GET', 'POST'])
@main.route('/showInstitute/<string:code>', methods=['GET', 'POST'])
@login_required
def showInstitute( code = None):

    selYear = session.get('year')

    if code is None:
        if 'POST' in request.method:
            code = request.form.get( 'code', None )
        elif 'GET ' in request.method:
            code = request.args.get( 'code', None )

    if code is None : # no args given, use user's institute
        return redirect( url_for('main.showMyInst') )

    institute, userInfo, summary = getInstituteInfo( code=code, year=selYear )

    try:
        guestPledges = getGuestPledgeInstSummary(instCode=code, year=selYear)
    except sa.orm.exc.NoResultFound:
        guestPledges = None

    # if there are guest pledges for the institute, update the summary info
    if guestPledges:
        summary[ 'Pledged'  ] += guestPledges[ 'Pledged'  ]
        summary[ 'Accepted' ] += guestPledges[ 'Accepted' ]
        summary[ 'Done'     ] += guestPledges[ 'Done'     ]
        summary[ 'EPRaccounted' ] += guestPledges[ 'Done'     ]
        summary[ 'EPRworkDone' ]  += guestPledges[ 'Done'     ]

    summary['authDue'] = 4.*summary['actAuthors']
    summary['appDue'] = summary['sumEprDue']
    
    if not institute or institute is None:
        flash('No institute found for code "%s" ' % code, 'error')

    if 'showDetails' not in session.keys():
        session['showDetails'] = "hideDetails"
        session.modified = True
    showDetails = session.get('showDetails', 'hideDetails')

    isEngOff = current_user.is_engOfficer()
    current_app.logger.debug(" user %s is engagement officer: %s" % (current_user.username, isEngOff) )

    return render_template( 'showInstitute.html', institute=institute,
                            form=None, year=selYear,
                            showDetails = showDetails,
                            guestPledges = guestPledges,
                            isEngOff = isEngOff,
                            data=userInfo, summary=summary,
                            toPct = 100.,
                            navInfo = getNavInfo( session['year'] ) )

@main.route('/statistics', methods=['GET', 'POST'])
@login_required
def statistics():

    selYear = session.get('year')
    maxYear = datetime.date.today().year
    # only show the actual year if we're in Dec:
    if datetime.date.today().month > 11:
        maxYear += 1

    instInfo = {}
    return render_template( 'stats.html', form=None,
                                endYear = maxYear,
                                navInfo = getNavInfo( session['year'] ) )

@main.route('/showInstitutes', methods=['GET', 'POST'])
@login_required
def showInstitutes():

    selYear = session.get('year')

    instInfo = {}

    return render_template( 'showInstitutes.html', form=None,
                                instInfo=instInfo, year=selYear,
                                navInfo = getNavInfo( session['year'] ) )

@main.route('/showCountries', methods=['GET', 'POST'])
@login_required
def showCountries():

    selYear = session.get('year')

    return render_template( 'showCountries.html', form=None,
                                year=selYear,
                                navInfo = getNavInfo( session['year'] ) )


@main.route('/showCountry', methods=['GET'])
@main.route('/showCountry/<string:name>', methods=['GET', 'POST'])
@login_required
def showCountry(name=None):

    if name is None :
        if 'POST' in request.method:
            name = request.form.get( 'name', None )
        elif 'GET ' in request.method:
            name = request.args.get( 'name', None )

    if name is None : # no args given, use user's country
        return redirect( url_for('main.showMyCountry') )

    selYear = session.get('year')
    countryInstInfo, summary = countrySummary( name, selYear )

    return render_template( 'showCountry.html', form=None,
                            year=selYear,
                            country=name,
                            summary=summary,
                            navInfo = getNavInfo( session['year'] ) )

@main.route('/showMyInst', methods=['GET'])
@login_required
def showMyInst():

    inst = db.session.query(EprInstitute).filter_by(id = current_user.mainInst).one()
    return redirect( url_for('main.showInstitute', code=inst.code) )

@main.route('/showMyCountry', methods=['GET'])
@login_required
def showMyCountry():

    inst = db.session.query(EprInstitute).filter_by(id = current_user.mainInst).one()
    return redirect( url_for('main.showCountry', name=inst.country) )

@main.route('/showMyInstPledges', methods=['GET', 'POST'])
@login_required
def showMyInstPledges():

    inst = db.session.query(EprInstitute).filter_by(id=current_user.mainInst).one()

    selYear = session.get('year')
    session['referrer'] = url_for('main.showMyInstPledges')
    return render_template( 'showPledges.html', year=selYear,
                             metaInfo = { 'user' : None, 
                                          'inst' : inst, 
                                          'proj' : None,
                                          'prefs': False,
                                          },
                             navInfo = getNavInfo( session['year'] ) )


@main.route('/showManagedPledges', methods=['GET', 'POST'])
@main.route('/showManagedPledges/<string:username>', methods=['GET', 'POST'])
@login_required

def showManagedPledges(username = None):
    user = current_user
    if username is not None:
        user = db.session.query(EprUser).filter_by(username=username).one()
        current_app.logger.info( 'going to show managed pledges for user %s' % user.username )

    if not user:
        current_app.logger.info( 'user with username %s not found, resetting to current user (%s)' % (username, user.username) )
        user = current_user

    selYear = session.get('year')
    session['referrer'] = url_for('main.showManagedPledges')

    current_app.logger.info('request to show pledges for tasks managed by %s in %s' % (user.username, selYear))

    myActivities, myInsts, myProjects, myTasks, pMap = getManagedPATItems( selYear )

    taskIds = [x.id for x in myTasks]
    for p, acts in myActivities.items():
        for act in acts:
            taskIds += [ t.id for t in db.session.query(Task).filter(Task.activityId == act.id).all() ]

    if not taskIds:
        msg = 'no managed tasks found for %s ' % user.username
        current_app.logger.warning( 'showManagedPledges> %s'  % msg )
        flash( msg, 'error' )
        return redirect( url_for('main.showMine') )

    plCodes = [ x[0] for x in db.session.query( Pledge.code ).filter( Pledge.taskId.in_(taskIds) ).all() ]
    if not plCodes:
        msg = 'no pledges for managed tasks found for %s ' % user.username
        current_app.logger.warning( 'showManagedPledges> %s'  % msg )
        flash( msg, 'error' )
        return redirect( url_for('main.showMine') )

    current_app.logger.info( 'showManagedPledges> found %d pledges in %d managed tasks for manager %s in %s ' % ( len(plCodes), len(taskIds), user.username, selYear ) )

    return render_template( 'showPledges.html', year=selYear,
                             userShifts = None, shiftInsts = None,
                             metaInfo = { 'user' : None,
                                          'inst' : None,
                                          'proj' : None,
                                          'prefs': False,
                                          'mgt': True,
                                          'taskCodes' : None,
                                          'plCodes' : ','.join(plCodes),
                                          },
                             user = user,
                             navInfo = getNavInfo( session['year'] ) )


@main.route('/showFavourites', methods=['GET'])
@main.route('/showFavourites/<string:username>', methods=['GET', 'POST'])
@login_required
def showFavourites(username = None):

    if username is None:
        user = current_user
    else:
        user = db.session.query(EprUser).filter_by(username=username).one()
        current_app.logger.debug( 'going to show favourites for user %s' % user.username )

    if not user:
        current_app.logger.info( 'user with username %s not found, resetting to current user (%s)' % (username, user.username) )
        user = current_user
    else:
        current_app.logger.info( 'request to show favourites for user %s ' % (user.username,) )

    selYear = session.get('year')
    session['referrer'] = url_for('main.showFavourites')

    taskCodes = getFavTaskCodes(user, selYear)
    pledgeCodes = getFavPledgeCodes(user, selYear)
    irtPlCodes  = getFavIRTPledgeCodes(user, selYear)

    if not taskCodes:
        msg = 'no favourite tasks or pledges found for %s ' % user.username
        current_app.logger.warning(msg)
        flash( msg, 'error' )
        return redirect( url_for('main.showMine') )

    current_app.logger.info( 'found %d favourited tasks for user %s in %s ' % ( len(taskCodes), user.username, selYear ) )
    current_app.logger.info( 'found %d favourited pledges for user %s in %s ' % ( len(pledgeCodes), user.username, selYear ) )
    current_app.logger.info( 'found %d InstRespTask pledges for user %s in %s ' % ( len(irtPlCodes), user.username, selYear ) )

    return render_template( 'showPledges.html', year=selYear,
                             userShifts = None, shiftInsts = None,
                             metaInfo = { 'user' : None,
                                          'inst' : None,
                                          'proj' : None,
                                          'prefs': True,
                                          'taskCodes' : ','.join( taskCodes ),
                                          'plCodes' : ','.join( pledgeCodes ),
                                          'irtPlCodes' : ','.join( irtPlCodes ),
                                          },
                             user = user,
                             navInfo = getNavInfo( session['year'] ) )

@main.route('/showMine', methods=['GET', 'POST'])
@login_required
def showMine():

    return showUser(current_user.username)

@main.route('/showUser', methods=['GET'])
@main.route('/showUser/<string:username>', methods=['GET','POST'])
@login_required
def showUser(username = None):

    if username is None:
        if 'POST' in request.method:
            username = request.form.get( 'username', None )
        elif 'GET ' in request.method:
            username = request.args.get( 'username', None )

    if username is None or username.strip() == '':
        current_app.logger.warning( "showUser> empty username found, redirecting to login page" )
        return redirect( url_for('auth.login') )

    try:
        user = db.session.query(EprUser).filter_by(username=username).one()
    except Exception as e:
        msg = "ERROR"
        if 'No row was found for one' in str(e):
            msg = 'Sorry, user with username %s not found in DB' % username
        else:
            msg = str(e)

        current_app.logger.error( "showUser> ERROR for %s from DB: %s" % (username, str(msg)) )
        flash(msg, 'error')
        target = get_redirect_target()
        return redirect( target or url_for('main.index') )

    selYear = session.get('year')
    session['referrer'] = url_for('main.showUser', username=username)

    shiftData, institutes = getShiftInfoUser(username, selYear)

    current_app.logger.debug( "showUser> len(shiftData) %s, len(institutes) %s " % (len(shiftData), len(institutes)) )

    user.instCode, = db.session.query(EprInstitute.code).filter(EprInstitute.id == user.mainInst).one()

    return render_template( 'showUserPledges.html', year=selYear,
                             userShifts = shiftData, shiftInsts = institutes,
                             metaInfo = { 'user' : user, 'inst' : None, 'proj' : None,
                                          'prefs': False,
                                          },
                             navInfo = getNavInfo( session['year'] ) )

@main.route('/findUser', methods=['GET'])
@login_required
def findUser():

    selYear = session.get('year')

    return render_template( 'showUser.html',
                            form=None, year=selYear,
                            navInfo = getNavInfo( session['year'] ) )

@main.route('/list', methods=['GET', 'POST'])
@login_required
def listAll():

    ADFSGroups = request.args.get('ADFSGroups')

    show_all = False
    if current_user.is_authenticated:
        show_all = bool(request.cookies.get('show_all', ''))
    groups = None
    if show_all:
        groups = ADFSGroups
    return render_template('index.html',
                           projects   = db.session.query(Project).all(),
                           activities = db.session.query(CMSActivity).all( ),
                           tasks      = db.session.query(Task).all(),
                           pledges    = db.session.query(Pledge).all(),
                           groups     = groups )

@main.route('/all')
@login_required
def show_all():
    resp = make_response(redirect(url_for('.index')))
    resp.set_cookie('show_all', '', max_age=30*24*60*60)
    return resp


@main.route('/newProject', methods=['GET','POST'])
@login_required
def newProject():

    if not ( current_user.is_administrator() ) :
        return forbidden('Insufficient privileges')

    form = ProjectForm()

    if form.validate_on_submit():
        if db.session.query(Project).filter_by(name=form.name.data, year=session['year']).all():
            flash("ERROR - Integrity error from database, the name of the project is already used.", 'error')
            form.name.data = ''
            return render_template('newItem.html', form=form,
                                    what='Project', parent=None,
                                    navInfo = getNavInfo( session['year'] ) )

        code = form.code.data
        if not code or len(code) == 0: code = uuid4()
        proj = Project(name = form.name.data,
                       desc=form.description.data,
                       code=str(code),
                       year=session['year'],
                       mxLvl=1,
                       )
        try :
            commitNew(proj)
        except Exception as e:
            flash('Error from DB when trying to add new project.\n(%s)' % str(e), 'error')
            return redirect( url_for('main.index') )

        flash('A new project was successfully created.')
    return render_template('newItem.html', form=form,
                            what='Project', parent=None,
                            navInfo = getNavInfo( session['year'] ) )

@main.route('/project/<int:id>', methods=['GET', 'POST'])
@login_required
def project(id):
    proj = db.session.query(Project).filter_by(id=id).first()
    if not proj:
        return abort(404)
    activities = proj.activities
    form = None
    if current_user.canManage('project', proj.code, year=session['year']) or \
        current_user.is_administrator():

        form = ProjectForm(obj=proj)
        if form.validate_on_submit():
            proj.name = form.name.data
            proj.description = form.description.data

            try :
                commitUpdated(proj)
            except Exception as e:
                flash('Error from DB when trying to update project.\n(%s)' % str(e), 'error')
                return redirect( url_for('main.index') )

            flash('Project info successfully updated.')

    return render_template('project.html', project=proj, act=activities, form=form)

@main.route('/findProjectForActivity', methods=['GET','POST'])
@login_required
@year_required(datetime.datetime.today().year)
def findProjectForActivity():

    form = SelectProjectForm()
    form.project.choices =  [ (i.code, i.name) for i in db.session.query(Project).filter_by(year=session['year'], status='ACTIVE').order_by('name')]

    if form.validate_on_submit():
        current_app.logger.info( "findProjectForActivity> form.proj=%s " % str(form.project) )

        projCode = form.project.data
        current_app.logger.info( "findProjectForActivity> got proj %s " % str(projCode) )

        return redirect( url_for('main.newActivity', projCode=projCode) )

    return render_template('selectProject.html',
                            year = session['year'],
                            form=form,
                            navInfo = getNavInfo( session['year'] ) )


@main.route('/newActivity/<string:projCode>', methods=['GET','POST'])
@login_required
@year_required(datetime.datetime.today().year)
def newActivity(projCode):

    # As there is no project (hence no projCode), we do no yet know the managers,
    # so we redirect before the checking
    if projCode == 'None':
        return redirect( url_for('main.findProjectForActivity') )

    if not (current_user.is_administrator() or
           current_user.canManage('project', projCode, year=session['year']) ) :
        flash('Sorry, you are not allowed to create activities for this project', 'error')
        return forbidden('Insufficient privileges')

    proj = db.session.query(Project).filter_by(code=projCode).one()

    current_app.logger.info( "main.newActivity> found proj %s for code %s" % (proj.name, projCode) )

    form = ActivityForm()

    if form.validate_on_submit():
        if db.session.query(CMSActivity).filter_by( name=form.name.data,
                                        projectId=proj.id,    # check only in the same project
                                        year=session['year'] ).all( ):
            flash("ERROR - Integrity error from database, the name of the activity is already used in this project.", 'error')
            form.name.data = ''
            return render_template('newItem.html', form=form,
                                   what='Activity', parent=proj,
                                   navInfo = getNavInfo( session['year'] ) )

        act = CMSActivity( name = form.name.data,
                           desc=form.description.data,
                           proj=proj,
                           code=str(uuid4()),
                           year=session['year']
                           )
        try:
            commitNew( act )
        except Exception as e:
            flash('Error from DB when trying to add new activity.\n(%s)' % str(e), 'error')
            return redirect( url_for('main.index') )

        flash('A new activity was successfully created.')
        return redirect( url_for('manage.project', code=proj.code) )

    return render_template('newItem.html', form=form,
                           year=session[ 'year' ],
                           what='Activity', parent=proj, grandParent=None,
                           navInfo = getNavInfo( session['year'] ) )


@main.route('/findActivityForTask', methods=['GET','POST'])
@login_required
@year_required(datetime.datetime.today().year)
def findActivityForTask():

    return render_template('selectActivity.html',
                            year = session['year'],
                            navInfo = getNavInfo( session['year'] ) )


@main.route('/makeNewTask', methods=['GET', 'POST'])
@login_required
@year_required(datetime.datetime.today().year)
def makeNewTask():

    actCode  = request.args.get('activities', None)

    return redirect( url_for('main.newTask', actCode=actCode) )

def getProjQTNeeds(prjCode, selYear):
    
    sq = ( db.session.query( sa.func.sum( Task.neededWork ) )
                    .join( CMSActivity, CMSActivity.id ==  Task.activityId )
                    .join( Project, Project.id == CMSActivity.projectId )
                    .filter( Task.status == 'ACTIVE' )
                    .filter( CMSActivity.status == 'ACTIVE' )
                    .filter( Project.status == 'ACTIVE' )
                    .filter( Project.code == prjCode )
                    .filter( Task.year == selYear )
    )

    sumNeeds = sq.one()

    qtNeedsRaw = sq.filter( Task.tType == 'Qualification' ).one()
    qtNeeds = qtNeedsRaw[0]

    if qtNeeds == None:
        qtNeeds = 0.

    current_app.logger.info( 'QT needs for project %s in %s are %s (%s)' % (prjCode, selYear, sumNeeds, qtNeeds) )
    return sumNeeds[0], qtNeeds

@main.route('/newTask/<string:actCode>', methods=['GET','POST'])
@login_required
@year_required(datetime.datetime.today().year)
def newTask(actCode):

    # As there is no activity (hence no actCode), we do no yet know the managers,
    # so we redirect before the checking
    if actCode == 'None' or actCode == 'Please select ...':
        return redirect( url_for('main.findActivityForTask') )

    selYear = session['year']
    act = db.session.query(CMSActivity).filter_by( code=actCode ).one( )

    # allow this only for admins and Project managers
    #-toDo: create management role (for Kerstin) and allow this as well
    #       and/or implement workflow for new tasks (or modifications of workNeeded)

    # in preparation for next year: allow activity managers to create a new task for the next year (wrt. getPledgeYear()
    if not ( current_user.is_administrator() or
             current_user.canManage('project', act.project.code, year=selYear) or
             ( selYear == getPledgeYear()+1 and current_user.canManage('activity', act.code, year=selYear) )
             ) :
        msg = 'Sorry, you (%s) are not allowed to create tasks for this activity. (%s)' % (current_user.username, current_user.canManage('project', act.project.code, year=selYear))
        flash(msg, 'error')
        return forbidden('Insufficient privileges')

    form = TaskAdminForm()

    # percentage of time needed at CERN
    form.pctAtCERN.choices = [ (i, i) for i in sorted( set( list(range(0,110,10)) + list(range(0,110,25)) ) ) ]

    # e.g. Perennial, One-off
    form.tType.choices = [ (i, i) for i in getTaskTypes() ]
    form.shiftTypeId.choices = [('','')]+[ (i, i) for i in getTaskShiftTypeIds() ]

    if form.validate_on_submit():

        current_app.logger.debug( " ++> form0: %s" % str(form) )
        current_app.logger.debug( " ++> form1: %s" % [ '%s : %s' % (str(x), str(y)) for x,y in form.__dict__.items() ] )
        current_app.logger.debug( " ++> form2: %s" % [ '%s : %s' % (str(x), str(y)) for x,y in form.locked.__dict__.items() ] )

        if not form.description.data or form.description.data.strip() == '':
            msg = "ERROR: a description is needed for the task"
            current_app.logger.error(msg)
            flash( msg, 'error' )
            return render_template( 'newTask.html', form=form, task=None, activity=act,
                                    navInfo=getNavInfo( session[ 'year' ] ) )

        taskInDb = db.session.query(Task).filter_by(name=form.name.data,
                                        activityId=act.id,
                                        year=session['year']).all()
        if taskInDb:
            msg = "ERROR - Integrity error from database, the name of the task is already used in this activity."
            current_app.logger.error(msg)
            flash(msg, 'error')
            form.name.data = ''
            return render_template('newTask.html', form=form, task=taskInDb, activity=act,
                                   navInfo = getNavInfo( session['year'] ) )

        # Check that newly committed needs are within ceiling for project
        newNeeds = float( form.neededWork.data)
        isWithin, actNeeds, ceiling = checkCeiling( session['year'], act.project, newNeeds )
        if not isWithin:
            msg = 'ERROR - requested amount of needed work (%s) on top of actual sum (%s) exceeds ceiling for project %s' % (newNeeds, actNeeds, ceiling)
            current_app.logger.error(msg)
            flash(msg, 'error')
            return render_template('newTask.html', form=form, task=taskInDb, activity=act,
                                   navInfo = getNavInfo( session['year'] ) )

        current_app.logger.info( ' ===> task %s locked from form: %s ' % (form.name.data, form.locked.data) )

        # for QTs: check that sum(QualificationTaskNeeds) < 0.1 * sum(projNeeds)
        if form.tType.data == 'Qualification':
            prjNeeds, qtNeeds = getProjQTNeeds(act.project.code, session['year'])
            newQTneeds = qtNeeds + float( form.neededWork.data )
            if newQTneeds > 0.1 * (prjNeeds-qtNeeds):
                msg = 'ERROR - requested amount of needed work for Qualification Task on top of actual sum (%s) exceeds threshold (10%) for project %s' % (newQTneeds, 0.1*prjNeeds)
                current_app.logger.error(msg)
                flash(msg, 'error')
                return render_template('newTask.html', form=form, task=taskInDb, activity=act,
                                    navInfo = getNavInfo( session['year'] ) )

        current_app.logger.info( '\n ===> lvl3 from form: %s \n' % (form.level3.data,) )
        lvl3 = form.level3.data
        if not lvl3 or lvl3 is None:
            lvl3 = db.session.query(Level3Name).filter_by(name='Unknown').one()
            lvl3ID = lvl3.id
            lvl3Name = lvl3.name
        else:
            try:
                lvl3ID, lvl3Name = re.match('^\s*(\d+)\s*:\s*(.*)\s*', lvl3).groups()
            except AttributeError:
                lvl3 = db.session.query(Level3Name).filter_by(name=lvl3).one()
                lvl3ID = lvl3.id
                lvl3Name = lvl3.name
            except Exception as e:
                msg = 'ERROR with apparently valid Lvl3 name found in form: "%s" - matching gave: %s' % (lvl3, str(e))
                flash( 'Please make sure to select a valid level-3 name from the pop-up list! ', 'error' )
                current_app.logger.error(msg)
                raise e


        task = Task(name = form.name.data,
                    desc = form.description.data,
                    act = act,
                    pctAtCERN = float( form.pctAtCERN.data ),
                    tType = form.tType.data,
                    shiftTypeId = form.shiftTypeId.data,
                    comment = form.comment.data,
                    needs  = float( form.neededWork.data ),
                    locked = form.locked.data if form.locked.data else False,
                    kind   = form.kind.data,
                    level3 = int(lvl3ID),
                    code = str(uuid4()),
                    year = session['year']
                    )
        try:
            commitNew(task)
        except Exception as e:
            msg = 'Error from DB when trying to add new task.\n(%s)' % str(e)
            current_app.logger.error(msg)
            flash(msg, 'error')
            return redirect( url_for('main.index') )

        if not sendMailTaskCreated( task.to_json() ):
            msg = "ERROR when sending mail for new task"
            current_app.logger.error(msg)
            flash(msg, 'error')

        flash('A new task was successfully created.')
        return redirect( url_for('manage.activity', code=act.code) )

    else: # form did not validate or first round:
        if form.errors:
            # make sure the new value for the Level3 name is kept in case of error:            
            # lvl3 = db.session.query(Level3Name).filter(Level3Name.name == request.form.get('lvl3Name')).one()
            # form.level3.process_data(lvl3.id)

            msg = '\nnewTask> validate on submit failed ... :( \nform:\n'
            msg += str( form.errors )
            msg += '\n'
            current_app.logger.error( msg )
            flash_errors( form )

    return render_template('newTask.html', form=form, task=None, activity=act,
                            navInfo = getNavInfo( session['year'] ) )

@main.route('/task/<int:id>', methods=['GET', 'POST'])
@login_required
def task(id):

    task = db.session.query(Task).filter_by(id=id).order_by(Task.timestamp).first()
    if not task:
        return abort(404)
    act = task.activity

    selYear = session['year']

    form = None
    activePledges = None
    target = None
    if 'referrer' in session and is_safe_url( session[ 'referrer' ] ):
        target = session[ 'referrer' ]

    isAdmin = False
    canManage = False
    if current_user.is_administrator() or \
        current_user.canManage('activity', task.activity.code, year=selYear) or \
        current_user.canManage('project', task.activity.project.code, year=selYear) :

        canManage = True
        current_app.logger.info( "task> user %s can edit task " % current_user.username )

        activePledges = (db.session.query(Pledge).filter_by( taskId=task.id )
                                     .filter(Pledge.status != 'rejected')
                                     .filter(Pledge.year == selYear)
                                     .all())

        form = None
        msg = ''

        # allow full editing only for admins and Project managers
        # -toDo: create management role (or eGroup??) (for Kerstin) and allow this as well
        #       and/or implement workflow for new tasks (or modifications of workNeeded)
        # in preparation for next year: allow activity managers to create a new task for the next year (wrt. getPledgeYear())
        if ( current_user.is_administrator() or
             current_user.canManage('project', task.activity.project.code, year=selYear)  or
             ( selYear == getPledgeYear()+1 and current_user.canManage('activity', act.code, year=selYear)
            ) ):
            isAdmin = True
            form = TaskAdminForm( obj=task )
            current_app.logger.info( "task> user %s can admin-edit task " % current_user.username )
        else:
            form = TaskForm( obj=task )

        form.tType.choices = [ (i, i) for i in getTaskTypes() ]

        if isAdmin and task.shiftTypeId:
            current_app.logger.info( "setting shiftTypeId '%s' " % task.shiftTypeId.strip().lower() )
            form.shiftTypeId.choices = [(task.shiftTypeId.strip().lower(),task.shiftTypeId.strip().lower())]
            form.shiftTypeId.choices += [('','')] + [ (x,x) for x in getTaskShiftTypeIds() if x != task.shiftTypeId.strip().lower() ]
        else:
            if isAdmin : # only the TaskAdminForm has these fields
                current_app.logger.info( "setting shiftTypeId for non-shift task" )
                form.shiftTypeId.choices = [('','')] + [ (x,x) for x in getTaskShiftTypeIds() ]

        if isAdmin:
            form.level3.choices = [(0, '')] + [(x.id, x.name) for x in getLevel3Names(selYear)]
            actL3 = db.session.query(Level3Name).filter_by(id=task.level3).one_or_none()
            if not actL3 or actL3 is None:
                actL3 = db.session.query(Level3Name).filter_by(name='Unknown').one()
            current_app.logger.info("setting lvl3Name '%s' (%d) " % (actL3.name, actL3.id) )
            form.level3.process_data(actL3.id)

        if form.validate_on_submit():

            current_app.logger.info( 'task> subCan %s ' % request.form['subCan'] )

            if request.form.get('subCan') == 'Cancel':
                flash('Updating task was cancelled ... ')
                if is_safe_url( session['referrer'] ): target = session['referrer']
                current_app.logger.info( "tasks> target %s, ses.ref: %s" % (target, session['referrer']) )

                return redirect( target or url_for('main.index') )

            # if name has changed, check if we have a clash ...
            if task.name != form.name.data :
                if db.session.query(Task).filter_by(name=form.name.data,
                                    activityId=task.activity.id,     # check only in the same activity
                                    year=selYear).all():
                    flash("ERROR - Integrity error from database, the name of the task is already used in this activity.", 'error')
                    form.name.data = task.name
                    return render_template('task.html', task=task, activities=act,
                                           isAdmin=isAdmin, activePledges=activePledges, target=target,
                                           form=form, taskPctDone=getTaskPctDone(task),
                                           navInfo = getNavInfo( selYear ) )

            # keep old values for mail:
            oldTaskJson = task.to_json()

            # Check that newly committed needs are within ceiling for project, correct for already existing needs:
            newNeeds = float( form.neededWork.data ) - float(task.neededWork)
            isWithin, actNeeds, ceiling = checkCeiling( session[ 'year' ], act.project, newNeeds )
            if not isWithin :
                msg = 'ERROR - requested amount of effective needed work (%s (new-old)) on top of actual sum (%s) exceeds ceiling %s for project %s' % (
                newNeeds, actNeeds, ceiling, act.project.name)
                current_app.logger.error( msg )
                flash( msg, 'error' )
                return render_template( 'task.html', task=task, activities=act,
                                        isAdmin=isAdmin, activePledges=activePledges, target=target,
                                        form=form, taskPctDone=getTaskPctDone( task ),
                                        navInfo=getNavInfo( selYear ) )
            else:
                msg = "Request OK, effective needed work for task (%s (new-old)) fits within ceiling for project (%s)" % (newNeeds, ceiling)
                current_app.logger.error( msg )

            # update task
            task.name = form.name.data
            task.description = form.description.data
            task.pctAtCERN = form.pctAtCERN.data
            task.tType = form.tType.data
            task.comment = form.comment.data
            if isAdmin :
                task.shiftTypeId = form.shiftTypeId.data
                task.neededWork = form.neededWork.data
                if task.kind != form.kind.data:
                    msg += 'WARNING: kind of task can not be changed, will keep it at: %s' % task.kind
                    flash( msg )
            task.locked = form.locked.data

            #-toDo: add logging
            task.timestamp = datetime.datetime.utcnow()

            try:
                commitUpdated(task)
            except Exception as e:
                msg += '\nError from DB when trying to update task.\n(%s)' % str(e)
                flash(msg, 'error')
                current_app.logger.error( msg )
                if is_safe_url( session['referrer'] ): target = session['referrer']
                current_app.logger.error( "tasks> target %s, ses.ref: %s" % (target, session['referrer']) )
                return redirect( target or url_for('main.index') )

            newTaskJson = db.session.query(Task).filter_by(code = task.code).one().to_json()
            if not sendMailTaskUpdated( oldTaskJson, newTaskJson ) :
                msg += "\nERROR when sending mail for updating task"
                current_app.logger.error( msg )
                flash( msg, 'error' )

            msg += '\nTask info successfully updated.'
            flash( msg )
            if is_safe_url( session['referrer'] ): target = session['referrer']
            current_app.logger.info( "tasks> target %s, ses.ref: %s" % (target, session[ 'referrer' ]) )
            return redirect( target or url_for('main.index') )

        else: # validation failed
            if form.errors:
                msg += 'main.views.task(id=%s)> ERROR in form: %s' % (task.id, str(form))
                msg += '\ntask> validate on submit failed ... :( \nform:\n'
                msg += str( form.errors )
                msg += '\n'
                current_app.logger.error( msg )
                flash_errors(form)

    return render_template('task.html', task=task, activities=act,
                           isAdmin=canManage, activePledges=activePledges,
                           target=target,
                           form=form, taskPctDone=getTaskPctDone(task),
                           navInfo = getNavInfo( selYear ) )

def checkPledgingAllowed():

    if current_user.is_wizard(): return True

    if 'assets' not in session:
        current_app.logger.warning( "checkPledgingAllowed> no assets found, enabling pledges " )
        return True

    pledgesEnabled = int(session['assets'].get('pledgesEnabled', -1))
    current_app.logger.info( "checkPledgingAllowed> pledgesEnabled: %s " % str(pledgesEnabled) )

    return pledgesEnabled == -1 or pledgesEnabled == 1

@main.route('/makeNewPledge', methods=['GET', 'POST'])
@login_required
def makeNewPledge():

    if not checkPledgingAllowed():
        msg = 'Sorry, creating new pledges is not allowed'
        flash( msg, 'error' )
        current_app.logger.error('makeNewPledge> %s' % msg)
        return redirect( get_redirect_target() or url_for('main.showMine') )

    task = request.args.get('tasks', None)

    if not task:
        return render_template('selectTask.html',
                               year=session[ 'year' ],
                               user = current_user,
                               navInfo = getNavInfo( session['year'] ) )

    return redirect( url_for('main.newPledge', taskCode=task) )

@main.route('/newPledgeForUser/<string:cmsId>', methods=['GET'])
@login_required
def newPledgeForUser(cmsId):

    if not checkPledgingAllowed():
        msg = 'Sorry, creating new pledges is not allowed'
        flash( msg, 'error' )
        current_app.logger.error('makeNewPledge> %s' % msg)
        return redirect( get_redirect_target() or url_for('main.showMine') )

    return render_template( 'selectTask.html',
                            year=session[ 'year' ],
                            user=db.session.query(EprUser).filter_by(cmsId=cmsId).one(),
                            navInfo=getNavInfo( session[ 'year' ] ) )

@main.route('/newPledgePost', methods=['GET','POST'])
@login_required
def newPledgePost():

    if not checkPledgingAllowed():
        msg = 'Sorry, creating new pledges is not allowed'
        flash( msg, 'error' )
        current_app.logger.error('makeNewPledge> %s' % msg)
        return redirect( get_redirect_target() or url_for('main.showMine') )

    if request.method.lower() == 'post':
        cmsId    = request.form.get('cmsId', None)
        taskCode = request.form.get('tasks', None)
    else: 
        cmsId    = request.args.get('cmsId', None)
        taskCode = request.args.get('tasks', None)

    if not cmsId :
        try:
            current_app.logger.warning( 'newPledgePost> no cmsId given, resetting to %s ! Form: "%s" ' % (current_user.cmsId, str(request.form) ) )
        except UnicodeEncodeError:
            pass
        cmsId = current_user.cmsId

    if not taskCode:
        try:
            current_app.logger.warning( 'newPledgePost> no taskCode given ! Form: "%s" ' % str(request.form) )
        except UnicodeEncodeError:
            pass
        return url_for('main.makeNewPledge')

    return createNewPledge(taskCode, cmsId)

@main.route('/newPledge/<string:taskCode>', methods=['GET','POST'])
@login_required
def newPledge(taskCode):

    return createNewPledge(taskCode, current_user.cmsId)


def getManagedInsts( user, selYear ):

    instList = (db.session.query(Manager.itemCode)
                .filter(Manager.status.in_( ['active', 'allowed'] ) )
                .filter(Manager.year == selYear)
                .filter(Manager.userId == user.id)
                .filter(Manager.itemType == 'institute')
                .all() )
    current_app.logger.debug('getManagedInsts> found insts managed by %s in %s: %s' % (user.username, selYear, instList) )
    return instList

def createNewPledge(taskCode, cmsIdIn):

    if not checkPledgingAllowed():
        msg = 'Sorry, creating new pledges is not allowed'
        flash( msg, 'error' )
        current_app.logger.error('createNewPledge> %s' % msg)
        return redirect( get_redirect_target() or url_for('main.showMine') )

    selYear = session['year']

    try:
        cmsId = int(cmsIdIn)
    except:
        current_app.logger.warning('createNewPledge> got cmsID %s which can not be converted to int. Reverting to cmsId of current user (%s, %s)' % (cmsIdIn, current_user.cmsId, current_user.username) )
        cmsId = current_user.cmsId

    current_app.logger.debug('createNewPledge> request to create new pledge for taskCode %s, cmsId %s in %s! ' % (taskCode, selYear, cmsId) )

    if not taskCode:
        current_app.logger.warning('no taskCode given to createNewPledge ! ')
        return url_for('main.makeNewPledge')

    if any( x in taskCode.strip() for x in ['Please', 'select']) :
        return logAndRedirect( msg="Could not find a task for code %s ! " % (taskCode,), redirUrl=url_for( 'main.index' ), level='error')

    task = None
    try:
        if taskCode.strip() != '':
           task = db.session.query(Task).filter_by(code=taskCode).one()
    except Exception as e:
        return logAndRedirect( msg="Could not find a task for code %s ! got: %s " % (taskCode, str(e)), redirUrl=url_for( 'main.index' ), level='error')

    curUserInst = db.session.query(EprInstitute).filter_by(id=current_user.mainInst).one()
    isCurrentInstMgr = current_user.is_administrator() or current_user.canManage('institute', curUserInst.code, year=session['year'])
    isCurrentProjMgr = current_user.is_administrator() or current_user.canManage('project', task.activity.project.code, year=session['year'])
    isCurrentTaskMgr = current_user.is_administrator() or current_user.canManage('task', task.code, year=session['year'])
    canManageSomeInst = getManagedInsts( current_user, selYear ) != []
    canPledgeForUser = isCurrentInstMgr or isCurrentTaskMgr

    current_app.logger.info( 'createNewPledge++> user %s: isCurrentInstMgr: %s, isProjMgr(%s): %s, mgtInsts: %s (isAdmin: %s, year:%s), canPld4User: %s' % 
        (current_user.name, isCurrentInstMgr, task.activity.project.name, isCurrentProjMgr, canManageSomeInst, current_user.is_administrator( ), selYear, canPledgeForUser) )

    # instManagers and PMs can pledge for this and last year, normal users only for this year
    # isAllowed = ( (isCurrentInstMgr and (selYear == getPledgeYear() or selYear == getPledgeYear() - 1)) or
    #               (isCurrentProjMgr and (selYear == getPledgeYear() or selYear == getPledgeYear() - 1)) or
    #               (selYear == getPledgeYear())
    #               )
    #-ap, 2020-03-17: as of now, pledges for last year are not allowed any more for anyone
    # if you update this, also re-activate the confirmation in templates/newPledge.html:114
    isAllowed = ( (isCurrentInstMgr and (selYear == getPledgeYear()) ) or
                  (isCurrentProjMgr and (selYear == getPledgeYear()) ) or
                  ( current_user.is_administrator( ) ) or
                  (selYear == getPledgeYear())
                  )

    if not isAllowed:
        msg = "New pledges are not allowed for %s " % selYear
        return logAndRedirect( msg='%s - getPledgeYear():%s' % (msg, getPledgeYear()), redirUrl=url_for('main.index'), level='error' )

    form = PledgeUserForm()

    isAdmin = False
    if (current_user.is_administrator() or
        ( taskCode.strip() != '' and current_user.canManage('task', taskCode, year=session['year'])) ):
        current_app.logger.info('user %s can manage task %s (isAdmin: %s)' % (current_user.name, taskCode, current_user.is_administrator()) )
        isAdmin = True
        form = PledgeAdminForm()

    if not isAdmin and task.locked:
        return logAndRedirect( "Pledges for locked tasks are not allowed.", url_for('main.index'), 'error' )

    if task.shiftTypeId and task.shiftTypeId.strip().lower() in getTaskShiftTypeIds():
        msg = "Pledges for shift tasks are not allowed. Please sign up for the corresponding shift in the shift-tool at https://cmsonline.cern.ch."
        return logAndRedirect( msg, url_for('main.index'), 'error' )

    # check if we're handling a different user here ...
    user = None
    if cmsId == current_user.cmsId:
        user = db.session.query(EprUser).filter_by(cmsId=current_user.cmsId).one()
    else:
        user = db.session.query(EprUser).filter_by(cmsId=cmsId).one()
        msg = "request to pledge for different user: %s (cmsid: %s)" % (user.username, user.cmsId)
        current_app.logger.info( msg )

    plUserInst = db.session.query(EprInstitute).filter_by(id=user.mainInst).one()
    isInstMgr = current_user.is_administrator() or current_user.canManage('institute', plUserInst.code, year=session['year'])

    instMgrStr = 'is NOT'
    if isInstMgr : instMgrStr = 'is'
    current_app.logger.info( "isInstMgr: current_user %s %s inst. manager for user %s, inst %s" %
                             (current_user.username, instMgrStr, user.username, plUserInst.code) )

    # by using the TimeLineUser we make sure the default inst for a new pledge is
    # the user's main inst at the time of pledge, even if the user has changed the
    # institute during the year

    if form.validate_on_submit():

        # set some defaults (to avoid exceptions later):
        selUser = request.form.get('userId', None).strip()
        selInst = request.form.get('instId', None).strip()
        selUserHrID = -1

        try:
            selUserHrID = re.match(r'.*\(CERNid:\s*(\d+)\)\s*', selUser).group(1)   #  #relyOnSearchFormat
        except AttributeError:
            current_app.logger.info( '++> no HRid found for : "%s"' % selUser )
            pass
        except Exception as e:
            current_app.logger.error( "cannot find HRid in %s " % selUser )
            raise e

        current_app.logger.info( '++> selUser: "%s"' % selUser )
        current_app.logger.info( '++> selInst: "%s"' % selInst )
        current_app.logger.info( '++> hrId-re: "%s"' % selUserHrID )

        # see if a different user was selected in the form:
        try:
            #     #relyOnSearchFormat
            hrId = int( selUserHrID )
            user = db.session.query(EprUser).filter_by( hrId=hrId ).one()
            current_app.logger.info( "newPledge> got hrId %i for %s" % (hrId, user.username) )

        except Exception as e:
            current_app.logger.error( "newPledge> got: %s" % str(e) )
            msg = "User %s not found, please (re-)select." % selUser
            current_app.logger.error(msg)
            flash(msg, 'error')
            return render_template('newPledge.html', form=form, what='Pledge',
                                   year=session[ 'year' ],
                                   selUser = user, selUserInst = plUserInst,
                                   canManageSomeInst = canManageSomeInst,
                                   canPledgeForUser = canPledgeForUser,
                                   isInstMgr = isInstMgr,
                                   isCurrentInstMgr = isCurrentInstMgr,
                                   targetTask=task, act=task.activity, proj=task.activity.project,
                                   navInfo = getNavInfo( session['year'] ) )

        # now get the latest entry for the user for that year ...
        plTlUser = db.session.query(TimeLineUser).filter_by(cmsId=user.cmsId, year=selYear).order_by( TimeLineUser.timestamp.desc() ).first()

        if ( (not current_user.is_administrator())  and
             (user.status == 'EXMEMBER') ) :  # check if other statuses also don't allow to do EPR ...
            msg = "User '%s' (%s) has status of %s - no new pledges allowed. " % (user.username, hrId, user.status)
            return logAndRedirect( msg = msg, redirUrl=url_for( 'main.index' ), level='warning', flashLvl='error')

        plUserInst = db.session.query(EprInstitute).filter_by( code=plTlUser.instCode ).one( )
        if not selInst:  # no institute given, use the default one for the user's timeline
            plInst = plUserInst
        else:
            plInst = db.session.query(EprInstitute).filter_by( code=selInst ).one( )
        isInstMgr = current_user.is_administrator() or current_user.canManage( 'institute', plUserInst.code, year=session[ 'year' ] )

        current_app.logger.info( 'createNewPledge++> found inst %s for this pledge, user %s (%s) (current_user %s isInstMgr: %s)' % (plInst.code, user.username, user.cmsId, current_user.username, isInstMgr) )

        # only applicants can pledge for Qualification tasks:
        if task.tType == 'Qualification':
            # for now simply use the time-line info we already have (also for performance), 
            # if that changes, we may need to call the function:
            # plUserIsAppl = isApplicant(cmsId, selYear)
            # current_app.logger.info( "\n===> newPledge> user Id %i is applicant: %s\n " % (cmsId, str(plUserIsAppl)) )
            if (plTlUser.dueApplicant <= 0.): # not an applicant, refuse:
                msg = "Only applicants can pledge for Qualification tasks, user %s is not an applicant." % user.username
                # return logAndRedirect( msg = msg, redirUrl=url_for( 'main.index' ), level='warning', flashLvl='error')
                current_app.logger.error(msg)
                flash(msg, 'error')
                return render_template('newPledge.html', form=form, what='Pledge',
                                    year=session[ 'year' ],
                                    selUser = user, selUserInst = plUserInst,
                                    canManageSomeInst = canManageSomeInst,
                                    canPledgeForUser = canPledgeForUser,
                                    isInstMgr = isInstMgr,
                                    isCurrentInstMgr = isCurrentInstMgr,
                                    targetTask=task, act=task.activity, proj=task.activity.project,
                                    navInfo = getNavInfo( session['year'] ) )

        workTimePld = float(form.workTimePld.data)

        current_app.logger.info( "createNewPledge> got user (username): %s pledge %s" % (user.username, workTimePld) )

        sumPld, sumAcc, sumDone = getPledgeSum(user.id, selYear)

        # check if user will be overpledged (== more than 12 months pledged) with this pledge
        if workTimePld+sumPld > 12.:
            msg = "Illegal amount of work pledged for user %s, task %s: " % (user.username, task.name)
            msg += " overall sum of %2.2f months is larger than 12 months." % (workTimePld+sumPld,)
            current_app.logger.error( 'createNewPledge> '+msg )
            flash(msg, 'error')

            return render_template('newPledge.html', form=form, what='Pledge',
                                   year=session[ 'year' ],
                                   selUser = user, selUserInst = plUserInst,
                                   canManageSomeInst = canManageSomeInst,
                                   canPledgeForUser = canPledgeForUser,
                                   isInstMgr = isInstMgr,
                                   isCurrentInstMgr = isCurrentInstMgr,
                                   targetTask=task, act=task.activity, proj=task.activity.project,
                                   navInfo = getNavInfo( session['year'] ) )

        # check if the amount of work pledged in this plege is between 0 and 12 months:
        if workTimePld< 0 or workTimePld > 12. or workTimePld > task.neededWork :
            msg = "[0] Illegal amount of work pledged: "
            if workTimePld < 0 or workTimePld > 12.:
                msg += " %2.2f months is not between 0 and 12 months." % (workTimePld,)
            if workTimePld > task.neededWork:
                msg += " %2.2f months is larger than work needed by task (code %s): %2.2f." % (workTimePld, task.code, task.neededWork)
            current_app.logger.error( 'createNewPledge> '+msg )
            flash(msg, 'error')

            return render_template('newPledge.html', form=form, what='Pledge',
                                   year=session[ 'year' ],
                                   selUser = user, selUserInst = plUserInst,
                                   canManageSomeInst = canManageSomeInst,
                                   canPledgeForUser = canPledgeForUser,
                                   isInstMgr = isInstMgr,
                                   isCurrentInstMgr = isCurrentInstMgr,
                                   targetTask=task, act=task.activity, proj=task.activity.project,
                                   navInfo = getNavInfo( session['year'] ) )

        workTimeAcc = 0
        workTimeDone = 0
        status = 'new'
        if isAdmin:
            # selUser = form.userId.data
            # selInst = form.instId.data
            workTimePld = form.workTimePld.data
            workTimeAcc = form.workTimeAcc.data
            workTimeDone = form.workTimeDone.data
            status = form.status.data

        current_app.logger.info( "createNewPledge> got tCode %s from req.form %s (req.args %s) " % (taskCode, str(request.form), str(request.args)) )
        task = db.session.query(Task).filter_by(code=taskCode).one()
        pledge = Pledge(taskId = task.id,
                        userId = user.id,
                        instId = plInst.id,
                        workTimePld = workTimePld,
                        workTimeAcc = workTimeAcc,
                        workTimeDone = workTimeDone,
                        status = status,
                        year = selYear,
                        workedSoFar = form.workedSoFar.data,
                        isGuest=(plInst.id != plUserInst.id),
                    )
        current_app.logger.info( "createNewPledge> new pledge created: %s " % str(pledge.to_json()) )
        try:
            commitNew(pledge)
        except CMSDbException as e:
            msgErr = 'Error from DB when trying to add new pledge.\nLikely a pledge for this person and task already exists - please check below.\n(%s)\n' % str(e)
            flash(msgErr, 'error')
            current_app.logger.warning( msgErr )
            db.session.rollback()
            return redirect( url_for('main.index') )
        except Exception as e:
            msgErr = 'Error from DB when trying to add new pledge.\n(%s)' % str(e)
            flash(msgErr, 'error')
            current_app.logger.warning( msgErr )
            db.session.rollback()
            return redirect( url_for('main.index') )

        if not sendMailPledgeCreated( pledge.to_json() ):
            msgErr = "ERROR when sending mail for new pledge"
            flash(msgErr, 'error')
            current_app.logger.error( '%s : %s ' % (msgErr, str(pledge)) )

        msg = 'A new pledge was successfully created for user %s, task %s.' % (user.username, task.name)
        flash(msg)
        current_app.logger.info(msg)
        target = get_redirect_target()
        current_app.logger.info( "createNewPledge> got target %s " % target )
        # override with session['referrer'] if valid:
        if session.get('referrer') and is_safe_url(session.get('referrer',None)):
            target = session.pop('referrer') # remove entry from dict as well
        return redirect( target or url_for('main.index') )

    else: # form validation failed
        if form.is_submitted and form.errors:
            if form.errors != {'workTimePld': ['This field is required.'], 'status': ['Not a valid choice']} :
                msg = 'createPlege> Form validation had errors: %s ' % str(form.errors)
                flash(msg, 'error')
                current_app.logger.warning(msg)

    return render_template('newPledge.html', form=form, what='Pledge',
                            year=session[ 'year' ],
                            selUser = user, selUserInst = plUserInst,
                            canManageSomeInst=canManageSomeInst,
                            canPledgeForUser = canPledgeForUser,
                            isInstMgr = isInstMgr,
                            isCurrentInstMgr=isCurrentInstMgr,
                            targetTask=task, act=task.activity, proj=task.activity.project,
                            navInfo = getNavInfo( session['year'] ) )

@main.route('/rejectPledge/<int:id>', methods=['GET', 'POST'])
@login_required
def rejectPledge(id):

    if not checkPledgingAllowed():
        msg = 'Sorry, updating pledges is not allowed'
        flash( msg, 'error' )
        current_app.logger.error('rejectPledge> %s' % msg)
        return redirect( get_redirect_target() or url_for('main.showMine') )

    pl = db.session.query(Pledge).filter_by(id=id).first()
    if not pl:
        return abort(404)
    task = db.session.query(Task).filter_by(id=pl.taskId).one()
    plUser = db.session.query(EprUser).filter_by(id=pl.userId).one()

    if not ( (current_user.is_administrator() or
              current_user.canManage('task', task.code, year=session['year']) or
              current_user.canManageUser(plUser, year=session['year']) ) or
              ( pl.status=='new' and pl.userId == current_user.id )
            ):
        msg = 'Sorry, you are not authorised for this action.'
        flash( msg, 'error' )
        current_app.logger.warning( 'rejectPledge> ERROR %s ' % msg)
        return redirect( url_for('main.index') )

    form = RejectPledgeForm()
    if form.validate_on_submit():
        pl.status = 'rejected'
        pl.code   = pl.code + time.strftime('%Y%m%d-%H%M%S')
        pl.timestamp = datetime.datetime.utcnow()

        try:
            commitUpdated(pl)
        except Exception as e:
            msg = 'Error from DB when trying to reject pledge.\n(%s)' % str(e)
            flash( msg, 'error')
            current_app.logger.warning( 'rejectPledge> ERROR %s ' % msg )
            target = get_redirect_target()
            return redirect( target or url_for('main.index') )

        if not sendMailPledgeRejected( pl.to_json(), reason=form.reason.data ):
            msg = "ERROR when sending mail for rejected pledge"
            flash( msg, 'error')
            current_app.logger.warning( 'rejectPledge> ERROR %s ' % msg )

        msg = 'Pledge (%s) rejected.' % pl.code
        flash( msg )
        current_app.logger.info( 'rejectPledge> %s ' % msg )

        target = get_redirect_target()
        # override with session['referrer'] if valid:
        if is_safe_url(session.get('referrer')):
            target = session.pop('referrer', url_for('main.index')) # remove entry from dict as well
        return redirect( target or url_for('main.index') )

    return render_template('rejectPledge.html', form=form, pledge=pl, task=task,
                            plUser=plUser,
                            inst=db.session.query(EprInstitute).filter_by(id=plUser.mainInst).one(),
                            navInfo = getNavInfo( session['year'] ) )

@main.route('/approveIRPledgeByTaskInst/<string:tCode>/<string:iCode>', methods=['POST'])
@login_required
def approveIRPledgeByTaskInst(tCode, iCode):

    if not checkPledgingAllowed():
        msg = 'Sorry, updating pledges is not allowed'
        flash( msg, 'error' )
        current_app.logger.error('approveIRPledgeByTaskInst> %s' % msg)
        return redirect( get_redirect_target() or url_for('main.showMine') )

    current_app.logger.info("main.approveIRPledgeByTaskInst> got codes %s %s " % (tCode, iCode) )

    pledges = (db.session.query(Pledge)
                         .join( Task, Task.id == Pledge.taskId )
                         .join( EprInstitute, EprInstitute.id == Pledge.instId )
                         .filter( Task.code == tCode )
                         .filter( EprInstitute.code == iCode )
                         .filter( Pledge.status != 'rejected' )
                         .all() )

    current_app.logger.info('approveIRPledgeByTaskInst> going to approve %d pledges for %s %s ' % (len(pledges), iCode, tCode) )

    status = 200
    errMsg = ''
    infoMsg = ''
    for pledge in pledges:
        msg0, httpStatusCode = approveSinglePledge(pledge)
        infoMsg += '%s\n\t' % msg0
        if httpStatusCode != 200:
            msg = "Error approving pledge id %d - got %s " % (pledges.id, msg0)
            errMsg += '%s\n\t' % msg
            current_app.logger.warning('approveIRPledgeByTaskInst> %s' % msg)
            flash(msg, 'error')
            status = httpStatusCode
        
    current_app.logger.info('approveIRPledgeByTaskInst> approved %d pledges for %s %s ' % (len(pledges), iCode, tCode) )
    current_app.logger.debug('approveIRPledgeByTaskInst> details: \n\t status: %d, \n\t infoMsg: %s, \n\t errMsg: %s' % (status, infoMsg, errMsg) )

    if status == 200:
        return jsonify( { 'msg' : infoMsg } ), status
    else:
        return jsonify( { 'msg' : errMsg + ' \n\n ' + infoMsg,  } ), status

@main.route('/approvePledgeByCode/<string:code>', methods=['POST'])
@login_required
def approvePledgeByCode(code):

    if not checkPledgingAllowed():
        msg = 'Sorry, updating pledges is not allowed'
        flash( msg, 'error' )
        current_app.logger.error('approvePledgeByCode> %s' % msg)
        return redirect( get_redirect_target() or url_for('main.showMine') )

    if 'undefined' in code:
        msg = "main.approvePledgeByCode> got illegal code for plege to approve: %s (%s)" % (code, str(request.form))
        current_app.logger.warning(msg)
        flash(msg, 'error')
        return jsonify( { 'msg' : msg } ), 400

    try:
        taskId, userId, instId = code.split(':')
    except Exception as e:
        msg = "main.approvePledgeByCode> got exceptionwhen unpacking code %s - got: %s" % (code, str(e))
        current_app.logger.warning(msg)
        flash(msg, 'error')
        return jsonify( { 'msg' : msg } ), 400

    current_app.logger.info("main.approvePledgeByCode> got code %s " %  code)
    current_app.logger.info("main.approvePledgeByCode> form:  %s " % str(request.form) )

    pledge = db.session.query(Pledge).filter_by(code=code.replace(':','+')).one()

    infoMsg, httpStatusCode = approveSinglePledge(pledge)
    return jsonify( { 'msg' : infoMsg } ), httpStatusCode

@main.route('/approveSelectedPledges', methods=['POST'])
@login_required
def approveSelectedPledges():

    if not checkPledgingAllowed():
        msg = 'Sorry, updating pledges is not allowed'
        flash( msg, 'error' )
        current_app.logger.error('approveSelectedPledges> %s' % msg)
        return redirect( get_redirect_target() or url_for('main.showMine') )

    plSel = json.loads( request.form.get('pledges', None) )

    if plSel is None or len(plSel) == 0:
        msg = "main.approveSelectedPledges> no pledges found in request: %s " % str(request.form)
        current_app.logger.warning(msg)
        flash(msg, 'error')
        return jsonify( { 'msg' : msg } ), 400

    current_app.logger.info("main.approveSelectedPledges> got pledges to approve %s " % plSel )

    errMsg = []
    infoMsg = []
    httpCode = []
    failInfo = []
    httpStatusCode = 200
    for plCode in plSel:
        current_app.logger.info("main.approveSelectedPledges> processing pledge %s for approval " % plSel )
        pledge = db.session.query(Pledge).filter_by(code=plCode.replace(':','+')).one_or_none()

        if pledge is None:
            failInfo.append( plCode )
            noPlMsg = 'ERROR> no pledge with code "%s" found in DB' % plCode
            current_app.logger.warning( noPlMsg )
            errMsg.append( noPlMsg )
            httpCode.append( 'HTTPCODE> %s : %s' % (plCode, -404) )
            httpStatusCode = 400
        else:
            iMsg, httpStat = approveSinglePledge(pledge)
            if httpStat != 200:
                failInfo.append( plCode )
                errMsg.append( 'ERROR> %s : %s' % (plCode, iMsg) )
                httpCode.append( 'HTTPCODE> %s : %s' % (plCode, httpStat) )
                httpStatusCode = 400
            else:
                httpCode.append( 'HTTPCODE> %s : %s' % (plCode, httpStat) )
                infoMsg.append( 'INFO> %s : %s' % (plCode, iMsg) )

    return jsonify( { 'msg' : '\n'.join(infoMsg), 'err' : '\n'.join(errMsg), 'httpStats': '\n'.join(httpCode) } ), httpStatusCode

def approveSinglePledge(pledge):

    if not checkPledgingAllowed():
        msg = 'Sorry, updating pledges is not allowed'
        flash( msg, 'error' )
        current_app.logger.error('approveSinglePledge> %s' % msg)
        return redirect( get_redirect_target() or url_for('main.showMine') )

    if pledge.status not in ['new', 'accepted'] :
        msg = "old status is '%s' -- no update needed " %  pledge.status
        current_app.logger.info("main.approveSinglePledge(%s)> %s" % (pledge.code, msg) )
        return msg, 200

    plPrevJson = pledge.to_json()
    taskId, userId, instId = pledge.taskId, pledge.userId, pledge.instId

    task = db.session.query(Task).filter_by(id=taskId).one()

    current_app.logger.info("main.approveSinglePledge(%s)> old status is '%s' " %  (pledge.code, pledge.status) )
    current_app.logger.info("main.approveSinglePledge(%s)> taskId %s, code %s " % (pledge.code, taskId, task.code) )

    if not ( current_user.is_administrator() or
             current_user.canManage('task', task.code, year=session['year']) ):
        msg = 'Sorry, you (%s) are not an admin or mananger of this task (%s)' % (current_user.username, task.name)
        current_app.logger.error(msg)
        flash( msg, 'error' )
        current_app.logger.warning(msg)
        return msg, 403

    infoMsg = None
    errMsg  = ''

    # first check for "accepted" -> "done" transition, to make sure we don't do an accidental "new"->"accepted"->"done" step:
    if pledge.status == 'accepted':
        current_app.logger.info( "main.approveSinglePledge(%s)> status transition acc->done " % pledge.code)

        # update the work time done to the accepted one, but only if not yet set before:
        if pledge.workTimeDone == 0.:
            pledge.workTimeDone = pledge.workTimeAcc

        wtd = pledge.workTimeDone

        # now that we have updated the done work for the pledge, check the values
        checkOK, msg = checkPledgeDone( pledge.workTimePld, pledge.workTimeAcc, pledge.workedSoFar, pledge.workTimeDone, task, plPrevJson )
        if not checkOK :
            current_app.logger.error( msg )
            db.session.rollback() # make sure the changes to the pledge are not stored
            current_app.logger.error( msg + ' -- DB rolled back.' )
            return msg, 403

        pledge.status = 'done'
        infoMsg = 'Pledge %s set to done with %f done' % (pledge.code, wtd)

    # now check if we have a request for a "new" -> "accepted" transition:
    if pledge.status == 'new':
        infoMsg = ''
        current_app.logger.info( "main.approveSinglePledge(%s)> status transition new->acc " % pledge.code)
        # update the accepted work time to the pledged one, but only if not yet set before:
        if pledge.workTimeAcc == 0.:
            pledge.workTimeAcc = pledge.workTimePld

        wta = pledge.workTimeAcc
        wtd = pledge.workTimeDone

        # now that we have updated the accepted work for the pledge, check the values
        checkOK, msg = checkPledgeApproval( pledge.workTimePld, pledge.workTimeAcc, pledge.workedSoFar, pledge.workTimeDone, task, plPrevJson )
        if not checkOK and msg.startswith('Time worked so far') and 'can not be larger than pledged' in msg:
            # reset too large value of workedSoFar to workTimePld:
            infoMsg += 'Reset a too large value of workedSoFar (%2.2f) to workTimePld (%2.2f). ' % (pledge.workedSoFar, pledge.workTimePld) 
            pledge.workedSoFar = pledge.workTimePld
            checkOK = True

        if not checkOK :
            current_app.logger.error( msg )
            db.session.rollback() # make sure the changes to the pledge are not stored
            current_app.logger.error( msg + ' -- DB rolled back.' )
            flash( 'ERROR: %s (%s)' % (msg, pledge.code), 'error')
            return msg, 403

        pledge.status = 'accepted'
        infoMsg += 'Pledge %s approved with %f accepted, %f done' % (pledge.code.replace('+',':'), wta, wtd)

    try:
        commitUpdated(pledge)
    except Exception as e:
        errMsg += 'Error from DB when trying to approve pledge.\n(%s)' % str(e)
        current_app.logger.error( errMsg )
        return msg, 403

    newPledgeJson = db.session.query(Pledge).filter_by(code=pledge.code).one().to_json()

    if not sendMailPledgeApproved(newPledgeJson):
        errMsg += "ERROR when sending mail for approving pledge (%s)" % pledge.code

    if errMsg:
        current_app.logger.error( errMsg )
        flash(errMsg, 'error')

    if infoMsg:
        current_app.logger.info( infoMsg )
        flash(infoMsg)

    return infoMsg, 200

@main.route('/pledge/<int:id>', methods=['GET', 'POST'])
@login_required
def pledge(id):

    if not checkPledgingAllowed():
        msg = 'Sorry, updating pledges is not allowed'
        flash( msg, 'error' )
        current_app.logger.error('pledge> %s' % msg)
        return redirect( get_redirect_target() or url_for('main.showMine') )

    try:
        pl = db.session.query(Pledge).filter_by(id=id).one()
    except Exception as e:
        msg = "ERROR: could not find Pledge for id %i " % id
        flash( msg, 'error' )
        current_app.logger.warning( 'pledge> ERROR %s ' % msg )
        return redirect( url_for('main.index') )

    # store the status of the existing pledge so we can use it when
    # calculating the task-related info
    plPrevJson = pl.to_json()

    task = db.session.query(Task).filter_by(id=pl.taskId).one()
    plUser = db.session.query(EprUser).filter_by(id=pl.userId).one()
    userInst = db.session.query(EprInstitute).filter_by(id=plUser.mainInst).one()
    plInst = db.session.query(EprInstitute).filter_by(id=pl.instId).one()

    isInstMgr = False
    if ( current_user.is_administrator() or
         current_user.canManageUser( plUser, year=session[ 'year' ] ) ) :
        isInstMgr = True
        current_app.logger.info( 'user %s is institute manager for user %s ? (isInstMgr: %s)' % (current_user.name, plUser, isInstMgr) )

    canManage = current_user.canManage('task', task.code, year=session['year'])
    canAdmin = ( canManage or current_user.is_administrator() )

    current_app.logger.debug('==3> user %s canAdmin: %s (canMgItem: %s) - canMgUser: %s' % (current_user.username, canAdmin, canManage, current_user.canManageUser(plUser, year=session['year'])) )

    if pl.status.lower() == 'rejected':
        return render_template( 'pledge.html', pledge=pl,
                                task=task, user=plUser, userInst=userInst, plInst=plInst,
                                form=None, canAdmin=canAdmin,
                                navInfo=getNavInfo( session[ 'year' ] ) )

    # check if the user can admin the pledge, then show the admin form.
    # this has to be done before the check for status "new" and user is owner, as
    # otherwise the admin-user won't see the admin page for his/her own pledges
    if ( pl.status.lower() != 'rejected' and (canAdmin or isInstMgr) ):  #-toDo: decide if "done" pledges should be editable or not, here they are.

        form = PledgeAdminForm(obj=pl)
        form.status.default = pl.status

        if form.validate_on_submit():

            # see if there is a new institute selected, if not, use the user's main one
            newInstCode = request.form.get('instId', userInst.code)
            newInst = userInst
            if newInstCode.strip() != '' and newInstCode != userInst.code:
                try:
                    newInst = db.session.query(EprInstitute).filter_by(code=newInstCode).one()
                except Exception as e:
                    current_app.logger.warning( 'could not find inst for code %s (pledge id %s), got: %s ' % (newInstCode, id, str(e) ) )

            workTimePld = form.workTimePld.data
            workTimeAcc = form.workTimeAcc.data
            workedSoFar = form.workedSoFar.data

            if workTimePld is None: workTimePld = 0.
            if workTimeAcc is None: workTimeAcc = 0.
            if workedSoFar is None: workedSoFar = 0.

            # the workTimeDone and status can only be changed by PAT managers, not institute managers
            status = pl.status
            workTimeDone = pl.workTimeDone
            if canAdmin:
                workTimeDone = form.workTimeDone.data
                status = form.status.data

            # now see what the requested change is:
            # if the requested new status is 'accepted', we need to check
            # if the approved values fit with the needs of the task:

            current_app.logger.debug( 'pledge new state: "%s" (from "%s")' % (form.status.data.lower( ), pl.status) )
            current_app.logger.debug( 'pledge new done : %s (from %s)' % (workTimeDone, pl.workTimeDone) )
            current_app.logger.debug( 'pledge newInstCode : %s (from %s, user: %s(%d))' % (newInstCode, pl.instId, userInst.code, userInst.id) )

            if status.lower() == 'accepted':

                # check: if workTimeAcc was not updated in the form, use workTimePld from the form
                if canAdmin and workTimeAcc == 0:
                    workTimeAcc = form.workTimePld.data

                # check if the approval is OK or if something is overcommitted. We only need to do this
                # if the new workTimeAcc is larger than the original (previous) one:
                if workTimeAcc > pl.workTimeAcc:
                    checkOK, msg = checkPledgeApproval(workTimePld, workTimeAcc, workedSoFar, workTimeDone, task, plPrevJson)
                    if not checkOK:
                        current_app.logger.error(msg)
                        flash(msg, 'error')

                        return render_template('pledge.html', pledge=pl,
                                       task=task, user=plUser, userInst=userInst, plInst=plInst,
                                       form=form, canAdmin=canAdmin,
                                       navInfo = getNavInfo( session['year'] ) )

            # if the requested new status is 'done', we need to check
            # if the present done values fit with the needs of the task,
            # but only if the new workTimeDone is larger than the original (previous) one:
            if status.lower() == 'done':

                # check: if workTimeDone was not updated in the form, use workTimeAcc from the form
                if canAdmin and workTimeDone == 0:
                    workTimeDone = form.workTimeAcc.data

                if workTimeDone > pl.workTimeDone:

                    checkOK, msg = checkPledgeDone(workTimePld, workTimeAcc, workedSoFar, workTimeDone, task, plPrevJson)
                    current_app.logger.debug("checkPledgeDone> got : %s %s" % (checkOK, msg) )
                    if not checkOK:
                        current_app.logger.error(msg)
                        flash(msg, 'error')

                        return render_template('pledge.html', pledge=pl,
                                       task=task, user=plUser, userInst=userInst, plInst=plInst,
                                       form=form, canAdmin=canAdmin,
                                       navInfo = getNavInfo( session['year'] ) )

                    sumTaskDone = getTaskDone(task=task)
                    totDoneWork = sumTaskDone + float( workTimeDone )
                    if (totDoneWork > float( task.neededWork ) + 1.) :
                        msg = "Total work time done would be %2.2f (%2.2f + %2.2f), but can not be larger than needed for task %2.2f +1 month (task name: %s) " % (
                            totDoneWork, sumTaskDone, float( workTimeDone ), task.neededWork, task.name)
                        current_app.logger.error(msg)
                        flash(msg, 'error')
                        return render_template( 'pledge.html', pledge=pl,
                                                task=task, user=plUser, userInst=userInst, plInst=plInst,
                                                form=form, canAdmin=canAdmin,
                                                navInfo=getNavInfo( session[ 'year' ] ) )

            # continue with the update of the pledge

            oldPledgeJson = pl.to_json()

            # update the pledge. Tracking of changes is done via the SQLAlchemy-Continuum package
            # update values in case they got changed:

            pl.workTimePld  = workTimePld
            pl.workTimeAcc  = workTimeAcc
            pl.workTimeDone = workTimeDone

            pl.workedSoFar  = workedSoFar

            pl.status = status
            pl.year   = session['year']

            pl.timestamp = datetime.datetime.utcnow()

            if newInstCode is not None and \
               newInstCode != '' and \
               newInst.id != pl.instId: # handle move of instiute for pledge
                current_app.logger.info( 'plegde> request to change institute for pledge %s from %s to %s' % (pl.code, pl.instId, newInstCode) )
                try:
                    newInst = db.session.query(EprInstitute).filter_by(code=newInstCode).one()
                    pl.isGuest = True
                    pl.instId =  newInst.id
                    pl.code = '+'.join( [ str(x) for x in pl.code.split('+')[:2]+[newInst.id] ] )
                    current_app.logger.info( 'plegde> changed institute for pledge %s from %s to %s' % (pl.code, pl.instId, newInstCode) )
                except Exception as e :
                    msg = 'Got Exception when trying to handle guest pledge: %s ' % str( e )
                    msg += '\n ... newInstCode: "%s" ' % str( newInstCode )
                    current_app.logger.error( msg )

            newPledgeJson = pl.to_json()

            try:
                commitUpdated(pl)
            except Exception as e:
                msg = 'Failed to update pledge info: %s' % str(e)
                flash( msg, 'error' )
                current_app.logger.warning( 'pledge> ERROR %s ' % msg )

                return render_template('pledge.html', pledge=pl,
                                        task=task, user=plUser, userInst=userInst, plInst=plInst,
                                        form=form, canAdmin=canAdmin,
                                        navInfo = getNavInfo( session['year'] ) )

            newPledgeJson = db.session.query(Pledge).filter_by(code=pl.code).one().to_json()
            if not sendMailPlegdeUpdated(oldPledgeJson, newPledgeJson):
                msg = "ERROR when sending mail for updating pledge"
                flash( msg, 'error' )
                current_app.logger.warning( 'pledge> ERROR %s ' % msg )

            msg = 'Pledge info successfully updated.'
            flash( msg )
            current_app.logger.info( 'pledge> %s ' % msg )

            target = get_redirect_target()
            # override with session['referrer'] if valid:
            if 'referrer' in session and is_safe_url(session['referrer']):
                target = session.pop('referrer') # remove entry from dict as well
            return redirect( target or url_for('main.index') )
        else: # form validation failed
            if form.errors:
                msg = 'pledge/%s> Form validation had errors: %s ' % (id, str(form.errors))
                flash(msg, 'error')
                current_app.logger.warning(msg)

        return render_template('pledge.html', pledge=pl,
                                        task=task, user=plUser, userInst=userInst, plInst=plInst,
                                        form=form, canAdmin=canAdmin,
                                        navInfo = getNavInfo( session['year'] ) )

    # Here we come only if the current_user can not admin the pledge.
    # The user as "owner" of a pledge can only edit while the
    # status of the pledge is "new", so we can simply update the object
    if pl.status == 'new' and current_user.id == pl.userId:
        form = PledgeUserForm(obj=pl)

        if form.validate_on_submit():
            # users can only edit their pledges while they are "new", so we
            # update the existing pledge instead of adding a new one for history
            workTimePld = form.workTimePld.data
            workedSoFar = form.workedSoFar.data

            if workedSoFar is None: workedSoFar = 0.

            # check if user or task is overcommitted:
            if workTimePld< 0 or workTimePld > 12. or workTimePld > task.neededWork :
                msg = "[2] Illegal amount of work pledged: "
                if workTimePld < 0 or workTimePld > 12.:
                    msg += " %2.2f months is not between 0 and 12 months." % (workTimePld,)
                if workTimePld > task.neededWork:
                    msg += " %2.2f months is larger than work needed by task (%s): %2.2f." % (workTimePld, task.code, task.neededWork)
                current_app.logger.error(msg)
                flash(msg, 'error')

                return render_template('pledge.html', pledge=pl,
                               task=task, user=plUser, userInst=userInst, plInst=plInst,
                               form=form, canAdmin=canAdmin,
                               navInfo = getNavInfo( session['year'] ) )

            # sanity checks done, update pledge now
            pl.workTimePld = workTimePld
            pl.workedSoFar = workedSoFar

            try:
                commitUpdated(pl)
            except Exception as e:
                db.session.rollback()
                msg = 'Failed to update pledge info: %s' % str(e)
                flash( msg, 'error' )
                current_app.logger.warning( 'pledge> ERROR %s ' % msg )
                return render_template('pledge.html', pledge=pl,
                               task=task, user=plUser, userInst=userInst, plInst=plInst,
                               form=form, canAdmin=canAdmin,
                               navInfo = getNavInfo( session['year'] ) )

            #-toDo: check if mail should be send here as it is an update of a new pledge.

            msg = 'Pledge (%s) info successfully updated.' % pl.code
            flash( msg )
            current_app.logger.info( 'pledge> %s ' % msg )
            target = get_redirect_target()
            # override with session['referrer'] if valid:
            if 'referrer' in session and is_safe_url(session['referrer']):
                target = session.pop('referrer') # remove entry from dict as well
            return redirect( target or url_for('main.index') )

        return render_template('pledge.html', pledge=pl,
                               task=task, user=plUser, userInst=userInst, plInst=plInst,
                               form=form, canAdmin=canAdmin,
                               navInfo = getNavInfo( session['year'] ) )

    # all other cases (non-Admin, user not owner of the ('new') pledge: reject

    msg = 'Pledge (%s) info could not be updated. (status=%s, userId/Name=%i/%s (canManage: %s, admin: %s), plUser:%i' % (pl.code, pl.status, current_user.id, current_user.name, canManage, current_user.is_administrator(), pl.userId)
    current_app.logger.error(msg.encode('utf-8'))
    flash(msg.encode('utf-8'), 'error')
    target = get_redirect_target()
    # override with session['referrer'] if valid:
    if 'referrer' in session and is_safe_url(session['referrer']):
        target = session.pop('referrer') # remove entry from dict as well
    return redirect( target or url_for('main.index') )

@main.route('/getMailAddrsForPledgees', methods=['POST'])
@login_required
def getMailAddrsForPledgees():

    infoMsg = ''
    errMsg  = ''

    year    = request.form.get('year', getPledgeYear())
    codeList = request.form.get('plCodes', None)

    if not codeList:
        errMsg += 'No pledge codes found in request '
        current_app.logger.error( errMsg )
        return jsonify( { 'mailAddrs' : [], 'ccList' : [] , 'msg' : errMsg } ), 200

    plCodes = [ x.replace(':', '+') if x is not None else 'n/a' for x in json.loads( codeList ) ]

    current_app.logger.info("main.getMailAddrsForPledgees> got %d codes (%s) " %  (len(plCodes), str(plCodes)) )

    if not plCodes: # nothing requested, return empty list with message ...
        errMsg += 'No pledges found for : %s' % str(plCodes)
        current_app.logger.error( errMsg )
        return jsonify( { 'mailAddrs' : [], 'ccList' : [] , 'msg' : errMsg } ), 200

    pList = None
    try:
        plList = db.session.query(Pledge).filter( Pledge.code.in_(plCodes) ).all()
    except Exception as e:
        errMsg += 'main.getMailAddrsForPledgees> Error from DB when trying to get list of pledges for plCodes %s - got: %s' % (plCodes, str(e))
        current_app.logger.error( errMsg )
        return jsonify( { 'msg' : errMsg } ), 404

    if not plList:
        errMsg += 'No pledges found for : %s' % str(plCodes)
        current_app.logger.error( errMsg )
        return jsonify( { 'msg' : errMsg } ), 404

    mailAddresses = set()
    for pl in plList:
        try:
            plUser = db.session.query(EprUser).filter_by(id=pl.userId).one()
            email = getUserEmail(plUser.username)
            if email: mailAddresses.add(email)
        except Exception as e:
            current_app.logger.error( "main.getMailAddrsForPledgees> ERROR retrieving email for username %s - continuing with managers ... " % plUser.username )

    mgrList = []

    infoMsg = 'found %d mails for %d pledges ' % (len(mailAddresses), len(plList))
    return jsonify( { 'mailAddrs' : list(mailAddresses), 'ccList' : mgrList , 'msg' : infoMsg } ), 200


@main.route('/getMailAddrsForInstLeaders', methods=['POST'])
@login_required
def getMailAddrsForInstLeaders():

    infoMsg = ''
    errMsg  = ''

    year    = int(request.form.get('year', getPledgeYear()))
    codeList = request.form.get('instCodes', None)

    if not codeList:
        errMsg += 'No institute codes found in request '
        current_app.logger.error( errMsg )
        return jsonify( { 'mailAddrs' : [], 'ccList' : [] , 'msg' : errMsg } ), 200

    instCodes = [ x.replace(':', '+') if x is not None else 'n/a' for x in json.loads( codeList ) ]

    current_app.logger.info("main.getMailAddrsForInstLeaders> got %d codes (%s) " %  (len(instCodes), str(instCodes)) )

    if not instCodes: # nothing requested, return empty list with message ...
        errMsg += 'No institute codes  found for : %s' % str(instCodes)
        current_app.logger.error( errMsg )
        return jsonify( { 'mailAddrs' : [], 'ccList' : [] , 'msg' : errMsg } ), 200

    iList = None
    try:
        instList = db.session.query(EprInstitute).filter( EprInstitute.code.in_(instCodes) ).all()
    except Exception as e:
        errMsg += 'main.getMailAddrsForInstLeaders> Error from DB when trying to get list of pledges for plCodes %s - got: %s' % (instCodes, str(e))
        current_app.logger.error( errMsg )
        return jsonify( { 'msg' : errMsg } ), 403

    if not instList:
        errMsg += 'No pledges found for : %s' % str(instCodes)
        current_app.logger.error( errMsg )
        return jsonify( { 'msg' : errMsg } ), 403

    instLeaders = []
    for inst in instList:
        instLeaders += (db.session.query(Manager)
                        .filter( Manager.itemCode == str(inst.code) )
                        .filter( Manager.itemType == 'institute' )
                        .filter( Manager.year == year )
                        .filter( Manager.role == Permission.MANAGEINST )
                        .filter( Manager.status == 'active' )
                        .all() )

    current_app.logger.info('instLeaders: %s ' % str(instLeaders) )

    mailAddresses = set()
    for instLeader in instLeaders:
        try :
            email = getUserEmail( instLeader.user.username )
            if email: mailAddresses.add(email)
        except Exception as e:
            current_app.logger.error( "main.getMailAddrsForInstLeaders> ERROR retrieving email for username %s - continuing with managers ... " % instLeader.username )

    infoMsg = 'found %d mails for %d institutes ' % (len(mailAddresses), len(instList))
    return jsonify( { 'mailAddrs' : list(mailAddresses), 'ccList' : [] , 'msg' : infoMsg } ), 200


@main.route('/updateWorkDone/<int:id>', methods=['GET', 'POST'])
@login_required
def updateWorkDone(id):

    try:
        pl = db.session.query(Pledge).filter_by(id=id).one()
    except Exception as e:
        flash("ERROR: could not find Pledge for id %i " % id, 'error')
        return redirect( url_for('main.index') )

    # store the status of the existing pledge so we can use it when
    # calculating the task-related info
    plPrevJson = pl.to_json()

    task = db.session.query(Task).filter_by(id=pl.taskId).one()
    plUser = db.session.query(EprUser).filter_by(id=pl.userId).one()
    userInst = db.session.query(EprInstitute).filter_by(id=plUser.mainInst).one()
    plInst = db.session.query(EprInstitute).filter_by(id=pl.instId).one()

    canAdmin = ( current_user.canManage('task', task.code, year=session['year']) or
                 current_user.canManageUser(plUser, year=session['year']) or
                 current_user.is_administrator() )

    # check if the user can adming the pledge, then show the admin form.
    # this has to be done before the check for status "new" and user is owner, as
    # otherwise the admin-user won't see the admin page for his/her own pledges
    if ( (canAdmin) or (pl.status == 'accepted' and current_user.id == pl.userId) ):

        # Here we come only if the current_user is the owner of the pledge or an admin.
        form = PledgeUpdateWorkForm(obj=pl)

        if form.validate_on_submit():
            # users can only edit their pledges while they are "new", so we
            # update the existing pledge instead of adding a new one for history
            workedSoFar = form.workedSoFar.data
            workTimePld = pl.workTimePld

            # check if user or task is overcommitted:
            if workedSoFar < 0 or workedSoFar > 12. or workedSoFar > workTimePld :
                msg = "Illegal amount of work done so far: "
                if workedSoFar < 0 or workedSoFar > 12.:
                    msg += " %2.2f months is not between 0 and 12 months." % (workTimePld,)
                if workedSoFar > workTimePld:
                    msg += " %2.2f months is larger than work pledged: %2.2f." % (workedSoFar, workTimePld)
                current_app.logger.error(msg)
                flash(msg, 'error')

                return render_template('pledgeUpdateWork.html', pledge=pl,
                               task=task, user=plUser, userInst=userInst, plInst=plInst,
                               form=form, canAdmin=canAdmin,
                               navInfo = getNavInfo( session['year'] ) )

            # sanity checks done, update pledge now
            pl.workedSoFar = workedSoFar

            try:
                commitUpdated(pl)
            except Exception as e:
                db.session.rollback()
                flash('Failed to update pledge info for work done so far: %s' % str(e), 'error')
                return render_template('pledgeUpdateWork.html', pledge=pl,
                               task=task, user=plUser, userInst=userInst, plInst=plInst,
                               form=form, canAdmin=canAdmin,
                               navInfo = getNavInfo( session['year'] ) )

            #-toDo: check if mail should be send here as it is an update of a new pledge.

            flash('Pledged work done so far successfully updated to %s.' % str(workedSoFar))
            target = get_redirect_target()
            # override with session['referrer'] if valid:
            if is_safe_url(session['referrer']):
                target = session.pop('referrer') # remove entry from dict as well
            return redirect( target or url_for('main.index') )

        if form.errors:
            for k, v in form.errors.items():
                val = form[k].data
                flash( 'Errors processing form: invalid value for %s is %s - should be %s.' % (k, str(val), ' '.join(v) ) )

        return render_template('pledgeUpdateWork.html', pledge=pl,
                               task=task, user=plUser, userInst=userInst, plInst=plInst,
                               form=form, canAdmin=canAdmin,
                               navInfo = getNavInfo( session['year'] ) )

    # all other cases (non-Admin, user not owner of the ('new') pledge: reject

    msg = 'Pledge (%s) info could not be updated. (status=%s, userId/Name=%i/%s (tskMgr: %s, admin: %s), plUser:%i' % (pl.code, pl.status, current_user.id, current_user.name, current_user.canManage('task', task.code, year=session['year']), current_user.is_administrator(), pl.userId)
    current_app.logger.error(msg)
    flash(msg, 'error')
    target = get_redirect_target()
    # override with session['referrer'] if valid:
    if is_safe_url(session['referrer']):
        target = session.pop('referrer') # remove entry from dict as well
    return redirect( target or url_for('main.index') )

@main.route('/showShifts', methods=['GET', 'POST'])
@login_required
def showShifts():

    selYear = session.get('year')

    session['referrer'] = url_for('main.showShifts')
    return render_template( 'showShifts.html', year=selYear,
                             navInfo = getNavInfo( session['year'] ) )


@main.route('/showTask/<string:code>', methods=['GET', 'POST'])
@login_required
def showTask(code):

    selYear = session.get('year')

    task = None
    try:
        task = db.session.query(Task).filter_by(code=code).one()
    except sa.orm.exc.NoResultFound:
        try :
            task = db.session.query( Task ).filter_by( id=int(code) ).one()
            code = task.code
        except sa.orm.exc.NoResultFound:
            return redirect( url_for( 'main.index' ) )
        except ValueError :
            return redirect( url_for( 'main.index' ) )

    act  = task.activity
    proj = act.project

    taskInfo = task.to_json()
    taskInfo[ 'activity' ] = act.name
    taskInfo ['project'] = proj.name
    taskInfo['id'] = task.id
    taskInfo['pctAtCERN'] = task.pctAtCERN
    taskInfo['kind'] = task.kind

    mgrsName, mgrs = getAPTManagers(act.code, proj.code, task.code, year=selYear)
    taskInfo['managers'] = { 'project': '', 'activity': '', 'task':'' }

    current_app.logger.debug( 'showTask++0> found %d proj, %d act, %d task managers for task code %s in %d' % (len(mgrs['project']), len(mgrs['activity']), len(mgrs['task']), task.code, selYear) )

    for patType in taskInfo['managers'].keys():
        mails = []
        for m in mgrs[ patType ]:
            if m['egroup'] and m['egroup'].strip() != '':
                mails.append( '%s@cern.ch' % m['egroup'] )
            else:
                u = db.session.query(EprUser).filter_by(id=m['userId']).one()
                eMail = getUserEmail( u.username )
                current_app.logger.debug(  'showTask++2> %s %s : %s %s ' % (patType, m, u.username, eMail) )
                mails.append( eMail )
        taskInfo[ 'managers' ][patType] = ','.join(mails)
        observers = getObserverEmails( patType, code, selYear )
        if observers :
            if 'observers' not in taskInfo:
                taskInfo['observers'] = {}
            taskInfo[ 'observers' ][patType] = ','.join(observers)

    # check if the current user can manage the actual task directly:
    canManageTask = False
    if current_user.is_administrator() or \
        current_user.canManage('task', task.code, year=selYear) or \
        current_user.canManage('activity', task.activity.code, year=selYear) or \
        current_user.canManage('project', task.activity.project.code, year=selYear) :
        canManageTask = True

    # check if the current user can manage next year's task:
    if ( current_user.is_administrator() or
         current_user.canManage('project', task.activity.project.code, year=selYear)  or
         ( selYear == getPledgeYear()+1 and current_user.canManage('activity', act.code, year=selYear)
        ) ):
        canManageTask = True

    taskInfoItems = [ 'description', 'neededWork', 'pctAtCERN',
                      'kind', 'locked', 'taskType', 'comment',
                      'shiftTypeId', 'status', 'level3',
                      ]


    current_app.logger.debug( 'showTask++4> user %s can manage task code %s in %d: %s' % (current_user.username, task.code, selYear, canManageTask) )

    favTaskCodes = getFavTaskCodes()

    session['referrer'] = url_for('main.showTask', code=code)

    return render_template( 'showTask.html', year=selYear,
                             taskInfo=taskInfo, task=task,
                             taskInfoItems = taskInfoItems,
                             lvl3Map = getLevel3NameMap(selYear),
                             favTaskCodes = favTaskCodes,
                             canManageTask = canManageTask,
                             navInfo = getNavInfo( selYear ) )

@main.route('/showPledge/<string:code>', methods=['GET', 'POST'])
@login_required
def showPledge(code):

    selYear = session.get('year')
    code = code.replace(':', '+')

    try:
        pledge = db.session.query(Pledge).filter_by(code=code).one()
    except sa.orm.exc.NoResultFound:
        msg = "Could not find Pledge for %s " % code
        current_app.logger.warning(msg)
        flash(msg, 'error')
        return redirect( url_for('main.index') )

    task  = db.session.query(Task).filter_by( id = pledge.taskId).one()
    act  = task.activity
    proj = act.project

    plInfo = pledge.to_json()
    plInfo[ 'task' ] = task.name
    plInfo[ 'taskType' ] = task.tType
    plInfo[ 'activity' ] = act.name
    plInfo ['project'] = proj.name

    mgrsName, mgrs = getAPTManagers(act.code, proj.code, task.code)
    plInfo['managers'] = { 'project': '', 'activity': '', 'task':'' }

    current_app.logger.debug( 'showPledge++1> %s ' % str(mgrs) )

    for patType in plInfo['managers'].keys():
        mails = []
        for m in mgrs[ patType ]:
            u = db.session.query(EprUser).filter_by(id=m['userId']).one()
            eMail = getUserEmail( u.username )
            current_app.logger.debug(  'showPledge++2> %s %s : %s %s ' % (patType, m, u.username, eMail) )
            mails.append( eMail )
        plInfo[ 'managers' ][patType] = ','.join(mails)

    plUser = db.session.query(EprUser).filter_by( id=pledge.userId ).one()
    plInst = db.session.query(EprInstitute).filter_by( id=pledge.instId ).one()

    plInfo['User'] =  '%s' % (plUser.name,)
    plInfo['Institute'] =  '%s' % (plInst.name,)
    plInfo['Task'] =  '<a href="%s">%s</a>' % ( url_for('main.showTask', code=task.code), task.name,)

    plInfoItems = [ 'User', 'Institute', # 'Task',
                    'workTimePld', 'workTimeAcc', 'workTimeDone',
                    'workedSoFar',
                    'status', 'isGuest'
                      ]

    favPledgeCodes = getFavPledgeCodes()

    session['referrer'] = url_for('main.showPledge', code=code)

    return render_template( 'showPledge.html', year=selYear,
                             plInfo=plInfo, plInfoItems = plInfoItems,
                             favPledgeCodes = favPledgeCodes,
                             navInfo = getNavInfo( session['year'] ) )


@main.route('/splitPledge/<string:plCode>', methods=['GET', 'POST'])
@login_required
def splitPledge(plCode=None):

    selYear = session.get('year')

    if not plCode:
        plCode = request.form.get('plCode', None)

    try:
        pledge = db.session.query(Pledge).filter_by(code=plCode).one()
    except sa.orm.exc.NoResultFound:
        msg = "Could not find Pledge for %s " % plCode
        current_app.logger.warning(msg)
        flash(msg, 'error')
        return redirect( url_for('main.index') )

    plInst = db.session.query(EprInstitute).filter_by(id=pledge.instId).one()

    form = PledgeSplitForm()
    current_app.logger.debug( 'splitPledge++1> request to split pledge %s ' % str(plCode) )

    if form.validate_on_submit():
        current_app.logger.debug( 'splitPledge++2> splitting pledge %s giving %s EPR month(s) to %s' % (plCode, form.newInstWork.data, form.newInst.data ) )

        newInstCode = form.newInst.data
        newInstWork = form.newInstWork.data
        (status, msg, retCode) = doPledgeSplit(plCode, newInstCode, newInstWork)

        if status == 'OK':
            flash(msg)
        else:
            flash( msg, 'error' )
        current_app.logger.info( 'splitPledge++2> splitting pledge returned: %s: %s' % (status, msg) )

        return redirect( url_for('main.showPledge', code=plCode) )

    session['referrer'] = url_for('main.showPledge', code=plCode)
    return render_template( 'splitPledge.html', year=selYear,
                            form=form, plCode = plCode, pledge=pledge, institute=plInst,
                            navInfo = getNavInfo( session['year'] ) )


@main.route('/showProjStats', methods=['GET'])
@login_required
def showProjStats():

    selYear = session.get('year')

    prjIdList = db.session.query(Project.id).filter(Project.year == selYear).filter(Project.status=='ACTIVE').all()
    current_app.logger.info( 'showProjStats> found proj ids: %s' % prjIdList )

    return render_template( 'showProjStats.html', year=selYear,
                            prjIdList = prjIdList,
                            navInfo = getNavInfo( session['year'] ) )


@main.route('/newInstRespPledge', methods=['GET'])
@main.route('/newInstRespPledge/<string:instCode>', methods=['GET'])
@login_required
def newInstRespPledge(instCode=None):

    if not checkPledgingAllowed():
        msg = 'Sorry, creating new pledges is not allowed'
        flash( msg, 'error' )
        current_app.logger.error('newInstRespPledge> %s' % msg)
        return redirect( get_redirect_target() or url_for('main.showMine') )

    selYear = session.get('year')

    if instCode is None:
        instCode = db.session.query(EprInstitute).filter(EprInstitute.id == current_user.mainInst).one().code

    isCurrentInstMgr, selInst = canManageSelectedInst( instCode, selYear )
    isProjMgr, projList = canManageProject( selYear )

    # as of 2020-04-29, also Project Managers are allowed to create new pledges for InstResp tasks
    canCreatePledge = isCurrentInstMgr or isProjMgr

    current_app.logger.info('newInstRespPledge> user %s in %s: currentInstMgr: %s - isProjMgr: %s (%s)' % (current_user.username, selYear, isCurrentInstMgr,  isProjMgr, projList) )

    # only team leaders are allowed to pledge for InstResp tasks
    if not canCreatePledge:
        msg = "Sorry, you (%s) are not allowed to create Institute Responsibility pledges for %s in %s" % (current_user.username, selInst.code, selYear)
        return logAndRedirect(msg=msg, redirUrl=url_for('main.index'), level='error')

    # instManagers and PMs can pledge for this and last year
    isAllowed = ( (canCreatePledge and (selYear == getPledgeYear() or selYear == getPledgeYear() - 1)) )

    if not isAllowed:
        msg = "New Institute Responsibility pledges are not allowed for %s " % selYear
        return logAndRedirect(msg='%s - getPledgeYear():%s' % (msg, getPledgeYear()), redirUrl=url_for('main.index'), level='error')

    isAllowed = isAllowed or current_user.isManager(Permission.MANAGEPROJ, selYear)

    form = InstRespPledgeForm()
    form.taskName.data = ''
    form.taskId.data = -1
    form.workPledged.data = 0

    if form.validate_on_submit():
        current_app.logger.info( 'newInstRespPledge> form validated %s:' % str(form) )

    return render_template( 'newInstRespPledge.html', year=selYear,
                            form=form,
                            inst = selInst,
                            navInfo = getNavInfo( session['year'] ) )

@main.route( '/editInstRespPledge/<string:taskId>/<string:instCode>', methods=['GET', 'POST'] )
@login_required
def editInstRespPledge( taskId, instCode ) :

    if not checkPledgingAllowed():
        msg = 'Sorry, updating pledges is not allowed'
        flash( msg, 'error' )
        current_app.logger.error('editInstRespPledge> %s' % msg)
        return redirect( get_redirect_target() or url_for('main.showMine') )

    selYear = session.get('year')

    current_app.logger.info( 'editInstRespPledge> request to edit InstResp pledge for taskId/instCode : %s / %s in %s' % (taskId, instCode, selYear) )

    task = db.session.query(Task).filter(Task.id == taskId).one()
    if not task:
        return logAndRedirect( 'ERROR: no task with id %s found in %s' % (taskId, selYear),
                                url_for( 'main.showMine' ), level='error' )

    inst = db.session.query(EprInstitute).filter(EprInstitute.code == instCode).one()

    current_app.logger.info( 'editInstRespPledge> user %s is is_administrator: %s' % (current_user.username, current_user.is_administrator()))

    isInstMgr, inst0 = isInstManager(inst=inst)
    current_app.logger.info( 'editInstRespPledge> user %s is instManager for %s: %s' % (current_user.username, inst.code, isInstMgr))

    isPATMgr = current_user.canManage('task', task.code, selYear)
    current_app.logger.info( 'editInstRespPledge> isPATMgr: %s - for task %s, inst %s in %s' % (isPATMgr, task.name, inst.code, selYear))

    if not (current_user.is_administrator() or isPATMgr or isInstMgr):
        return logAndRedirect('Sorry, you are not allowed to edit pledges for InstResp task %s for inst %s' % (task.name, inst.code),
                       url_for('main.showMine'),
                       level='error')

    current_app.logger.debug( 'editInstRespPledge> isAdmin or PATmgr or InstMgr: %s/%s/%s - for task %s, inst %s in %s' % (current_user.is_administrator(), isPATMgr, isInstManager, task.name, inst.code, selYear))

    # admins can also act on pledges for institutes other than their own/home one:
    if current_user.is_administrator():
        if instCode is None:
            current_app.logger.info( 'editInstRespPledge> got invalid instCode (%s) -- aborting ...' % instCode)
            return redirect( url_for( 'main.editInstRespPledge', taskId=taskId ) )

        inst = db.session.query(EprInstitute).filter(EprInstitute.code == instCode).one()
        current_app.logger.info( 'editInstRespPledge> acting as admin on behalf of inst %s for InstResp task %s (%s) in %s' %
                                 (instCode, task.name, task.id, selYear) )

    res = getInstRespPledgeSummary(taskId=task.id, instCode=inst.code, year=selYear)

    current_app.logger.info( 'editInstRespPledge> got %d pledges for task %s, inst %s in %s' % (len(res), task.name, inst.code, selYear))

    if len( res ) == 0 :  # nothing to do ...
        return logAndRedirect( 'No InstResp pledges found for task "%s" (%s) and inst %s '% (task.name, inst.code, selYear),
                               url_for('main.showMine'), level='warning' )

    # for a given task/institute combination, there should be exactly one pledge or none at all.
    if len(res) > 1:
        return logAndRedirect('ERROR: wrong number of pledges for InstResp Task %s for inst %s in %s: %d should be 1' %
                              (task.name, inst.code, selYear, len(res)),
                              None, level='error' )

    if len( res ) > 0 :
        taskRes, wPld, wAcc, wDone, wSoFar, pStatus, instRes = res[0]
    else:
        wPld, wAcc, wDone, wSoFar, pStatus = (0., 0., 0., 0., '')
        taskRes = task
        instRes = inst
        pStatus = 'new'

    # check status of pledges (they all should have the same, as they are updated in parallel) and only allow changes if status is "new"
    if pStatus != 'new' and not current_user.is_wizard():
        return logAndRedirect('ERROR: editing pledges for InstResp Task %s for inst %s in %s not allowed: status is "%s" and not "new" ... ' %
                              (task.name, inst.code, selYear, pStatus),
                              None, level='error' )

    pledgeInfo = { 'status': pStatus, 
                   'taskId': task.id,
                   'workTimePld':  wPld,
                   'workTimeAcc':  wAcc,
                   'workTimeDone': wDone,
                   'workedSoFar':  wSoFar,
                   }

    current_app.logger.info( 'editInstRespPledge> pledgeInfo: %s' % str( pledgeInfo ) )

    irPledges = getInstRespPledges( taskId=task.id, instCode=inst.code, year=selYear )
    current_app.logger.info( 'editInstRespPledge> found %d pledges for InstResp task %s (%s) and inst %s in %d' %
                                ( len( irPledges ), task.name, task.id, inst.code, selYear) )

    form = InstRespPledgeEditForm()

    if form.validate_on_submit():
        current_app.logger.info( 'editInstRespPledge> form: %s' % str(form.data) )
        if ( # form.workTimePld.data != wPld or
             form.workTimeAcc.data != wAcc or
             form.workTimeDone.data != wDone or
             form.workedSoFar.data != wSoFar or
             form.status.data != pStatus
        ) :
            current_app.logger.info( '\neditInstRespPledge> change detected ...' )
            current_app.logger.info( 'old: %s' % str(pledgeInfo) )
            current_app.logger.info( 'new: %s' % str(form.data) )
            current_app.logger.info( '\n' )

            irPledges = getInstRespPledges( taskId=task.id, instCode=inst.code, year=selYear )
            current_app.logger.info( 'editInstRespPledge> found %d pledges for InstResp task %s (%s) and inst %s in %d' %
                                     ( len( irPledges ), task.name, task.id, inst.code, selYear) )

            # calculate which fraction of the pledged work was accepted/done to re-calculate the values for the individual pledges
            fracAcc  = float(form.workTimeAcc.data)/float(wPld) if float(wPld) > 0. else 0.
            fracDone = float(form.workTimeDone.data)/float(form.workTimeAcc.data) if float(form.workTimeAcc.data)>0. else 0.

            current_app.logger.info( '===> fracAcc : %s (%s/%s)' % (fracAcc, float(form.workTimeAcc.data), float(wPld)) )
            current_app.logger.info( '===> fracDone: %s (%s/%s)' % (fracDone, float(form.workTimeDone.data), float(form.workTimeAcc.data) ) )

            # loop over all individual pledges for this InstResp pledge and update accordingly.
            # This commits each pledge individually and sends the mail
            nOk = 0
            for pl, t, a, p, u, i in irPledges :
                if pl.status == 'rejected': continue
                checkOK, msg = doUpdateInstRespPledge( pl, fracAcc, fracDone, form.status.data, t)
                if not checkOK :
                    current_app.logger.error( msg )
                    flash( msg, 'error' )
                    return render_template( 'editInstRespPledge.html', year=selYear,
                                            form=form, workPledged=wPld,
                                            user=current_user, task=task, inst=inst, pledgeInfo=pledgeInfo,
                                            canAdmin = current_user.is_administrator(),
                                            isInstMgr = isInstMgr or current_user.is_administrator(),
                                            irPledges = irPledges,
                                            navInfo=getNavInfo( session[ 'year' ] ) )
                else:
                    nOk += 1

            msg = 'Successfully updated %d (of %d) pledges for InstResp task %s (%s) and inst %s in %d' % ( nOk, len( irPledges ), task.name, task.id, inst.code, selYear )
            logAndRedirect(msg, redirUrl= url_for('main.editInstRespPledge', taskId=taskId, instCode=instCode) )
        else:
            current_app.logger.info( '==> no pledge in form: %s ' % str(form.data) )

    else:
        current_app.logger.info( '==> form.data: %s ' % str(form.data) )
        current_app.logger.info( '==> form.errs: %s ' % str(form.errors) )
        if form.errors:
            msg = 'editInstRespPledge> task:%s -- Form validation had errors: %s ' % (taskId, str( form.errors ))
            logAndRedirect(msg, url_for('main.editInstRespPledge', taskId=taskId, instCode=instCode))
            # form.workTimePld.default = wPld

    return render_template( 'editInstRespPledge.html', year=selYear,
                            form=form, workPledged = wPld,
                            user=current_user, task=task, inst=inst, pledgeInfo = pledgeInfo,
                            canAdmin = current_user.is_administrator(),
                            isInstMgr = isInstMgr or current_user.is_administrator(),
                            irPledges = irPledges,
                            navInfo = getNavInfo( session['year'] ) )

def doUpdateInstRespPledge(pl, fracAcc, fracDone, status, task):

    # sanitize what we get back ... 
    workTimePld = pl.workTimePld if pl.workTimePld is not None else 0.
    workTimeAcc = pl.workTimePld * fracAcc if pl.workTimePld is not None else 0.
    workedSoFar = pl.workedSoFar if pl.workedSoFar is not None else 0.
    workTimeDone = pl.workTimeAcc * fracDone  if pl.workTimeAcc is not None else 0.

    if workedSoFar < workTimeDone:
        workedSoFar = workTimeDone

    # now see what the requested change is:
    # if the requested new status is 'accepted', we need to check
    # if the approved values fit with the needs of the task:

    current_app.logger.debug( 'doUpdateInstRespPledge> pledge new state: "%s" (from "%s")' % (status, pl.status) )
    current_app.logger.debug( 'doUpdateInstRespPledge> pledge new acc  : %s (from %s)' % (workTimeAcc, pl.workTimeAcc) )
    current_app.logger.debug( 'doUpdateInstRespPledge> pledge new done : %s (from %s)' % (workTimeDone, pl.workTimeDone) )
    current_app.logger.debug( 'doUpdateInstRespPledge> pledge new soFar: %s (from %s)' % (workedSoFar, pl.workedSoFar) )

    if status.lower() == 'accepted' :

        # check if the approval is OK or if something is overcommitted. We only need to do this
        # if the new workTimeAcc is larger than the original (previous) one:
        # allow a larger overshoot when checking InstResp tasks
        if workTimeAcc > pl.workTimeAcc :
            checkOK, msg = checkPledgeApproval( workTimePld, workTimeAcc, workedSoFar, workTimeDone, task, None, totalTaskOvershoot=10. )

    # if the requested new status is 'done', we need to check
    # if the present done values fit with the needs of the task,
    # but only if the new workTimeDone is larger than the original (previous) one:
    if status.lower() == 'done' :

        # check if the approval is OK or if something is overcommitted. We only need to do this
        # if the new workTimeAcc is larger than the original (previous) one:
        # allow a larger overshoot when checking InstResp tasks
        if workTimeAcc > pl.workTimeAcc :
            checkOK, msg = checkPledgeApproval( workTimePld, workTimeAcc, workedSoFar, workTimeDone, task, None, totalTaskOvershoot=10. )

        # allow a larger overshoot when checking InstResp tasks
        if workTimeDone > pl.workTimeDone :
            checkOK, msg = checkPledgeDone( workTimePld, workTimeAcc, workedSoFar, workTimeDone, task, None, totalTaskOvershoot=10. )
            current_app.logger.debug( "checkPledgeDone> got : %s %s" % (checkOK, msg) )
            if not checkOK :
                return checkOK, msg

    # continue with the update of the pledge

    oldPledgeJson = pl.to_json()

    # update the pledge. Tracking of changes is done via the SQLAlchemy-Continuum package
    # update values in case they got changed:

    pl.workTimePld = workTimePld
    pl.workTimeAcc = workTimeAcc
    pl.workTimeDone = workTimeDone

    pl.workedSoFar = workedSoFar

    pl.status = status
    pl.year = session[ 'year' ]

    pl.timestamp = datetime.datetime.utcnow()

    newPledgeJson = pl.to_json()

    try :
        commitUpdated( pl )
    except Exception as e :
        msg = 'Failed to update pledge info: %s' % str( e )
        return False, msg

    newPledgeJson = db.session.query( Pledge ).filter_by( code=pl.code ).one().to_json()
    if not sendMailPlegdeUpdated( oldPledgeJson, newPledgeJson ) :
        msg = "ERROR when sending mail for updating pledge"
        flash( msg, 'error' )
        current_app.logger.warning( 'pledge> ERROR %s ' % msg )

    msg = 'Pledge info successfully updated.'
    return True, msg

@main.route( '/updateInstRespWorkDone/<string:taskId>/<string:instCode>', methods=['GET', 'POST'] )
@login_required
def updateInstRespWorkDone( taskId, instCode) :

    if not checkPledgingAllowed():
        msg = 'Sorry, updating pledges is not allowed'
        flash( msg, 'error' )
        current_app.logger.error('rejectupdateInstRespWorkDoneInstRespPledge> %s' % msg)
        return redirect( get_redirect_target() or url_for('main.showMine') )

    current_app.logger.info( 'updateInstRespWorkDone> request to update WorkDone for InstResp pledge for task : %s' % taskId )
    selYear = session.get('year')

    task = db.session.query(Task).filter(Task.id == taskId).one()
    inst = db.session.query(EprInstitute).filter(EprInstitute.code == instCode).one()

    return render_template( 'updateInstRespPledge.html', year=selYear,
                            task=task, inst=inst,
                            navInfo = getNavInfo( session['year'] ) )

@main.route( '/rejectInstRespPledge/<string:taskId>/<string:instCode>', methods=['GET', 'POST'] )
@login_required
def rejectInstRespPledge( taskId, instCode ) :

    if not checkPledgingAllowed():
        msg = 'Sorry, updating pledges is not allowed'
        flash( msg, 'error' )
        current_app.logger.error('rejectInstRespPledge> %s' % msg)
        return redirect( get_redirect_target() or url_for('main.showMine') )

    current_app.logger.info( 'rejectInstRespPledge> request to reject InstResp pledge for task : %s' % taskId )
    selYear = session.get('year')

    task = db.session.query(Task).filter(Task.id == taskId).one()
    if not task:
        return logAndRedirect( 'ERROR: no task with id %s found in %s' % (taskId, selYear),
                                url_for( 'main.showMine' ), level='error' )

    inst = db.session.query(EprInstitute).filter(EprInstitute.code == instCode).one()

    isInstMgr, inst = isInstManager(inst=inst)
    current_app.logger.info( 'rejectInstRespPledge> user %s is instManager for %s: %s' % (current_user.username, inst.code, isInstMgr))

    isPATMgr = current_user.canManage('task', task.code, selYear)
    current_app.logger.info( 'rejectInstRespPledge> isPATMgr: %s - for task %s, inst %s in %s' % (isPATMgr, task.name, inst.code, selYear))

    if not (current_user.is_administrator() or isPATMgr):
        return logAndRedirect('Sorry, you are not allowed to reject pledges for InstResp task %s for inst %s' % (task.name, inst.code),
                       url_for('main.showMine'),
                       level='error')

    # admins can also act on pledges for institutes other than their own/home one:
    if current_user.is_administrator():
        if instCode is None:
            current_app.logger.info( 'rejectInstRespPledge> got invalid instCode (%s) -- aborting ...' % instCode)
            return redirect( url_for( 'main.showMine' ) )

        inst = db.session.query(EprInstitute).filter(EprInstitute.code == instCode).one()
        current_app.logger.info( 'rejectInstRespPledge> acting as admin on behalf of inst %s for InstResp task %s (%s) in %s' %
                                 (instCode, task.name, task.id, selYear) )


    res = getInstRespPledgeSummary(taskId=task.id, instCode=inst.code, year=selYear)

    current_app.logger.info( 'rejectInstRespPledge> got %d InstResp Pledge for task %s, inst %s in %s' % (len(res), task.name, inst.code, selYear))

    if len(res) != 1:
        return logAndRedirect('rejectInstRespPledge> ERROR: wrong number of pledges for InstResp Task %s for inst %s in %s: %d should be 1' % 
                (task.name, inst.code, selYear, len(res)), None, level='error' )


    taskRes, wPld, wAcc, wDone, wSoFar, pStatus, instRes = res[0]
    pledgeInfo = { 'status': 'new', 'taskId': task.id,
                   'workTimePld':  wPld,
                   'workTimeAcc':  wAcc,
                   'workTimeDone': wDone,
                   'workedSoFar':  wSoFar,
                   }

    current_app.logger.info( 'rejectInstRespPledge> pledgeInfo: %s' % str( pledgeInfo ) )

    form = InstRespPledgeRejectForm()

    if form.validate_on_submit():
        current_app.logger.info( 'rejectInstRespPledge> form.reason: %s' % str(form.reason.data) )

        irPledges = getInstRespPledges(taskId=task.id, instCode=inst.code, year=selYear)
        current_app.logger.info( 'rejectInstRespPledge> found %d pledges for InstResp task %s (%s) and inst %s in %d' % (len(irPledges), task.name, task.id, inst.code, selYear) )

        # print( 'irPledges: ', irPledges)

        for pl, t, a, p, u, i in irPledges:
            pl.status = 'rejected'
            pl.code   = pl.code + time.strftime('%Y%m%d-%H%M%S')
            pl.timestamp = datetime.datetime.utcnow()
            db.session.add(pl)

        try :
            db.session.commit()
        except Exception as e :
            db.session.rollback()
            msg = 'Error from DB when trying to reject pledge -- rolled back ... \n(%s)' % str(e)
            flash( msg, 'error')
            current_app.logger.warning( 'rejectInstRespPledge> ERROR %s ' % msg )
            target = get_redirect_target()
            return redirect( target or url_for('main.index') )

        for pl, t, a, p, u, i in irPledges:
            if not sendMailPledgeRejected( pl.to_json(), reason=form.reason.data ):
                msg = "ERROR when sending mail for rejected pledge"
                flash( msg, 'error')
                current_app.logger.warning( 'rejectInstRespPledge> ERROR %s ' % msg )

        msg = 'Rejected %d pledges for InstResp task %s (%s) and inst %s in %d' % (len(irPledges), task.name, task.id, inst.code, selYear)
        flash( msg )
        current_app.logger.info( 'rejectInstRespPledge> %s ' % msg )

        target = get_redirect_target()
        # override with session['referrer'] if valid:
        if is_safe_url(session.get('referrer')):
            target = session.pop('referrer', url_for('main.index')) # remove entry from dict as well
        return redirect( target or url_for('main.index') )

    else:
        if form.errors:
            msg = 'rejectInstRespPledge> task:%s -- Form validation had errors: %s ' % (taskId, str( form.errors ))
            logAndRedirect(msg, url_for('main.rejectInstRespPledge', taskId=taskId, instCode=instCode))

    return render_template( 'rejectInstRespPledge.html', year=selYear,
                            task=task, inst=inst, pledgeInfo=pledgeInfo,
                            form=form,
                            navInfo = getNavInfo( session['year'] ) )


@main.route('/approveSelectedIRPledges', methods=['POST'])
@login_required
def approveSelectedIRPledges():

    if not checkPledgingAllowed():
        msg = 'Sorry, updating pledges is not allowed'
        flash( msg, 'error' )
        current_app.logger.error('approveSelectedIRPledges> %s' % msg)
        return redirect( get_redirect_target() or url_for('main.showMine') )

    plSel = json.loads( request.form.get('pledges', None) )

    if plSel is None or len(plSel) == 0:
        msg = "main.approveSelectedIRPledges> no pledges found in request: %s " % str(request.form)
        current_app.logger.warning(msg)
        flash(msg, 'error')
        return jsonify( { 'msg' : msg } ), 400

    current_app.logger.info("main.approveSelectedIRPledges> got pledges to approve %s " % plSel )

    errMsg = []
    infoMsg = []
    httpCode = []
    failInfo = []
    httpStatusCode = 200
    for plCode in plSel:
        instCode, taskCode = plCode
        current_app.logger.info("main.approveSeapproveSelectedIRPledgeslectedPledges> processing pledge %s for approval " % plCode )
        pledges = ( db.session.query(Pledge)
                        .join(Task, Pledge.taskId == Task.id)
                        .join(EprInstitute, EprInstitute.id == Pledge.instId)
                        .filter(Task.code == taskCode)
                        .filter(EprInstitute.code == instCode)
                        .all() )
        for pledge in pledges:
            if pledge is None:
                failInfo.append( plCode )
                noPlMsg = 'ERROR> no pledge with code "%s" found in DB' % plCode
                current_app.logger.warning( noPlMsg )
                errMsg.append( noPlMsg )
                httpCode.append( 'HTTPCODE> %s : %s' % (plCode, -404) )
                httpStatusCode = 400
            else:
                iMsg, httpStat = approveSinglePledge(pledge)
                if httpStat != 200:
                    failInfo.append( plCode )
                    errMsg.append( 'ERROR> %s : %s' % (plCode, iMsg) )
                    httpCode.append( 'HTTPCODE> %s : %s' % (plCode, httpStat) )
                    httpStatusCode = 400
                else:
                    httpCode.append( 'HTTPCODE> %s : %s' % (plCode, httpStat) )
                    infoMsg.append( 'INFO> %s : %s' % (plCode, iMsg) )

    return jsonify( { 'msg' : '\n'.join(infoMsg), 'err' : '\n'.join(errMsg), 'httpStats': '\n'.join(httpCode) } ), httpStatusCode
