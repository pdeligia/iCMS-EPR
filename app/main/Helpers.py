import socket
import time
import subprocess
import sqlalchemy
import copy

from datetime import date, datetime

from flask import redirect, url_for, current_app, flash, session, request
from flask_login import current_user

from sqlalchemy import exc, func, desc, and_, case, orm, or_

from sqlalchemy_extensions import group_concat, QueryFactory

from urllib.parse import urlparse, urljoin

from .. import db, cache

from ..models import Permission, permsMap, EprUser, Project, CMSActivity, Task, Pledge, Shift, ShiftTaskMap, \
    EprInstitute, Manager, Category, EprCeiling, Level3Name, \
                     AllInstView, TimeLineInst, TimeLineUser, getTaskShiftTypeIds, \
                     commitNew, commitUpdated

from ..models import getEgroupsForUser

from .EprAssets import EprAssets

from icms_orm.common import UserPreferences, Announcement

from config import prodHostList
from sqlalchemy.orm import exc as sa_exc

# from icmsutils.businesslogic.servicework import get_person_shifts_done

class HelperError(Exception):
    def __init__(self, msg):
        self.msg = msg
    def __repr__(self):
        return repr(self.msg)


def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('https') and \
           ref_url.netloc == test_url.netloc

def get_redirect_target():

    for target in request.args.get('next'), request.referrer:
        if not target:
            continue
        if is_safe_url(target):
            return target

    return None

def logAndRedirect(msg, redirUrl=None, level='info', flashLvl=None):

    if level == 'error':
        current_app.logger.error( msg )
        flashLvl = 'error'
    elif level == 'warning':
        current_app.logger.warning( msg )
        flashLvl = 'warning'
    else:
        current_app.logger.info( msg )

    # override with explicit setting:
    if flashLvl is not None:
        flash( msg, flashLvl )
    else:
        flash( msg )

    target = redirUrl if redirUrl else get_redirect_target()
    return redirect( target )

def makeCountryInstMenu() :

    # create sub-menus in the navBar using the bootstrap extension from:
    # https://vsn4ik.github.io/bootstrap-submenu/
    # keep the list of countries and institutes sorted by name (which
    # seems not to work inside the template, so we do it here.

    memAndCoop = db.session.query(EprInstitute).filter( EprInstitute.cmsStatus.in_( ['Yes', 'Cooperating', 'Associated']) ).order_by( EprInstitute.name ).all()
    items = [(inst.country, inst.code) for inst in memAndCoop]
    countryInst = { }
    for c, i in items :
        if c not in countryInst.keys( ) : countryInst[ c ] = [ ]
        countryInst[ c ].append( i )
    cimTmpl_c = """
                    <li class="dropdown-submenu">
                        <a tabindex="0"> %(country)s </a>
                        <ul class="dropdown-menu">
                            %(instList)s
                        </ul>
                    </li>
"""
    cimTmpl_i = """
                            <li> <a tabindex="0" href="%(url)s"> %(code)s </a> </li>
"""
    cim = ''
    for k in sorted( countryInst.keys( ) ) :
        cimInst = ''
        for i in sorted( countryInst[ k ] ) :
            url = url_for("main.showInstitute", code=i.encode( 'ascii', 'xmlcharrefreplace' ).decode('utf-8') )
            cimInst += cimTmpl_i % { 'country' : k, 'url': url, 'code' : i.encode( 'ascii', 'xmlcharrefreplace' ).decode('utf-8') }
        cim += cimTmpl_c % { 'country' : k, 'instList' : cimInst }
    return cim

def makeCountryMenu() :

    cmTmpl_c = '<li> <a href="%s"> %s </a> </li>'
    memAndCoop = db.session.query(EprInstitute).filter( EprInstitute.cmsStatus.in_( ['Yes', 'Cooperating', 'Associated']) ).order_by( EprInstitute.name ).all()

    items = [(inst.country, ) for inst in memAndCoop]
    countryList = []
    for c, in items :
        if c not in countryList :
            countryList.append( c )

    cMenu = ''
    for c in sorted(countryList):
            cMenu += cmTmpl_c % ( url_for("main.showCountry", name="%s" % c ) ,c)

    return cMenu

def flash_errors(form):
    """Flashes form errors"""
    for field, errors in form.errors.items():
        for error in errors:
            flash(u"Error in the %s field - %s" % (
                getattr(form, field).label.text,
                error
            ), 'error')

def getLevel3NameMap(selYear):
    lvl3Map = {}
    lvl3Items = db.session.query(Level3Name).filter(Level3Name.startYear>=selYear).filter(Level3Name.endYear<=selYear).all()
    lvl3Items += db.session.query(Level3Name).filter(Level3Name.startYear>=selYear).filter(Level3Name.endYear == None).all()
    for item in lvl3Items:
        lvl3Map[item.id] = item.name
    return lvl3Map

def getPledgeYear( defaultYear=-1 ):

    # Return the "default year" for pledges. This can differ from
    # the actual calendar year, as pledges may be allowed only for
    # the last year in the early months of the current year.

    if not session:
        pldYear = EprAssets().getAssets('pledgeYear', defaultYear)
        current_app.logger.warning( 'getPledgeYear> no session found, assuming testing ... pldYear from assets: %s (%s)' % (pldYear, defaultYear) )
        return pldYear

    # always re-load assets from DB
    session[ 'assets' ] = EprAssets().getAssets('all', defaultYear)
    session.modified = True

    if not session[ 'assets' ] :
        current_app.logger.warning("ERROR -- no assets could be retrieved !!! ")

    # print( '=+=+> session[assets] : %s' % session[ 'assets' ] )
    plYear = int( session[ 'assets' ][ 'pledgeYear' ] )

    current_app.logger.debug( 'got Pledge year from assets: %d' % int(plYear) )

    return int(plYear)

def getRequiredWork(year):

    if not session:
        # current_app.logger.debug( 'getRequiredWork> no session found, assuming testing ... ' )
        return 4. # return some reasonable default for testing ...

    # always re-load assets from DB
    session[ 'assets' ] = EprAssets().getAssets('all', None)
    session.modified = True

    # convert back the
    reqWork = session[ 'assets' ][ 'reqWork' ]

    # the assets come back as json where only strings are allowed as keys:
    if str(year) in reqWork:
        return reqWork[ str(year) ]
    else:
        return 999.

def getCspPerAuthor(year):

    # 2015: (0.266)
    # 2016: 8.1 CSP -> 0.374 EPR
    # 2017: 8.9 CSP -> 0.411 EPR
    # 2018: 9.57 CSP -> 0.442 EPR
    # 2019: 3.40 CSP -> 0.157 EPR
    # 2020: 3.40 CSP -> 0.157 EPR
    # >2020: unknown so far

    if 'assets' not in session:
        session[ 'assets' ] = EprAssets().getAssets(db.session, 'all', None)
        session.modified = True

    nominalCspPerAuthor = session[ 'assets' ][ 'cspPerAuthor' ]

    # the assets come back as json where only strings are allowed as keys:
    if str(year) not in nominalCspPerAuthor.keys():
        return -1

    cspPerAuthor = nominalCspPerAuthor[ str(year) ]

    # A hard-coded number for 2015 caused a bug in the display of the 2017 data.
    # This bug was discovered late during pledging, so on request from Kerstin Borras
    # (engagement office), we put back a correction factor to show consistent (even
    # if wrong) numbers for 2017:
    if int(year) == 2017:
        cspPerAuthor = cspPerAuthor * (0.266/0.411)

    return cspPerAuthor * getCSPtoEPR(year)

def isApplicant(cmsId, year):

    # authorBlocked, not suspended, right category, not author

    q = QueryFactory.q_get_user_last_line(year=year, cms_id=cmsId)
    res = q.one()
    # current_app.logger.info( "isApplicant> user Id %i timeline: %s" % (cmsId, str(res)) )

    return (res.dueApplicant > 0.)

def isInstManager(user=None, inst=None, year = None, instCode=None):

    selYear = year
    if year is None:
        selYear = session.get( 'year' )

    if user is None:
        user = current_user

    if inst is None:
        inst = db.session.query(EprInstitute).filter(EprInstitute.id == user.mainInst).one()

    aM, pM, tM, iM = getManagers( selYear )

    if instCode is None:
        instCode = inst.code

    return instCode in iM, inst

def getTaskPctDone(task):

    if float(task.neededWork) < 0.01 :
        current_app.logger.info( " ??? no work needed for %s " % task.name )
        return '0.0'

    res = db.session.query(func.sum(Pledge.workTimeDone)).filter(Pledge.taskId==task.id).one()
    sum, = res
    if not sum: sum = 0.

    pctDone = 0.
    if float(task.neededWork) > 0. : pctDone = float(sum)/float(task.neededWork)

    return '%2.2f' % pctDone

def getPledgeSum(userId, year):

    result = (db.session.query( func.sum(Pledge.workTimePld).label('sumPld'),
                               func.sum(Pledge.workTimeAcc).label('sumAcc'),
                               func.sum(Pledge.workTimeDone).label('sumDone') )
                        .filter(Pledge.userId==userId, Pledge.year==year, Pledge.status!='rejected')
                        ).all()

    current_app.logger.info( "res: %s " % str(result) )
    sumPld, sumAcc, sumDone = (0,0,0)
    try:
        sumPld, sumAcc, sumDone = [ float(i) for i in result[0] ]
    except:
        pass

    current_app.logger.info( "user Id %i has %2.2f pledged, %2.2f accepted %2.2f done." % (userId, sumPld, sumAcc, sumDone) )

    return (sumPld, sumAcc, sumDone)

def getTaskAccepted(task):
    res = db.session.query( func.sum( Pledge.workTimeAcc ) )\
                    .filter( Pledge.taskId == task.id )\
                    .filter( Pledge.status == 'accepted' )\
                    .one( )

    sum, = res
    if not sum: sum = 0.

    return float( sum )

def getTaskDone(task):
    res = db.session.query( func.sum( Pledge.workTimeDone ) )\
                    .filter( Pledge.taskId == task.id )\
                    .filter( Pledge.status == 'done' )\
                    .one( )

    sumVal, = res
    if not sumVal: sumVal = 0.

    return float( sumVal )

import cProfile
from io import StringIO
import pstats
import contextlib

infoTextTmpl = {}
infoTextTmpl[2015 ] = ""
infoTextTmpl[2016 ] = """<h4>EPR 2016:</h4>
        <p>
            EPR due for an author: %3.1f months core tasks<br />
            Non-core tasks will be opened as soon as core is covered.<br />
            EPR worked on non-core tasks will be accounted with a downscale of 0.25.<br />
            Shift work expected for an author: %2.1f central shift credit points (CSP), equivalent to %4.3f EPR months<br />
            "Pledged" shifts are shifts which are in the future (i.e. until the end of the year).
            As they can still be   changed/cancelled, they do not count to the shifts actually "done" (i.e. in the past).<br />
        </p>
"""

infoTextDefaultTmpl = """<h4>EPR %(year)s:</h4>
        <p>
            EPR due for an author: %%3.1f months %(core)s tasks (including shifts) <br />
            <b>Shift work</b> expected for an author: %%3.2f central shift credit points (CSP), equivalent to %%4.3f EPR months<br />
            For more information on central shift accounting, and how to convert your shift work to EPR months (or "EPR credits") see: <a href='https://twiki.cern.ch/twiki/bin/view/CMS/CentralShifts%(year)s#Central_credit_accounting'>this document.</a>
        </p>
"""

@contextlib.contextmanager
def profiled():
    pr = cProfile.Profile()
    pr.enable()
    yield
    pr.disable()
    s = StringIO()
    ps = pstats.Stats(pr, stream=s).sort_stats('cumulative')
    ps.print_stats()
    # uncomment this to see who's calling what
    # ps.print_callers()
    print( s.getvalue() )

def getCSPtoEPR(year):

    # Since 2012 [1], the conversion has always been:
    #    5 shift credits = 1 EPR week.  ("5 Shift credits are counted as 1 week of service work.").
    #
    # Then, the conversion to EPR month was always computed as:
    #    1 shift credit * (1 EPR week / 5 shift credits) * (12 EPR months / 52 EPR weeks) = 0.0462 EPR months
    # (note that this only counts 364 days per year (7*52)
    #
    # [1] https://twiki.cern.ch/twiki/bin/view/CMS/CentralShifts2012

    return (1./5.) * (12./52.)

@cache.memoize(timeout=300)
def getNavInfo(year):

    # Assume the text of the default template won't change for the future years.
    # In case it does, move this also to an ApplicationAsset.
    for y in range( 2017, getPledgeYear() + 2 ) :
        infoTextTmpl[ y ] = (infoTextDefaultTmpl % { 'year' : str( y ), 'core' : 'core' if y < 2020 else '' }).replace( '%%', '%' )

    # ensure the default year for pledges is set correctly
    if not session[ 'year' ] or int( session[ 'year' ] ) < 1900:
        session[ 'year' ] = getPledgeYear()
        session.modified = True

    try:
        allProj=db.session.query(Project).filter_by(year=year).order_by(Project.name).all()
        allInst=db.session.query(EprInstitute).filter_by(year=year).order_by(EprInstitute.name).all()
    except exc.InvalidRequestError as e:
        current_app.logger.warning( "getNavInfo> got unknown InvalidRequestError for year %s: %s [rolling back ...]" % (year, str(e)) )
        db.session.rollback()
        allProj = db.session.query(Project).filter_by( year=year ).order_by( Project.name ).all( )
        allInst = db.session.query(EprInstitute).filter_by( year=year ).order_by( EprInstitute.name ).all( )

    countryInstMenu = makeCountryInstMenu()
    countryMenu = makeCountryMenu()

    level = "testing"
    for prodHostName in prodHostList:
        if socket.gethostname().startswith(prodHostName):
            level = 'production'

    infoText = ""
    if len(infoTextTmpl.get( int(year), '')) > 0:
        infoText = infoTextTmpl.get( int(year),'') % (getRequiredWork(year), getCspPerAuthor(year)/getCSPtoEPR(year), getCspPerAuthor(year) )

    gitInfo = None
    if current_user.is_wizard():
        gitInfo = run_in_shell( '/usr/bin/git describe --all --long', shell = True )

    hasManagedTasks = False
    try:
        mgtTasks = ( db.session.query( Manager.itemCode )
                            .filter( Manager.userId == current_user.id )
                            .filter( Manager.year == year )
                            .filter( Manager.role.in_( [Permission.MANAGETASK, Permission.MANAGEACT, Permission.MANAGEPROJ] ) )
                            .filter( Manager.status == 'active' )
                            .all() )
    except Exception as e:
        raise
    current_app.logger.debug('Found %d items managed by user %s' % (len(mgtTasks),current_user.username) )

    # Get the list of all eGroups the user is member of (from his LDAP entry),
    # then match them against the eGroups stored as management eGroups. Add these
    # PAT items to the list before returning
    eGroupList = [ x.replace('@cern.ch', '') for x in getEgroupsForUser( current_user.username ) ]
    mgtEgs = (db.session.query(Manager).filter( Manager.year == year)
                                       .filter( Manager.role.in_([Permission.MANAGETASK, Permission.MANAGEACT, Permission.MANAGEPROJ]) )
                                       .filter( Manager.status == 'active')
                                       .filter( Manager.egroup.in_( eGroupList ) )
                                       .all()
            )

    if ( (mgtTasks and len( mgtTasks ) > 0) or
         (mgtEgs and len(mgtEgs) > 0) ):
        hasManagedTasks = True

    current_app.logger.debug('Found %d items with eGroups to manage for user %s - hasManagedTasks: %s' % (len(mgtEgs),current_user.username, hasManagedTasks) )

    announcements = db.session.query(Announcement).filter(Announcement.end_datetime > datetime.now()).\
        filter(Announcement.start_datetime < datetime.now()).\
        filter(or_(Announcement.application == 'epr', Announcement.application == 'common')).all()

    current_app.logger.debug('Found %d active announcements to display' % len(announcements))

    return { 'announcements' :  announcements,
             'proj' : allProj,
             'inst': allInst,
             'countryInstMenu' : countryInstMenu,
             'countryMenu' : countryMenu,
             'level' : level,
             'infoText' : infoText,
             'cspPerAuthor' : getCspPerAuthor(year),
             'cspToEpr' : getCSPtoEPR(year),
             'gitInfo' : gitInfo,
             'managesTasks' : hasManagedTasks,
             }

def run_in_shell(*popenargs, **kwargs):
    process = subprocess.Popen(*popenargs, stdout=subprocess.PIPE, **kwargs)
    stdout = process.communicate()[0]
    returnCode = process.returncode
    cmd = kwargs.get('args')
    if cmd is None:
        cmd = popenargs[0]
    if returnCode:
        if returnCode == 128:
            return "No git repo info available."
        else:
            raise subprocess.CalledProcessError(returnCode, cmd, output=stdout)
    return stdout


def getTaskActivitiesForProject(projId):

    try:
        activities = db.session.query(CMSActivity).filter_by(projectId=projId).all()
    except:
        current_app.logger.error( 'can not get activity list for projId %s ' % projId )
        return None

    return activities

# todo: understand
def getInstituteInfo(code, year):

    current_app.logger.info( "getInstituteInfo> req for %s in %i " % (code, year) )

    if code.lower() == 'all':
        # need just the last timeline per each institute
        sq = QueryFactory.sq_time_line_inst(year=year)
        allInst = db.session.query(TimeLineInst, EprInstitute.name.label('instName'))\
            .join(sq, TimeLineInst.code == sq.c.code)\
            .join(EprInstitute, TimeLineInst.code == EprInstitute.code)\
            .filter( TimeLineInst.year == year)\
            .filter(EprInstitute.cernId > 0).all()

        instInfo = []
        # with profiled():
        for result in allInst:
            instName = result.instName
            inst = result[0]
            institute, userInfo, summary = getInstituteInfoByCode(code=inst.code, year=year)
            instInfo.append(  { 'code' : inst.code.upper(), 'name' : instName, 'summary' : summary, 'userInfo' : userInfo } )

        return instInfo

    emptySummary = { 'check'        : '',
                'Expected'     : 0.,
                'Pledged'      : 0.,
                'Accepted'     : 0.,
                'Done'         : 0.,
                'Tasks'        : [ ],
                'Authors'      : 0.,
                'ShiftsDone'   : 0.,
                'ShiftsPld'    : 0.,
                'EPRpledged'   : 0.,
                'EPRpldFrac'   : 0.,
                'EPRaccounted' : 0.,
                'EPRaccFrac'   : 0.,
                'sumEprDue'    : -1,
                'sumAthDue'    : -1,
                'actAuthors'   : -1,
                'EPRworkDone' : 0.,
                'Applicants' : 0.,
                'AppPld'     : 0.,
                'AppAcc'     : 0.,
                'AppDone'    : 0.,
                'AthPld'     : 0.,
                'AthAcc'     : 0.,
                'AthDone'    : 0.,
                }

    try:
        inst = db.session.query(TimeLineInst).filter_by(code=code, year=year).order_by(TimeLineInst.timestamp.desc()).first()
        if inst is None: # try to see if user manually entered code with lower-case chars:
            code = code.upper() # fix also for subsequent use
            inst = db.session.query(TimeLineInst).filter_by(code=code, year=year).order_by(TimeLineInst.timestamp.desc()).first()
    except Exception as e:
        current_app.logger.info( "getInstituteInfo> (1) got unknown exception : %s" % str(e) )
        return None, [], emptySummary


    if inst is None:
        current_app.logger.info( "getInstituteInfo> (2) institute not found for code '%s' " % str( code ) )
        return None, [ ], emptySummary

    # with profiled():
    return getInstituteInfoByCode(inst.code, year)

# todo: shift work seems summed up no matter what - do we need the distinction between task shifts and non-task shifts?
# todo: concatenate activities (if needed) the way it's done for projects
# todo: service work done by people not matching status like 'CMS%' will not be displayed!

def getInstituteInfoByCode(code, year):

    subq = QueryFactory.sq_total_dues_by_inst(year=year, inst_code=code)

    (selInst, sumApplicantDue, sumAuthorDue, actAuthors) = db.session.query(TimeLineInst, subq.c.duesApplicant, subq.c.duesAuthor, subq.c.nAuthors).\
            filter(and_(TimeLineInst.code == code, TimeLineInst.year == year)).\
            outerjoin(subq, TimeLineInst.code == subq.c.instCode).\
            order_by(desc(TimeLineInst.timestamp)).limit(1).one()

    if actAuthors is None: actAuthors = 0.

    # # todo: this table contains the most up-to-date snapshot, right?
    # actAuthors = db.session.query(EprUser.cmsId)\
    #                .join(Institute, EprUser.mainInst == EprInstitute.id)\
    #                .filter(EprUser.authorReq == True)\
    #                .filter(EprInstitute.code == code)\
    #                .filter(EprUser.status == 'CMS')\
    #                .filter(EprUser.year == year)\
    #                .count()

    current_app.logger.debug( "got %7.2f active authors for inst code %s year %s " % (actAuthors, selInst.code, year) )

    sq1 = QueryFactory.sq_time_line_user(year=year, inst_code=code if code != 'all' else None, status='CMS%')

    # SQ2 summing the pledge-related values over Pledges related to the current year, institute and not to the shifts.
    # todo: cross checks indicate the dev version does count shifts here
    sq2 = db.session.query(
        Pledge.userId.label('userId'),
        func.sum(Pledge.workTimePld).label('sumPledged'),
        func.sum(Pledge.workTimeAcc).label('sumAccepted'),
        func.sum(Pledge.workTimeDone).label('sumWorkTimeDone'),
        func.sum(Pledge.workTimeDone * case({'CORE': 1.0}, value=Task.kind, else_=0.25)).label('eprWorkDone'),
        func.sum(Pledge.workTimePld  * case({'CORE': 1.0}, value=Task.kind, else_=0.25 )).label('eprWorkPledged')
    ).join(Task, Pledge.taskId == Task.id).join(EprInstitute, Pledge.instId == EprInstitute.id)\
        .filter(and_(Task.tType.notlike('%shift%'), Pledge.year == year))\
        .filter(and_(EprInstitute.code == code, Pledge.status.notlike('%rejected%')))\
        .group_by(Pledge.userId).subquery()

    # SQ3 summing the work done as shifts. Joined (transitively via User) with SQ1 determining the limiting timestamps
    # to be considered for current inst.
    # Note: this part won't work until there is a range of time lines saved for the year

    # use the shift-task mapping only for 2017 onwards.
    # As we only here have the weightScales to apply, we need to create a separate subquery for older data
    if year > 2016:
        sq3 = db.session.query( Shift.userId.label('hrId'),
                                func.sum( (Shift.weight * ShiftTaskMap.weight) *
                                          (1. + ( ShiftTaskMap.addedValueCredits/7.)) ).label('sumShiftWeight') )\
                .filter( Shift.subSystem == ShiftTaskMap.subSystem )\
                .filter( Shift.shiftType == ShiftTaskMap.shiftType )\
                .filter( Shift.flavourName == ShiftTaskMap.flavourName ) \
                .filter( Shift.year == ShiftTaskMap.year ) \
                .filter( ShiftTaskMap.year == year ) \
                .filter( ShiftTaskMap.status == 'ACTIVE' )
    else:  # no weightScales to apply, no shift-task-mapping:
        sq3 = db.session.query( Shift.userId.label( 'hrId' ),
                                func.sum( Shift.weight ).label( 'sumShiftWeight' ) ) \
                .filter( Shift.year == year )

    # now add the other filters as needed:
    if year > 2015:
        sq3 = sq3.filter(Shift.year == year )\
                 .join(EprUser, EprUser.hrId == Shift.userId).join(sq1, EprUser.cmsId == sq1.c.cmsId)\
                 .filter(and_(Shift.shiftStart > sq1.c.firststamp,
                              Shift.shiftStart < sq1.c.endDate))
    else:
        sq3 = sq3.filter(and_(Shift.shiftStart >= date(year, 1, 1), Shift.shiftEnd <= date(year, 12, 31)))

    sq3 = sq3.group_by(Shift.userId)
    # now split into shifts done and future (pledged) shifts:
    # all shifts ending today are counted as "done":
    sq3Done = sq3.filter(Shift.shiftEnd  <= date.today()).subquery()
    sq3Pld  = sq3.filter(Shift.shiftEnd   > date.today()).subquery()

    # SQ4 selecting unique 'user id - project name' pairs
    sq4 = db.session.query(Pledge.userId.label('userId'), Project.name.label('project'))\
        .filter(Pledge.year == year)\
        .join(Task, Pledge.taskId == Task.id).join(CMSActivity, Task.activityId == CMSActivity.id)\
        .join(Project, CMSActivity.projectId == Project.id).distinct().subquery()

    # SQ5 using SQ4 to concatenate the projects that users pledged for
    sq5 = db.session.query(sq4.c.userId.label('userId'),
                           group_concat(sq4.c.project).label('projects')).group_by(sq4.c.userId).subquery()

    # Will need another subquery because the laststamp selected below is the last of timestamps for a CMS%-like status
    # And we want to see the very last status. Slippery :(

    LastLineUser = orm.aliased(TimeLineUser, QueryFactory.sq_get_user_last_line(year=year, inst_code = code if code != 'all' else None))

    instUsers = db.session.query(EprUser, Category, TimeLineUser.mainProj, sq1.c.laststamp, sq1.c.dueApplicant, sq1.c.dueAuthor,
                                    TimeLineUser.isSuspended.label( "isSuspended" ),
                                    sq2.c.sumWorkTimeDone, sq2.c.sumPledged, sq2.c.sumAccepted,
                                    sq2.c.eprWorkDone, sq2.c.eprWorkPledged,
                                    sq5.c.projects,
                                    sq3Done.c.sumShiftWeight.label( "shiftsDone" ),
                                    sq3Pld.c.sumShiftWeight.label( "shiftsPld" ),
                                    LastLineUser.status.label( "lastCmsStatus" )
                                 ) \
                                .join(sq1, EprUser.cmsId == sq1.c.cmsId)\
                                .join(TimeLineUser, and_(TimeLineUser.cmsId == EprUser.cmsId, TimeLineUser.timestamp == sq1.c.laststamp))\
                                .join(LastLineUser, LastLineUser.cmsId == EprUser.cmsId) \
                                .outerjoin(sq5, EprUser.id == sq5.c.userId)\
                                .outerjoin(sq2, EprUser.id == sq2.c.userId)\
                                .outerjoin(Category, EprUser.category == Category.id)\
                                .outerjoin(sq3Done, EprUser.hrId == sq3Done.c.hrId)\
                                .outerjoin(sq3Pld, EprUser.hrId == sq3Pld.c.hrId)\
                                .all()


    # print '\n==> inst %s with %s users\n' % (code, len(instUsers))

    if not sumApplicantDue: sumApplicantDue = 0.
    if not sumAuthorDue: sumAuthorDue = 0.

    instIsCoopOrAssoc = False
    if db.session.query(EprInstitute).filter_by(code = code).one().cmsStatus != 'Yes':
        sumApplicantDue = 0.
        sumAuthorDue = 0.
        instIsCoopOrAssoc = True

    summary = { 'check' : '',
                'Expected'  : 0.,
                'Pledged'   : 0.,
                'Accepted'  : 0.,
                'Done'      : 0.,
                'Tasks'     : [],
                'Authors'   : 0.,
                'ShiftsDone'   : 0.,
                'ShiftsPld'    : 0.,
                'EPRpledged'   : 0.,
                'EPRpldFrac'   : 0.,
                'EPRaccounted' : 0.,
                'EPRaccFrac'   : 0.,
                'sumEprDue' : sumApplicantDue,
                'sumAthDue' : sumAuthorDue,
                'actAuthors' : float(actAuthors),
                'EPRworkDone' : 0.,
                'Applicants' : 0.,
                'AppPld'     : 0.,
                'AppAcc'     : 0.,
                'AppDone'    : 0.,
                'AthPld'     : 0.,
                'AthAcc'     : 0.,
                'AthDone'    : 0.,
                }

    userInfo = []
    instApplicants = 0
    for entry in instUsers:
        user = entry[0]
        name = entry[0].name

        username = entry[0].username
        category = entry[1].name if entry[1] is not None else None
        # ignore users w/o a username/account:
        if not username: continue

        # Next line has no effect (summary['Expected'] is later overwritten and never read in between)
        # if not user.isSuspended and user.status != "CMSEMERITUS" : summary['Expected'] += 1.

        userSum = {'workPledged': entry.sumPledged,
                   'workAccepted': entry.sumAccepted,
                   'workDone': entry.sumWorkTimeDone,
                   'shiftsDone': entry.shiftsDone or 0.,
                   'shiftsPld' : entry.shiftsPld or 0.,
                   'project': entry.projects,
                   'activity': '',
                   'eprWorkDone': entry.eprWorkDone,
                   'eprWorkPledged' : entry.eprWorkPledged,
                   }

        for key in userSum.keys():
            if userSum[key] is None and any(x in key for x in ('Pledged', 'Done', 'Accepted', 'Accounted')):
                userSum[key] = .0

        # from iCMSEPR-39:
        # "According to Central Shift twiki page here
        # https://twiki.cern.ch/twiki/bin/viewauth/CMS/CentralShifts2016#Central_credit_accounting
        # the conversion from shift credits to EPR week is given by this formula
        # 5 shift credits = 1 EPR week"
        # so we convert the shifts "days" (weights) to EPR months
        userSum[ 'shiftsDone' ] = userSum[ 'shiftsDone' ] * getCSPtoEPR( year )
        userSum[ 'shiftsPld'  ] = userSum[ 'shiftsPld'  ] * getCSPtoEPR( year )

        summary['Pledged']  += float( userSum['workPledged'] )
        summary['Accepted'] += float( userSum['workAccepted'] )
        summary['Done']     += float( userSum['workDone'] )
        summary['ShiftsPld'] += float( userSum['shiftsPld'])
        summary[ 'ShiftsDone' ] += float( userSum[ 'shiftsDone' ] )

        # EPR accounted: sum of work done for person, where work for "nonCore" tasks is scaled with ).25
        eprAccounted = float(userSum['shiftsDone']) + float(userSum['eprWorkDone'])
        summary['EPRaccounted'] += float( eprAccounted )
        summary[ 'EPRworkDone' ] += float( userSum[ 'eprWorkDone' ] )
        # ... and similar for EPR pledged
        eprPledged = float(userSum['shiftsPld']) + float(userSum['eprWorkPledged'])
        summary['EPRpledged'] += float( eprPledged )

        userStatus = user.status
        if userStatus is None: userStatus = ''
        presentInst = None
        try:
            presentInst = db.session.query(EprInstitute).filter_by(id=user.mainInst).one()
        except orm.exc.NoResultFound:
            pass
        except Exception as e:
            current_app.logger.warning( "got Exception when checking for presentInst: %s" % str(e) )

        if presentInst is not None and presentInst.code != code:
            userStatus += '<br /><em>now at: %s</em>' % presentInst.code

        eprDue = entry.dueAuthor + (entry.dueApplicant if entry.lastCmsStatus != 'EXMEMBER' else 0)
        if instIsCoopOrAssoc : eprDue = 0.

        authOrApplicant = 'Auth' if user.authorReq else '-'
        if isApplicant( user.cmsId, year ):
            instApplicants += 1
            if authOrApplicant == '-': 
                authOrApplicant = 'Appl'
            else:
                authOrApplicant += ' & Appl'
            summary['AppPld'] += float( userSum['workPledged'] )
            summary['AppAcc'] += float( userSum['workAccepted'] )
            summary['AppDone'] += float( userSum['workDone'] ) + float(userSum['shiftsDone'])
        else:
            summary['AthPld'] += float( userSum['workPledged'] )
            summary['AthAcc'] += float( userSum['workAccepted'] )
            summary['AthDone'] += float( userSum['workDone'] ) + float(userSum['shiftsDone'])


        ratAccntDue = 1.
        if eprDue > 0.05: ratAccntDue = eprAccounted / eprDue
        userInfo.append ( { 'check' : '',
                            'project' : userSum['project'],
                            'mainProj' : entry.mainProj,
                            'activity' : userSum['activity'],
                            'username' : username,
                            'name'     : name,
                            'pledged' : userSum['workPledged'],
                            'accepted' : userSum['workAccepted'],
                            'done'    : userSum['workDone'],
                            # todo: dropped the separation between task shifts and non-task shifts. very bad?
                            'shiftsDone'  : userSum['shiftsDone'],
                            'shiftsPld'  : userSum['shiftsPld'],
                            'suspended' : entry.isSuspended,
                            'isAuthor': user.authorReq,
                            'eprDue' : eprDue,
                            'EPRpledged' : eprPledged,
                            'EPRaccounted' : eprAccounted,
                            'category' : category,
                            'status' : userStatus,
                            'cmsId' : user.cmsId,
                            'ratAccntDue' : ratAccntDue,
                            'authOrAppl'  : authOrApplicant,
                          } )

    summary['Expected'] = float( summary['actAuthors']) * float(getRequiredWork(year))
    summary['Applicants'] = instApplicants

    return selInst, userInfo, summary

def getShiftSummary( selYear, shiftTaskMapReq ) :

    currentShiftDay = date.today( )
    if currentShiftDay.year != selYear :
        currentShiftDay = date( selYear, 12, 31 )  # last day of selected year

    sq1 = QueryFactory.sq_time_line_user(year=selYear, inst_code=None, status='CMS%')

    sq3 = (db.session.query( Shift.userId.label( 'hrId' ),
                             func.sum( Shift.weight ).label( 'sumShiftWeight' ) )
                     .filter( Shift.year == selYear )
           )

    if selYear > 2015:
        sq3 = sq3.join(EprUser, EprUser.hrId == Shift.userId).join(sq1, EprUser.cmsId == sq1.c.cmsId)\
            .filter(and_(Shift.shiftStart > sq1.c.firststamp,
                         Shift.shiftStart < sq1.c.endDate))
    else:
        sq3 = sq3.filter(and_(Shift.shiftStart >= date(selYear, 1, 1), Shift.shiftEnd <= date(selYear, 12, 31)))

    if selYear > 2016: # use only mapped shifts for 2017 onwards:
        sq3 = sq3.filter( Shift.subSystem == shiftTaskMapReq.subSystem ) \
                 .filter( Shift.shiftType == shiftTaskMapReq.shiftType ) \
                 .filter( Shift.flavourName == shiftTaskMapReq.flavourName )

    sq3 = sq3.group_by(Shift.userId)

    # now split into shifts done and future (pledged) shifts:
    sq3Done = sq3.filter(Shift.shiftEnd     < date.today()).subquery()
    sq3Pld  = sq3.filter(Shift.shiftStart  >= date.today()).subquery()

    shiftSummary = (db.session.query( sq3Done.c.hrId,
                                      sq3Done.c.sumShiftWeight.label( "shiftsDone" ),
                                      sq3Pld.c.sumShiftWeight.label( "shiftsPld" )
                     )
                    .outerjoin(sq3Pld, sq3Done.c.hrId == sq3Pld.c.hrId)
                    .distinct( )
                    .all( ))

    #-todo: understand why this returns an empty list for any user if all their shifts are in the future

    cspDone = 0
    cspPld = 0
    for item in shiftSummary :
        cspDone += item.shiftsDone if item.shiftsDone else 0
        cspPld += item.shiftsPld if item.shiftsPld else 0
    nUser = len( shiftSummary )
    # current_app.logger.debug("Shift summary for shift %s/%s/%s: nUser %s, cspDone %s, cspPld %s " %
    #                          (shiftTaskMapReq.subSystem, shiftTaskMapReq.shiftType, shiftTaskMapReq.flavourName, nUser, cspDone, cspPld) )

    return nUser, cspDone, cspPld

def getShiftTaskSummary( taskCode, selYear ):

    # cspToEpr = (12./365.)
    cspToEpr = getCSPtoEPR( selYear ) # (1./5.)*(12./52.)

    sumNUser, sumCspDone, sumCspPld = 0,0,0
    stmList = None
    try :
        stmList = db.session.query(ShiftTaskMap).filter_by( year=selYear,
                                                taskCode=taskCode,
                                                status='ACTIVE' ).all( )
    except sa_exc.NoResultFound :
        current_app.logger.info( "getShiftTaskSummary> No shift-task-map found for taskCode %s in %s" % (taskCode, selYear) )
        return sumNUser, sumCspDone, sumCspPld

    # current_app.logger.debug( "getShiftTaskSummary> found %d shift-task-maps for %s in %s" % (len(stmList), taskCode, selYear))

    for stm in stmList :
        # stmInfo = { 'subSystem'   : stm.subSystem,
        #             'flavourName' : stm.flavourName,
        #             'shiftType'   : stm.shiftType,
        #             'id'          : stm.id,
        #             }

        nUser, cspDone, cspPld = getShiftSummary( selYear, stm )
        sumNUser   += nUser
        sumCspDone += cspDone
        sumCspPld  += cspPld
    # current_app.logger.debug( "getShiftTaskSummary> found %s shift-entities for task %s in %d" % (len(stmList), taskCode, selYear) )
    # current_app.logger.debug( "getShiftTaskSummary> sumNUser: %s, sumCspDone: %s, sumCspPld: %s" % (sumNUser, sumCspDone, sumCspPld))
    # current_app.logger.debug( "getShiftTaskSummary> stmList: %s " % str(stmList) )

    return sumNUser, sumCspDone*cspToEpr, sumCspPld*cspToEpr

def userCanManageTask(user, tCode, year) :
    return user and ( user.canManage('task', tCode, year) or user.is_administrator() )

#-toDo: make these "service aware"
editTaskTmpl = '<a href="%s"> <img src=/static/editIcon.png alt="edit task" height="21" width="21"/> </a>'
editPlTmpl   = '<a href="%s"> <img src=/static/editIcon.png alt="edit pledge" height="21" width="21"/> </a>'
splitPlTmpl  = '<br /><a href="%s"> split pledge </a>'
rejectPlTmpl = '<a href="%s"> <img src=/static/rejIcon.png alt="reject pledge" height="21" width="21"/> </a>'

def updateTaskMap( mapInstance, aName, tName, mapIndex, valObj, verbose=False ):

    if aName not in mapInstance:
        if verbose: current_app.logger.info( "adding aName %s " % aName )
        mapInstance[aName] = {}
    if tName not in mapInstance[aName]:
        if verbose: current_app.logger.info( 'adding tName %s for %s ' % (tName, aName) )
        mapInstance[aName][tName] = {}

    if mapIndex not in mapInstance[aName][tName].keys():
        if verbose: current_app.logger.info( "new entry for %s " %  mapIndex )
        mapInstance[aName][tName][mapIndex] = [ float(x) for x in valObj ] + [aName, tName]
    else:
        if verbose: current_app.logger.info( "updating %s " %  mapIndex )
        for i,v in enumerate(valObj):
            mapInstance[aName][tName][mapIndex][i] += float(v)

    return mapInstance

def updateActMap( mapInstance, aName, mapIndex, valObj, verbose=False ):

    if aName not in mapInstance:
        if verbose: current_app.logger.info( "adding aName %s " %  aName )
        mapInstance[aName] = {}

    if mapIndex not in mapInstance[aName].keys():
        if verbose: current_app.logger.info( "new entry for %s " %  mapIndex )
        mapInstance[aName][mapIndex] = [float(x) for x in valObj] + [aName]
    else:
        if verbose: current_app.logger.info( "updating %s " %  mapIndex )
        for i,v in enumerate(valObj):
            mapInstance[aName][mapIndex][i] += float(v)

    return mapInstance

def getProjectInstCountryInfo(projId, year):

    result = []

    try:
        projReq = db.session.query(Project).filter_by(id=projId).one()
        if projReq.year != year:
            proj = db.session.query(Project).filter_by(name=projReq.name, year=year).one()
            projReq = proj
    except Exception as e:
        if "no row was found for" in str(e).lower():
            msg = "No project with name %s found for %i. " % (projReq.name, year)
            current_app.logger.error( "getProjectInstCountryInfo> "+msg )
            raise HelperError(msg)

    try:
        result = (db.session.query( Pledge, EprInstitute.country, EprInstitute.code, Task.name, CMSActivity.name )
                        .filter_by(year=year)
                        .filter( Pledge.status != 'rejected')
                        .join( Task, Task.id == Pledge.taskId)
                        .join( CMSActivity, CMSActivity.id == Task.activityId )
                        .join( Project, Project.id == CMSActivity.projectId )
                        .join( EprInstitute, EprInstitute.id == Pledge.instId )
                        .filter(Project.id == projReq.id)
                    ).order_by(EprInstitute.code).all()
    except Exception as e:
        msg = "ERROR when retrieving info for projId %i for %i, got %s " % (projId, year, str(e))
        current_app.logger.error( "getProjectInstCountryInfo> "+msg )
        raise HelperError(msg)

    current_app.logger.info( "getProjectInstCountryInfo> got %i entries for pId %i, year %i" % (len(result), projId, year) )

    projList, instList, authInstMap = getInstAuthForProj( projReq.name, year )
    current_app.logger.info( "getProjectInstCountryInfo> got %i entries for pId %i, year %i" % (len(result), projId, year) )

    projInstMap = {}
    projCountryMap = {}
    projActInstMap = {}
    projActCountryMap = {}
    aNames = set()
    tNames = {}
    instCodes = set()
    countryNames = set()
    instSums    = {}
    countrySums = {}
    authInstRatMap = {}
    nAuthInst = {}
    authExpInst = {}
    for (p, country, iCode, tName, aName) in result:
        aNames.add( aName )
        if aName not in tNames.keys(): tNames[aName] = set()
        tNames[aName].add( tName )
        instCodes.add( iCode )
        countryNames.add( country )

        # ensure there is no "None" in the values:
        if p.workTimePld  is None: p.workTimePld  = 0.
        if p.workTimeAcc  is None: p.workTimeAcc  = 0.
        if p.workTimeDone is None: p.workTimeDone = 0.

        updateTaskMap( projInstMap, aName, tName, iCode, [ p.workTimePld, p.workTimeAcc, p.workTimeDone ] )
        updateTaskMap( projCountryMap, aName, tName, country, [ p.workTimePld, p.workTimeAcc, p.workTimeDone ] )

        updateActMap( projActInstMap, aName, iCode, [ p.workTimePld, p.workTimeAcc, p.workTimeDone ] )
        updateActMap( projActCountryMap, aName, country, [ p.workTimePld, p.workTimeAcc, p.workTimeDone ] )

        if iCode not in instSums.keys():
            instSums[iCode] = [ 0., 0., 0. ]
        instSums[iCode][0] += float( p.workTimePld )
        instSums[iCode][1] += float( p.workTimeAcc )
        instSums[iCode][2] += float( p.workTimeDone )

        if country not in countrySums.keys():
            countrySums[country] = [ 0., 0., 0. ]
        countrySums[country][0] += float( p.workTimePld )
        countrySums[country][1] += float( p.workTimeAcc )
        countrySums[country][2] += float( p.workTimeDone )

        if iCode not in authInstRatMap : authInstRatMap[iCode] = { }
        # print 'projName: %s, aim[pname]: %s' % (projReq.name, authInstMap[projReq.name])
        authMonths = 0.
        if authInstMap and projReq.name in authInstMap.keys() and iCode in authInstMap[projReq.name]:
            authMonths = ( getRequiredWork(year) * float( authInstMap[projReq.name][iCode] ) )

        if projReq.name not in authExpInst: authExpInst[projReq.name] = {}
        authExpInst[projReq.name][iCode] = authMonths
        if projReq.name not in nAuthInst: nAuthInst[projReq.name] = {}
        nAuthInst[projReq.name][iCode] = authMonths/getRequiredWork(year)

        for k in range(3):
            authInstRatMap[iCode][k] = ( instSums[iCode][k] / authMonths ) if (authMonths > 0.) else 0.

    data = { 'projInstMap' : projInstMap,
             'projCountryMap' : projCountryMap ,
             'aNames' : aNames ,
             'tNames' : tNames ,
             'instCodes' : sorted(instCodes) ,
             'countryNames' : sorted(countryNames) ,
             'projReq' : projReq ,
             'instSums' : instSums ,
             'countrySums' : countrySums ,
             'projActInstMap' : projActInstMap ,
             'projActCountryMap' : projActCountryMap ,
             'authInstRatMap' : authInstRatMap,
             'nAuthInst' : nAuthInst,
             'authExpInst' : authExpInst,
           }

    return data

def getProjectInfo(proj=-1, user=None, allUsers=True, year=2015, individualIRTPledges=False):

    aM, pM, tM, iM = getManagers( year )

    start = time.time()
    query = (db.session.query( Pledge, Task, CMSActivity, Project )
                    .join( Task, Task.id == Pledge.taskId )
                    .join( CMSActivity, CMSActivity.id == Task.activityId )
                    .join( Project, Project.id == CMSActivity.projectId )
                    .filter( Pledge.year == year )
                    .filter( Pledge.status != 'rejected' )
                    .filter( Task.status == 'ACTIVE' )                    
            )
    if not individualIRTPledges:
        query = query.filter( Task.tType != 'InstResp' )

    if proj != -1:
        query = query.filter(Project.id == proj)

    allItems = query.all()

    current_app.logger.info( "getProjectInfo> query done in %s sec., found %i rows." % (time.time()-start, len(allItems)) )

    projItems = []
    start = time.time()
    for i in allItems:

        (pl, t, a, p) = i

        # prepare a few things:
        aName = t.activity.name
        if not aName: aName = 'unknown'
        if  aName[0] == '[':
            actName = aName[ aName.find(']')+1: ]
        else:
            actName = aName

        l3Name = '-'
        l3id = t.level3
        if l3id and l3id > 0: 
            l3Name = db.session.query(Level3Name.name).filter(Level3Name.id==l3id).one()

        tName = t.name
        if not tName or tName is None:
            current_app.logger.error('getProjectInfo> Got task w/o name: id %s, code %s, activity %s' % (t.id, t.code, t.activity) )
            tName = 'noName'
        if  tName[0] == '[':
            tName = t.name.split(']',2)[2]

        tName += '<br />(<a href="%s">task</a>)' % url_for('main.showTask', code=str(t.code))

        # see if the user can manage this item
        userCanManage = current_user.is_administrator() or (t.code in tM or t.activity.code in aM or p.code in pM)

        # current_app.logger.debug("getProjectInfo> item: %s %s %s %s : canManage: %s " % (pl, t, a, p, userCanManage) )

        editTask = ''
        # if ( (t.tType and 'shift' not in t.tType.lower()) and userCanManage ):
        if ( userCanManage ): # allow to edit shift tasks ...
            editTask = editTaskTmpl % ( url_for('manage.task', code='%s' % str(t.code) ), )

        editPl = ''
        # if ( (pl.status == 'new' and pl.userId == current_user.id) or userCanManage ):
        #     editPl = editPlTmpl % ( url_for('main.pledge', id='%s' % str(pl.id) ), )
        # elif ( (pl.status == 'accepted' and pl.userId == current_user.id) or userCanManage ):
        #     # allow updating the work already done for the the pledge by the user who created it if it is already accepted
        #     editPl = editPlTmpl % ( url_for('main.updateWorkDone', id='%s' % str(pl.id) ), )

        try:
            plInst = db.session.query(EprInstitute).filter_by(id=pl.instId).one()
        except orm.exc.NoResultFound:
            msg = 'Could not find an institute for pledge id %s, instId %s' % (pl.id, pl.instId)
            msg += 'replacing with dummy institute'
            current_app.logger.error(msg)
            plInst = db.session.query(EprInstitute).filter_by(code='ZZZ').one()

        plUser = db.session.query(EprUser).filter_by(id=pl.userId).one()
        # allow editing/rejecting only for pledges for non-InstResp tasks
        if t.tType != 'InstResp':
            # allow editing the pledge by the user who created it only if it is not yet accepted (or done)
            if ( (pl.status == 'new' and pl.userId == current_user.id) or userCanManage or plUser.canBeManagedBy(current_user, year) ):
                editPl = editPlTmpl % ( url_for('main.pledge', id='%s' % str(pl.id) ), )
            elif ( pl.status == 'accepted' and (pl.userId == current_user.id or userCanManage or plUser.canBeManagedBy(current_user, year) ) ):
                # allow updating the work already done for the the pledge by the user who created it if it is already accepted
                editPl = editPlTmpl % ( url_for('main.updateWorkDone', id='%s' % str(pl.id) ), )
            if ( current_user.is_administrator() or plUser.canBeManagedBy(current_user, year) ) :
                editPl += splitPlTmpl % ( url_for('main.splitPledge', plCode='%s' % str(pl.code) ) )

            if  ( (pl.status == 'new' and pl.userId == current_user.id) 
                or userCanManage or plUser.canBeManagedBy(current_user, year) ):
                rejectPl = rejectPlTmpl % ( url_for('main.rejectPledge', id='%s' % str(pl.id)), )
            # current_app.logger.info( "plUser %s .canBeManagedBy(%s, %s) : %s " % (plUser.username, current_user.username, year, plUser.canBeManagedBy(current_user, year)) )
        else:
            if ( current_user.is_administrator() or current_user.isTeamLeader( plInst.code, year) ):
                editPl = editPlTmpl % ( url_for('main.editInstRespPledge', taskId='%s' % str(t.id), instCode='%s' % plInst.code ), )


        rejectPl = ''
        if  userCanManage :
            rejectPl = rejectPlTmpl % ( url_for('main.rejectPledge', id='%s' % str(pl.id)), )

        u = db.session.query(EprUser).filter_by(id=pl.userId).one()
        try:
            inst = db.session.query(EprInstitute).filter_by(id=u.mainInst).one()
        except orm.exc.NoResultFound:
            msg = 'Could not find an institute for user %s (%s %s), mainInst %s' % (u.username, u.name, u.cmsId, u.mainInst)
            msg += 'replacing with dummy institute'
            current_app.logger.error(msg)
            inst = db.session.query(EprInstitute).filter_by(code='ZZZ').one()

        plInfo = '%i:%i:%i' % (pl.taskId, pl.userId, pl.instId)

        plDoneFract = 0.
        # add a protection to prevent crash/http-500:
        if pl.workTimeDone is None:
            pl.workTimeDone = 0.0
            current_app.logger.error("found None for workTimeDone in pledge: %s - reset to 0.0" % pl.id )

        if pl.workTimeDone is not None:
            if pl.workTimePld > 0: plDoneFract = pl.workTimeDone/pl.workTimePld
        else:
            current_app.logger.error("found None for workTimeDone in pledge: %s " % pl.id )

        row = [ '', p.name, actName, l3Name,
                tName, t.neededWork, getTaskPctDone(t),
                t.pctAtCERN,
                t.tType if (int( year ) != 2016) else '%s<br />(%s)' % (t.tType, t.kind),
                t.comment,
                editTask,
                "%s<br />(%s)" % (u.name, inst.code),
                plInst.code,
                pl.workTimePld, pl.workTimeAcc, pl.workTimeDone, plDoneFract,
                pl.status, pl.year,
                editPl, rejectPl,
                plInfo, t.name,
                p.code, a.code, t.code
                ]
        projItems.append( row )

    current_app.logger.info( "getProjectInfo> %i projItems prepared in %s sec " % (len(projItems),  time.time()-start) )

    return projItems

def getManagers( year ) :
    # first check direct/personal manager info:
    managerOf = db.session.query(Manager).filter_by( userId=current_user.id, year=year ).filter( Manager.status.in_( ['active', 'allowed']) ).all( )

    # now check indirect (via eGroup) manager info:
    eGroupList = [ x.replace('@cern.ch', '') for x in getEgroupsForUser(current_user.username) ]
    managerOf += (db.session.query(Manager).filter( Manager.egroup.in_(eGroupList) )
                               .filter( Manager.status.in_( ['active', 'allowed']) )
                               .filter( Manager.year==year )
                               .all( ) )

    pM = [ m.itemCode for m in managerOf if (m.itemType == 'project'  and m.role == Permission.MANAGEPROJ) ]
    aM = [ m.itemCode for m in managerOf if (m.itemType == 'activity' and m.role == Permission.MANAGEACT) ]
    tM = [ m.itemCode for m in managerOf if (m.itemType == 'task'     and m.role == Permission.MANAGETASK) ]

    iM = [ m.itemCode for m in managerOf if (m.itemType == 'institute' and m.role == Permission.MANAGEINST) ]

    if len(pM+aM+tM) > 0 :
        current_app.logger.info( 'getManagers> user %s is manager of: %i proj, %i act, %i tasks in %s' % (current_user.username, len(pM), len(aM), len(tM), year) )
        # if len(aM)>0: current_app.logger.debug('+++>>> aM: %s ' % ', '.join( [ str(x) for x in aM ] ) )
    else:
        current_app.logger.info( 'getManagers> user %s is NOT manager of a proj, act, or task in %s' % (current_user.username, year) )

    return aM, pM, tM, iM


def getGuestPledgeInstSummary(instCode, year):

    # get all guest pledges for this institute
    inst = db.session.query(EprInstitute).filter_by(code=instCode).one()
    pledges = db.session.query(Pledge).filter_by(instId=inst.id, year=year)\
                          .filter(Pledge.status != 'rejected')\
                          .filter(Pledge.isGuest == True)\
                          .all()

    if not pledges:
        return None

    try:
        current_app.logger.info( "found %i guest pledges for %s [:5]: %s" % (len(pledges), inst.code, str( [str(x) for x in pledges[:5] ]) ) )
    except UnicodeEncodeError:
        pass

    summary = { 'Pledged'  : 0.,
                'Accepted' : 0.,
                'Done'     : 0.,
                }

    for p in pledges:
        summary[ 'Pledged'  ] += p.workTimePld
        summary[ 'Accepted' ] += p.workTimeAcc
        summary[ 'Done'     ] += p.workTimeDone

    return summary

def getGuestPledgeInstInfo(instCode, year):

    # get all guest pledges for this institute
    inst = db.session.query(EprInstitute).filter_by(code=instCode).one()
    if inst is None:
        # try again with code converted to upper case, in case of user mistyping:
        instCode = instCode.upper()
        inst = db.session.query(EprInstitute).filter_by( code=instCode ).one( )
    pledges = db.session.query(Pledge).filter_by(instId=inst.id, year=year)\
                          .filter(Pledge.status != 'rejected')\
                          .filter(Pledge.isGuest == True)\
                          .all()

    try:
        current_app.logger.info( "found %i guest pledges for %s [:5]: %s" % (len(pledges), inst.code, str(pledges[:5])) )
    except UnicodeEncodeError:
        pass

    if not pledges:
        return None

    return formatPledges(pledges, None, inst, year, guestPledge=True)

def getPledgeInstInfo(instCode, year):

    # get all pledges for this institute
    inst = None
    try:
        inst = db.session.query(EprInstitute).filter_by(code=instCode.upper()).one()
    except orm.exc.NoResultFound:
        current_app.logger.warning("getPledgeInstInfo> no institute found for %s in %s" %(instCode, year) )
        return formatPledges( [], None, inst, year)

    pledges = db.session.query(Pledge).filter_by(instId=inst.id, year=year)\
                          .filter(Pledge.status != 'rejected')\
                          .all()
    try:
        current_app.logger.info( "found %i pledges for %s " % (len(pledges), inst.code) )
    except UnicodeEncodeError:
        pass

    pledgeList = []
    if not pledges:
        return pledgeList

    return formatPledges(pledges, None, inst, year)

def getPledgeInfoByTaskIDs(taskIDs):

    pledges = (db.session.query(Pledge, EprUser, EprInstitute)
                         .join(EprUser, EprUser.id == Pledge.userId)
                         .join(EprInstitute, EprInstitute.id == Pledge.instId)
                         .filter( Pledge.taskId.in_( taskIDs))\
                         .filter( Pledge.status != 'rejected')\
                         .all())

    pledgeList = []
    if not pledges:
        current_app.logger.info( "found NO pledges for %d taskIDs [%s]" % (len(taskIDs), str(taskIDs)) )
        return pledgeList

    current_app.logger.info( "found %i pledges for %d taskIDs in %s" % (len(pledges), len(taskIDs), pledges[0][0].year ) )

    # return formatPledges(pledges, None, None, pledges[0].year, allowIRTasks=True)

    data = []
    for p, u, i in pledges:
        if p.workTimePld is None:
            p.workTimePld = 0.
            current_app.logger.warning( "getPledgeInfoByTaskIDs> resetting workTimePld to zero for pledge id/code %s/%s" % (p.id, p.code) )
        if p.workTimeAcc is None:
            p.workTimeAcc = 0.
            current_app.logger.warning( "getPledgeInfoByTaskIDs> resetting workTimeAcc to zero for pledge id/code %s/%s" % (p.id, p.code) )
        if p.workTimeDone is None:
            p.workTimeDone = 0.
            current_app.logger.warning( "getPledgeInfoByTaskIDs> resetting workTimeDone to zero for pledge id/code %s/%s" % (p.id, p.code) )

        ratio = -1.
        if p.workTimePld > 0:
            ratio = p.workTimeDone / p.workTimePld
        
        edit = ''
        reject = ''
        data.append( ['', u.name, i.code, p.workTimePld, p.workTimeAcc, p.workTimeDone, ratio, p.status,  p.year, edit, reject, p.code ] )

    return data

def getPledgeInfoByPledgeCodes(plCodes):

    pledges = db.session.query(Pledge).filter( Pledge.code.in_( plCodes ))\
                          .filter( Pledge.status != 'rejected')\
                          .all()

    pledgeList = []
    if not pledges:
        current_app.logger.info( "found NO pledges for %d plIDs [%s]" % (len(plCodes), str(plCodes)) )
        return pledgeList

    try:
        current_app.logger.info( "found %i pledges for %d plIDs in %s" % (len(pledges), len(plCodes), pledges[0].year ) )
    except UnicodeEncodeError:
        pass

    return formatPledges(pledges, None, None, pledges[0].year)

def getPledgeInfo(username, year):

    if username.lower() == 'all':
        selUser = None
        pledges = db.session.query(Pledge).filter_by(year=year).filter(Pledge.status != 'rejected').all()
    else:
        # get all pledges for this user
        selUser = db.session.query(EprUser).filter_by(username=username).one()
        pledges = db.session.query(Pledge)\
                        .filter(Pledge.userId==selUser.id)\
                        .filter(Pledge.status != 'rejected')\
                        .filter(Pledge.year==year)\
                        .all()
        try:
            current_app.logger.info( "Helpers::getPledgeInfo> found %i pledges for %s " % (len(pledges), selUser.username) )
        except UnicodeEncodeError:
            current_app.logger.info( "Helpers::getPledgeInfo> unicode error for cmsid %s " % selUser.cmsId )

    pledgeList = []
    if not pledges:
        return pledgeList

    return formatPledges(pledges, selUser, None, year)

def formatPledges(pledges, selUser, selInst, year, guestPledge=False, allowIRTasks=False):

    aM, pM, tM, iM = getManagers( year )

    pledgeList = []
    for pl in pledges:

        t = db.session.query(Task).filter_by(id=pl.taskId).one()
        
        #-ap: new policy: show them but colour them ... 
        # ignore these, as they have a different formatting ... 
        # if t.tType == 'InstResp' : continue 

        aName = t.activity.name
        if  aName[0] == '[':
            actName = aName[ aName.find(']')+1: ]
        else:
            actName = aName

        l3Name = '-'
        l3id = t.level3
        if l3id and l3id > 0: 
            l3Name = db.session.query(Level3Name.name).filter(Level3Name.id==l3id).one()

        tName = t.name
        if not tName or tName is None:
            current_app.logger.error('formatPledges> Got task w/o name: id %s, code %s, activity %s' % (t.id, t.code, t.activity) )
            tName = 'noName'
        if  tName[0] == '[':
            tName = t.name.split(']',2)[2]

        tName += '<br />(<a href="%s">task</a>)' % url_for('main.showTask', code=str(t.code))

        p = t.activity.project

        if not selUser:
            plUser = db.session.query(EprUser).filter_by(id=pl.userId).one()
        else:
            plUser = selUser

        if not selInst:
            userInst = db.session.query(EprInstitute).filter_by(id=plUser.mainInst).one()
            plInst   = db.session.query(EprInstitute).filter_by(id=pl.instId).one()
        else:
            userInst = selInst
            plInst   = selInst

        userInst = db.session.query(EprInstitute).filter_by(id=plUser.mainInst).one()

        # current_app.logger.info(  "formatPledges> '%s'.canManageUser('%s'): %s" % (current_user.username, plEprUser.username, current_user.canManageUser(plUser, year)) )

        # see if the user can manage this item
        userCanManage = current_user.is_administrator() or \
                        (t.code in tM or t.activity.code in aM or p.code in pM)

        editTask = ''
        # if ( (t.tType and 'shift' not in t.tType.lower()) and userCanManage ):
        if ( userCanManage ): # allow to edit shift tasks ...
            # editTask = editTaskTmpl % ( url_for('main.task', id='%s' % str(t.id) ), )
            editTask = editTaskTmpl % ( url_for('manage.task', code='%s' % str(t.code) ), )

        # allow editing the pledge by the user who created it only if it is not yet accepted (or done)
        editPl = ''
        rejectPl = ''
        # allow editing/rejecting only for pledges for non-InstResp tasks
        if t.tType != 'InstResp':
            if ( (pl.status == 'new' and pl.userId == current_user.id) or userCanManage or plUser.canBeManagedBy(current_user, year) ):
                editPl = editPlTmpl % ( url_for('main.pledge', id='%s' % str(pl.id) ), )
            elif ( pl.status == 'accepted' and (pl.userId == current_user.id or userCanManage or plUser.canBeManagedBy(current_user, year) ) ):
                # allow updating the work already done for the the pledge by the user who created it if it is already accepted
                editPl = editPlTmpl % ( url_for('main.updateWorkDone', id='%s' % str(pl.id) ), )
            if ( current_user.is_administrator() or plUser.canBeManagedBy(current_user, year) ) :
                editPl += splitPlTmpl % ( url_for('main.splitPledge', plCode='%s' % str(pl.code) ) )

            if  ( (pl.status == 'new' and pl.userId == current_user.id) 
                or userCanManage or plUser.canBeManagedBy(current_user, year) ):
                rejectPl = rejectPlTmpl % ( url_for('main.rejectPledge', id='%s' % str(pl.id)), )
            # current_app.logger.info( "plUser %s .canBeManagedBy(%s, %s) : %s " % (plUser.username, current_user.username, year, plUser.canBeManagedBy(current_user, year)) )
        else:
            if ( current_user.is_administrator() or current_user.isTeamLeader( plInst.code, year) ):
                editPl = editPlTmpl % ( url_for('main.editInstRespPledge', taskId='%s' % str(t.id), instCode='%s' % plInst.code ), )

        plInfo = '%i:%i:%i' % (pl.taskId, pl.userId, pl.instId)
        if pl.workTimePld is None:
            pl.workTimePld = 0.
            current_app.logger.warning( "resetting workTimePld to zero for pledge id/code %s/%s" % (pl.id, pl.code) )
        if pl.workTimeAcc is None:
            pl.workTimeAcc = 0.
            current_app.logger.warning( "resetting workTimeAcc to zero for pledge id/code %s/%s" % (pl.id, pl.code) )
        if pl.workTimeDone is None:
            pl.workTimeDone = 0.
            current_app.logger.warning( "resetting workTimeDone to zero for pledge id/code %s/%s" % (pl.id, pl.code) )

        plDoneFract = 0.
        if pl.workTimePld > 0: plDoneFract = pl.workTimeDone/pl.workTimePld
        try:
            if guestPledge:
                row = [ '', p.name, actName+'<div id="activity-desc"></div>', l3Name,
                    tName, t.neededWork, getTaskPctDone(t),
                    t.pctAtCERN,
                        t.tType if (int(year)!=2016) else '%s<br />(%s)' % (t.tType, t.kind),
                        t.comment,
                    "%s<br />(%s)" % (plUser.name, userInst.code),
                    plInst.code,
                    pl.workTimePld, pl.workTimeAcc, pl.workTimeDone, plDoneFract,
                    pl.status,
                    plInfo, t.name,
                    p.code, t.activity.code, t.code ]
            else:
                row = [ '', p.name, actName + '<div id="activity-desc"></div>', l3Name,
                        tName, t.neededWork, getTaskPctDone( t ),
                        t.pctAtCERN,
                        t.tType if (int(year)!=2016) else '%s<br />(%s)' % (t.tType, t.kind),
                        t.comment,
                        editTask,
                        "%s<br />(%s)" % (plUser.name, userInst.code),
                        plInst.code,
                        pl.workTimePld, pl.workTimeAcc, pl.workTimeDone, plDoneFract,
                        pl.status, pl.year,
                        editPl, rejectPl,
                        plInfo, t.name,
                        p.code, t.activity.code, t.code ]
            # current_app.logger.info(  "\n++> guestPledge: %s - row : %s \n" % (guestPledge, str(row)) )

            pledgeList.append( row )
        except AttributeError as e:
            msg = "ERROR: AttributeError when formatting pledge for %s - got: %s " % (pl, str(e))
            msg += "\n  ... project :", p
            msg += "\n  ... activity:", t.activity
            msg += "\n  ... task    :", t
            msg += "\n  ... user    :", plUser
            current_app.logger.error( msg )
        except Exception as e0:
            msg = "ERROR: Other Exception formatting pledge for %s - got: %s " % (pl, str(e0))
            msg += "\n  ... project :", p
            msg += "\n  ... activity:", t.activity
            msg += "\n  ... task    :", t
            msg += "\n  ... user    :", plUser
            current_app.logger.error( msg )

    current_app.logger.debug(
        'formatPledges> formatted %d pledges (selUser: %s, selInst: %s, year: %s, guestPledge: %s)' %
        (len(pledgeList), selUser.username if selUser else 'n/a', selInst.code if selInst else 'n/a', year, guestPledge )
    )

    return pledgeList

def countrySummary( name, year ) :
    keys = [ 'Expected', 'Pledged', 'Accepted', 'Done', 'Authors', 'actAuthors', 'ShiftsDone', 'ShiftsPld', 'sumEprDue' ]
    summary = { 'nInst': 0,
                'Expected' : 0, 'Pledged' : 0, 'Accepted' : 0, 'Done' : 0,
                'Authors' : 0, 'actAuthors' : 0, 'ShiftsDone' : 0, 'ShiftsPld' : 0,
                'sumEprDue' : 0 }
    countryInstInfo = [ ]
    for a, i in (db.session.query( AllInstView, EprInstitute )
                         .filter( EprInstitute.country == name )
                         .filter( EprInstitute.cernId  > 0)
                         .filter( AllInstView.code == EprInstitute.code )
                         .filter( AllInstView.year == year )
                         .all( )) :

        if not summary :
            summary[ 'nInst' ] = 1
            instInfo = a.to_json( )
            for k in instInfo.keys( ) :
                summary[ k ] = instInfo[ k ]
        else :
            summary[ 'nInst' ] += 1
            instInfo = a.to_json( )
            for k in keys :
                summary[ k ] += instInfo[ k ]
        summary[ 'name' ] = name
        summary[ 'code' ] = ''

        countryInstInfo.append( a.to_json( ) )

    current_app.logger.debug( "\ncountrySummary> %s %s : \n %s \n" % (name, str(year), str(summary)) )
    current_app.logger.debug( "\ncountryInstInf> %s %s : \n %s \n" % (name, str(year), str(countryInstInfo)) )

    summary[ 'PledgedFract' ] = 0
    if float( summary[ 'Expected' ]+summary['sumEprDue'] ) > 0 :
        summary[ 'PledgedFract' ] = float( summary[ 'Pledged' ] ) / float( summary[ 'Expected' ]+summary['sumEprDue'] )
    summary[ 'DoneFract' ] = 0
    if float( summary[ 'Expected' ]+summary['sumEprDue'] ) > 0 :
        summary[ 'DoneFract' ] = float( summary[ 'Done' ] ) / float( summary[ 'Expected' ]+summary['sumEprDue'] )
    return countryInstInfo, summary


def getShiftInfo( subSys, year) :

    return shiftInfo(subSys=subSys, username=None, inst=None, year=year)

def getShiftInfoUser( username, year) :

    return shiftInfo(subSys=None, username=username, inst=None, year=year)

def getShiftWeightScales(year):

    stmList = ( db.query( ShiftTaskMap )
                  .filter(ShiftTaskMap.year == year)
                  .filter(ShiftTaskMap.status == 'ACTIVE')
                  .all() )

    if len(stmList) == 0: return None

    res = { }
    for stm in stmList:
        key = ('%s|%s|%s') % (stm.subSystem, stm.shiftType, stm.flavourName)
        res[key] = (stm.weight, stm.addedValueCredits)

    return res

def shiftInfo(subSys='all', username=None, inst=None, year=None):

    if year is None: year = getPledgeYear()

    weightScales = getShiftWeightScales( year )

    subQuery = db.session.query( Shift, EprUser, EprInstitute )\
                            .join( EprUser, EprUser.hrId == Shift.userId )\
                            .join( EprInstitute, EprUser.mainInst == EprInstitute.id ) \
                            .filter( EprUser.mainInst == EprInstitute.id )\
                            .filter( Shift.year == year )

    if subSys is not None and subSys.strip( ).lower( ) != 'all' :
        subQuery = subQuery.filter(Shift.subSystem == subSys )

    if username is not None:
        subQuery = subQuery.filter( EprUser.username == username ) \
                           .filter( Shift.userId == EprUser.hrId )

    shifts = []
    if year > 2016 and username is not None:
        stmList = db.session.query(ShiftTaskMap).filter_by(year=year, status='ACTIVE').all()
        for stm in stmList:
            subQuery0 = subQuery.filter( Shift.subSystem == stm.subSystem ) \
                                .filter( Shift.shiftType == stm.shiftType ) \
                                .filter( Shift.flavourName == stm.flavourName )
            shifts += subQuery0.all()
        # print "querying for User ", username, year
        # print 'got: %d shifts: %s' % (len(shifts), str(shifts))

    else:
        shifts = subQuery.all( )

    current_app.logger.debug( "shiftInfo> found %i shifts for user %s" % (len( shifts ), username ) )

    data = [ ]
    keys = set( )
    timeSumDone = { }
    nShiftsDone = { }
    sumWeights = { }
    timeSumPld = { }
    nShiftsPld = { }
    sumWeightsPld = { }
    institutes = []
    for s, u, inst in shifts :
        key = (s.userId, s.subSystem, s.shiftType, s.flavourName, inst.code)
        wKey = ('%s|%s|%s') % (s.subSystem, s.shiftType, s.flavourName)

        institutes.append( inst.code )

        shiftWeight = s.weight if s.weight is not None else 0.
        if weightScales and wKey in weightScales:
            shiftWeight = shiftWeight * weightScales[wKey][0] + weightScales[wKey][1]

        # if a key shows up for the first time, initialise the maps and data store
        if key not in keys :
            keys.add( key )
            data.append( { 'check'       : '',
                           'shifter'     : u.name,
                           'institute'   : inst.code,
                           'userId'      : s.userId,
                           'subSystem'   : s.subSystem,
                           'shiftType'   : s.shiftType,
                           'shiftTypeId' : s.shiftTypeId,
                           'flavour'     : s.flavourName,
                           'weight'      : shiftWeight,
                           } )
            timeSumDone[ key ] = 0.
            nShiftsDone[ key ] = 0
            sumWeights[ key ] = 0.
            timeSumPld[ key ] = 0.
            nShiftsPld[ key ] = 0
            sumWeightsPld[ key ] = 0.

        # make sure we also count/update the first shifts
        if s.shiftStart > datetime.today( ) :
            timeSumPld[ key ] += float( (s.shiftEnd - s.shiftStart).total_seconds( ) ) / 86400.  # convert to days
            try:
                sumWeightsPld[ key ] += float( shiftWeight )
            except Exception as e:
                current_app.logger.warning("Helpers::shiftInfo> strange shift-weight of type %s found: %s(%s) - got: %s " % (type(s.weight), shiftWeight, s.weight, str(e)) )
            nShiftsPld[ key ] += 1
        else :
            timeSumDone[ key ] += float( (s.shiftEnd - s.shiftStart).total_seconds( ) ) / 86400.  # convert to days
            sumWeights[ key ] += float( shiftWeight )
            nShiftsDone[ key ] += 1

    for d in data :
        key = (d[ 'userId' ], d[ 'subSystem' ], d[ 'shiftType' ], d[ 'flavour' ], d['institute'])
        d.update( { 'shiftTimeDone' : timeSumDone[ key ],
                    'nShiftsDone' : nShiftsDone[ key ],
                    'sumWeights'    : sumWeights[ key ],
                    'sumWeightsPld' : sumWeightsPld[ key ],
                    'shiftTimePld'  : timeSumPld[ key ],
                    'nShiftsPld' : nShiftsPld[ key ],
                    } )

    current_app.logger.debug( "shiftInfo> returning %i shifts for %i institutes" % (len( shifts ), len(institutes)) )

    return data, institutes

def getAPTManagers( act, proj, task, year=None ) :

    if year is None: year = getPledgeYear()

    current_app.logger.debug( "getAPTManagers0> proj %s, act %s, task %s in %d" % (proj, act, task, year) )

    mgrs = { 'project' : [ ], 'activity' : [ ], 'task' : [ ] }
    mgrsFull = { 'project' : [ ], 'activity' : [ ], 'task' : [ ] }
    if proj :
        iType, iCode = ('project', proj)
        for m, uName in (db.session.query( Manager, EprUser.name )
                                 .filter( Manager.itemType == iType )
                                 .filter( Manager.itemCode == iCode )
                                 .filter( Manager.userId == EprUser.id )
                                 .filter( Manager.role == permsMap[iType] )
                                 .filter( Manager.year == year )
                                 .filter( Manager.status == 'active' )
                                 .all()) :
            if m.egroup and m.egroup.strip() != '' :
                mgrs[ 'project' ].append( m.egroup.strip() )
            else:
                mgrs[ 'project' ].append( uName )
            mgrsFull[ 'project' ].append( m.to_json() )
    if act :
        iType, iCode = ('activity', act)
        for m, uName in (db.session.query( Manager, EprUser.name )
                                 .filter( Manager.itemType == iType )
                                 .filter( Manager.itemCode == iCode )
                                 .filter( Manager.userId == EprUser.id )
                                 .filter( Manager.role == permsMap[iType] )
                                 .filter( Manager.year == year )
                                 .filter( Manager.status == 'active' )
                                 .all()) :
            if m.egroup and m.egroup.strip() != '' :
                mgrs[ 'activity' ].append( m.egroup.strip() )
            else:
                mgrs[ 'activity' ].append( uName )
            mgrsFull[ 'activity' ].append( m.to_json() )
    if task :
        iType, iCode = ('task', task)
        for m, uName in (db.session.query( Manager, EprUser.name )
                                 .filter( Manager.itemType == iType )
                                 .filter( Manager.itemCode == iCode )
                                 .filter( Manager.userId == EprUser.id )
                                 .filter( Manager.role == permsMap[iType] )
                                 .filter( Manager.year == year )
                                 .filter( Manager.status == 'active' )
                                 .all()) :
            if m.egroup and m.egroup.strip() != '' :
                mgrs[ 'task' ].append( m.egroup.strip() )
            else:
                mgrs[ 'task' ].append( uName )
            mgrsFull[ 'task' ].append( m.to_json() )

    current_app.logger.debug( "getAPTManagers1> mgrs for proj %s, act %s, task %s " % (mgrs[ 'project' ], mgrs[ 'activity' ], mgrs[ 'task' ]) )

    return mgrs, mgrsFull


def getFavTaskCodes(user=None, year=None):

    if user is None:
        user = current_user
    taskList = []
    try:
        prefs, = db.session.query( UserPreferences.prefs )\
                .filter( UserPreferences.hrid == user.hrId)\
                .filter( UserPreferences.app_name =='epr')\
                .one()

        taskList = prefs['tasks'] if 'tasks' in prefs else []
    except orm.exc.NoResultFound: # nothing in yet, make a new/fresh object:
        return []

    current_app.logger.debug( 'found %d favourite tasks overall for user %s ' % (len(taskList), user.username,) )

    if year:
        taskListFiltered = []
        yearTasks = db.session.query(Task.code).filter(Task.year == year).filter(Task.status=='ACTIVE').all()
        for tCode in taskList:
            if (tCode,) in yearTasks: taskListFiltered.append( tCode )
        current_app.logger.debug( 'found %d favourite tasks in %s for user %s ' % (len(taskListFiltered), year, user.username,) )
        return taskListFiltered

    return taskList

def getFavPledgeCodes(user=None, year=None):

    if user is None:
        user = current_user
    plList = []
    try:
        prefs, = db.session.query( UserPreferences.prefs )\
                .filter( UserPreferences.hrid == user.hrId)\
                .filter( UserPreferences.app_name =='epr')\
                .one()

        plList = prefs['pledges'] if 'pledges' in prefs else []
    except orm.exc.NoResultFound: # nothing in yet, make a new/fresh object:
        return []

    if year:
        plListFiltered = []
        yearPledges = db.session.query(Pledge.code).filter(Pledge.year == year).filter(Pledge.status!='DELETED').all()
        for pCode in plList:
            if (pCode,) in yearPledges: plListFiltered.append( pCode )
        return plListFiltered

    return plList

def getFavIRTPledgeCodes(user=None, year=None):

    if user is None:
        user = current_user

    plList = []
    try:
        prefs, = db.session.query( UserPreferences.prefs )\
                .filter( UserPreferences.hrid == user.hrId)\
                .filter( UserPreferences.app_name =='epr')\
                .one()

        plList = prefs['irtPledges'] if 'irtPledges' in prefs else []
    except orm.exc.NoResultFound: # nothing in yet, make a new/fresh object:
        return []

    if year:
        plListFiltered = []
        yearPledges = db.session.query(Pledge.code).filter(Pledge.year == year).filter(Pledge.status!='DELETED').all()
        for pCode in plList:
            if (pCode,) in yearPledges: plListFiltered.append( pCode )
        return plListFiltered

    return plList

# maps the name of the project in EPR to the ones in the "old " iCMS system:
projNameMap = {  'BRIL'                      : ['BRIL'],
                 'CT-PPS'                    : ['CTPPS'],
                 'DAQ'                       : ['DAQ'],
                 'ECAL'                      : ['EC'],
                 'General'                   : ['GEN'],
                 'HCAL'                      : ['HC'],
                 'HGCAL'                     : ['HGCAL'],
                 'L1 Trigger'                : ['L1 Trigger', 'L1TRG'],
                 'Muon'                      : ['MUCSC', 'MUDT', 'MUGEM', 'Muon', 'MURPC'],
                 'Muon-CSC'                  : ['MUCSC', 'MUDT', 'MUGEM', 'Muon', 'MURPC'],
                 'Muon-DT'                   : ['MUCSC', 'MUDT', 'MUGEM', 'Muon', 'MURPC'],
                 'Muon-GEM'                  : ['MUCSC', 'MUDT', 'MUGEM', 'Muon', 'MURPC'],
                 'Muon-RPC'                  : ['MUCSC', 'MUDT', 'MUGEM', 'Muon', 'MURPC'],
                 'Offline And Computing'     : ['OFF', 'OFFCOMP', 'Offline And Computing'],
                 'PPD'                       : ['PPD'],
                 'Run Coordination'          : ['RUN'],
                 'Technical Coordination'    : ['TECH'],
                 'Tracker'                   : ['TK', 'Tracker'],
                 'Trigger Coordination'      : ['TRG', 'TRGCOORD'],
                 'UPGRADE'                   : ['UPGRADE'],
                 'Upgrade'                   : ['UPGRADE'],
}


def findiCMSProjCode( pName ):
    """
         iCNS old                   icms new
    +-----------------------+    ------------------------
    | BRIL                  |     BRIL
    | CORE                  |     CT-PPS
    | CTPPS                 |     DAQ
    | DAQ                   |     ECAL
    | DI                    |     General
    | EC                    |     HCAL
    | GEN                   |     HGCAL
    | HC                    |     L1 Trigger
    | HGCAL                 |     Muon
    | L1 Trigger            |     Muon-CSC
    | L1TRG                 |     Muon-DT
    | MUCSC                 |     Muon-GEM
    | MUDT                  |     Muon-RPC
    | MUGEM                 |     Offline And Computing
    | Muon                  |     PPD
    | MURPC                 |     Run Coordination
    | OFF                   |     Technical Coordination
    | OFFCOMP               |     Tracker
    | Offline And Computing |     Trigger Coordination
    | PPD                   |     UPGRADE
    | RUN                   |     Upgrade
    | TECH                  |
    | TK                    |
    | Tracker               |
    | TRG                   |
    | TRGCOORD              |
    | UPGRADE               |

    """

    # if 'CT-PPS' == pName : return ['CTPPS']
    # if 'ECAL' == pName: return ['EC']
    # if 'General' == pName : return ['GEN']
    # if 'HCAL' == pName: return ['HC']
    # if 'L1 Trigger' == pName : return ['L1TRG', pName]
    # if pName.lower().startswith('muon'): return ['MUCSC', 'MUDT', 'MUGEM', 'Muon', 'MURPC']
    # if 'Offline And Computing' == pName : return ['OFF', 'COMP', pName]
    # if 'Run Coordination' == pName : return ['RUN']
    # if 'Technical Coordination' == pName : return ['TECH']
    # if 'Tracker' == pName : return ['TK', pName]
    # if 'Trigger Coordination' == pName : return ['TRG', 'TRGCOORD']
    # if 'upgrade' in pName.lower() : return ['UPGRADE']

    return projNameMap.get( pName, [pName] ) # return the incoming request if nothing is found (testing)

def reverseMap( codeOld ):

    # Note: "CORE" and "DI" are not mapped as they do not
    # have a corresponding "new" equivalent project (at
    # least not to my knowledge ...)

    result = []
    for k, v in projNameMap.items():
        if codeOld in v: result.append( k )

    # return the incoming request if nothing is found (tesing)
    if not result: result = [ codeOld ]

    return result

def getInstAuthForProj(projCode, year):

    # institute time lines appear several times for a year (at least twice for the "ahead of time creation" and the subsequent "finalisation"
    # therefore first create a subquery getting the timelines for each distinct user by cmsId, then sum them up. See issue ICMSEPR-173.

    sq = ( db.session.query( TimeLineUser.cmsId.distinct().label('cmsId'),
                             TimeLineUser.mainProj.label('mainProj'),
                             TimeLineInst.code.label('instCode'),
                             TimeLineUser.yearFraction.label('yearFraction') )
                      .join( TimeLineInst, TimeLineInst.code == TimeLineUser.instCode)
                      .filter( TimeLineInst.year == year )
                      .filter( TimeLineInst.cmsStatus == 'Yes' )
                      .filter( TimeLineUser.isAuthor == True)
                      .filter( TimeLineUser.year == year )
                      .filter( TimeLineUser.status == 'CMS' )
                      .filter( TimeLineUser.mainProj != '' )
        )
    if projCode != 'all': # needs mapping to iCMS mainProj codes :(
        # print 'got projCode: ', projCode, findiCMSProjCode(projCode)
        sq = sq.filter( TimeLineUser.mainProj.in_( findiCMSProjCode(projCode) ) )

    sq = sq.subquery()

    subQuery = ( db.session.query( sq.c.mainProj, sq.c.instCode, func.sum(sq.c.yearFraction) )
                      .order_by( sq.c.instCode, sq.c.mainProj )
                      .group_by( sq.c.instCode, sq.c.mainProj )
                 )

    res = subQuery.all()

    current_app.logger.debug( 'getInstAuthForProj> for %s in %s, found proj/inst/sum: %s ' % (projCode, year, str(res)) )

    projList = set()
    instList = set()
    authInstMap = {}
    for projOld, instCode, nInstAuthors in res:
        projs = reverseMap(projOld)
        if not projs : continue
        [ projList.add( proj ) for proj in projs ]
        instList.add( instCode )
        for proj in projs:
            if proj not in authInstMap: authInstMap[proj] = {}
            authInstMap[proj][instCode] = nInstAuthors

    current_app.logger.debug( 'getInstAuthForProj> for %s in %s, found projList    %s ' % (projCode, year, str(projList)) )
    current_app.logger.debug( 'getInstAuthForProj> for %s in %s, found instList    %s ' % (projCode, year, str(instList)) )
    current_app.logger.debug( 'getInstAuthForProj> for %s in %s, found authInstMap %s ' % (projCode, year, str(authInstMap)) )

    # print "aim = ", authInstMap
    # print 'pl  = ', projList

    return projList, instList, authInstMap

def doPledgeSplit(plCode, newInstCode, newInstWork):

    current_app.logger.info( 'request to split pledge %s, giving %s EPR month(s) to %s' % (plCode, newInstWork, newInstCode) )

    if not ( current_user.is_administrator() ):
        msg = 'You (%s) are not allowed to split pledges, sorry.' % current_user.username
        current_app.logger.error(msg)
        return ("ERROR", msg, 403)

    if not plCode or plCode.split() == '':
        msg = 'No valid pledge code found in request'
        current_app.logger.error(msg)
        return ("ERROR", msg, 403)

    try:
        plCode = plCode.replace( ':', '+' )
        oldPledge = db.session.query(Pledge).filter_by(code = plCode).one()
    except sa_exc.NoResultFound:
        msg = 'No pledge found in DB for code %s' % plCode
        current_app.logger.error( msg )
        return ("ERROR", msg, 403)

    if oldPledge.isGuest and not current_user.is_administrator():
        msg = 'Not touching guest pledge'
        current_app.logger.error( msg )
        return ("ERROR", msg, 403)

    if not newInstCode or newInstCode.split() == '':
        msg = 'No valid newInstCode found in request'
        current_app.logger.error( msg )
        return ("ERROR", msg, 403)

    try:
        newInst = db.session.query(EprInstitute).filter_by(code=newInstCode).one()
    except sa_exc.NoResultFound:
        msg = 'No valid newInst found for instCode %s' % newInstCode
        current_app.logger.error( msg )
        return ("ERROR", msg, 403)

    if newInstWork is None:
        msg = 'Invalid work (%s) for newInst %s found in request' % (newInstWork, newInstCode)
        current_app.logger.error( msg )
        return ("ERROR", msg, 403)

    newInstWork = float(newInstWork)
    if ( newInstWork < 0. or newInstWork > 12. or
         newInstWork > float(oldPledge.workTimePld) ):
        msg = 'Invalid work (%s) for newInst %s found in request, should be between 0. and %s' % (newInstWork, newInstCode, min(12., float(oldPledge.workTimePld)))
        current_app.logger.error( msg )
        return ("ERROR", msg, 403)

    p0 = None
    try:
        p0 = db.session.query(Pledge).filter_by(userId = oldPledge.userId, instId=newInst.id, taskId=oldPledge.taskId).filter(Pledge.status != 'rejected').one()
    except sa_exc.NoResultFound:
        pass
    if p0: # there is already a pledge for this task/user/inst -> fail.
        msg = 'A pledge for task/user/inst=%s/%s/%s already exists - nothing done.' % (oldPledge.taskId, oldPledge.userId, newInst.id)
        current_app.logger.error( msg )
        return ("ERROR", msg, 403)

    current_app.logger.info( 'request for splitting pledge code %s sharing work of %s to new inst %s' % (plCode, newInstWork, newInstCode) )

    # first adjust the values for the existing pledge, then create the new one:
    oldFrac = float(oldPledge.workTimePld-float(newInstWork))/float(oldPledge.workTimePld)
    newWorkAcc = oldPledge.workTimeAcc*(1.-oldFrac)
    newWorkDone = oldPledge.workTimeDone*(1.-oldFrac)

    oldPledge.workTimePld = oldPledge.workTimePld-float(newInstWork)
    oldPledge.workTimeAcc = oldPledge.workTimeAcc * (oldFrac)
    oldPledge.workTimeDone = oldPledge.workTimeDone * (oldFrac)
    commitUpdated( oldPledge)

    newPledge = Pledge(oldPledge.taskId, oldPledge.userId, newInst.id,
                       newInstWork, newWorkAcc, newWorkDone,
                       oldPledge.status, oldPledge.year,
                       oldPledge.workedSoFar*(1.-oldFrac) if oldPledge.workedSoFar is not None else None,
                       )
    commitNew(newPledge)

    current_app.logger.info( 'pledge %s split into: \n\t%s\n\t%s' % (plCode, oldPledge, newPledge) )

    return ("OK", 'pledge %s successfully split (%s to %s)' % (plCode, newInstWork, newInstCode), 200)

def getProjectCeilings(year, projects):

    prjCodes = [ list(p.keys())[0] for p in projects ]
    ceilings = db.session.query(EprCeiling).filter_by(year=year).filter( EprCeiling.code.in_(prjCodes) ).all()

    current_app.logger.info( "getProjectCeilings> year: %s, requested: %s " % (year, str(ceilings)) )

    projectCeilings = []
    for item in projects:
        code, name = list(item.items())[0]
        item = { "name" : name, "code": code, "ceiling": -1 }
        for c in ceilings:
            if c.code == code:
                item = { "name" : name, "code" : code, "ceiling" : c.ceiling }
        projectCeilings.append( item )

    current_app.logger.info( "getProjectCeilings> year: %s, found: %s " % (year, str(projectCeilings)) )

    return projectCeilings

def checkCeiling(year, project, newNeed):

    projActivities = db.session.query(CMSActivity)\
                               .filter(CMSActivity.projectId == project.id)\
                               .filter(CMSActivity.status == 'ACTIVE')\
                               .all()

    projActIds = [ x.id for x in projActivities ]
    current_app.logger.info("isWithinCeiling> found %d activities for proj %s in %s " % (len(projActIds), project.name, year) )

    actualNeeds, = db.session.query(func.sum(Task.neededWork))\
                            .filter(Task.activityId.in_(projActIds))\
                            .filter(Task.status != 'rejected')\
                            .one()
    if not actualNeeds:
        current_app.logger.warning("isWithinCeiling> NO actualNeeds found for %d activities for proj %s in %s ?!?!?!" % (len( projActIds ), project.name, year) )
        actualNeeds = 0

    # projCeiling = getProjectCeilings(year, project.code )
    projCeiling = -1 # set the default
    try:
        projCeiling, = db.session.query(EprCeiling.ceiling).filter(EprCeiling.year == year).filter( EprCeiling.code == project.code ).one()
    except sa_exc.NoResultFound:
        pass

    current_app.logger.info("isWithinCeiling> found actual %s needs (ceiling: %s) of tasks for proj %s in %s " % (actualNeeds, projCeiling, project.name, year) )

    # flag OK if no ceiling is set or the total new needs are not larger than the ceiling
    isWithin = ( projCeiling < 0 ) or ((actualNeeds + newNeed)<=projCeiling)

    return [ isWithin, actualNeeds, projCeiling ]

def queryPATItems(what, whatStr, selYear):

    subq =  db.session.query( Manager, what ) \
        .filter( Manager.year == selYear ) \
        .filter( Manager.status == 'active' ) \
        .filter( Manager.itemType == whatStr ) \
        .filter( Manager.itemCode == what.code ) \
        .filter( Manager.role == permsMap[whatStr] ) \
        .filter( what.year == selYear ) \

    mgrs = subq.filter( Manager.userId == current_user.id ) \
               .order_by( what.name ) \
               .all()

    # Get the list of all eGroups the user is member of (from his LDAP entry),
    # then match them against the eGroups stored as management eGroups. Add these
    # PAT items to the list before returning
    eGroupList = getEgroupsForUser(current_user.username)
    mgrs += subq.filter( Manager.egroup.in_(eGroupList) ) \
                .order_by( what.name ) \
                .all( )

    return mgrs

def getManagedPATItems( selYear ) :
    myProjects = [ p for m, p in queryPATItems( Project, 'project', selYear ) ]
    pMap = { }
    myActivities = { }
    if len( myProjects ) > 0 :  # current_user is project manager, so show all activities for all the managed project(s):
        myActivities = { }
        for p in myProjects :
            if p.code not in pMap.keys() : pMap[ p.code ] = p
            myActivities[ p.code ] = db.session.query(CMSActivity).filter( CMSActivity.year == selYear ) \
                .filter( CMSActivity.projectId == p.id ) \
                .filter( CMSActivity.status != 'DELETED' ) \
                .all()

    # ... and add any additional activity, not yet in the list:
    for m, a in queryPATItems( CMSActivity, 'activity', selYear ) :
        if a.project.code not in pMap.keys() : pMap[ a.project.code ] = a.project
        if a.project.code not in myActivities.keys() :
            myActivities[ a.project.code ] = [ a ]
        else :
            if a not in myActivities[ a.project.code ] :
                myActivities[ a.project.code ].append( a )

    # most tasks are accessed via their activity.
    # Here we still list tasks which are explicit managed:
    myTasks = [ t for m, t in queryPATItems( Task, 'task', selYear ) if t.status == 'ACTIVE' ]
    myInsts = None  # for now. mngInstUsers seems broken ... [i for m,i in queryPATItems(Institute, 'institute', selYear)]
    return myActivities, myInsts, myProjects, myTasks, pMap

def canManageProject( selYear ):
    res = ( db.session.query(Manager)
                      .filter( Manager.userId == current_user.id )
                      .filter(Manager.year == selYear)
                      .filter(Manager.itemType == 'project')
                      .filter(Manager.status == 'active')
                      .all() )
    projList = []
    for item in res:
        projList.append( item.itemCode )

    return len(projList)>0, projList

def canManageSelectedInst( instCodeSel, selYear ) :

    selInst = db.session.query( EprInstitute ).filter_by( code=instCodeSel ).one()

    # wizards are allowed to manage institutes other than their own:
    if current_user.is_wizard() :
        if instCodeSel is None :
            selInst = db.session.query( EprInstitute ).filter_by( id=current_user.mainInst ).one()
            instCodeSel = selInst.code
        else :
            selInst = db.session.query( EprInstitute ).filter_by( code=instCodeSel ).one()

    isCurrentInstMgr = current_user.is_administrator() or current_user.canManage( 'institute', selInst.code, year=session[ 'year' ] )

    current_app.logger.info( 'canManageCurrentInst> user %s: inst: %s/%s, isInstMgr: %s, (isAdmin: %s, year:%s)' %
                             (current_user.name, instCodeSel, selInst.code, isCurrentInstMgr, current_user.is_administrator(), selYear) )

    return isCurrentInstMgr, selInst

def getInstUsers( instCode, selYear ) :
    sq1 = QueryFactory.sq_time_line_user( year=selYear, inst_code=instCode, status='CMS%' )

    sq2 = (db.session.query( Pledge.userId.label('userId'),
                            func.sum( Pledge.workTimePld ).label('workPld')
                          )
                .filter( Pledge.year == selYear )
                .filter( Pledge.status != 'rejected' )
                .group_by( Pledge.userId )
           ).subquery()

    res = (db.session.query( EprUser.cmsId,
                             EprUser.name,
                             EprUser.authorReq,
                             sq1.c.dueApplicant,
                             sq1.c.dueAuthor,
                             TimeLineUser.yearFraction * case( { True : 1.0 }, value=TimeLineUser.isAuthor, else_=0. ),
                             EprUser.id,
                             EprUser.username,
                             sq2.c.workPld
                             )
           .join( sq1, EprUser.cmsId == sq1.c.cmsId )
           .outerjoin( sq2, sq2.c.userId == EprUser.id )
           .join( TimeLineUser, and_( TimeLineUser.cmsId == EprUser.cmsId, TimeLineUser.timestamp == sq1.c.laststamp ) )
           .join( EprInstitute, EprUser.mainInst == EprInstitute.id )
           .filter( EprInstitute.code == instCode )
           .filter( EprUser.status.like( 'CMS%' ) )
           .all())
    current_app.logger.debug( "instUsers> found %s entries in DB " % len( res ) )
    return res

def getInstRespPledgeSummary( instCode, taskId=None, year=None ) :

    selYear = year
    if year is None:
        selYear = session.get( 'year' )

    sq = (db.session.query( Pledge.instId.label( 'instId' ),
                              func.sum( Pledge.workTimePld ).label( 'workPld' ),
                              func.sum( Pledge.workTimeAcc ).label( 'workAcc' ),
                              func.sum( Pledge.workTimeDone ).label( 'workDone' ),
                              func.sum( Pledge.workedSoFar ).label( 'workedSoFar' ),
                              Pledge.taskId.label( 'taskId' ),
                              Pledge.status.label( 'plStatus' )
                              )
            .join( Task, Task.id == Pledge.taskId )
            .filter( Pledge.year == selYear )
            .filter( Pledge.status != 'rejected' )
            .filter( Task.tType == 'InstResp' )
            .filter( Task.status == 'ACTIVE' )
            .group_by( Pledge.instId, Pledge.taskId, Pledge.status )
            .subquery())

    query = (db.session.query( Task,
                             sq.c.workPld, sq.c.workAcc, sq.c.workDone, sq.c.workedSoFar,
                             sq.c.plStatus,
                             EprInstitute )
            .join( sq, sq.c.taskId == Task.id )
            .join( CMSActivity, Task.activityId == CMSActivity.id )
            .join( Project, CMSActivity.projectId == Project.id )
            .join( EprInstitute, sq.c.instId == EprInstitute.id )
            .filter( Task.tType == 'InstResp' )
            .filter( Task.status == 'ACTIVE' )
             )

    if instCode:
        query = query.filter( EprInstitute.code == instCode )
    if taskId:
        query = query.filter( Task.id == taskId )

    res = query.all()

    return res

def getInstRespPledges( instCode, taskId, year=None ) :

    selYear = year
    if year is None:
        selYear = session.get( 'year' )

    query = (db.session.query( Pledge, Task, CMSActivity, Project,
                               EprUser, EprInstitute )
             .join( Task, Task.id == Pledge.taskId )
             .join( CMSActivity, Task.activityId == CMSActivity.id )
             .join( Project, CMSActivity.projectId == Project.id )
             .join( EprUser, Pledge.userId == EprUser.id)
             .filter( Pledge.year == selYear )
             .filter( Pledge.status != 'rejected' )
             .join( EprInstitute, Pledge.instId == EprInstitute.id )
             .filter( EprInstitute.code == instCode )
             .filter( Task.tType == 'InstResp' )
             .filter( Task.status == 'ACTIVE' )
             .filter( Task.id == taskId )
             )

    res = query.all()

    current_app.logger.info( 'getInstRespPledges> found %d pledges for InstResp task %s and inst %s in %d' % (len(res), taskId, instCode, selYear) )

    return res

def addToFavs( hrId, plCodes, taskCode, irtPledges=None) :

    try :
        fav = db.session.query( UserPreferences ).filter_by( hrid=hrId ).filter_by( app_name='epr' ).one()
    except sqlalchemy.orm.exc.NoResultFound :  # nothing in yet, make a new/fresh object:
        fav = UserPreferences( hrid=hrId, app_name='epr', prefs={ } )

    # make a deep copy, so we can update the full item later
    newPrefs = copy.copy( fav.prefs )

    if taskCode :
        taskList = [ ]
        prefTasks = newPrefs.get( 'tasks', [ ] )

        taskList += prefTasks
        if taskCode not in taskList :
            taskList.append( taskCode )

        newPrefs.update( { 'tasks' : taskList } )

    if plCodes :
        pledgeList = [ ]
        prefPls = newPrefs.get( 'pledges', [ ] )
        pledgeList += prefPls
        for plCode in plCodes:
            if plCode not in pledgeList :
                pledgeList.append( plCode )

        newPrefs.update( { 'pledges' : pledgeList } )

    if irtPledges :
        irtPlList = [ ]
        prefPls = newPrefs.get( 'irtPledges', [ ] )
        irtPlList += prefPls
        for plCode in irtPledges:
            if plCode not in irtPlList :
                irtPlList.append( plCode )

        newPrefs.update( { 'irtPledges' : irtPlList } )

    # now copy the new item, so that sqlalchemy sees it:
    fav.prefs = newPrefs
    db.session.add( fav )
    db.session.commit()

    if taskCode :
        current_app.logger.info( 'addToFavs> task code %s added to preferences for hrId %s' % (taskCode, hrId) )

    if plCodes :
        current_app.logger.info( 'addToFavs> pledge codes %s added to preferences for hrId %s' % (str(plCodes), hrId) )

    if irtPledges :
        current_app.logger.info( 'addToFavs> irtPledge codes %s added to preferences for hrId %s' % (str(irtPledges), hrId) )

    return

def getLevel3Names(selYear):

    res = ( db.session.query(Level3Name)
                      .filter(Level3Name.startYear<=selYear)
                      .filter( sqlalchemy.or_(  (Level3Name.endYear>=selYear), (Level3Name.endYear == -1)) )
                      .order_by(Level3Name.name)
                      .all() )

    return res

def getLevel3NamesForProj(selYear, act):

    actList = ( db.session.query( CMSActivity.id )
                          .join( Project, CMSActivity.projectId == act.projectId )
                .all())

    # current_app.logger.debug("actList for proj %s (%d): %s " % (act.project.name, act.projectId, str(actList)) )

    resDB = ( db.session.query(Level3Name)
                    .join( Task, Task.level3 == Level3Name.id )
                    .filter( Task.activityId.in_( actList ) )
                    .filter( Task.status == 'ACTIVE')
                    .filter(Level3Name.startYear<=selYear)
                    .filter( sqlalchemy.or_(  (Level3Name.endYear>=selYear), (Level3Name.endYear == -1)) )
                    .order_by(Level3Name.name)
                    .all() )
    res = list( set(resDB) )
    current_app.logger.info( 'getLevel3NamesForProj> found %d LV3names for act %s : %s' % (len(res), act.name, str(res) ) )

    defaultLV3 = db.session.query(Level3Name).filter( Level3Name.name == 'Unknown' ).one()
    return set( [defaultLV3] + res )

