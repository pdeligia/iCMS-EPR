from datetime import datetime
from flask_wtf import FlaskForm as Form
from wtforms import widgets, StringField,  TextAreaField, BooleanField, SelectField, SelectMultipleField, SubmitField, \
    IntegerField, DecimalField, DateTimeField, FieldList, TextField
from wtforms.validators import InputRequired, Optional, Length, Email, Regexp, NumberRange
from wtforms import ValidationError

from ..models import getTaskTypes, getTaskShiftTypeIds

class MultiCheckboxField(SelectMultipleField):
    widget = widgets.ListWidget(prefix_label=False)
    option_widget = widgets.CheckboxInput()

class UserForm(Form):
    name = StringField('Name ', validators=[InputRequired()])
    inst = StringField('Institute', validators=[InputRequired()])
    submit = SubmitField('Submit')


class ProjectForm(Form):

    name         = StringField('Short name', validators=[InputRequired()])
    description  = StringField('Description', validators=[InputRequired()])
    code         = StringField('Code', validators=[Optional()])

    submit = SubmitField('Submit')


class ActivityForm(Form):

    name         = StringField('Short name', validators=[InputRequired()])
    description  = StringField('Description', validators=[InputRequired()])

    submit = SubmitField('Submit')


class SelectProjectForm(Form):

    project = SelectField('Project')

    submit = SubmitField('Submit')

class SelectProjectActForm(Form):

    project  = SelectField('Project')
    activity = SelectField('Activity')

    submit = SubmitField('Submit')


class TaskForm(Form):

    name        = StringField('Short name (required)', validators=[InputRequired()])
    description = TextAreaField('Description (required)', validators=[InputRequired(), Length(max=2000)])

    pctAtCERN = DecimalField('Required % of presence at CERN', validators=[NumberRange(0., 100., 'between 0 and 100 percent')] )

    # e.g. Perennial, one-off, central shift, expert on call shift
    tType = SelectField('Type')

    locked = BooleanField( 'Locked' )

    comment = TextAreaField('Comment', validators=[Optional(), Length(max=2000)])

    submit = SubmitField('Submit')

class TaskAdminForm(TaskForm):

    level3 = StringField( 'Select Level3 Name', validators=[InputRequired()] )
    shiftTypeId = SelectField('Shift type')
    kind = SelectField( 'Kind', choices=[ (v, v) for v in [ 'CORE', 'NON-CORE' ] ], default='CORE' )
    neededWork  = DecimalField('Work needed (person-months)', validators=[InputRequired(),
                                                                          NumberRange(0., 1200.,'at least one month, max. 1200 months')])

class PledgeUserForm(Form):

    workTimePld  = DecimalField('Work pledge',
                                validators=[InputRequired(),
                                            # NumberRange(0.25, 12.,'at least one week (0.25 months), max 12 months')
                                            ])

    workedSoFar = DecimalField('Work already done',
                                validators=[Optional(),
                                           NumberRange(.0, 12.,'at least one month, max 12 months') ])

    submit = SubmitField('Submit')

class PledgeAdminForm(PledgeUserForm):

    workTimeAcc  = DecimalField('Accepted pledge', validators=[Optional()], default=0.)
    workTimeDone = DecimalField('Work done', validators=[Optional()], default=0.)

    status = SelectField('Status', choices = [('new', 'New'), ('accepted', 'Accepted'), ('done', 'Done')])

class PledgeUpdateWorkForm(Form):

    workedSoFar = DecimalField('Work already done',
                                validators=[InputRequired(),
                                            NumberRange(.0, 12.,'at least one month, max 12 months') ])

    submit = SubmitField('Submit')

class RejectPledgeForm(Form):

    reason = TextAreaField("Reason for rejecting", validators=[InputRequired(), Length(max=2000)])

    submit = SubmitField('Reject Pledge')

class YearSelectorForm(Form):

    yearList = range( 2015, datetime.today( ).year + 1 )
    year   = SelectField('Year for summary', coerce=int, choices = [ (i, j) for i, j in enumerate( yearList ) ] )
    submit = SubmitField('Set year')

class PledgeSplitForm(Form):

    newInst = StringField('Institute to share work with', validators=[InputRequired()] )
    newInstWork = DecimalField('Work to be assigned to the institute (person-months)',
                                validators=[InputRequired(),
                                            NumberRange(.0, 12.,'at least one month, max 12 months') ]
                               )

    submit = SubmitField('Submit')

class InstRespPledgeForm(Form):

    workPledged = DecimalField('Work pledged (person-months)',
                                validators=[InputRequired(),
                                            NumberRange(.1, 999999., 'at least 0.1 month')]
                               )
    taskName = StringField('InstResp Task Name', validators=[InputRequired()] )
    taskId = StringField('InstResp Task Id', validators=[InputRequired()] )

    submit = SubmitField('Create InstResp Pledge')

class InstRespPledgeEditForm(Form):

    # workTimePld  = DecimalField('Work pledged by institute (person-months)',
    #                             validators=[InputRequired(),
    #                                         # NumberRange(0.25, 12.,'at least one week (0.25 months), max 12 months')
    #                                         ])

    workedSoFar = DecimalField('Work already done by institute (person-months)',
                                validators=[Optional(),
                                            # NumberRange(.0, 12.,'at least one month, max 12 months')
                                            ])

    workTimeAcc  = DecimalField('Accepted pledge for institute (person-months)', validators=[Optional()])
    workTimeDone = DecimalField('Work done by institute (person-months)', validators=[Optional()])

    status = SelectField('Status', choices = [('new', 'New'), ('accepted', 'Accepted'), ('done', 'Done')])

    submit = SubmitField('Submit')

class InstRespPledgeUpdateWorkForm(Form):

    workedSoFar = DecimalField('Work already done (person-months)',
                                validators=[InputRequired(),
                                            NumberRange(.0, 12.,'at least one month, max 12 months') ])

    submit = SubmitField('Submit')

class InstRespPledgeRejectForm(Form):

    reason = TextAreaField("Reason for rejecting this pledge", validators=[InputRequired(), Length(max=2000)])

    submit = SubmitField('Reject Pledge')
