
from sqlalchemy.engine import reflection
from sqlalchemy import MetaData, Table
from sqlalchemy.sql import text

from sqlalchemy_continuum import version_class

from ..models import db, Pledge, Task

def toJson( item ):
    res = {}
    for k,v in item.__dict__.items():
        if k.startswith('_') : continue
        res[k] = v
        # print '-> k, v', k, v

    # print "returning res=", res
    return res

def getPledgeHistory(code, year):

    plHistory = []

    PledgeVersion = version_class( Pledge )

    plVersions = db.session.query( PledgeVersion ).filter( PledgeVersion.code == code ).order_by(PledgeVersion.timestamp.desc()).all()
    print( "got: plversions: ", plVersions )

    pl = db.session.query(Pledge).filter_by( code = code).one()

    # first = pl.versions[0]
    # print("1st version:", first)
    # second = first.next
    # if second:
    #     print("2nd  version:", second)
    #     third = second.next
    #     if third:
    #         print("3rd  version:", third)
    #         fourth = third.next
    #         if fourth:
    #             print("4th  version:", fourth)

    for item in plVersions:
        plHistory.append( toJson(item) )

    print('hist: ', plHistory)

    return plHistory

def getTaskHistory(code, year):

    return getItemHistory( Task, code, year )

def getItemHistory( ItemClass, code, year ) :

    history = []

    VersionClass = version_class( ItemClass )

    versions = db.session.query( VersionClass ).filter( VersionClass.code == code ).order_by(VersionClass.timestamp.desc()).all()
    # print( "got: versions: ", versions )

    pl = db.session.query( ItemClass ).filter_by( code = code).one()

    # first = pl.versions[0]
    # print("1st version:", first)
    # second = first.next
    # if second:
    #     print("2nd  version:", second)
    #     third = second.next
    #     if third:
    #         print("3rd  version:", third)
    #         fourth = third.next
    #         if fourth:
    #             print("4th  version:", fourth)

    for item in versions:
        history.append( toJson(item) )

    # print('hist: ', history)

    return history

