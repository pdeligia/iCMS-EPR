
from datetime import datetime
import json
import re

from flask import render_template, redirect, url_for, abort, flash, request,\
    current_app, make_response, jsonify, session

from sqlalchemy.orm import exc as sa_exc
import sqlalchemy

from flask_login import login_required, current_user
from flask_sqlalchemy import get_debug_queries

from ..main.JsonDiffer import Comparator

from ..api_1_0.decorators import crossdomain

from ..main.EprAssets import EprAssets

from . import manage

from .. import db
from ..models import Permission, EprUser, EprInstitute, Project, CMSActivity, \
                     Task, Manager, TimeLineUser, Category, Pledge, ShiftTaskMap, Level3Name, \
                     permsMap, getTaskTypes, getTaskShiftTypeIds

from .forms import ProjectManagerForm, ActivityManagerForm, TaskManagerForm, \
                   AddUserForm, AddInstituteForm, EditUserForm

from ..models import commitNew, commitUpdated, getEgroupsForUser

from ..main.Helpers import logAndRedirect, getNavInfo, getTaskActivitiesForProject, queryPATItems, getManagedPATItems, checkCeiling, getLevel3NamesForProj, getLevel3NameMap
from ..main.MailHelpers import sendMailTaskUpdated


@manage.after_app_request
def after_request(response):
    for query in get_debug_queries():
        if query.duration >= current_app.config['ICMS_SLOW_DB_QUERY_TIME']:
            current_app.logger.warning(
                'Slow query: %s\nParameters: %s\nDuration: %fs\nContext: %s\n'
                % (query.statement, query.parameters, query.duration,
                   query.context))
    return response

@manage.route('/', methods=['GET', 'POST'])
@login_required
def index():
    return redirect( url_for('manage.mine') )

@manage.route( '/all', methods=[ 'GET', 'POST' ] )
@manage.route( '/full', methods=[ 'GET', 'POST' ] )
@login_required
def full( ) :

    if current_user.is_anonymous:
        return redirect( url_for('auth.login'))

    selYear = session['year']

    # make sure only an admin or one of the proj/act/task mgrs can go beyond this ...
    patMgrs = ( db.session.query(Manager.userId)
                  .filter(Manager.itemType.in_(['project','activity','task']))
                  .filter(Manager.year == session[ 'year' ])
                  .all() )

    if (  ( not current_user.is_administrator( ) ) and
          ( (current_user.id,) not in patMgrs ) ) :
        flash( 'You are not a manager for this project, sorry.' )
        return redirect( url_for( 'main.showMine' ) )

    return render_template('manage/index.html',
                           projects=db.session.query(Project).filter_by(year=selYear).order_by(Project.name).all(),
                           activities=db.session.query(CMSActivity).filter_by(year=selYear).order_by(CMSActivity.name).all( ),
                           tasks=db.session.query(Task).filter_by(year=selYear).order_by(Task.name).all(),
                           navInfo = getNavInfo( session['year'] ))


@manage.route('/assets/setPledgeEnabled/<string:val>', methods=['GET'])
@login_required
def setPledgeEnabled(val):

    if not current_user.is_wizard( ) :
        flash( 'You are not a manager for this project, sorry.', 'error' )
        return redirect( url_for( 'main.showMine' ) )

    current_app.logger.info('setPledgeEnabled> val: %s' % str(val))
    if not session:
        current_app.logger.warning( 'setPledgeEnabled> no session' )

    EprAssets().updateAssets( 'pledgesEnabled', val )
     # always re-load assets from DB
    session[ 'assets' ] = EprAssets().getAssets('all', -1)
    session.modified = True

    return redirect( url_for('manage.assets') )


@manage.route('/assets/update/<string:key>/<string:val>', methods=['GET'])
@login_required
def updateAssets(key, val):

    if not current_user.is_wizard( ) :
        flash( 'You are not a manager for this project, sorry.', 'error' )
        return redirect( url_for( 'main.showMine' ) )

    current_app.logger.info('updateAssets> key: %s - val: %s' % (str(key), str(val)) )
    if not session:
        current_app.logger.warning( 'updateAssets> no session' )

    EprAssets().updateAssets( key, val )
     # always re-load assets from DB
    session[ 'assets' ] = EprAssets().getAssets('all', -1)
    session.modified = True

    return redirect( url_for('manage.assets') )

@manage.route('/assets', methods=['GET'])
@login_required
def assets():

    assets = session['assets']
    # assets.update( { 'pledgesEnabled': session.get('pledgesEnabled', '-') } )

    return render_template('manage/assets.html',
                            assets=assets,
                            navInfo = getNavInfo( session['year'] ))


@manage.route('/mine', methods=['GET', 'POST'])
@login_required
def mine():

    if current_user.is_anonymous:
        return redirect( url_for('auth.login'))

    selYear = session['year']

    myActivities, myInsts, myProjects, myTasks, pMap = getManagedPATItems( selYear )

    return render_template('manage/mine.html',
                           projects   = myProjects,
                           activities = myActivities,
                           tasks      = myTasks,
                           pMap = pMap,
                           institutes = myInsts,
                           navInfo = getNavInfo( session['year'] ))

def removeManagers( form, actMgrs, itemType, item, canManage, activePledges) :

    selYear = session['year']

    idList = [ int(i) for i in request.form.getlist( 'managers' ) ]
    removeList = []
    for mgr in form.managers.choices:
        if mgr[ 0 ] in idList : removeList.append( mgr )

    current_app.logger.info( "Request to remove manager(s) %s from %s" % (', '.join( [ x[1] for x in removeList ] ), item.name) )

    for m in actMgrs :
        if ( (m.egroup and m.egroup + '@cern.ch (eGroup)' in [ x[1] for x in removeList]) or
             (db.session.query(EprUser).filter_by( id=m.userId ).one( ).cmsId in [ int(x[0]) for x in removeList]) ) :
            m.status = 'deleted'
        try:
            commitUpdated(m)
        except Exception as e:
            return logAndRedirect( 'Error from DB when removing manager %s from %s code %s. - got: (%s)' % (m.name, itemType, item.name, str( e )),
                                   url_for('manage.index') )

    # refresh the list
    managers = db.session.query(Manager).filter_by( itemType=itemType, itemCode=item.code, status='active', role = permsMap[itemType], year=selYear ).all( )
    if managers :
        form.managers.choices = [ (i, db.session.query(EprUser).filter_by( id=m.userId ).one( ).name) for i, m in enumerate( managers ) ]
    else :
        form.managers.choices = [ ]

    msg = "Successfully removed manager(s) %s from %s" % (', '.join( [ x[1] for x in removeList ] ), item.name)
    flash( msg )
    current_app.logger.info( msg )

    return redirect( url_for( 'manage.%s' % itemType, code=item.code ) )

def removeObservers( form, actObs, itemType, item, canManage, activePledges) :

    selYear = session['year']

    idList = [ int(i) for i in request.form.getlist( 'observers' ) ]
    removeList = []
    for obs in form.observers.choices:
        if obs[ 0 ] in idList : removeList.append( obs )

    current_app.logger.info( "Request to remove observer(s) %s from %s" % (', '.join( [ x[1] for x in removeList ] ), item.name) )

    for m in actObs :
        if ( (m.egroup and m.egroup + '@cern.ch (eGroup)' in [ x[1] for x in removeList] )  or
             (db.session.query(EprUser).filter_by( id=m.userId ).one( ).cmsId in [ int(x[0]) for x in removeList]) ) :
            m.status = 'deleted'
            current_app.logger.debug( ":::+++:::> Removing observer(s) %s from %s" % (str(m), item.name) )
        try:
            commitUpdated(m)
        except Exception as e:
            return logAndRedirect( 'Error from DB when removing observer %s from %s code %s. - got: (%s)' % (m.name, itemType, item.name, str( e )),
                            url_for('manage.index') )

    # refresh the list
    observers = db.session.query(Manager).filter_by( itemType=itemType, itemCode=item.code, status='active', role = Permission.OBSERVE, year=selYear ).all( )
    if observers :
        form.observers.choices = [ (i, db.session.query(EprUser).filter_by( id=m.userId ).one( ).name) for i, m in enumerate( observers ) ]
    else :
        form.observers.choices = [ ]

    msg = "Successfully removed observer(s) %s from %s" % (', '.join( [ x[1] for x in removeList ] ), item.name)
    flash( msg )
    current_app.logger.info( msg )

    return redirect( url_for( 'manage.%s' % itemType, code=item.code ) )

def updateParams( form, itemType, item, canManage  ):

    task = item
    act = task.activity
    msg = ''

    # keep old values for mail:
    oldTaskJson = task.to_json()

    # Check that newly committed needs are within ceiling for project, correct for already existing needs:
    newNeeds = float(form.neededWork.data) - float(task.neededWork)
    isWithin, actNeeds, ceiling = checkCeiling(session['year'], act.project, newNeeds)
    if not isWithin:
        msg += 'ERROR - requested amount of effective needed work (%s (new-old)) on top of actual sum (%s) exceeds ceiling %s for project %s' % (
            newNeeds, actNeeds, ceiling, act.project.name)
        current_app.logger.error(msg)
        flash(msg, 'error')
        return redirect(url_for('manage.%s' % itemType, code=item.code))
    else:
        if newNeeds > 0. :
            msg += "Request OK, effective needed work for task (%s (new-old)) fits within ceiling for project (%s)" % (newNeeds, ceiling)
            current_app.logger.error(msg)

    # update task
    task.name = form.name.data
    task.description = form.description.data
    task.pctAtCERN = form.pctAtCERN.data
    task.tType = form.tType.data
    task.comment = form.comment.data

    task.shiftTypeId = form.shiftTypeId.data
    task.neededWork = form.neededWork.data
    task.locked = form.locked.data

    task.timestamp = datetime.utcnow()

    try:
        commitUpdated(task)
    except Exception as e:
        msg += '\nError from DB when trying to update task params.\n(%s)' % str(e)
        flash(msg, 'error')
        current_app.logger.error(msg)
        return redirect(url_for('manage.%s' % itemType, code=item.code))

    newTaskJson = db.session.query(Task).filter_by(code=task.code).one().to_json()
    if not sendMailTaskUpdated(oldTaskJson, newTaskJson):
        msg += "\nFound issue when sending mail for updating task params"
        current_app.logger.error(msg)
        # flash(msg, 'error')

    plComp = Comparator()
    plDiff = plComp.compare_dicts(oldTaskJson, newTaskJson)
    current_app.logger.info( 'manage:updateParams> task params updated: %s ' % str(plDiff) )

    msg += '\nTask params successfully updated.'
    flash(msg)
    return redirect( url_for( 'manage.%s' % itemType, code=item.code ) )


def updateItem( form, managers, code, itemType, item, what) :

    if what == 'description':
        item.description = form.description.data
    elif what == 'name':
        item.name = form.name.data

    try:
        commitUpdated(item)
    except Exception as e:
        flash('Error from DB when removing manager.\n(%s)' % str(e))
        return redirect( url_for('manage.index') )

    msg = "Successfully updated %s for %s" % (what, item.name)
    flash( msg )
    return redirect( url_for( 'manage.%s' % itemType, code=item.code ) )

def canManageItem(what, code):

    canManage = False
    if ( current_user.is_administrator() or
         current_user.canManage(what, code, year=session['year']) ):
        return True

    msg = 'You are not a manager for this %s, code %s, sorry.' % (what, code)
    current_app.logger.error(msg)
    flash(msg, 'error')

    return False

def changeActivityForTask(task, newVal):

    try:
        newAct = db.session.query(CMSActivity).filter_by(id =newVal).one()
        task.activity = newAct
        task.activityId = newAct.id
        msg = 'Activity for task changed to id %d ' % newAct.id
        current_app.logger.info('manage:changeActivityForTask> %s' % msg)
        # flash( msg )
    except Exception as e:
        return logAndRedirect( 'got exception when trying to change Activity for task to %d: \n\t %s' % (newVal, str(e)),
                        url_for( 'manage.task', code=task.code ) )

    try:
        commitUpdated(task)
    except Exception as e:
        return logAndRedirect( 'Error from DB when updating task.\n(%s)' % str(e), url_for('manage.task', code=task.code), 'error' )

    msg = 'task info for %s successfully updated - new activity is %s.' % (task.name, newAct.name)
    current_app.logger.info( 'manage:changeActivityForTask> %s' % msg )
    flash(msg)

    return redirect( url_for( 'manage.task', code=task.code ) )

def changeLevel3ForTask(task, newVal):

    current_app.logger.info('manage:changeLevel3ForTask> newVal: %s' % newVal)
    lvl3 = newVal
    if not lvl3 or lvl3 is None:
        lvl3 = db.session.query(Level3Name).filter_by(name='Unknown').one()
        lvl3ID = lvl3.id
        lvl3Name = lvl3.name
    else:
        # lvl3ID, lvl3Name = [x.strip() for x in lvl3.split(' : ') ]
        lvl3ID, lvl3Name = re.match('^\s*(\d+)\s*:\s*(.*)\s*', lvl3).groups()

    try:
        newL3 = db.session.query(Level3Name).filter_by(id = lvl3ID).one()
        task.level3 = lvl3ID
        msg = 'Level3Name for task changed to %s (%d) ' % (newL3.name, newL3.id)
        current_app.logger.info('manage:changeLevel3ForTask> %s' % msg)
        # flash( msg )
    except Exception as e:
        return logAndRedirect( 'manage:changeLevel3ForTask> Got exception when trying to change Level3Name for task to %d: \n\t %s' % (newVal, str(e)),
                        url_for( 'manage.task', code=task.code ) )

    try:
        commitUpdated(task)
    except Exception as e:
        return logAndRedirect( 'manage:changeLevel3ForTask> Error from DB when updating task.\n(%s)' % str(e), url_for('manage.task', code=task.code), 'error' )

    msg = 'task info for %s successfully updated - new level3Name is %s (%s).' % (task.name, newL3.name, newL3.id)
    current_app.logger.info('manage:changeLevel3ForTask> %s' % msg )
    flash(msg)

    return redirect( url_for( 'manage.task', code=task.code ) )

def updateTaskInfo( form, managers, code, itemType, item, what, canManage) :

    if what == 'description':
        item.description = form.description.data
    elif what == 'name':
        item.name = form.name.data
    elif what == 'params':
        return updateParams( form, itemType, item, canManage )

    try:
        commitUpdated(item)
    except Exception as e:
        flash('Error from DB when removing manager.\n(%s)' % str(e))
        return redirect( url_for('manage.index') )

    msg = "Successfully updated %s for %s" % (what, item.name)
    flash( msg )
    return redirect( url_for( 'manage.%s' % itemType, code=item.code ) )

def handleForm(request, form, patType, code, item, mgrs, obs, canManage, activePledges=None):

    current_app.logger.info("manage:handleForm> got form: %s " % str(request.form))

    if request.form.get( 'update', None ) :
        return updateItem( form, mgrs, code, patType, item, what='description' )

    if request.form.get( 'updateName', None ) :
        return updateItem( form, mgrs, code, patType, item, what='name' )

    if request.form.get( 'remove', None ) :
        return removeManagers( form, mgrs, patType, item, canManage, None )

    if request.form.get( 'addMgrs', None ) :
        return addMgrObs( 'manager', patType, code, item.name )

    if request.form.get( 'addMgtEgroup', None ) :
        return addMgtEgroup( 'manager', patType, code, item.name )

    if patType == 'task':
        if request.form.get('subCan', None) == 'Submit':
            return updateTaskInfo(form, mgrs, code, patType, item, what='params', canManage=canManage)
        if request.form.get( 'chgAct', None ): # change of activity requested
            return changeActivityForTask( item, newVal=request.form.get('activities') )
        if request.form.get( 'chgLvl3', None ): # change of activity requested
            return changeLevel3ForTask( item, newVal=request.form.get('level3') )
        if request.form.get('addObs', None):
            return addMgrObs('observer', patType, code, item.name)
        if request.form.get('removeObs', None):
            return removeObservers(form, obs, patType, item, canManage, activePledges)

    return redirect( url_for( 'manage.%s' % patType, code=code ) )

@manage.route('/project/<string:code>', methods=['GET', 'POST'])
@login_required
def project( code ) :

    try:
        project = db.session.query(Project).filter_by(code=code).one()
    except Exception as e:
        return logAndRedirect( "can not find project for code %s " % (code,),
                        url_for('manage.index'), 'error' )

    # as we have a unique code here, we know which year that belongs to
    selYear = project.year
    session['year'] = selYear
    session.modified = True

    canManage = canManageItem('project', code)
    if not canManage: return redirect( url_for( 'main.showMine' ) )

    managers = db.session.query(Manager).filter_by( itemType='project', itemCode=project.code, status='active', role=Permission.MANAGEPROJ, year=selYear ).all()
    current_app.logger.debug( "found %s managers for proj %s in %s " % (len(managers), project.name, selYear) )

    form = ProjectManagerForm(obj=project)
    form.description.default = project.description

    form.managers.choices = [ ]
    form.managers.choices = [ ]
    if managers:
        for i,m in enumerate( managers ):
            if m.egroup is not None:
                name = m.egroup + '@cern.ch (eGroup)'
                form.managers.choices.append( (-999999+i, name) ) # assume we'll never have more than 10^6 eGroups
            else:
                user = db.session.query(EprUser).filter_by( id=m.userId ).one()
                form.managers.choices.append( (user.cmsId, user.name) )

    if form.validate_on_submit():
        return handleForm( request, form, 'project', code, project, managers, [], canManage)

    else :  # form did not validate:
        if form.errors :
            msg = '\nmanage/project> validate on submit failed ... :( \nform:\n'
            msg += str( form.errors )
            msg += '\n'
            current_app.logger.error( msg )
            flash( form, 'error' )

    current_app.logger.info( "manage.project> rendering for %s (%i managers)" % (code, len(managers)) )

    return render_template('manage/project.html',
                           project=project, form=form,
                           year = selYear,
                           managers=managers,
                           navInfo = getNavInfo( session['year'] ))

@manage.route('/activity/<string:code>', methods=['GET', 'POST'])
@login_required
def activity(code):

    current_app.logger.info( 'manage:activity> code %s, user : %s ' % (code, current_user) )
    current_app.logger.info( 'manage:activity> got form: %s' % str(request.form) )

    # first check if the activity exists, otherwise the check of manager will fail and result in a wrong error message
    try:
        activity = db.session.query(CMSActivity).filter_by( code=code ).one( )
    except Exception as e:
        return logAndRedirect( "can not find activity for code %s " % (code,), url_for('manage.index'), 'error' )

    # as we have a unique code here, we know which year that belongs to
    selYear = activity.year
    session['year'] = selYear
    session.modified = True

    canManage = canManageItem('activity', code)
    if not canManage: return redirect( url_for( 'main.showMine' ) )

    managers = db.session.query(Manager).filter_by( itemType='activity', itemCode=activity.code, status='active', role=Permission.MANAGEACT, year=selYear ).all()
    current_app.logger.debug( "found %s managers for act %s in %s " % (len(managers), activity.name, selYear) )

    form = ActivityManagerForm(obj=activity)
    form.description.default = activity.description
    form.name.default = activity.name

    form.managers.choices = [ ]
    if managers:
        for i,m in enumerate( managers ):
            if m.egroup is not None:
                name = m.egroup + '@cern.ch (eGroup)'
                form.managers.choices.append( (-999999+i, name) ) # assume we'll never have more than 10^6 eGroups
            else:
                user = db.session.query(EprUser).filter_by( id=m.userId ).one()
                form.managers.choices.append( (user.cmsId, user.name) )

    activeTasks = ( db.session.query( Task )
                      .filter( Task.activityId == activity.id )
                      .filter( Task.status == 'ACTIVE' )
                      .all() )

    if form.validate_on_submit():
        return handleForm( request, form, 'activity', code, activity, managers, [], canManage)

    else :  # form did not validate:
        if form.errors :
            msg = '\nactivity> validate on submit failed ... :( \nform:\n'
            msg += str( form.errors )
            msg += '\n'
            current_app.logger.error( msg )
            flash( form, 'error' )

    return render_template('manage/activity.html',
                           activity=activity, form=form,
                           managers=managers,
                           activeTasks = activeTasks,
                           year=selYear,
                           navInfo = getNavInfo( selYear ))

@manage.route('/task/<string:code>', methods=['GET', 'POST'])
@login_required
def task(code):

    try:
        task = db.session.query(Task).filter_by(code=code).one()
    except Exception as e:
        msg = "can not find task for code %s " % (code,)
        current_app.logger.error(msg)
        flash(msg, 'error')
        return redirect( url_for('manage.index') )

    # as we have a unique code here, we know which year that belongs to
    selYear = task.year
    session['year'] = selYear
    session.modified = True

    canManage = canManageItem('task', task.code)
    if not canManage: return redirect( url_for( 'main.showMine' ) )

    managers = db.session.query(Manager).filter_by( itemType='task', itemCode=task.code, status='active', year=selYear, role=Permission.MANAGETASK).all()
 
    mgrForm = TaskManagerForm(obj=task)
    mgrForm.description.default = task.description

    mgrForm.tType.choices = [(i, i) for i in getTaskTypes()]

    taskShiftTypeId = ''
    if task.shiftTypeId is not None:
        mgrForm.shiftTypeId.choices = [(task.shiftTypeId.strip().lower(), task.shiftTypeId.strip().lower())]
        taskShiftTypeId = task.shiftTypeId.strip().lower()
    else:
        mgrForm.shiftTypeId.choices = [ ('', '') ]
    if ('', '') not in mgrForm.shiftTypeId.choices:
        mgrForm.shiftTypeId.choices += [('', '')]
    mgrForm.shiftTypeId.choices += [(x, x) for x in getTaskShiftTypeIds() if x != taskShiftTypeId]

    observers = db.session.query(Manager).filter_by( itemType='task', itemCode=task.code, status='active', year=selYear, role=Permission.OBSERVE).all()
    mgrForm.observers.choices = [ ]
    if observers:
        for i,o in enumerate( observers ):
            if o.egroup is not None:
                name = o.egroup + '@cern.ch (eGroup)'
                mgrForm.observers.choices.append( (-999999+i, name) )
            else:
                user = db.session.query(EprUser).filter_by( id=o.userId ).one()
                mgrForm.observers.choices.append( (user.cmsId, user.name) )

    mgrForm.managers.choices = [ ]
    if managers:
        for i,m in enumerate( managers ):
            if m.egroup is not None:
                name = m.egroup + '@cern.ch (eGroup)'
                mgrForm.managers.choices.append( (-999999+i, name) ) # assume we'll never have more than 10^6 eGroups
            else:
                user = db.session.query(EprUser).filter_by( id=m.userId ).one()
                mgrForm.managers.choices.append( (user.cmsId, user.name) )

    mgrForm.activities.choices = [ (x.id, x.name) for x in getTaskActivitiesForProject( task.activity.projectId ) ]
    mgrForm.activities.process_data( task.activity.id )

    actL3 = db.session.query(Level3Name).filter_by(id = task.level3).one_or_none()
    if actL3 is None:
        actL3 = db.session.query(Level3Name).filter( Level3Name.name == 'Unknown').one()
        msg = 'task/%s> WARNING: no valid lvl3 name found for task - resetting to "Unknown" ' % code
        flash(msg)
        current_app.logger.warning(msg)

    current_app.logger.info("manage:tasks> task.level3  %s - %s " % (task.level3, actL3) )
    mgrForm.level3.process_data( '%s : %s' % (actL3.id, actL3.name) )
    current_app.logger.info("manage:tasks> mgrForm.level3.data %s " % mgrForm.level3.data )

    activePledges = ( db.session.query( Pledge, EprUser.name, EprInstitute.code )
                      .join( EprUser, Pledge.userId == EprUser.id )
                      .join( EprInstitute, Pledge.instId == EprInstitute.id )
                      .filter( Pledge.taskId == task.id )
                      .filter( Pledge.status != 'rejected' )
                      .all() )

    if mgrForm.validate_on_submit():

        subCan = request.form.get('subCan')
        if subCan in ['Cancel', 'Submit']:
            if subCan == 'Cancel':
                msg = 'Updating task (%s) was cancelled ... ' % code
                flash(msg)
                current_app.logger.info("manage:tasks> %s " % msg)
            elif subCan == 'Submit':
                msg = 'Going to update task (%s) ... ' % code
                current_app.logger.info("manage:tasks> %s " % msg)

        return handleForm( request, mgrForm, 'task', code, task, managers, observers, canManage, activePledges)

    else: # initial rendering or validation error ...
        if mgrForm.errors:
            msg = "task/%s> ERROR when validating form-fields: %s - %s" % (code, str(mgrForm.errors), mgrForm.level3.data )
            flash(msg)
            current_app.logger.warning(msg)

    return render_template('manage/task.html', canManage=canManage,
                           task = task,
                           lvl3Name = actL3,
                           lvl3Map = getLevel3NameMap(selYear),
                           year = selYear,
                           mgrForm = mgrForm,
                           managers = managers,
                           observers = observers,
                           activePledges = activePledges,
                           navInfo = getNavInfo( session['year'] ) )

def addMgrObs(userType, what, code, name):

    # print "going to add managers: for %s code %s (name: %s)" % (what, code, name)

    return render_template( 'manage/users.html',
                            what=what, patCode=code,
                            userType=userType,
                            name=name,
                            navInfo = getNavInfo( session['year'] ) )

def addMgtEgroup(userType, what, code, name):

    # print "going to add mgt eGroup: for %s code %s (name: %s)" % (what, code, name)

    return render_template( 'manage/mgtEgroup.html',
                            what=what, patCode=code,
                            userType=userType,
                            name=name,
                            navInfo = getNavInfo( session['year'] ) )

@manage.route('/managers/<string:what>', methods=['GET', 'POST'])
@login_required
def managers(what):

    # print '++> managers> form: ', request.form
    name = request.form.get(what)

    current_app.logger.info( "manage.managers(%s)> form: %s " % (what, str(request.form) ) )

    patCode = request.form.get("patCode")

    # users = db.session.query(User.id, User.name, Institute.code, User.cmsId, User.hrId).join(Institute).filter(Institute.id==User.mainInst).all()
    # current_app.logger.info("manage:managers> got %i users" % len(users))

    return render_template( 'manage/users.html',
                            what=what, patCode=patCode,
                            userType='manager',
                            # users=users,
                            # inst=db.session.query(Institute).all(),
                            name=name,
                            navInfo = getNavInfo( session['year'] ) )

@manage.route('/egroup/<string:what>', methods=['GET', 'POST'])
@login_required
def egroup(what):

    patName = request.form.get(what)

    current_app.logger.info( "manage.egroup(%s)> form: %s " % (what, str(request.form) ) )

    patCode = request.form.get("patCode")

    return render_template( 'manage/mgtEgroup.html',
                            what=what, patCode=patCode,
                            # users=users,
                            # inst=db.session.query(Institute).all(),
                            patName = patName,
                            navInfo = getNavInfo( session['year'] ) )


@manage.route('/projMgr', methods=['POST'])
@login_required
def projectMgr():
    return managePAT()

def managePAT():
    #-toDo: check list against previous one and remove (set status to 'deleted') the ones no longer selected

    current_app.logger.info("manage.managePAT> got request %s " % request.form)

    selYear = session['year']

    users = None
    eGroup = None

    userType = request.form.get('userType', 'manager')
    userList = request.form.get('userList', None)
    eGname   = request.form.get('eGroup', None)
    if userList:
        users = [ db.session.query(EprUser).filter_by(cmsId=x).one() for x in request.form['userList'].split(',') if x.strip() ]
    if eGname:
        eGroup = eGname

    if not users and not eGroup:
        msg = 'userlist or eGroup invalid'
        current_app.logger.error( msg )
        return make_response( jsonify( { "status" : 'ERROR', "Error" : msg } ), 401 )

    patType = request.form.get('patType','')
    patCode = request.form.get('patCode','')

    if users:
        current_app.logger.info( "manage.managePAT> got request for type '%s' code '%s' - users %s " % ( patType, patCode, ','.join([u.username for u in users]) ) )
    if eGroup:
        current_app.logger.info( "manage.managePAT> got request for type '%s' code '%s' - eGroup %s " % ( patType, patCode, eGroup) )

    patItem = None
    roleType = None
    mgrObsTypeName = 'manager'
    if 'project' in patType:
        roleType = Permission.MANAGEPROJ
        try:
            patItem = db.session.query(Project).filter_by(code=patCode, year=selYear).one()
        except Exception as e:
            return make_response( jsonify({"status" : 'ERROR', "Error" : 'no %s found for %s' % (patType, patCode) }), 401 )

    if 'activity' in patType:
        roleType = Permission.MANAGEACT
        try:
            patItem = db.session.query(CMSActivity).filter_by(code=patCode, year=selYear).one()
        except Exception as e:
            return make_response( jsonify({"status" : 'ERROR', "Error" : 'no %s found for %s' % (patType, patCode) }), 401 )

    if 'task' in patType:
        if 'manager' in userType:
            roleType = Permission.MANAGETASK
        elif 'observer' in userType:
            roleType = Permission.OBSERVE
            mgrObsTypeName = 'observer'
        else:
            current_app.logger.error('manage.managePAT> invalid userType found: %s'  % userType)

        try:
            patItem = db.session.query(Task).filter_by(code=patCode, year=selYear).one()
        except Exception as e:
            return make_response( jsonify({"status" : 'ERROR', "Error" : 'no %s found for %s' % (patType, patCode) }), 401 )

    current_app.logger.info( "manage.managePAT> found %s code %s name %s - role %s (%s, %s)" % (patType, patCode, patItem.name, roleType, userType, mgrObsTypeName) )

    # make sure we did find a valid proj/act/task:
    if patType and patCode and patItem:
        oldMgrs = ( db.session.query(Manager.userId)
                              .filter(Manager.itemType==patType,
                                      Manager.itemCode==patCode,
                                      Manager.status=='active',
                                      Manager.role==permsMap[patType],
                                      Manager.year==selYear)
                    .all() )
        oldObs = []
        if 'task' in patType:
            oldObs = ( db.session.query(Manager.userId)
                                  .filter(Manager.itemType==patType,
                                          Manager.itemCode==patCode,
                                          Manager.status=='active',
                                          Manager.role==roleType,
                                          Manager.year==selYear)
                        .all() )

        uList = ''
        msg = ''
        if users:
            for u in users:
                # only add if not already in the list
                oldOnes = oldMgrs
                if 'observer' in userType : oldOnes = oldObs
                if (u.id,) not in oldOnes:
                    pm = Manager(itemType=patType, itemCode=patCode, userId=u.id, role=roleType, year=selYear)
                    current_app.logger.info( "manage.managePAT> adding new %s: %s for %s %s" % (mgrObsTypeName, pm, patType, patItem.name) )
                    uList += ' %s' % str(u.name)
                else:
                    msg = "manage.managePAT> %s already in list of %s(s) for %s %s " % (u.name, mgrObsTypeName, patType, patItem.name)
                    current_app.logger.warning(msg)
                    flash( "User %s is already in list of %s(s) for %s " % (u.name, mgrObsTypeName, patItem.name) )
                    return redirect( url_for('manage.%s'%(patType), code=patCode) )

                try:
                    commitNew(pm)
                except Exception as e:
                    return logAndRedirect('Error from DB when adding new %s %s to %s %s.\n(%s)' % (patType, mgrObsTypeName, patType, patItem.name, str(e)),
                                   url_for('manage.index') )
            try :
                msg += 'User(s) %s added as new %s(s) to %s %s' % (uList, mgrObsTypeName, patType, patItem.name)
            except :
                msg += 'User(s) %s added as new %s(s) to %s %s' % (uList, mgrObsTypeName, patType, patItem.code)

        if eGroup:
            #-toDo: check if eGroup exists
            # only add if not already in the list
            if (eGroup,) not in (db.session.query( Manager.egroup )
                                        .filter( Manager.itemType == patType,
                                                 Manager.itemCode == patCode,
                                                 Manager.status == 'active',
                                                 Manager.role == permsMap[ patType ],
                                                 Manager.year == selYear )
                                        .all()):
                ap = db.session.query(EprUser).filter_by(username='pfeiffer').one() # set some default user to satisfy the remote userId requirement in the DB
                egName = eGroup.replace('@cern.ch', '').lower()
                pm = Manager(itemType=patType, itemCode=patCode, userId=ap.id, role=roleType, year=selYear, egroup=egName)
                msg = "Adding new eGroup %s to mgr: %s for %s %s" % (egName, pm, patType, patItem.name)
                current_app.logger.info( 'manage.managePAT> %s' % msg )
                uList += '%s@cern.ch' % eGroup.replace('@cern.ch', '')
            else:
                return logAndRedirect( "manage.managePAT> eGroup %s already in list of mgrs for %s %s - ignoring request" % (eGroup, patType, patItem.name),
                                url_for('manage.%s'%(patType), code=patCode), 'error' )

            try:
                commitNew(pm)
            except Exception as e:
                return logAndRedirect( 'Error from DB when adding new %s manager to %s %s.\n(%s)' % (patType, patType, patItem.name, str(e)), url_for('manage.index'), 'error' )

            msg += 'eGroup(s) %s added to manager(s) of %s %s' % (uList, patType, patItem.code)

        current_app.logger.info( 'manage.managePAT> ' + msg )
        flash( msg )
        return redirect( url_for('manage.%s'%(patType), code=patCode) )
    else:
        return make_response( jsonify({"status" : 'ERROR', "Error" : 'no project/activity/task given'}), 401 )
    #-toDo: logging

    return redirect( url_for('manage.index') )

@manage.route('/activityMgr', methods=['POST'])
@login_required
def activityMgr():
    return managePAT()

@manage.route('/taskMgr', methods=['POST'])
@login_required
def taskMgr():
    return managePAT()

@manage.route('/allUsers', methods=['GET', 'POST'])
@login_required
def allUsers():

    # if not current_user.is_administrator():
    #     flash('Sorry you are not authorised for this action')

    return render_template('manage/allUsers.html', users = db.session.query(EprUser).all(),
                           allInst = db.session.query(EprInstitute).order_by(EprInstitute.name).all(),
                           navInfo = getNavInfo( session['year'] ) )

@manage.route('/manageInstUsers/<string:code>', methods=['GET'])
@login_required
def manageInstUsers(code):

    selYear = session.get('year')

    if ( not ( current_user.is_wizard()
               # or current_user.canManage('institute', code, year=selYear)
             ) ):
        flash('Sorry, this action is not yet available.', 'error')
        return redirect( url_for('main.index') )

    return render_template( 'manage/manageInstUsers.html',
                            form=None, year=selYear, code=code,
                            navInfo = getNavInfo( session['year'] ) )

@manage.route('/changeUserInst/<int:cmsId>', methods=['GET'])
@login_required
def changeUserInst(cmsId):

    selYear = session.get('year')
    user, oldInst = db.session.query(EprUser, EprInstitute.code).join(EprInstitute, EprUser.mainInst == EprInstitute.id).filter(EprUser.cmsId == cmsId).one()

    if ( not ( current_user.is_wizard()
               # or current_user.canManageUser(user, year=selYear)
             ) ):
        flash('Sorry, you are not authorised for this action.', 'error')
        return redirect( url_for('main.index') )

    # here we assume that we change only from the up-to-date entries for the user:
    user = db.session.query(EprUser).filter_by(cmsId=cmsId).one()
    oldInst = db.session.query(EprInstitute).filter_by(id=user.mainInst).one()

    return render_template( 'manage/changeUserInst.html',
                            form=None, year=selYear, user=user, oldInst=oldInst,
                            navInfo = getNavInfo( session['year'] ) )


@manage.route('/addUser', methods=['GET', 'POST'])
@login_required
def addUser():

    if not current_user.is_administrator():
        flash('Sorry you are not authorised for this action')
        return redirect( url_for('main.showMine') )

    form = AddUserForm()
    selYear = session['year']

    if form.validate_on_submit():

        dbUser = db.session.query(EprUser).filter_by(cmsId=form.cmsId.data).all()
        if len(dbUser) > 0:
            msg = "user with cmsId %i already in DB, not adding ... " % form.cmsId.data
            current_app.logger.error(msg)
            flash(msg, 'error')
            return render_template('manage/addUser.html', form=form ,
                                   navInfo = getNavInfo( session['year'] ))

        try:
            inst = db.session.query(EprInstitute).filter_by(code=form.instCode.data).one()
            current_app.logger.info('found inst for code %s: cernId %i' % (inst.code, inst.cernId))
        except Exception as e:
            msg = "ERROR no institute found for code %s " % form.instCode.data
            current_app.logger.error(msg)
            flash(msg, 'error')
            return render_template('manage/addUser.html', form=form ,
                                   navInfo = getNavInfo( session['year'] ))

        newUser = EprUser( username=form.username.data,
                        name=form.name.data,
                        hrId=form.hrId.data,
                        cmsId=form.cmsId.data,
                        instId=inst.cernId,
                        authorReq=form.authorReq.data,
                        status=form.status.data,
                        isSusp=form.isSusp.data,
                        categoryName=form.category.data,
                        year=selYear,
                        comment=form.comment.data,
                        mainInst=inst.id,
                        )
        current_app.logger.warning( '==++==++>>> EprUser> using db from: %s' % newUser.session().bind.engine.url )
        try:
            # commitNew(newUser)
            # db.session.add(newUser)
            db.session.commit()
        except Exception as e:
            return logAndRedirect( 'Error from DB when adding new user.\n(%s)' % str(e), url_for('manage.index'), 'error' )

        # also create a new TimeLineUser :
        cat = db.session.query(Category).filter_by(name=form.category.data).one()

        newTLUser = TimeLineUser()
        newTLUser.cmsId = form.cmsId.data,
        newTLUser.instCode = form.instCode.data
        newTLUser.status = form.status.data
        newTLUser.isAuthor = form.authorReq.data
        newTLUser.isSuspended = form.isSusp.data
        newTLUser.dueAuthor = 0.
        newTLUser.dueApplicant = 6.0
        newTLUser.yearFraction = 1.0
        newTLUser.category = cat.id
        newTLUser.comment = form.comment.data
        newTLUser.year = selYear

        try:
            # commitNew(newTLUser)
            # db.session.add(newTLUser)
            db.session.commit()
        except Exception as e:
            return logAndRedirect( 'Error from DB when adding new time line user.\n(%s)' % str(e), url_for('manage.index'), 'error' )

        flash( "User %s successfully added" % newUser.name )
        return redirect( url_for('manage.index') )

    return render_template('manage/addUser.html', form=form ,
                           navInfo = getNavInfo( session['year'] ))


@manage.route('/addInstitute', methods=['GET', 'POST'])
@login_required
def addInstitute():

    if not current_user.is_administrator():
        flash('Sorry you are not authorised for this action')
        return redirect( url_for('main.showMine') )

    form = AddInstituteForm()
    selYear = session['year']

    if form.validate_on_submit():
        newInstitute = EprInstitute( name=str(form.name.data),
                                  cernId=form.cernId.data,
                                  country=form.country.data,
                                  code=form.code.data,
                                  year=selYear,
                                  cmsStatus=form.status.data,
                                  comment=form.comment.data )

        try:
            commitNew(newInstitute)
        except Exception as e:
            flash('Error from DB when adding new institute.\n(%s)' % str(e))
            db.session.rollback()
            return redirect( url_for('manage.index') )

        flash("Institute %s successfully added" % newInstitute.name)
        return redirect( url_for('manage.index') )

    return render_template('manage/addInstitute.html', form=form ,
                           navInfo = getNavInfo( session['year'] ))

@manage.route('/manageProjTasks', methods=['POST'])
@manage.route('/manageProjTasks/<string:projName>', methods=['GET', 'POST'])
@login_required
def manageProjTasks(projName = None):

    current_app.logger.info( "manage.manageProjTasks> got request %s " % request.form )
    selYear = session[ 'year' ]

    if projName is None:
        projCode = request.form.get( 'projCode', None )
        proj = db.session.query(Project).filter_by(code=projCode, year=selYear).one()
        projName = proj.name
    else:
        proj = db.session.query(Project).filter_by(name=projName, year=selYear).one()

    if not proj:
        flash("no project with name %s found for year %s" % (projName, selYear), 'error')
        return redirect( url_for( 'manage.index' ) )

    if not ( current_user.is_administrator() or current_user.canManage('project', proj.code, selYear) ) :
        return logAndRedirect( "Sorry you are not allowed to manage project %s" % (projName,), url_for( 'manage.index' ), 'error' )

    current_app.logger.info( "managing project %s" % proj )

    return render_template( 'manage/manageProjTasks.html',
                            project=proj, year=selYear,
                            navInfo=getNavInfo(selYear) )


@manage.route('/mapShiftsToTasks', methods=['GET'])
@login_required
def mapShiftsToTasks():

    selYear = session.get('year')

    isMgr = False
    if not ( current_user.is_administrator() or current_user.isManager(mgtType='project', year=selYear) ) :
        isMgr = True

    session['referrer'] = url_for('manage.mapShiftsToTasks')
    return render_template( 'manage/mapShiftsToTasks.html', year=selYear, isMgr = isMgr,
                            navInfo = getNavInfo( session['year'] ) )


@manage.route('/shiftWeightScales', methods=['GET'])
@login_required
def shiftWeightScales():

    selYear = session.get('year')

    # https://twiki.cern.ch/twiki/bin/view/CMS/CentralShifts2017#Central_credit_accounting

    isMgr = False
    if ( current_user.is_administrator() or current_user.isManager(mgtType='project', year=selYear) ) :
        isMgr = True

    # # only handle mapped items
    # stmList = ( db.session.query( ShiftTaskMap.subSystem, ShiftTaskMap.shiftType, ShiftTaskMap.flavourName,
    #                               ShiftTaskMap.weight, ShiftTaskMap.id )
    #                       .filter_by( year=selYear, status='ACTIVE' )
    #                       .distinct(ShiftTaskMap.subSystem, ShiftTaskMap.shiftType, ShiftTaskMap.flavourName)
    #                       .all() )

    session['referrer'] = url_for('manage.shiftWeightScales')
    return render_template( 'manage/shiftWeightScales.html', year=selYear, isMgr = isMgr,
                            navInfo = getNavInfo( session['year'] ) )

@manage.route('/removeSTM/<int:idReq>', methods=['GET'])
@login_required
def removeSTM(idReq):

    selYear = session.get('year')

    if not ( current_user.is_administrator() or current_user.isManager(mgtType='project', year=selYear) ) :
        flash( "Sorry you are not allowed to manage shifts ", 'error' )
        return redirect( url_for( 'manage.mapShiftsToTasks' ) )

    current_app.logger.info('request to delete STM with id %s' % idReq)

    msg = None
    mapEntry = None
    try:
        mapEntry = db.session.query(ShiftTaskMap).filter_by(id=idReq, status='ACTIVE').one()
    except sa_exc.NoResultFound:
        msg = 'No ShiftTaskMap entry for id "%s" found.' % (idReq,)
    except Exception as e:
        msg = 'Exception when retrieving ShiftTaskMap entry "%s" - got "%s".' % (idReq, str(e))

    if msg or (mapEntry is None) :
        flash(msg, 'error')
        return redirect( url_for( 'manage.mapShiftsToTasks' ) )

    mapEntry.status = 'DELETED'
    mapEntry.timestamp = datetime.now()

    db.session.add(mapEntry)
    db.session.commit()

    flash( 'ShiftTaskMap entry with id %s successfully deleted' % idReq )

    return redirect( url_for( 'manage.mapShiftsToTasks' ) )


@manage.route('/editCeilings', methods=['GET'])
@login_required
def editCeilings():

    selYear = session.get('year')

    if not ( current_user.is_administrator() or current_user.isManager(mgtType='project', year=selYear) ) :
        flash( "Sorry you are not allowed to edit ceilings", 'error' )
        return redirect( url_for( 'manage.mine' ) )

    prjList = db.session.query(Project.code, Project.name)\
                        .filter(Project.year == selYear)\
                        .filter(Project.status == 'ACTIVE')\
                        .all()

    current_app.logger.info('request to edit ceilings for %d projects in %d' % (len(prjList), selYear) )

    session['referrer'] = url_for('manage.editCeilings')
    return render_template( 'manage/editCeilings.html',
                            year=selYear,
                            projects = prjList,
                            navInfo = getNavInfo( session['year'] ) )



@manage.route('/editUser', methods=['GET', 'POST'])
@manage.route('/editUser/<int:cmsId>', methods=['GET', 'POST'])
@login_required
def editUser(cmsId=None):

    if not current_user.is_administrator():
        flash('Sorry you are not authorised for this action')
        return redirect( url_for('main.showMine') )

    if not cmsId: cmsId = current_user.cmsId

    dbUser, dbInst = db.session.query( EprUser, EprInstitute ).join(EprInstitute, EprInstitute.id == EprUser.mainInst).filter( EprUser.cmsId == cmsId ).one()

    selYear = session['year']
    form = EditUserForm(obj=dbUser)
    form.instCode.data = dbInst.code

    if form.validate_on_submit():

        if form.username.data != dbUser.username:
            msg='updating username from %s to %s ' % (dbUser.username, form.username.data)
            flash(msg, 'info')
            dbUser.username = form.username.data
            current_app.logger.info( msg )

        if form.instCode.data != dbInst.code :
            msg='not changing institute ! '
            flash(msg, 'warning')
            current_app.logger.warning(msg)

        if form.name.data != dbUser.name :
            msg='not changing name ! '
            flash(msg, 'warning')
            current_app.logger.warning( msg )

        if form.status.data != dbUser.status :
            msg='not changing status ! '
            flash(msg, 'warning')
            current_app.logger.warning( msg )

        if form.category.data != dbUser.category :
            msg='not changing category ! '
            flash(msg, 'warning')
            current_app.logger.warning( msg )

        try:
            commitUpdated(dbUser)
        except Exception as e:
            msg = 'Error from DB when adding new user.\n(%s)' % str(e)
            current_app.logger.error(msg)
            flash(msg)
            return redirect( url_for('manage.index') )

        flash( "User %s successfully updated" % dbUser.name )
        return redirect( url_for('manage.index') )

    return render_template('manage/editUser.html', form=form, user=dbUser,
                           navInfo = getNavInfo( session['year'] ))

