import time
import json

from flask import jsonify, request, current_app, url_for
from flask_login import login_required, current_user

from . import api
from .. import db, cache

from sqlalchemy import and_, orm, case

from ..models import EprInstitute, AllInstView, TimeLineUser, TimeLineInst, EprUser, Project, CMSActivity, commitUpdated
from ..main.Helpers import getInstituteInfo, getInstAuthForProj

from ..main.Helpers import getPledgeYear, getInstUsers

from sqlalchemy_extensions import group_concat, QueryFactory

@api.route('/allInstitutes', methods=['GET', 'POST'])
@login_required
def allInstitutes():

    data = []
    for i in db.session.query(EprInstitute).all():
        data.append( i.to_json() )

    return jsonify( { 'institutes' : data }  )

@api.route('/searchInstCode', methods=['GET', 'POST'])
@login_required
def searchInstCode():

    search = None
    if 'GET' in request.method:
        search = request.args.get( 'term', None )
    elif 'POST' in request.method:
        search = request.form.get( 'term', None )

    if search is None : return jsonify ( { 'result' : [] })

    resDB = db.session.query(EprInstitute.code).\
                        filter(EprInstitute.code.ilike('%' + search + '%')).\
                        order_by(EprInstitute.code).all()

    # if nothing found yet, the user may have used the name in the search, try this:
    if not resDB:
        resDB = db.session.query(EprInstitute.code).\
                        filter(EprInstitute.name.ilike('%' + search + '%')).\
                        order_by(EprInstitute.code).all()

    results = []
    for (code,) in resDB:
        results.append( '%s' % code.strip() )

    return json.dumps( results )

@api.route('/searchInst', methods=['GET', 'POST'])
@login_required
def searchInst():

    search = None
    if 'GET' in request.method:
        search = request.args.get( 'term', None )
    elif 'POST' in request.method:
        search = request.form.get( 'term', None )

    current_app.logger.info( "search for %s using %s " % (search, request.method) )

    if search is None : return jsonify ( { 'result' : [] })

    resDB = db.session.query(EprInstitute.code, EprInstitute.name).\
                        filter(EprInstitute.code.ilike('%' + search + '%')).\
                        order_by(EprInstitute.code).all()

    # if nothing found yet, the user may have used the name in the search, try this:
    if not resDB:
        resDB = db.session.query(EprInstitute.code, EprInstitute.name).\
                        filter(EprInstitute.name.ilike('%' + search + '%')).\
                        order_by(EprInstitute.code).all()

    results = []
    for code, name in resDB:
        inst = db.session.query(EprInstitute).filter_by(code=code).one()
        results.append( '%s - %s' % (code, name) )

    return json.dumps( results )

@api.route('/instByName/<string:name>', methods=['GET', 'POST'])
@login_required
def instByName(name):
    inst = db.session.query(EprInstitute).filter_by(name=name).one()
    return jsonify( inst.to_json() )

@api.route('/instByCode/<string:codeName>', methods=['GET', 'POST'])
@login_required
def instByCode(codeName):

    code, name = codeName.split(' - ', 1)

    inst = db.session.query(EprInstitute).filter_by(code=code)\
                          .filter(EprInstitute.cmsStatus.in_( ['Yes', 'Cooperating', 'Associated']))\
                          .one()

    current_app.logger.info( "instByCode> code '%s', found: %s " % (code, inst.to_json() ) )

    return jsonify( { 'inst' : inst.to_json() } )

@api.route('/instituteInfo/<string:code>', methods=['POST'])
@login_required
def instituteInfo( code ):

    year = int( request.form.get('year', getPledgeYear()) )

    if code.lower( ) == 'all':
        instInfo = getInstituteInfo( code, year )
        return jsonify( { 'institute' : 'all',
                          'data' : instInfo } )

    institute, userInfo, summary = getInstituteInfo( code, year )

    return jsonify( { 'institute' : institute.to_json(),
                      'data' : userInfo,
                      'summary' : summary} )

@api.route('/instituteOverview', methods=['GET', 'POST'])
@login_required
def instituteOverview():

    year = int( request.form.get('year', getPledgeYear()) )
    current_app.logger.info( "got request for overview for year %s" % year )

    instStats = {}
    instList = (db.session.query( EprInstitute.code, EprInstitute.cmsStatus, EprInstitute.country)
                          .filter( EprInstitute.cmsStatus.in_( ['Yes', 'Cooperating', 'Associated']))
                          .all() )
    for item in instList :
        k, v, c = item
        instStats[k] = (v, c)

    data = [ ]
    for x in (db.session.query(AllInstView)
                          .join(EprInstitute, AllInstView.code == EprInstitute.code)
                          .filter(AllInstView.year == year)
                          .filter(EprInstitute.cmsStatus.in_( ['Yes', 'Cooperating', 'Associated']))
                          .all()) :
        instInfo = x.to_json()
        instInfo[ 'cmsStatus' ] = instStats[ x.code ][0]
        instInfo[ 'country' ] = instStats[ x.code ][1]

        if instInfo[ 'cmsStatus' ] != 'Yes': # reset applicant due for Cooperating and Associated institutes
            instInfo['sumEprDue'] = 0.

        data.append( instInfo )

    current_app.logger.info( "len of data %s " % len(data) )

    return jsonify( { 'institute' : 'all',
                      'data' : data } )

@api.route('/manageInstUsers', methods=['POST'])
@login_required
def manageInstUsers():

    year = int( request.form.get('year', getPledgeYear()) )
    data = request.form.get('data', None)
    instCode = request.form.get('instCode', None)

    current_app.logger.info( "manageInstUsers> got: year %i inst %s data (len: %i) : " % (year, instCode, len(data.split('&')) ) )
    current_app.logger.info( str(data) )

    res = ( db.session.query(TimeLineUser.cmsId,
                             TimeLineUser.isSuspended,
                             TimeLineUser.isAuthor,
                             TimeLineUser)
                      .filter(TimeLineUser.year == year)
                      .filter(TimeLineUser.instCode == instCode)
                      .all())

    suspReq = {}
    for (id,val) in [ x.replace('susp-','').split('=') for x in data.split('&') if x.startswith('susp-')]:
        if   'yes' in val.lower() : suspReq[int(id)] = True
        elif 'no'  in val.lower() : suspReq[int(id)] = False

    authReq = {}
    for (id,val) in [ x.replace('auth-','').split('=') for x in data.split('&') if x.startswith('auth-')]:
        if   'yes' in val.lower() : authReq[int(id)] = True
        elif 'no'  in val.lower() : authReq[int(id)] = False

    nUpdated = 0
    for cmsId, suspDb, authDb, tlUser in res:

        # check if change in suspension status requested:
        if cmsId in suspReq.keys() and suspDb != suspReq[cmsId]:
            current_app.logger.info( "manageInstUsers> going to update entry for %i (isSuspended) from %s to %s" % (cmsId, suspDb, suspReq[cmsId]) )
            tlUser.isSuspended = suspReq[cmsId]
            commitUpdated(tlUser)
            nUpdated += 1
            # now also update the User info
            user = db.session.query(EprUser).filter_by(cmsId=cmsId).one()
            user.isSuspended = suspReq[cmsId]
            commitUpdated(user)

        # check if change in authorship status requested
        if cmsId in authReq.keys() and authDb != authReq[cmsId]:
            current_app.logger.info( "manageInstUsers> going to update entry for %i (isAuthor) from %s to %s" % (cmsId, authDb, authReq[cmsId]) )
            tlUser.isAuthor = authReq[cmsId]
            commitUpdated(tlUser)
            nUpdated += 1
            # now also update the User info
            user = db.session.query(EprUser).filter_by(cmsId=cmsId).one()
            user.authorReq = authReq[cmsId]
            commitUpdated(user)

    return jsonify( { 'status' : 'successfully updated info for %i users' % nUpdated } )

@api.route( '/manageProjTasks', methods=[ 'POST' ] )
@login_required
def manageProjTasks( ) :

    year = int( request.form.get( 'year', getPledgeYear() ) )
    data = request.form.get( 'data', None )
    projCode = request.form.get( 'projCode', None )

    current_app.logger.info( "manageProjTasks> got: year %i proj %s data (len: %i) : " % (year, projCode, len( data.split( '&' ) )) )
    current_app.logger.info( str(data) )

    proj = db.session.query(Project).filter_by(code=projCode, year=year).one()

    acts = db.session.query(CMSActivity).filter_by(projectId=proj.id, year=year).all()
    tasks = []
    for a in acts:
        tasks += a.tasks

    statReq = { }
    for (tCode, val) in [ x.replace( 'taskStat-', '' ).split( '=' ) for x in data.split( '&' ) if x.startswith( 'taskStat-' ) ] :
        if 'active' in val.lower( ) :
            statReq[ tCode ] = 'ACTIVE'
        elif 'deleted' in val.lower( ) :
            statReq[ tCode ] = 'DELETED'

    lockReq = { }
    for (tCode, val) in [ x.replace( 'taskLock-', '' ).split( '=' ) for x in data.split( '&' ) if x.startswith( 'taskLock-' ) ] :
        if 'yes' in val.lower( ) :
            lockReq[ tCode] = True
        elif 'no' in val.lower( ) :
            lockReq[ tCode] = False

    nUpdated = 0
    for t in tasks:
        # check if there is a request to change the status
        if t.code in statReq.keys() and t.status != statReq[t.code]:
            current_app.logger.info( "manageProjTasks> going to update entry for %s (status) from %s to %s" % (t.code, t.status, statReq[t.code]) )
            t.status = statReq[t.code]
            commitUpdated(t)
            nUpdated += 1

        # check if there is a request to change the locking
        if t.code in lockReq.keys( ) and t.locked != lockReq[ t.code ] :
            current_app.logger.info( "manageProjTasks> going to update entry for %s (locked) from %s to %s" % (t.code, t.locked, lockReq[ t.code ]) )
            t.locked = lockReq[ t.code ]
            commitUpdated( t )
            nUpdated += 1

    return jsonify( { 'status' : 'successfully updated info for %i requests' % nUpdated } )


@api.route( '/instAuthorsForProject', methods=[ 'POST' ] )
@login_required
def instAuthorsForProject( ) :

    year = int( request.form.get( 'year', getPledgeYear() ) )
    projCode = request.form.get( 'projCode', None )

    current_app.logger.info( "instAuthorsForProject> got: year %i proj %s : " % (year, projCode) )

    projList, instList, authInstMap = getInstAuthForProj(projCode, year)

    return jsonify( {'projCode' : projCode,
                     'nAuthors' : authInstMap,
                     'projList' : sorted(list(projList)),
                     'instList' : sorted( list( instList ) )
                     } )


@api.route( '/instUsers', methods=[ 'POST' ] )
@login_required
def instUsers( ) :

    selYear = int( request.form.get( 'year', getPledgeYear() ) )
    instCode = request.form.get( 'instCode', None )

    current_app.logger.info( "instUsers> request for users of inst %s in year %i: " % (instCode, selYear) )

    res = getInstUsers( instCode, selYear )
    data = [ ]
    for i, n, a, appDue, authDue, author, uId, username, wPld in res :
        data.append( { 'sel' : ' ', 'name' : n,
                       'due' : authDue, 'appDue' : appDue,
                       'author' : author, 'cmsId' : i,
                       'pledged': wPld if wPld else 0.,
                       'username': username } )

    current_app.logger.info( "instUsers> found %d users for %s in %d : " % ( len(data), instCode, selYear) )

    return jsonify( { 'data': data } )

@api.route( '/instHistInfo', methods=[ 'POST' ] )
@login_required
def instHistInfo( ):

    selYear = int( request.form.get('year', getPledgeYear()) )
    yearRange = range(2017, selYear+1)   # [selYear-3, selYear-2, selYear-1]
    code = request.form.get('iCode', None)

    current_app.logger.info('\n+++++ instHistInfo> found request for %s in %s \n' % (code, yearRange) )

    res = (db.session.query( AllInstView.year, AllInstView.expected,
                             AllInstView.done, AllInstView.doneFract,
                             AllInstView.eprAccounted, AllInstView.eprAccFract,
                             AllInstView.sumEprDue
                           )
                          .filter( AllInstView.code==code )
                          .filter( AllInstView.year.in_( yearRange ) )
                          .order_by(AllInstView.year)
                          .all()
                )

    histInfo = []
    for item in res:
        histInfo.append( { "year" : item[0],
                           "expected" : item[1]+item[6],
                           "done" : item[2],
                           "doneFract" : item[3],
                           "accounted" : item[4],
                           "acctFract" : item[5],
                           "due" : item[6],
                           } )

    current_app.logger.info('instHistInfo> found histInfo for %s in %s: %s ' % (code, yearRange, histInfo) )

    return jsonify( { "data" : histInfo } )
