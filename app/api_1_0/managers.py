
from datetime import datetime
import re

from flask import jsonify, request, current_app, url_for
from flask_login import login_required, current_user

from . import api
from .. import db, cross_origin
from ..models import Manager, Permission, permsMap, EprUser, commitUpdated, Project, CMSActivity, Task

from ..main.MailHelpers import getMgrEmails
from ..main.Helpers import getAPTManagers, getPledgeYear

def getPatMgr( pat, patName, selYear):
    data = []
    for m, u, p in (db.session.query(Manager, EprUser, pat)
                        .join( EprUser, Manager.userId == EprUser.id )
                        .join( pat, Manager.itemCode == pat.code)
                        .filter( Manager.year == selYear )
                        .filter( Manager.status == 'active' )
                        .filter( Manager.itemType == patName)
                        .order_by( Manager.itemCode )
                        .all()):
       data.append( { 'item_type': m.itemType,
                    'id' : m.id,
                    'item_code': p.name,
                    'year': m.year,
                    'users': { 'name': u.name, 'cmsid': u.cmsId }
                } )

    return data

@api.route('/managers', methods=['GET'])
# @login_required
@cross_origin()
def getManagers():

    selYear = request.form.get('year', 2021)

    start = datetime.now()
    data = []
    # institute managers:
    for m, u in (db.session.query(Manager, EprUser)
                               .join(EprUser, Manager.userId == EprUser.id)
                               .filter( Manager.year == selYear )
                               .filter( Manager.status == 'active' )
                               .filter( Manager.itemType == 'institute')
                               .order_by( Manager.itemCode )
                               .all()):
        data.append( { 'item_type': m.itemType,
                        'id' : m.id,
                        'item_code': m.itemCode,
                        'year': m.year,
                        'users': { 'name': u.name, 'cmsid': u.cmsId }
                    } )

    mgrs = { 'institute': len(data) }

    # pat managers:
    for pat, patName in [ (Project, 'project'), (CMSActivity, 'activity'), (Task, 'task')]:
        res = getPatMgr( pat, patName, selYear )
        mgrs.update( { patName: len(res) } )
        data += res

    current_app.logger.info( '\ngetManagers> managers found: %s (query took %s)' % (mgrs, datetime.now()-start) )

    return jsonify( data )


@api.route('/allManagers')
@login_required
def getAllManagers():

    data = []
    for u in db.session.query(Manager).all():
        data.append( u.to_json() )

    current_app.logger.info( '\ngetAllManagers> data is %s ' % str(data) )

    return jsonify( { 'data' : data } )

@api.route('/updateManager', methods=['POST'])
@login_required
def updateManager():

    itemType = request.form.get('itemType')
    itemCode = request.form.get('itemCode')
    userIdIn = request.form.get('userid')

    if itemCode.strip() == '' or itemType.strip() == '' or userIdIn == '':
        msg = "Error: invalid values given for itemType:itemCode:userId '%s:%s:%s' " % (itemType, itemCode, userIdIn)
        current_app.logger.error( msg )
        return jsonify( {'status' : 'error', 'msg' : msg } )

    if (not (current_user.is_administrator( )
             # or current_user.canManage('institute', code, year=selYear)
             )) :
        msg = "Error: Sorry you are not allowed to update the manager info for %s:%s:%s " % (itemType, itemCode, userIdIn)
        current_app.logger.error( msg )
        return jsonify( {'status' : 'error', 'msg' : msg } )

    roleIn   = request.form.get('role')
    status   = request.form.get('status')

    # retrieve the requested manager item:
    try:
        userId = int( userIdIn )

        # set default
        if roleIn.strip( ) == '' :
            role = Permission.READ
        else :
            role = int( roleIn )

        mgr = db.session.query(Manager).filter_by(itemType=itemType, itemCode=itemCode, userId=userId).one()

        # update values and commit
        mgr.role = role
        mgr.status = status
        mgr.timestamp = datetime.utcnow()

        commitUpdated(mgr)

    except Exception as e:
        msg = "Error updating manager for %s:%s:%s - got %s " % (itemType, itemCode, userIdIn, str(e))
        current_app.logger.error( msg )
        return jsonify( {'status' : 'error', 'msg' : msg } )

    msg = 'manager for %s:%s:%s successfully updated to role %s status %s' % (itemType, itemCode, userId, role, status)
    return jsonify( {'status' : 'OK', 'msg' : msg } )


@api.route('/getManager', methods=['GET', 'POST'])
@login_required
def getManager():

    proj = request.form.get('project',  None)
    act  = request.form.get('activity', None)
    task = request.form.get('task'   ,  None)
    year = request.form.get('year'   ,  None)
    if not year:
        year = getPledgeYear()

    mgrs, mgrsFull = getAPTManagers( act, proj, task, year )

    return jsonify( { 'mgr' : mgrs } )


@api.route('/getObservers/<string:task>', methods=['GET', 'POST'])
@login_required
def getObservers(task):

    year = request.form.get('year'   ,  None)
    if not year:
        year = getPledgeYear()

    obsList = ( db.session.query( Manager, EprUser.name )
                          .filter( Manager.itemType == 'task' )
                          .filter( Manager.itemCode == task )
                          .filter( Manager.role == Permission.OBSERVE )
                          .filter( Manager.year == year )
                          .filter( Manager.status == 'active' )
                          .filter( Manager.userId == EprUser.id )
                          .all()
                )
    observers = []
    oNames = []
    for o, oName in obsList:
        observers.append( o.to_json() )
        oNames.append( oName )

    return jsonify( { 'obs' : { 'names': oNames, 'obsFull' : observers } } )


@api.route('/getPMemails', methods=['POST'])
@login_required
def getPMemails():

    proj = request.form.get('project',  None)
    return jsonify( { 'pmEmails' : getMgrEmails('project', proj) } )
