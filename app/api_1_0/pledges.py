import re
import json
import time
import datetime

from flask_login import login_required, current_user

from flask import jsonify, request, current_app, url_for, flash

from sqlalchemy import func

from . import api
from ..models import Pledge, EprUser, Task, CMSActivity, Project, EprInstitute, Level3Name, commitNew, commitUpdated, CMSDbException
from ..main.Helpers import getPledgeInstInfo, getGuestPledgeInstInfo, getPledgeInfoByTaskIDs, getPledgeInfoByPledgeCodes, \
                            doPledgeSplit, getLevel3NameMap
from ..main.HistoryHelpers import getPledgeHistory
from ..main.MailHelpers import sendMailPledgeCreated, sendMailPledgeRejected, sendMailPlegdeUpdated

from ..main.Helpers import getPledgeYear, getInstUsers, getManagers, editPlTmpl, editTaskTmpl, rejectPlTmpl, getInstRespPledgeSummary, \
                           canManageSelectedInst, addToFavs, isInstManager, getPledgeSum, canManageProject

from .. import db, cache

@api.route('/pledges')
@login_required
def getAllPledges():

    data = []
    for t in db.session.query(Pledge).all():
        data.append( t.to_json() )

    current_app.logger.info( '\n data is %s ' % str(data) )

    return jsonify( { 'data' : data } )

@api.route('/pledge/<int:id>')
@login_required
def getPledge(id):
    pledge = db.session.query(Pledge).filter_by(id=id).one()
    return jsonify( pledge.to_json() )

@api.route('/pledgeByCode/<string:code>')
@login_required
def getPledgeByName(code):
    taskId, userId, instId = code.split(':')
    pledge = db.session.query(Pledge).filter_by(taskId=taskId, userId=userId, instId=instId).one()
    return jsonify(pledge.to_json())

@api.route('/pledgesForTask', methods=['POST'])
@login_required
def pledgesForTask():

    code = request.form.get( 'code', None )
    code = code.replace('_', '/')

    try:
        task = db.session.query(Task).filter_by(code=code).one()
    except Exception as e:
        msg =  'pledgesForTask> no task found for code %s - got %s ' % (code, str(e))
        current_app.logger.info( msg )
        return jsonify( { "error": msg } )

    current_app.logger.info( "pledgesForTask> found task %s " % task.code )

    pledges = db.session.query(Pledge).filter_by(taskId = task.id).filter(Pledge.status!='rejected').all()
    info = ''
    data = [ ['User (Inst)', 'PledgeInst', 'Pledged', 'Accepted', 'Done'] ]
    for p in pledges:
        user = db.session.query(EprUser).filter_by(id=p.userId).one()
        userInst = db.session.query(EprInstitute).filter_by(id=user.mainInst).one()
        plInst   = db.session.query(EprInstitute).filter_by(id=p.instId).one()
        info += '<br /> '+user.name + ' : ' + str(p.workTimePld)
        userLink = '<a href="%s"> %s </a>' % (url_for('main.showUser', username=user.username), user.name+' ('+userInst.code+')')
        instLink = '<a href="%s"> %s </a>' % (url_for('main.showInstitute', code=plInst.code), plInst.code)
        data.append( [ userLink , instLink, str(p.workTimePld), str(p.workTimeAcc), str(p.workTimeDone) ] )

    current_app.logger.info("pledgesForTask> found data for %i pledges " % (len(pledges)) )

    return jsonify( { 'nPledges': len(pledges), 'info' : info, 'data' : data } )

@api.route('/pledgesForInst', methods=['POST'])
@login_required
def pledgesForInst():

    instCode = request.form.get('instCode', None)
    year = request.form.get('year', getPledgeYear())

    pledgeList = getPledgeInstInfo(instCode, year)
    if pledgeList is None:
        pledgeList = []

    return jsonify( { 'allProjects' : pledgeList }  )

@api.route('/guestPledgesForInst', methods=['POST'])
@login_required
def guestPledgesForInst():

    instCode = request.form.get('instCode', None)
    year = int(request.form.get('year', getPledgeYear()))

    current_app.logger.info( "guestPledgesForInst> request for %s in %i " % (instCode, year) )

    pledgeList = getGuestPledgeInstInfo(instCode, year)
    if pledgeList is None:
        pledgeList = []

    return jsonify( { 'guestPledges' : pledgeList }  )

@api.route('/pledgesForTaskCodes', methods=['POST'])
@login_required
def pledgesForTaskCodes():

    taskCodesStr = request.form.get( 'taskCodes', None )
    year = int(request.form.get('year', getPledgeYear()))

    if not taskCodesStr:
        msg =  'pledgesForTaskCodes> no taskCodes found in request '
        current_app.logger.info( msg )
        return jsonify( { "error": msg } )

    taskCodes = taskCodesStr.split(',')
    taskIDs = ( db.session.query( Task.id ).filter(Task.code.in_(taskCodes)).all() )

    pledges = getPledgeInfoByTaskIDs(taskIDs)

    current_app.logger.info("pledgesForTaskCodes> found data for %i pledges " % (len(pledges)) )

    return jsonify( { 'allProjects': pledges } )

@api.route('/pledgesForPledgeCodes', methods=['POST'])
@login_required
def pledgesForPledgeCodes():

    plCodesStr = request.form.get( 'plCodes', None )
    year = int(request.form.get('year', getPledgeYear()))

    current_app.logger.debug( 'pledgesForPledgeCodes> request for pledges with codes %s ' % plCodesStr )

    if not plCodesStr:
        msg =  'pledgesForPledgeCodes> no plCodes found in request '
        current_app.logger.info( msg )
        return jsonify( { "error": msg } )

    plCodes = plCodesStr.split(',')

    pledges = getPledgeInfoByPledgeCodes(plCodes)

    current_app.logger.info("pledgesForPledgeCodes> found data for %i (of %i) pledges " % (len(pledges), len(plCodes)) )

    return jsonify( { 'allProjects': pledges } )

@api.route('/mailForPledges', methods=['POST'])
@login_required
def mailForPledges():

    plList = request.form.get( 'pledges', None )
    year = int(request.form.get('year', getPledgeYear()))

    mailList = ['not.yet.implemeted...']
    return jsonify( { 'mails' : mailList } )

@api.route('/pledgeInfoForPledgeCodes', methods=['POST'])
@login_required
def pledgeInfoForPledgeCodes():

    plCodesStr = request.form.get( 'plCodes', None )
    year = int(request.form.get('year', getPledgeYear()))

    if not plCodesStr:
        msg =  'pledgeInfoForPledgeCodes> no plCodes found in request '
        current_app.logger.info( msg )
        return jsonify( { "error": msg } )

    plCodes = plCodesStr.split(',')
    pledges = db.session.query( Pledge ).filter(Pledge.code.in_(plCodes)).all()

    pInfo = [ x.to_json() for x in pledges ]

    return jsonify( { 'plInfo' : pInfo } )

@api.route('/pledgeHistory', methods=['POST'])
@login_required
def pledgeHistory():

    plCode = request.form.get( 'plCode', None )
    year = int(request.form.get('year', getPledgeYear()))

    current_app.logger.debug( "got request for pledge history: %s" % str(request.form) )

    if not plCode:
        return jsonify( { 'data' : { 'code' : plCode, 'history' : [] } } )

    plCode = plCode.replace(':', '+')
    plHistory = getPledgeHistory(plCode, year)

    current_app.logger.debug( "returning pledge history of length %d" % len(plHistory) )

    return jsonify( { "data" : plHistory } )

@api.route('/splitPledge', methods=['POST'])
@login_required
def splitPledge():

    current_app.logger.debug( "got request to split pledge: form='%s'" % str(request.form) )

    plCode = request.form.get( 'plCode', None )
    newInstCode = request.form.get( 'newInst', None )
    newInstWork = request.form.get( 'newInstWork', None )

    status, msg, retCode = doPledgeSplit(plCode, newInstCode, newInstWork)

    return jsonify( { "status" : status, 'msg' : msg } ), retCode

def jsonifyAndLog(status, msg, htmlRet, msgDetail='', target=''):
    if htmlRet == 200:
        current_app.logger.info( '%s -- detail: \n%s' % (msg, msgDetail) )
    else:
        current_app.logger.error( '%s -- detail: \n%s' % (msg, msgDetail) )

    return jsonify( { "status" : status, 'msg' : msg, 'detail': msgDetail, 'target': target } ), htmlRet

def createNewInstRespPledge(task, userId, plInst, wPl, selYear, sendMail=False):
    status = 'OK'
    pledge = Pledge( taskId=task.id,
                     userId=userId,
                     instId=plInst.id,
                     workTimePld=wPl,
                     workTimeAcc=0.,
                     workTimeDone=0.,
                     status='new',
                     year=selYear,
                     workedSoFar=0,
                     isGuest=False,
                     )

    try :
        commitNew( pledge )
    except CMSDbException as e :
        msgErr = 'Error from DB when trying to add new pledge.\nLikely a pledge for this person and task already exists - please check below.'
        msgDetail = '(%s)\n' % str(e )
        status = 'ERROR'
        return jsonifyAndLog(status, msgErr, msgDetail, 400)
    except Exception as e :
        msgErr = 'Error from DB when trying to add new pledge.'
        msgDetail = '(%s)\n' % str(e )
        status = 'ERROR'
        return jsonifyAndLog(status, msgErr, msgDetail, 400)

    # send mail for addition, though only when asked for
    if sendMail:
        if not sendMailPledgeCreated( pledge.to_json() ):
            msgErr = "ERROR when sending mail for new pledge"
            flash(msgErr, 'error')
            current_app.logger.error( 'createNewInstRespPledge> %s for pledge %s ' % (msgErr, str(pledge)) )

    status = 'OK'
    msg = 'Successfully created new pledge of %s for %s in task %s (%d)' % (wPl, plInst.code, task.name, selYear)
    return jsonifyAndLog(status, msg, 200)

@api.route( '/createInstRespPledge', methods=[ 'POST' ] )
@login_required
def createInstRespPledge():

    # print('\n\n==> form: ', request.form, '\n\n')

    selYear = int(request.form.get('year', getPledgeYear()))
    instCodeReq = request.form.get( 'instCode', None )
    userIdsReq = request.form.get( 'userIdList', None )
    taskIdReq = request.form.get( 'taskId', None )
    workPldReq = request.form.get( 'workPledged', None )
    pldContact = request.form.get( 'contactName', '' )

    # <input type="hidden" id="contactId" value="Camporesi, Tiziano - CERN (CERNid: 379858)">
    contactHrID = int( re.match( r'^.*\(CERNid:\s*(\d+)\)\s*$', pldContact ).group( 1 ) ) if pldContact != '' else None

    current_app.logger.debug( "createInstRespPledge> got request for InstResp tasks for %s year %s" % (instCodeReq, selYear) )

    current_app.logger.debug( "createInstRespPledge> got reqeuest %s" % request )

    current_app.logger.debug( "createInstRespPledge> got userIdsReq %s" % str(userIdsReq) )
    current_app.logger.debug( "createInstRespPledge> got taskIdReq %s" % str(taskIdReq) )
    current_app.logger.debug( "createInstRespPledge> got workPldReq %s" % str(workPldReq) )
    current_app.logger.debug( "createInstRespPledge> got contactId '%s' -- id: %s" % (str(pldContact), contactHrID) )

    status = 'OK'
    errMsg = []

    isCurrentInstMgr, selInst = canManageSelectedInst( instCodeReq, selYear )
    isProjMgr, projList = canManageProject( selYear )

    # as of 2020-04-29, also Project Managers are allowed to create new pledges for InstResp tasks
    canCreatePledge = isCurrentInstMgr or isProjMgr

    # only team leaders are allowed to pledge for InstResp tasks
    if not canCreatePledge:
        msg = "Sorry, you (%s) are not allowed to create Institute Responsibility pledges for %s in %s" % (current_user.username, selInst.code, selYear)
        return jsonifyAndLog( 'ERROR', msg, 400)

    # instManagers and PMs can pledge for this and last year
    isAllowed = ( (canCreatePledge and (selYear == getPledgeYear() or selYear == getPledgeYear() - 1)) )

    if not isAllowed:
        msg = "New Institute Responsibility pledges are not allowed for %s " % selYear
        return jsonifyAndLog( 'ERROR', msg, 400)

    if not pldContact:
        return jsonifyAndLog( 'ERROR', 'createInstRespPledge> ERROR: no contact specified', 400)

    if not userIdsReq:
        return jsonifyAndLog( 'ERROR', 'createInstRespPledge> ERROR: no users specified', 400)

    existing = getInstRespPledgeSummary( instCodeReq, taskId=taskIdReq, year=selYear )
    if existing:
        return jsonifyAndLog('ERROR', 'createInstRespPledge> ERROR: an InstResp pledge for this task/inst combination (%s/%s) exists already' % (taskIdReq, selInst.code), 400)

    userIds = [ int(x) for x in userIdsReq.split(',') ]
    if not userIds:
        return jsonifyAndLog( 'ERROR', 'createInstRespPledge> ERROR: no users specified', 400)

    taskId = int(taskIdReq)
    task = db.session.query(Task).filter(Task.id==taskId).one()
    if not task:
        return jsonifyAndLog( 'ERROR', 'createInstRespPledge> ERROR: no task found for %s' % taskId, 400 )

    plInst = db.session.query(EprInstitute).filter(EprInstitute.code == instCodeReq).one()

    workPld = float( workPldReq )
    if workPld < 0. or workPld > task.neededWork:
        msg='createInstRespPledge> ERROR: invalid amount (%s) of work pledged for task %s, should be between 0 and %d' % (workPld, taskId, task.neededWork)
        return jsonifyAndLog( 'ERROR', msg, 400 )

    # check also if there are already other pledges (possibly from other insitutes) and abort if too much is pledged overall

    current_app.logger.info('createInstRespPledge> going to create %d individual pledges of %s for InstRep task "%s" ' % (len(userIds), instCodeReq, task.name))

    instUsers = getInstUsers( instCodeReq, selYear )
    sumPld = 0.
    nPld = 0
    # first create the pledges for the applicants, as they get their full AppDue for the year pledged:
    appPld = 0.
    nAppl = 0
    for cmsId, name, authReq, appDue, authDue, author, userId, username, wPld in instUsers:
        if cmsId not in userIds: continue
        if appDue > 0:
            appPld += appDue
            sumPld += appDue
            current_app.logger.info('createInstRespPledge> creating individual applicant pledges for %s (%s) of %s in %s' % (name, cmsId, appDue, selYear) )
            if appPld > workPld+0.01:
                msg = 'createInstRespPledge> NOT creating individual applicant pledges for %s (%s) of %s in %s -- %s over InstResp pledge %s !' % (
                name, cmsId, appDue, selYear, appPld, workPld)
                current_app.logger.info( msg )
                sumPld -= appDue
                appPld -= appDue
                status = "ERROR"
                errMsg.append( msg )
            else:
                # if we're good to go, count the applicants and make the new pledge:
                nAppl += 1
                nPld += 1
                res, htmlCode = createNewInstRespPledge( task, userId, plInst, appDue, selYear )
                if htmlCode != 200:
                    current_app.logger.error( str( res.json ) )
                    status = 'ERROR'
                    errMsg.append( res.json['msg'] )

    # calculate the amount of work for each remaining individual pledge:
    pldWork = 0.
    if ( (float(len(userIds)) - float(nAppl) ) > 0. ):
        pldWork = float( workPld - sumPld ) / ( float( len(userIds)) - float(nAppl) )
    current_app.logger.info('createInstRespPledge> after %d applicants, %s of %s still to do (%s), each: %s' % (nAppl, workPld-appPld, workPld, float(len(userIds)) - float(nAppl), pldWork) )

    for cmsId, name, authReq, appDue, authDue, author, userId, username, wPld in instUsers:
        if cmsId not in userIds: continue
        if appDue > 0: continue

        # here we have only selected team members who are non-applicants
        sumPld += pldWork
        current_app.logger.info('createInstRespPledge> creating individual pledges for %s (%s) of %s in %s' % (name, cmsId, pldWork, selYear) )
        if sumPld > workPld+0.01:
            msg = 'createInstRespPledge> NOT creating individual pledges for %s (%s) of %s in %s -- %s over InstResp pledge %s !' % (
            name, cmsId, pldWork, selYear, sumPld, workPld)
            current_app.logger.info( msg )
            status = "ERROR"
            errMsg.append( msg )
        else:
            nPld += 1
            res, htmlCode = createNewInstRespPledge( task, userId, plInst, pldWork, selYear )
            if htmlCode != 200 :
                current_app.logger.error( str(res.json) )
                status = 'ERROR'
                errMsg.append( res.json[ 'msg' ] )

    # get the pledge codes for this task/inst combination and add them to the favourites of the contact:
    irPlCodes = ( db.session.query(Pledge.code)
                    .filter( Pledge.year == selYear )
                    .filter( Pledge.taskId == task.id )
                    .filter( Pledge.status != 'rejected' )
                    .filter( Pledge.instId == plInst.id )
                    .all())

    plCodes = [ x[0] for x in irPlCodes ]
    current_app.logger.debug("Pledge Codes to observe for %s: %s " % (contactHrID, str(plCodes)) )
    addToFavs( contactHrID, [], task.code, plCodes)
    current_app.logger.info("Favs for %s set: task: %s (%s) -- pledges: %s" % (contactHrID, task.name, task.id, str(plCodes)) )

    if status == 'OK':
        msg = 'successfully created %s pledges (%s months each) for %s for InstResp task "%s" in %s' % (nPld, pldWork, instCodeReq, task.name, selYear)
        return jsonifyAndLog( status, msg, 200 )
    else:
        return jsonifyAndLog( status, '<br/>\n'.join(errMsg), 400 )


@api.route( '/pledgesForInstResp', methods=[ 'POST' ] )
@login_required
def pledgesForInstResp():

    selYear = int(request.form.get('year', getPledgeYear()))
    taskId = int( request.form.get( 'taskId', -1 ) )
    instId = int( request.form.get( 'instId', -1 ) )

    current_app.logger.debug( "pledgesForInstResp> got request form: %s" % (str(request.form),) )

    res = (db.session.query( Pledge, EprUser )
            .join( Task, Task.id == Pledge.taskId )
            .join( EprUser, EprUser.id == Pledge.userId)
            .filter( Pledge.year == selYear )
            .filter( Pledge.status != 'rejected' )
            .filter( Task.tType == 'InstResp' )
            .filter( Task.status == 'ACTIVE' )
            .filter( Task.id == taskId )
            .filter( Pledge.instId == instId )
            .all() )

    data = []
    for p, u in res:
        data.append( ['', u.name, p.workTimePld, p.workTimeAcc, p.workTimeDone, p.status, u.cmsId, u.username ] )

    current_app.logger.debug( "pledgesForInstResp> res: %s" % (str(data),) )

    return jsonify( { 'data': data } ), 200

@api.route( '/instRespPledges', methods=[ 'POST' ] )
@login_required
def instRespPledges():

    selYear = int(request.form.get('year', getPledgeYear()))
    userIdsReq = request.form.get( 'userIdList', '' )
    userIds = []
    if userIdsReq != '':
        userIds = [ int(x) for x in userIdsReq.split(';') ]
    projId = int( request.form.get( 'projId', -1 ) )

    current_app.logger.debug( "instRespPledges> got request form: %s" % (str(request.form),) )

    current_app.logger.info( "instRespPledges> got request for pledges for InstResp tasks for userIds %s projId %s in year %s" % (str(userIds), projId, selYear) )

    lvl3Map = getLevel3NameMap( selYear )

    res = None
    if len(userIds) > 0:
        res = (db.session.query( Task, Pledge, EprUser, EprInstitute )
                .join( Pledge, Pledge.taskId == Task.id )
                .join( EprUser, Pledge.userId == EprUser.id )
                .join( EprInstitute, Pledge.instId == EprInstitute.id )
                .filter( Pledge.year == selYear )
                .filter( Pledge.status != 'rejected')
                .filter( Pledge.userId.in_( userIds) )
                .filter( Task.tType == 'InstResp' )
                .filter( Task.status == 'ACTIVE' )
                .all()
                )
    elif projId > -2:
        sq = (db.session.query( Pledge.instId.label('instId'),
                                func.sum(Pledge.workTimePld).label('workPld'),
                                func.sum(Pledge.workTimeAcc).label('workAcc'),
                                func.sum(Pledge.workTimeDone).label('workDone'),
                                Pledge.taskId.label('taskId'),
                                Pledge.status.label('plStatus')
                               )
              .join( Task, Task.id == Pledge.taskId )
              .filter( Pledge.year == selYear )
              .filter( Pledge.status != 'rejected' )
              .filter( Task.tType == 'InstResp' )
              .filter( Task.status == 'ACTIVE' )
              .group_by( Pledge.instId, Pledge.taskId, Pledge.status )
              .subquery() )

        query = (db.session.query( Task,
                                 sq.c.workPld, sq.c.workAcc, sq.c.workDone,
                                 sq.c.plStatus,
                                 EprInstitute )
                .join( sq, sq.c.taskId == Task.id )
                .join( CMSActivity, Task.activityId == CMSActivity.id )
                .join( Project, CMSActivity.projectId == Project.id )
                .join( EprInstitute, sq.c.instId == EprInstitute.id )
                .filter( Task.tType == 'InstResp' )
                .filter( Task.status == 'ACTIVE' )
                 )
        if projId > 0:
            query = query.filter( Project.id == projId )

        res = query.all()

    current_app.logger.debug( "instRespPledges> res: %s " % str(res) )

    aM, pM, tM, iM = getManagers( selYear )

    data = []
    if res is None:
        current_app.logger.warning("instRespPledges> no tasks for instRespPleges found for proj %s in %s " % (projId, selYear) )
        return jsonify( {'allProjects': data} ), 200

    taskFracMap = {}
    for item in res:
        if projId > -2:
            task, wPld, wAcc, wDone, plStatus, inst = item
            uName = ''
            plInfo = '-'
            plCode = ''
        else:
            task, pl, user, inst = item
            uName = user.name
            wPld = pl.workTimePld
            wAcc = pl.workTimeAcc
            wDone = pl.workTimeDone
            plInfo = '%i:%i:%i' % (pl.taskId, pl.userId, pl.instId)
            plStatus = pl.status
            plCode = pl.code

        taskFracDone = 0. # toDo: fix
        if task.code not in taskFracMap.keys():
            taskFracMap[task.code] = { 'sum': 0., 'needs': 0. }
        taskFracMap[task.code]['sum'] += wDone
        taskFracMap[task.code]['needs'] = task.neededWork

        plFracDone = wDone/wPld if wPld > 0. else -1.

        # see if the user can manage this item
        userCanManage = current_user.is_administrator() or (task.code in tM or task.activity.code in aM or task.activity.project.code in pM)

        # current_app.logger.debug("getProjectInfo> item: %s %s %s %s : canManage: %s " % (pl, t, a, p, userCanManage) )

        editTask = ''
        if ( userCanManage ): # allow to edit tasks ...
            editTask = editTaskTmpl % ( url_for( 'manage.task', code='%s' % str(task.code) ), )

        # allow editing the pledge by the user who created it only if it is not yet accepted (or done)
        editPl = ''
        if ( (plStatus == 'new' and inst.code in iM) or userCanManage ):
            editPl = editPlTmpl % ( url_for('main.editInstRespPledge', taskId='%s' % str(task.id), instCode='%s' % inst.code) )
        elif ( (plStatus == 'accepted' and inst.code in iM) or userCanManage ):
            # allow updating the work already done for the the pledge by the user who created it if it is already accepted
            editPl = editPlTmpl % ( url_for('main.updateInstRespWorkDone', taskId='%s' % str(task.id), instCode='%s' % inst.code) )

        rejectPl = ''
        if userCanManage :
            rejectPl = rejectPlTmpl % ( url_for('main.rejectInstRespPledge', taskId='%s' % str(task.id), instCode='%s' % inst.code) )

        lvl3Name = '-'
        try:
            lvl3Name = lvl3Map[task.level3]
        except KeyError:
            pass
        data.append( [ '', # checkbox for selection
                       task.activity.project.name,
                       task.activity.name,
                       lvl3Name,
                       task.name,
                       task.neededWork,
                       taskFracDone,
                       task.pctAtCERN,
                       task.tType,
                       task.comment,
                       editTask,
                       uName,
                       inst.code,
                       wPld,
                       wAcc,
                       wDone,
                       plFracDone,
                       plStatus,
                       editPl,
                       rejectPl,
                       plInfo,
                       task.name,
                       plCode, 
                       task.activity.code, 
                       task.code
         ] )

    for item in data:
        item[6] = 0
        if taskFracMap[ item[-1] ]['needs'] > 0.:
            item[6] = taskFracMap[ item[-1] ]['sum']/taskFracMap[ item[-1] ]['needs']

    current_app.logger.debug( "instRespPledges> data: %s " % str(data) )

    return jsonify( { 'allProjects': data } ), 200

@api.route( '/instRespPledgesForInst', methods=[ 'POST' ] )
@login_required
def instRespPledgesForInst():

    selYear = int(request.form.get('year', getPledgeYear()))
    instCodeReq = request.form.get( 'instCode', '' )

    current_app.logger.debug( "instRespPledgesForInst> got request form: %s" % (str(request.form),) )

    lvl3Map = getLevel3NameMap( selYear )

    res = getInstRespPledgeSummary(instCode=instCodeReq, year=selYear)

    current_app.logger.debug( "instRespPledgesForInst> res: %s " % str(res) )

    editPlTmpl   = '<a href="%s"> <img src=/static/editIcon.png alt="edit IRT pledge" height="21" width="21"/> </a>'

    isInstMgr, inst = isInstManager(instCode=instCodeReq)

    data = [ ]
    for item in res :
        task, wPld, wAcc, wDone, wSoFar, plStatus, inst = item

        taskFracDone = 0.  # toDo: fix

        plFracDone = wDone / wPld if wPld > 0. else -1.

        # the row in data here has to have the same structure as the one in instRespPledges() above:
        editTask = ''
        uName = ''
        editPl = ''
        rejectPl = ''
        plInfo = ''
        plCode = ''

        lvl3Name = '-'
        try:
            lvl3Name = lvl3Map[task.level3]
        except KeyError:
            pass

        if current_user.is_administrator() or isInstManager:
            editPl = editPlTmpl % ( url_for('main.editInstRespPledge', taskId='%s' % task.id, instCode='%s' % str(inst.code) ) )

        row = ['',  # col 0 -- checkbox for selection
               task.activity.project.name,
               task.activity.name,
               lvl3Name,
               task.name,
               task.neededWork,
               taskFracDone,
               task.pctAtCERN,
               task.tType,
               task.comment,  # 9
               editTask,
               uName,         # 11
               inst.code,
               wPld,
               wAcc,
               wDone,
               plFracDone,  # 16
               plStatus,    # 17
               editPl,
               rejectPl,
               plInfo,       # 20
               task.name,    # 21
               plCode, task.activity.code, task.code   # 22,23,24
               ]

        data.append( row )

    current_app.logger.debug( "instRespPledgesForInst> data (len=%d): %s " % (len(data), str(data)) )

    return jsonify( { 'allProjects': data } ), 200

def getIRTWorkSummary(taskId, instCode, year):
    task = db.session.query(Task).filter( Task.id == taskId).one_or_none()
    
    plds, = (db.session.query( func.sum(Pledge.workTimePld).label('sumPldWork') ) 
                    .join(EprInstitute, EprInstitute.id == Pledge.instId)
                    .filter( Pledge.taskId == taskId)
                    .filter( Pledge.year == year)
                    .filter( Pledge.status != 'rejected')
                    .filter( EprInstitute.code == instCode)
                    .all())

    return plds.sumPldWork, task.neededWork

def getIRTUserPledgeForUpdate(taskId, cmsId, instCode, year):

    pld,userId = (db.session.query(Pledge, EprUser.id) 
                    .join(EprUser, EprUser.id == Pledge.userId)
                    .join(EprInstitute, EprInstitute.id == Pledge.instId)
                    .filter( Pledge.taskId == taskId)
                    .filter( Pledge.year == year)
                    .filter( Pledge.status != 'rejected')
                    .filter( EprUser.cmsId == cmsId)
                    .filter( EprInstitute.code == instCode)
                    .one_or_none())
    return pld, userId

def updatePledgedWork(taskId, cmsId, instCode, year, pldWork):
    pld, userId = getIRTUserPledgeForUpdate(taskId, cmsId, instCode, year)
    if not pld:
        msg = "NO pledge found for: taskId %s, cmsId %s, instCode %s, year %s, pldWork %s " % (taskId, cmsId, instCode, year, pldWork)
        current_app.logger.error( "updatePledgedWork> %s " %msg) 
        return (False, False, msg)

    oldPledgeJson = pld.to_json()
    sumPldWork, neededWork = getIRTWorkSummary(taskId, instCode, year)

    newPldWork = sumPldWork - float(oldPledgeJson['workTimePld']) + float(pldWork)
    if newPldWork > neededWork:
        msg = "ERROR Needed work for taskId %s is %6.2f, overall sum would be %6.2f -- aborting update " % (taskId, neededWork, newPldWork)
        current_app.logger.error( "updatePledgedWork> %s " %msg) 
        return (False, False, msg)


    updated = False
    if pldWork and float(pld.workTimePld) != float(pldWork):
        (sumPld, sumAcc, sumDone) = getPledgeSum(userId, year)
        if sumPld + float(pldWork) - float(pld.workTimePld) > 12.:
            msg = "pledge for: taskId %s, cmsId %s, instCode %s, year %s, pldWork %s is above limit of 12 months (%s) for user." % (taskId, cmsId, instCode, year, pldWork, sumPld-float(pld.workTimePld)+pldWork)
            # flash(msg, 'error')
            current_app.logger.error( "updatePledgedWork> %s" % msg )
        else:
            oldPld = pld.workTimePld
            pld.workTimePld = float(pldWork)
            commitUpdated( pld )
            msg = "pledge updated for: taskId %s, cmsId %s, instCode %s, year %s, pldWork %s (%s)" % (taskId, cmsId, instCode, year, pldWork, oldPld)
            current_app.logger.info( "updatePledgedWork> %s" % msg) 
            updated = True

            # send mail out for update
            newPledgeJson = db.session.query( Pledge ).filter_by( code=pld.code ).one().to_json()
            if not sendMailPlegdeUpdated( oldPledgeJson, newPledgeJson ) :
                msg = "ERROR when sending mail for updating IRT pledge"
                flash( msg, 'error' )
                current_app.logger.warning( 'updatePledgedWork> ERROR %s ' % msg )

    return (True, updated, msg)

@api.route( '/updateIRTpledges', methods=[ 'POST' ] )
@login_required
def updateIRTpledges():
    
    selYear = int(request.form.get('year', getPledgeYear()))
    instCodeReq = request.form.get( 'instCode', '' )
    taskIDReq   = request.form.get( 'taskId', '' )
    dataReq   = request.form.get( 'data', '' )

    current_app.logger.debug( "updateIRTpledges> got request form: %s" % (str(request.form),) )

    if not dataReq.strip():
        msg = "updateIRTpledges> ERROR: no data found in form. Aborting."
        flash(msg, 'error')
        return jsonify( { 'msg': msg } ), 500

    isInstMgr, inst = isInstManager(instCode=instCodeReq)
    if not (isInstMgr or current_user.is_administrator()):
        msg = "You (%s) are not allowed to update this IRT pledge." % str(current_user.username)
        current_app.logger.error( "updateIRTpledges> %s" % msg )
        flash( msg, 'error')
        return jsonify( { 'msg': msg } ), 401

    nOK = 0
    nUpd = 0
    errMsg = ''
    for item in dataReq.split('&'):
        cmsId = -1
        pld = 0
        try:
            cmsId, pld = re.match( r'^user-(\d+)-pld=([.\d]+)$', item ).groups()
        except Exception as e:
            current_app.logger.error( "updateIRTpledges> could not match '%s' - got: %s " % (item, str(e)) )
            continue
        # print( '\n ==> found cmsId %s with pld %s ' % (cmsId, pld) )

        ret = False
        upd = False
        (ret, upd, msg) = updatePledgedWork(taskIDReq, cmsId, instCodeReq, selYear, float(pld))
        if not ret:
            errMsg += '\n%s' % msg
            msg = "could not update '%s' " % (item,)
            flash( msg, 'error')
            current_app.logger.error( "updateIRTpledges> %s" % msg )
            continue
        
        if upd:
            nUpd += 1
        nOK += 1

    if nUpd != len( dataReq.split('&') ):
        msg = 'ERROR only %d of %d items could be handled: %s' % (nUpd, len( dataReq.split('&') ), errMsg)
        flash(msg, 'error')
        return jsonify( { 'msg': msg } ), 500

    msg = 'OK - %d (of %d) pledges were updated.' % (nUpd, nOK)
    flash(msg)
    return jsonify( { 'msg': msg } ), 200

def removeIRPledgeItem(taskId, cmsId, instCode, year):
    pld, userId = getIRTUserPledgeForUpdate(taskId, cmsId, instCode, year)
    if not pld:
        msg = "NO pledge found for: taskId %s, cmsId %s, instCode %s, year %s " % (taskId, cmsId, instCode, year)
        current_app.logger.error( "removeIRPledgeItem> %s " %msg) 
        return (False, False, msg)

    updated = False

    pld.status = 'rejected'
    pld.code   = pld.code + time.strftime('%Y%m%d-%H%M%S')
    pld.timestamp = datetime.datetime.utcnow()
    commitUpdated( pld )
    msg = "pledge rejected for: taskId %s, cmsId %s, instCode %s, year %s" % (taskId, cmsId, instCode, year)
    current_app.logger.info( "removeIRPledgeItem> %s" % msg) 
    updated = True

    # send out mail for reject
    if not sendMailPledgeRejected( pld.to_json(), reason='Removed from InstResp pledge by team leader' ):
        msg = "ERROR when sending mail for removing IRT pledge"
        flash( msg, 'error')
        current_app.logger.warning( 'removeIRPledgeItem> ERROR %s ' % msg )

    return (True, updated, msg)

@api.route( '/removeIRTpledge', methods=[ 'POST' ] )
@login_required
def removeIRTpledge():
    
    selYear = int(request.form.get('year', getPledgeYear()))
    instCodeReq = request.form.get( 'instCode', '' )
    taskIDReq   = request.form.get( 'taskId', '' )
    dataReq   = json.loads(request.form.get( 'data', '' ))

    current_app.logger.debug( "removeIRTpledge> got request form: %s" % (str(request.form),) )

    if not dataReq:
        msg = "removeIRTpledge> ERROR: no data found in form. Aborting."
        flash(msg, 'error')
        return jsonify( { 'msg': msg } ), 500

    isInstMgr, inst = isInstManager(instCode=instCodeReq)
    if not (isInstMgr or current_user.is_administrator()):
        msg = "You (%s) are not allowed to remove IRT pledges" % str(current_user.username)
        current_app.logger.error( "removeIRTpledge> %s" % msg )
        flash( msg, 'error')
        return jsonify( { 'msg': msg } ), 401

    nOK = 0
    nRem = 0
    errMsg = ''

    for cmsId in dataReq:
        current_app.logger.error( "removeIRTpledge> processing cmsId '%s'" % (cmsId,) )

        ret = False
        upd = False
        (ret, upd, msg) = removeIRPledgeItem(taskIDReq, cmsId, instCodeReq, selYear)
        if not ret:
            errMsg += '\n%s' % msg
            msg = "could not remove  pld for cmsId '%s' " % (cmsId,)
            flash( msg, 'error')
            current_app.logger.error( "removeIRTpledge> %s" % msg )
            continue
        
        if upd:
            nRem += 1
        nOK += 1


    if nRem != len( dataReq ):
        msg = 'ERROR only %d of %d items could be handled: %s' % (nRem, len( dataReq ), errMsg)
        flash(msg, 'error')
        return jsonify( { 'msg': msg } ), 500

    msg = 'OK - %d (of %d) pledges were removed.' % (nRem, nOK)
    flash(msg)
    return jsonify( { 'msg': msg } ), 200


def addIRTpledgeUser(task, user, plInst, year, pld):

    updated = False
    errMsg = ''

    (sumPld, sumAcc, sumDone) = getPledgeSum(user.id, year)
    if sumPld + float(pld) > 12.:
        msg = "pledge for: taskId %s, cmsId %s, instCode %s, year %s, pldWork %s is above limit of 12 months for user." % (task.id, user.cmsId, plInst.code, year, pld)
        flash(msg, 'error')
        current_app.logger.error( "updatePledgedWork> %s" % msg )
        return (False, False, msg)
    else:
        res, htmlCode = createNewInstRespPledge( task, user.id, plInst, pld, year, sendMail=True )
        if htmlCode != 200:
            msg = "ERROR adding new IRT pledge for: taskId %s, cmsId %s, instCode %s, year %s, pldWork %s " % (task.id, user.cmsId, plInst.code, year, pld)
            current_app.logger.error( "addIRTpledgeUser> %s " %msg) 
            status = 'ERROR'
            errMsg = res.json['msg']
            return (False, False, errMsg)

    msg = "new user added to IRT pledge: taskId %s, cmsId %s, instCode %s, year %s, pldWork %s " % (task.id, user.cmsId, plInst.code, year, pld)
    current_app.logger.info( "addIRTpledgeUser> %s" % msg) 
    updated = True

    return (True, updated, msg)

@api.route( '/addUserToIRTpledge', methods=[ 'POST' ] )
@login_required
def addUserToIRTpledge():
    
    selYear = int(request.form.get('year', getPledgeYear()))
    instCodeReq = request.form.get( 'instCode', '' )
    taskIDReq   = request.form.get( 'taskId', '' )
    dataReq   = json.loads( request.form.get( 'data', '' ) )

    current_app.logger.debug( "addUserToIRTpledge> got request form: %s" % (str(request.form),) )
    current_app.logger.debug( "addUserToIRTpledge> got dataReq: %s" % dataReq )

    if not dataReq:
        msg = "addUserToIRTpledge> ERROR: no data found in form. Aborting."
        flash(msg, 'error')
        return jsonify( { 'msg': msg } ), 500

    isInstMgr, inst = isInstManager(instCode=instCodeReq)
    current_app.logger.debug( "\n\n +++ addUserToIRTpledge> user %s inst: %s - isInstMgr: %s, is admin: %s (or: %s" % (current_user.username, inst, isInstMgr, current_user.is_administrator(), (isInstMgr or current_user.is_administrator()) ) )

    if not (isInstMgr or current_user.is_administrator()):
        msg = "You (%s) are not allowed to update IRT pledges" % str(current_user.username)
        current_app.logger.error( "addUserToIRTpledge> %s" % msg )
        flash( msg, 'error')
        return jsonify( { 'msg': msg } ), 401

    item = dataReq
    hrId = item['user'][:-1].split('(CERNid:')[1].strip()     # Pfeiffer, Andreas - CERN (CERNid: 422405)
    plUser = db.session.query(EprUser).filter( EprUser.hrId == hrId).one()
    try:
        pld = float( item['pld'] )
    except ValueError:
        msg = "Illegal value for work pledged: %s has to be a positve number" % item['pld']
        current_app.logger.error( "addUserToIRTpledge> %s" % msg )
        flash( msg, 'error')
        return jsonify( { 'msg': msg } ), 500

    if pld < 0:
        msg = "Illegal value for work pledged: %s has to be > 0" % pld
        current_app.logger.error( "addUserToIRTpledge> %s" % msg )
        flash( msg, 'error')
        return jsonify( { 'msg': msg } ), 500

    task = db.session.query(Task).filter(Task.id == int(taskIDReq)).one()

    current_app.logger.debug( "\n\n +++ addUserToIRTpledge> user %s inst: %s - is admin: %s canManage(%s, %s) %s" % 
        (current_user.username, inst, current_user.is_administrator(), plUser, selYear, current_user.canManageUser(plUser, year=selYear) ) )

    if not current_user.is_administrator() and not current_user.canManageUser(plUser, year=selYear):
        msg = "You (%s) can only add members of the team(s) you manage." % str(current_user.username)
        current_app.logger.error( "addUserToIRTpledge> %s" % msg )
        flash( msg, 'error')
        return jsonify( { 'msg': msg } ), 401

    nOK = 0
    nAdd = 0
    errMsg = ''

    current_app.logger.info( "addUserToIRTpledge> processing cmsId '%s' pld: %s" % (plUser.cmsId, pld) )
    ret = False
    upd = False
    (ret, upd, msg) = addIRTpledgeUser(task, plUser, inst, selYear, pld)
    if not ret:
        errMsg += '\n%s' % msg
        msg = "could not add new pledge '%s' " % (item,)
        flash( msg, 'error')
        current_app.logger.error( "addUserToIRTpledge> %s" % msg )
    
    if upd:
        nAdd += 1
    nOK += 1

    if nAdd != 1:
        msg = 'ERROR only %d of %d items could be handled: %s' % (nAdd, 1, errMsg)
        flash(msg, 'error')
        return jsonify( { 'msg': msg } ), 500

    msg = 'OK - user %s was added to this IRT pledge.' % plUser.name
    flash(msg)
    return jsonify( { 'msg': msg } ), 200



@api.route( '/findLastYearInstRespPledge', methods=[ 'POST' ] )
@login_required
def findLastYearInstRespPledge():

    # print('\n\n==> form: ', request.form, '\n\n')

    selYear = int(request.form.get('year', getPledgeYear()))
    instCodeReq = request.form.get( 'instCode', None )
    userIdsReq = request.form.get( 'userIdList', None )
    taskIdReq = request.form.get( 'taskId', None )
    workPldReq = request.form.get( 'workPledged', None )
    pldContact = request.form.get( 'contactName', None )

    # # <input type="hidden" id="contactId" value="Camporesi, Tiziano - CERN (CERNid: 379858)">
    # contactHrID = int( re.match( r'^.*\(CERNid:\s*(\d+)\)\s*$', pldContact ).group( 1 ) ) if pldContact != '' else None
        
    current_app.logger.debug( "createInstRespPledge> got request for InstResp tasks for %s year %s" % (instCodeReq, selYear) )

    current_app.logger.debug( "createInstRespPledge> got reqeuest %s" % request )

    current_app.logger.debug( "createInstRespPledge> got userIdsReq %s" % str(userIdsReq) )
    current_app.logger.debug( "createInstRespPledge> got taskIdReq %s" % str(taskIdReq) )
    current_app.logger.debug( "createInstRespPledge> got workPldReq %s" % str(workPldReq) )
    # current_app.logger.debug( "createInstRespPledge> got contactId '%s' -- id: %s" % (str(pldContact), contactHrID) )

    status = 'OK'
    errMsg = []

    isCurrentInstMgr, selInst = canManageSelectedInst( instCodeReq, selYear )
    isProjMgr, projList = canManageProject( selYear )

    # as of 2020-04-29, also Project Managers are allowed to create new pledges for InstResp tasks
    canCreatePledge = isCurrentInstMgr or isProjMgr

    # only team leaders are allowed to pledge for InstResp tasks
    if not canCreatePledge:
        msg = "Sorry, you (%s) are not allowed to create Institute Responsibility pledges for %s in %s" % (current_user.username, selInst.code, selYear)
        return jsonifyAndLog( 'ERROR', msg, 400)

    # instManagers and PMs can pledge for this and last year
    isAllowed = ( (canCreatePledge and (selYear == getPledgeYear() or selYear == getPledgeYear() - 1)) )

    if not isAllowed:
        msg = "New Institute Responsibility pledges are not allowed for %s " % selYear
        return jsonifyAndLog( 'ERROR', msg, 400)

    if not pldContact:
        return jsonifyAndLog( 'ERROR', 'createInstRespPledge> ERROR: no contact specified', 400)

    # if not userIdsReq:
    #     return jsonifyAndLog( 'ERROR', 'createInstRespPledge> ERROR: no users specified', 400)

    existing = getInstRespPledgeSummary( instCodeReq, taskId=taskIdReq, year=selYear )
    if existing:
        return jsonifyAndLog('ERROR', 'createInstRespPledge> ERROR: an InstResp pledge for this task/inst combination (%s/%s) exists already' % (taskIdReq, selInst.code), 400)

    taskId = int(taskIdReq)
    task = db.session.query(Task).filter( Task.id == taskId ).one()

    if not task:
        return jsonifyAndLog( 'ERROR', 'createInstRespPledge> ERROR: no task found for %s' % taskId, 400 )

    plInst = db.session.query(EprInstitute).filter(EprInstitute.code == instCodeReq).one()
    current_app.logger.debug("createInstRespPledge> found plInst %s " % str(plInst) )

    act = db.session.query(CMSActivity).filter( CMSActivity.id == task.activityId ).one_or_none()
    proj = db.session.query(Project).filter( Project.id == act.projectId ).one_or_none()

    if not act or not proj:
        return jsonifyAndLog( 'ERROR', 'createInstRespPledge> ERROR: no act/proj (%s/%s) found for task %s' % (act.id, proj.id, taskId), 400 )

    # get the pledge codes for this task/act/proj/inst combination from last year (match by PAT-names) to be copied over
    oldIRTPledges = ( db.session.query(Pledge)
                    .join( Task, Task.id == Pledge.taskId )
                    .join( CMSActivity, CMSActivity.id == Task.activityId )
                    .join( Project, Project.id == CMSActivity.projectId )
                    .filter( Task.name == task.name )
                    .filter( Task.tType == 'InstResp' )
                    .filter( CMSActivity.name == act.name )
                    .filter( Project.name == proj.name )
                    .filter( Pledge.year == selYear-1 )
                    .filter( Pledge.status != 'rejected' )
                    .filter( Pledge.instId == plInst.id )
                    .all())
    current_app.logger.debug("createInstRespPledge> found %d old pledge Codes for %s: %s " % (len(oldIRTPledges), selYear-1, str(oldIRTPledges)) )

    plCodes = [ x.code for x in oldIRTPledges ]
    current_app.logger.debug("createInstRespPledge> found pledge Codes for %s: %s " % (selYear-1, str(plCodes)) )

    nPld = 0
    for oldPl in oldIRTPledges:
        nPld += 1
        res, htmlCode = createNewInstRespPledge( task, oldPl.userId, plInst, oldPl.workTimePld, selYear )
        if htmlCode != 200 :
            current_app.logger.error( str(res.json) )
            status = 'ERROR'
            errMsg.append( res.json[ 'msg' ] )

    # get the pledge codes for this task/inst combination and add them to the favourites of the contact:
    irPlCodes = ( db.session.query(Pledge.code)
                    .filter( Pledge.year == selYear )
                    .filter( Pledge.taskId == task.id )
                    .filter( Pledge.status != 'rejected' )
                    .filter( Pledge.instId == plInst.id )
                    .all())

    # current_app.logger.debug("Pledge Codes to observe for %s: %s " % (contactHrID, str(plCodes)) )
    # addToFavs( contactHrID, [], task.code, plCodes)
    # current_app.logger.info("Favs for %s set: task: %s (%s) -- pledges: %s" % (contactHrID, task.name, task.id, str(plCodes)) )

    pldWork = -42
    if status == 'OK':
        msg = 'successfully copied %s pledges from last year for %s for InstResp task "%s" in %s' % (nPld, instCodeReq, task.name, selYear)
        target = url_for('main.editInstRespPledge', taskId=task.id, instCode=instCodeReq)
        return jsonifyAndLog( status, msg, 200, target=target )
    else:
        return jsonifyAndLog( status, '<br/>\n'.join(errMsg), 400 )
