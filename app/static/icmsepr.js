
function getUAICInfo(url, pId, year) {
    // get the number of users/authors/institutes/countries for a given project (-1 for all projects)
    $.ajax({
        url : url,
        data : {
            "year" : year,
            "proj"  : pId,
        },
        method:"POST",
        success: function( result ){
            var data = result[pId];
            var infoString = "";
            if ( data && 'nUsers'  in data ) {
                infoString = "<b>Pledges</b> for this project are from " + data['nUsers'] + " people (of which " + data['nAuthors'] + " are authors) from " + data['nInsts'] + " institutes in " + data['nCountries'] + " countries.";
            }
            console.log(infoString);

            $('<div/>', {id:'nPledgeUserInst'}).appendTo('div.page-header');
            $("#nPledgeUserInst").html(infoString);
      }});
}


function getProjectCounts(url, pId, year) {
    // get the number of users/authors/institutes/countries for a given project (-1 for all projects)
    $.ajax({
        url : url,
        data : {
            "year" : year,
            "proj"  : pId,
        },
        method:"POST",
        success: function( result ){
            console.log( pId );
            console.log( result.data );
            var projName = result.data[pId];
            console.log( projName );
            var data = result.data[projName];
            if (data !== undefined) {
                // console.log( result.data[projName] );
                var infoString = "<b>Registered</b> for this project are " + data['nUsers'] + " people (of which " + data['nAuthors'] + " are authors) from " + data['nInsts'] + " institutes in " + data['nCountries'] + " countries.";
                console.log(infoString);

                $('<div/>', {id: 'nProjectCount'}).appendTo('div.page-header');
                $("#nProjectCount").html(infoString);
            }
      }});
}

function getPrjWorkInfo(url, pId, year) {
    // get the pld/acc/done work of all/authors for a given project/year
    $.ajax({
        url : url,
        data : {
            "year" : year,
            "proj"  : pId
        },
        method:"GET",
        success: function( result ){
            // console.log('result: ', result);
            var data = result[pId];
            var infoString = "<b>Work (pledged,accepted,done)</b> for this project: (" + data['all'] + ") -- of which ("+data['authors']+") is from authors.";
            // console.log(infoString);

            $('<div/>', {id:'prjWorkInfo'}).appendTo('div.page-header');
            $("#prjWorkInfo").html(infoString);
      }});
}

function setupHelpButton() {
    $("#showHideHelp").on('click', function(event) {
        $("#info-help").addClass('brick padded');
        var buttonHtml = "Hide Help";
        if ( $('#showHideHelp').html().indexOf("Hide") != -1 ) {
            buttonHtml = "Show Help";
            $("#info-help").hide();
        }
        if ( $('#showHideHelp').html().indexOf("Show") != -1 ) {
            buttonHtml = "Hide Help";
            $("#info-help").show();

        }
        $('#showHideHelp').html(buttonHtml).button().button("refresh");
    });
}

function getColour(val, thrList) {
    if ( ! thrList ) { thrList = [0.8, 0.42, -0.1] }
    var col = '';
    if ( val == '-') {
        col = 'deeppink';
    } else if ( parseFloat(val) >= thrList[0]  ) {
          col = 'lawngreen';
    } else if ( parseFloat(val) >= thrList[1] ) {
         col = 'orange';
    } else if ( parseFloat(val) >= thrList[2] ) {
        col = 'orangered';
    }
    return col;
}

function getColourReverse(val) {
    var col = '';
    if ( val == '-') {
        col = 'deeppink';
    } else if ( parseFloat(val) > 0.50  ) {
        col = 'orangered';
    } else if ( parseFloat(val) > 0.25 ) {
        col = 'orange';
    } else if ( parseFloat(val) > -0.1 ) {
        col = 'lawngreen';
    }
    return col;
}

function updateInstProj(selectedUser) {
    if ( selectedUser ) {
        // get the user's institute and update the #instId selection accordingly
        var url = $SCRIPT_ROOT + "/api/v1.0/userByName/"+selectedUser;
        var jqxhr = $.ajax( { url : url,
                              dataType: "json" } )
                          .done(function(msg) {
                              console.log( msg );

                              var userHtml = '<a href="'+$SCRIPT_ROOT+'/showUser/'+msg.user.username+'"> '+ msg.user.name + '</a>';
                              console.log(userHtml);
                              $("#userName").html( userHtml );

                              var instHtml = '<a href="'+$SCRIPT_ROOT+'/showInstitute/'+msg.inst.code+'"> '+ msg.inst.code + '</a>';
                              instHtml += ' -- manage users for  <a href="'+$SCRIPT_ROOT+'/manage/manageInstUsers/'+msg.inst.code+'"> '+ msg.inst.code + '</a>';
                              $("#instId").html( instHtml );

                              var pmHtml = '';
                              if (msg.projectsManaged) {
                                 for (i=0; i<msg.projectsManaged.length; i++) {
                                     pmHtml += '<a href="'+$SCRIPT_ROOT+'/manage/project/'+msg.projectsManaged[i]+'"> '+ msg.projectsManaged[i] + '</a> -- ';
                                 }
                              }
                              var amHtml = '';
                              if (msg.actsManaged) {
                                 for (i=0; i<msg.actsManaged.length; i++) {
                                     amHtml += '<a href="'+$SCRIPT_ROOT+'/manage/activity/'+msg.actsManaged[i]+'"> '+ msg.actsManaged[i] + '</a> -- ';
                                 }
                              }
                              var tmHtml = '';
                              if (msg.tasksManaged) {
                                 for (i=0; i<msg.tasksManaged.length; i++) {
                                     tmHtml += '<a href="'+$SCRIPT_ROOT+'/manage/task/'+msg.tasksManaged[i]+'"> '+ msg.tasksManaged[i] + '</a> -- ';
                                 }
                              }
                              var imHtml = '';
                              if (msg.instsManaged) {
                                 for (i=0; i<msg.instsManaged.length; i++) {
                                     imHtml += '<a href="'+$SCRIPT_ROOT+'/manage/task/'+msg.instsManaged[i]+'"> '+ msg.instsManaged[i] + '</a> -- ';
                                 }
                              }
                              $("#pmList").html( pmHtml );
                              $("#amList").html( amHtml );
                              $("#tmList").html( tmHtml );
                              $("#imList").html( imHtml );

                              $("#log").html( msg );
                              $("#redirLink").html( '<a href="'+$SCRIPT_ROOT+'/showUser/'+msg.user.username+'"> Pledges for user</a>' );
                              var userInfo = JSON.stringify( { user : msg.user } );

                              $('li', "#userData").remove();
                              if (msg.user) {
                                  var udList = $("#userData ul");
                                  var udItem = $('<li>');
                                  for (var key in msg.user) {
                                      console.log(key + ' : '+ msg.user[key]);
                                      udItem.append( $('<span> '+key+' : '+msg.user[key]+'</span><br>') );
                                  };
                                  // udItem.add($('<span> name        : '+msg.user.name+'</span><br>'));
                                  // udItem.add($('<span> cmsId       : '+msg.user.cmsId+'</span><br>'));
                                  // udItem.add($('<span> category    : '+msg.user.category +'</span><br>'));
                                  // udItem.add($('<span> status      : '+msg.user.status +'</span><br>'));
                                  // udItem.add($('<span> username    : '+msg.user.username +'</span><br>'));
                                  // udItem.add($('<span> suspended   : '+msg.user.suspended +'</span><br>'));
                                  // udItem.add($('<span> authorBlock : '+msg.user.authorReq +'</span><br>'));
                                  udList.append( udItem );
                              }

                              $('li', "#userTimeLine").remove();
                              if (msg.timeLines) {
                                 for (i=0; i<msg.timeLines.length; i++) {
                                     var timeLinesList = $("#userTimeLine ul");
                                     var item = $('<li>');
                                     for (key in msg.timeLines[i]){
                                            var changed = false;
                                            if (i<msg.timeLines.length-1 && msg.timeLines[i][key]!==msg.timeLines[i+1][key]){
                                                changed = true;
                                            }
                                            item.append($('<span'+(changed ? ' style=\'color: red;\'' : '')+'>'+key+': '+msg.timeLines[i][key]+'</span><br>'));
                                     }
                                     timeLinesList.append(item);
                                 }
                              }
                          })
                          .fail(function( jqXHR, textStatus ) {
                              alert( "error getting info for user "+selectedUser+":"+textStatus);
                          })
                          .always(function(msg) {
        });
    }
};

function getInstId( userFromSearch ) {
    var instId = '';
    if ( userFromSearch ) {
        //    #relyOnSearchFormat
        instId = userFromSearch.substring( userFromSearch.lastIndexOf(' - ')+3, userFromSearch.indexOf('(CERNid:') );
    }
    return instId;
};

function updateUserId(selectedUser) {
    if ( selectedUser ) {
        // get the user's institute and update the #instId selection accordingly
        var url = $SCRIPT_ROOT + "/api/v1.0/userByName/"+selectedUser;
        var jqxhr = $.ajax( { url : url,
                              dataType: "json" } )
                          .done(function(msg) {
                              $("#userId").html( msg.user.cmsId );
                              $("#log").html( msg );
                          })
                          .fail(function( jqXHR, textStatus ) {
                              alert( "error getting info for user "+selectedUser+":"+textStatus);
                          })
                          .always(function(msg) {
        });
    }
};

function loadlist(selobj,url,codeattr,nameattr)
{
    $(selobj).empty().show();
    $(selobj).html("<option>Please select ... </option>");
    $.getJSON(url,{},function(data)  {
        console.log(data);
        $.each(data.data, function(i,obj)  {
            $(selobj).append($('<option>')
                        .val(obj[codeattr])
                        .html(obj[nameattr]));
        });
    });
}

function setupColSearch(table) {

    // Restore state
    var state = table.state.loaded();
    if ( state ) {
      table.columns().eq( 0 ).each( function ( colIdx ) {
        var colSearch = state.columns[colIdx].search;
        if ( colSearch.search ) {
          $( 'input', table.column( colIdx ).footer() ).val( colSearch.search );
        }
      } );
      table.draw();
    }

    // Apply the search
    table.columns().every( function () {
        var that = this;
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                // console.log(this);
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    $("tfoot input").css( { "width" : "100%", "padding" : "3px", "box-sizing" : "border-box" } );
}

function setupColSearchIRPT(irpTable) {

    // Restore state
    var state = irpTable.state.loaded();
    if ( state ) {
      irpTable.columns().eq( 0 ).each( function ( colIdx ) {
        var colSearch = state.columns[colIdx].search;
        if ( colSearch.search ) {
          $( 'input', irpTable.column( colIdx ).footer() ).val( colSearch.search );
        }
      } );
      irpTable.draw();
    }

    // Apply the search
    irpTable.columns().every( function () {
        var that = this;
        $( 'input', this.footer() ).on( 'keyup', function () {
            // console.log('irpTable: keyup: ', that.search(), this.value, irpTable.columns() )
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
}

function setupColSearchH(table) {

    // Restore state
    var state = table.state.loaded();
    if ( state ) {
      table.columns().eq( 0 ).each( function ( colIdx ) {
        var colSearch = state.columns[colIdx].search;
        if ( colSearch.search ) {
          $( 'input', table.column( colIdx ).header().row(0) ).val( colSearch.search );
        }
      } );

      table.draw();
    }

    // Apply the search
    table.columns().every( function () {
        var that = this;
        $( 'input', this.header() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value, regex=true )
                    .draw();
            }
        } );
    } );

    $("thead input").css( { "width" : "100%", "padding" : "3px", "box-sizing" : "border-box" } );
}

function icmsEprDOM() {
    // see: https://datatables.net/examples/basic_init/dom.html
    return '<"top"lifr>t<"bottom"flp>';
}

function icmsEprButtons( data ) {

    var actTable = $.fn.dataTable.tables( {visible: true, api: true} );

    // override if specified
    if (data.table) { actTable = data.table; }

    if ( actTable ) {

        actTable.buttons(0, null).container().empty();

        new $.fn.dataTable.Buttons(actTable, {
            buttons: icmsEprButtonsSetup(actTable, data.addOnButtons, data.noSelAll, data.canAdmin)
        });

        $('#' + data.buttonDivName).empty();
        actTable.buttons(0, null).container().appendTo(
            $('#' + data.buttonDivName)
        );
    }

}

function icmsEprButtonsSetup(table, addOnButtons, noSelAll) {
    var defaultButtons = [];
    defaultButtons.push([
            "colvis"
    ]);
    if ( noSelAll === undefined || !noSelAll ) {
        defaultButtons.push([
            {
                text: 'Select all',
                action: function () {
                    table.rows().select();
                }
            }
            ]);
    };
    defaultButtons.push( [
            {
                text: 'Select none',
                action: function () {
                    table.rows().deselect();
                }
            },
            {
                extend: 'collection',
                text: 'Export selected ... ',
                autoClose: true,
                buttons: [
                    {
                        extend: 'copy',
                        text: 'Copy to clipboard',
                        exportOptions: {
                            modifier: {
                                selected: true
                            }
                        }
                    },
                    {
                        extend: 'csv',
                        text: 'Export to CSV file',
                        fieldSeparator: ';',
                        exportOptions: {
                            modifier: {
                                selected: true
                            }
                        }
                    },
                    {
                        extend: 'excel',
                        text: 'Export to Excel file',
                        exportOptions: {
                            modifier: {
                                selected: true
                            }
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        orientation: 'landscape',
                        pageSize: 'A3',
                        exportOptions: {
                            columns: ':visible',
                            modifier: {
                                selected: true
                            }
                        },
                        customize: function (doc) {
                            var wList = [];
                            for (i = 0; i < table.columns(':visible').length; i++) {
                                wList.push('*');
                            }
                            doc.widths = wList;
                        }
                    },
                ]
            }
        ]);
    if (addOnButtons) {
        for (i=0; i<addOnButtons.length; i++) {
            defaultButtons.push(addOnButtons[i]);
        }
    };
    return defaultButtons;
}

function approvePledges(table, selPledges) {

    // here we now have the full data of the selected rows. Iterate and call approvePlegdge ...
    if ( !selPledges ) {
        console.log("approvePledges> no pledges selected");
        return;
    }
    if ( !selPledges.body ) {
        console.log("approvePledges> empty list of selected pledges");
        return;
    }

    var plCodes = [];
    var plCodeColumn = 21;
    for (i=0; i<selPledges.body.length; i++) {
        if (selPledges.body[i].length < 21) { plCodeColumn = selPledges.body[i].length - 1; }
        plCodes.push( selPledges.body[i][plCodeColumn] );
    }

    var url = $SCRIPT_ROOT + "/approveSelectedPledges";
    var jqxhr = $.ajax( { 
                    url: url, 
                    data: { 'pledges': JSON.stringify(plCodes) }, 
                    method:'POST',
                 } )
                .done( function(retData, textStatus, jqXHR) {
                    if ( textStatus != "success" ) {
                            alert("Got error when approving selected pledges: " + retData['msg']);
                    }
                    alert("got: " + retData['msg'] + " status: "+ textStatus);
                    console.log('approvePledges got: ' + retData.msg);
                    console.log('approvePledges got: ' + retData.err);
                    console.log('approvePledges got: ' + retData.httpStats);
                    table.ajax.reload();
                    // document.location.reload();
                })
                .fail( function( data ) {
                    // alert( 'ERROR AJAX fail when approving selected pledges:\n' + data);
                    console.log(data);
                    console.log('approvePledges got msg: ' + data.responseJSON.msg);
                    console.log('approvePledges got err: ' + data.responseJSON.err);
                    console.log('approvePledges got stat: ' + data.responseJSON.httpStats);
                    alert( data.responseJSON.err );
                    // table.ajax.reload();
                    document.location.reload();
                });
}    

function sendMailInstLeaders(table, selInsts) {

    // here we now have the full data of the selected rows. Iterate and call approvePlegdge ...
    if ( !selInsts ) {
        // console.log("no institutes selected");
        return;
    }
    if ( !selInsts.body ) {
        // console.log("empty list of selected institutes");
        return;
    }
    // select the list of codes:
    var instCodes = [];
    for (i=0; i<selInsts.body.length; i++) {
        // console.log(selInsts.body[i]);
        instCodes.push( selInsts.body[i][ 1 ] )
    }
    console.log(instCodes);
    var postData = { 'instCodes' : JSON.stringify( instCodes ) }
    var jqxhr = $.ajax( { url: $SCRIPT_ROOT + "/getMailAddrsForInstLeaders", data:postData, method:'POST' } )
                .done( function(retData, textStatus, jqXHR) {
                        // table.ajax.reload();
                        if ( textStatus != "success" ) {
                            alert("Error sending mails to institute leaders: " + retData['msg']);
                        }
                        // alert("got: " + retData['msg'] + ' \n'  + retData.mailAddrs + " \n status: "+ textStatus);
                        var mailToString = 'mailto:'+retData.mailAddrs+'?Subject=[iCMS-EPR] Request%20for%20information';
                        if ( retData.ccList ) {
                            mailToString += '&cc='+retData.ccList;
                        }
                        window.location = mailToString;

                        return;
                       })
                .fail( function(data) {
                         alert( 'ERROR sending mails to institute leaders:\n'+data.responseJSON.msg );
                       });
}

function sendMail(table, selPledges) {

    // here we now have the full data of the selected rows. Iterate and call approvePlegdge ...
    if ( !selPledges ) {
        // console.log("no pledges selected");
        return;
    }
    if ( !selPledges.body ) {
        // console.log("empty list of selected pledges");
        return;
    }
    // console.log(selPledges.body[i]);
    // select the list of codes:
    var plCodes = [];
    for (i=0; i<selPledges.body.length; i++) {
        plCodes.push( selPledges.body[i][ selPledges.body[i].length - 1 ] ); // rely on last column being pledge-code
    }
    var postData = { 'plCodes' : JSON.stringify( plCodes ) }
    var jqxhr = $.ajax( { url: $SCRIPT_ROOT + "/getMailAddrsForPledgees", data:postData, method:'POST' } )
                .done( function(retData, textStatus, jqXHR) {
                        // table.ajax.reload();
                        if ( textStatus != "success" ) {
                            alert("Error sending mails to pledgees: " + retData['msg']);
                        }
                        // alert("got: " + retData['msg'] + ' \n'  + retData.mailAddrs + " \n status: "+ textStatus);
                        var mailToString = 'mailto:'+retData.mailAddrs+'?Subject=[iCMS-EPR] Request%20for%20information';
                        if ( retData.ccList ) {
                            mailToString += '&cc='+retData.ccList;
                        }
                        window.location = mailToString;

                        return;
                       })
                .fail( function(data) {
                         alert( 'ERROR sending mails to pledgees:\n'+data.responseJSON.msg );
                       });
}

function getMailAddrs(table, selPledges) {

    // here we now have the full data of the selected rows. Iterate and call approvePlegdge ...
    if ( !selPledges ) {
        // console.log("ERROR: no pledges selected");
        return;
    }
    if ( !selPledges.body ) {
        // console.log("ERROR: empty list of selected pledges");
        return;
    }
    // select the list of codes:
    var plCodes = [];
    for (i=0; i<selPledges.body.length; i++) {
        plCodes.push( selPledges.body[i][ selPledges.body[i].length - 1 ] ); // rely on last column being pledge-code
    }
    var postData = { 'plCodes' : JSON.stringify( plCodes ) }
    var jqxhr = $.ajax( { url: $SCRIPT_ROOT + "/getMailAddrsForPledgees", data:postData, method:'POST' } )
                .done( function(retData, textStatus, jqXHR) {
                        // table.ajax.reload();
                        if ( textStatus != "success" ) {
                            alert("Error sending mails to pledgees: " + retData['msg']);
                        }
                        // alert("got: " + retData['msg'] + ' \n'  + retData.mailAddrs + " \n status: "+ textStatus);
                        var mailAddrs = ''+retData.mailAddrs;
                        // console.log("got: " + mailAddrs);
                        $("#mail-addrs").val(mailAddrs).select();
                        $("#dialog-mail-addrs").dialog().show();

                        return;
                       })
                .fail( function(data) {
                         // console.log (data);
                         alert( 'ERROR getting mail addresses for pledgees:\n'+data.responseJSON.msg );
                       });
}

function addEvtListeners( tableId, table, year ) {

    function makeTable( data ) {
        var innerTable = $("<table id='innerTable' class='tablesorter innerPledgeTable' width='30%' margin-left='40%' cellpadding='0px'></table>");
        var tableHead = $('<thead/>');
        var tableBody = $('<tbody/>');
        innerTable.append( tableHead );
        innerTable.append( tableBody );
        $.each(data, function(rowIndex, r) {
            if (rowIndex == 0) {
                var rowH = $("<tr/>");
                $.each(r, function(colIndex, c) {
                    rowH.append($("<th/>").html(c));
                });
                tableHead.append(rowH);
            } else {
                var rowB = $("<tr/>");
                $.each(r, function(colIndex, c) {
                    rowB.append($("<td/>").html(c));
                });
                tableBody.append(rowB);
            }
        });
        return innerTable;
    };

    // Add event listener for showing/hiding pledge info
    $('#'+tableId+' tbody').on('click', 'td.task-pledgeInfo', function () {
        var tr = $(this).parents('tr');
        var row = table.row( tr );
        var taskCode = row.data().task.code;
        var taskPlInfo = $(this).find("div#task-pledges");
        if (taskPlInfo.html() == '') {
            var jqxhr = $.ajax( {
                            url: $SCRIPT_ROOT + "/api/v1.0/pledgesForTask",
                            method : "POST",
                            data: { "year" : year,
                                    'code' : taskCode.replace('/','_')
                                  },
                         } )
              .done(function(data, textStatus, jqXHR) {
                  // console.log(textStatus);
                  // console.log(data);

                  if ( row.child.isShown() ) {
                      // This row is already open - close it
                      row.child.hide();
                      tr.removeClass('shown');
                  } else {
                      // Open this row (the format() function would return the data to be shown)
                      var innerPlegeTable = makeTable( data.data );
                      innerPlegeTable.tablesorter( { theme : 'blue' } );
                      row.child( innerPlegeTable ).show();
                      tr.addClass('shown');
                  }
               })
              .fail(function() {
                 alert( "error getting pledgeInfo for "+taskCode );
              });
        }
    } );

    // Add event listener for showing/hiding task description
    $('#'+tableId+' tbody').on('click', 'td.task-description', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr ).data();
        var taskCode = row.task.code;
       var itemDesc = $(this).find("div#task-desc");
        if (itemDesc.html() == '') {
            // Assign handlers immediately after making the request,
            // and remember the jqXHR object for this request
            var jqxhr = $.ajax( {
                            url: $SCRIPT_ROOT + "/api/v1.0/taskByCode",
                            method : "POST",
                            data: { "year" : year ,
                                    'code' : taskCode.replace('/','_') },
                         } )
              .done(function(data, textStatus, jqXHR) {
                  var mgrString = data.task.description+'<br/><b>'+data.numMgrs+' Managers:<br/>';
                  mgrString += '<a href="mailto:'+data.mgrEmails+'?Subject=Request%20for%20information">'+data.mgrs+'</a></b>'
                  itemDesc.html(mgrString).css("font-style", "italic").show();
              //  })
              // .fail(function() {
              //   alert( "error for "+taskCode );
              });
        }
        itemDesc.toggle();
    } );

    // Add event listener for showing/hiding activity description
    $('#'+tableId+' tbody').on('click', 'td.activity-description', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr ).data();
        var taskCode = row.task.code;
        var itemDesc = $(this).find("div#activity-desc");
        if (itemDesc.html() == '') {
            // Assign handlers immediately after making the request,
            // and remember the jqXHR object for this request
            var jqxhr = $.ajax( {
                            url: $SCRIPT_ROOT + "/api/v1.0/activityByTaskCode",
                            method : "POST",
                            data: { "year" : year ,
                                    'code' : taskCode.replace('/','_') },
                         } )
              .done(function(data, textStatus, jqXHR) {
                      var mgrString = data.activity.description+'<br/><b>'+data.numMgrs+' Managers:<br/>';
                      mgrString += '<a href="mailto:'+data.mgrEmails+'?Subject=Request%20for%20information">'+data.mgrs+'</a></b>'
                      itemDesc.html(mgrString).css("font-style", "italic").show();
              //  })
              // .fail(function() {
              //   alert( "error for "+taskCode );
              });
        }
        itemDesc.toggle();
    } );

    // Add event listener for showing/hiding project description
    $('#'+tableId+' tbody').on('click', 'td.project-description', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr ).data();
        var taskCode = row.task.code;
        var itemDesc = $(this).find("div#project-desc");
        if (itemDesc.html() == '') {
            // Assign handlers immediately after making the request,
            // and remember the jqXHR object for this request
            var jqxhr = $.ajax( {
                            url: $SCRIPT_ROOT + "/api/v1.0/projectByTaskCode",
                            method : "POST",
                            data: { "year" : year,
                                    'code' : taskCode.replace('/','_') },
                         } )
              .done(function(data, textStatus, jqXHR) {
                  var mgrString = data.project.description+'<br/><b>'+data.numMgrs+' Managers:<br/>';
                  mgrString += '<a href="mailto:'+data.mgrEmails+'?Subject=Request%20for%20information">'+data.mgrs+'</a></b>'
                  itemDesc.html(mgrString).css("font-style", "italic").show();
              //  })
              // .fail(function() {
              //   alert( "error for "+taskCode );
              });
        }
        itemDesc.toggle();
    } );
};


function footerSummaries ( row, data, start, end, display, columns, api ) {
        if (data.length > 0) {
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ? i.replace(/[\$,]/g, '')*1 : typeof i === 'number' ?  i : 0;
            };

            var i = -1;
            var numCols = columns;
            $( "#projectTaskTable tfoot tr" ).children("td").each( function() {
                i++;
                if ( numCols.indexOf(i) != -1 ) {
                    // totals over all pages
                    totals = api.column( i ).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    },0 );

                    // totals_page over this page
                    totals_page = api.column( i, { page: 'current'} ).data().reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                    totals_page = parseFloat(totals_page);
                    totals      = parseFloat(totals);
                    // Update footer
                    $(this).html( '<b>'+totals_page.toFixed(2)+"<br />"+totals.toFixed(2)+'</b>' );
                } else if (i==3) {
                    $(this).html('<b>Sum this page <br /> Sum all pages</b>');
                } else {
                    $(this).text('');
                }
            });
        }
    };


function footerSummary ( row, data, start, end, display, api, tableName, numCols, numColsF, colHdr ) {
    if (data.length > 0) {
        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {
            return typeof i === 'string' ? i.replace(/[\$,]/g, '')*1 : typeof i === 'number' ?  i : 0;
        };

        var totsPage = {};
        var tots = {};

        var hdrCol = 3;
        if (colHdr) { hdrCol = colHdr; }
        // console.log( 'sumHeaders at: ' + hdrCol.toFixed(2) );

        var i = -1;
        $( "#"+tableName+" tfoot tr" ).children("td").each( function() {
            i++;
            // console.log('table:', tableName, 'i=', i, ' -- api:', api.column( i ).visible() );
            // skip hidden columns in the evaluation
            if ( ! api.column( i ).visible() ) {
                i++;
            }

            if ( numCols.indexOf(i) != -1 ) {
                // totals over all pages
                totals = api.column( i ).data().reduce( function (a, b) {
                    var sum = -1 ;
                    if ( intVal(a) !== NaN && intVal(b) !== NaN ) {
                        sum = intVal(a) + intVal(b);
                    }
                    // console.log('table:', tableName, 'i=', i, ' -- fS 0: a=', a, intVal(a), ' b=', b, intVal(b), ' sum: ', sum );
                    return sum;
                },0 );

                // totals_page over this page
                totals_page = api.column( i, { page: 'current'} ).data().reduce( function (a, b) {
                    // console.log('fS 1: a=', intVal(a), ' b=', intVal(b) )
                    var sum = -1 ;
                    if ( intVal(a) !== NaN && intVal(b) !== NaN ) {
                        sum = intVal(a) + intVal(b);
                    }
                    return sum;
                }, 0 );

                totals_page = parseFloat(totals_page);
                totals      = parseFloat(totals);
                // Update footer
                $(this).html( '<b>'+totals_page.toFixed(2)+"<br />"+totals.toFixed(2)+'</b>' );
                totsPage[i] = totals_page;
                tots[i] = totals;
            } else {
                if (i == hdrCol) {
                    $(this).html('<b>Sum this page <br /> Sum all pages</b>');
                } else {
                    $(this).text('');
                }
            }
            // update the ratios
            if ( numColsF.indexOf(i) != -1 ) {
                var itemWorked      = parseFloat(tots[i-1])/parseFloat(tots[numCols[0]]);
                var itemWorked_page = parseFloat(totsPage[i-1])/parseFloat(totsPage[numCols[0]]);
                // Update footer
                $(this).html('<b>' + itemWorked_page.toFixed(3) + "<br />" + itemWorked.toFixed(3) + '</b>');
            }
        });
    }
}

// set some defaults for all the DataTable objects:

$.extend( true, $.fn.dataTable.defaults, {

    dom : '<"top"lifr>t<"bottom"flp>',

    buttons : [
            "colvis",
            {
                text: 'Select all',
                action: function () {
                    table.rows().select();
                }
            },
            {
                text: 'Select none',
                action: function () {
                    table.rows().deselect();
                }
            },
            {
                extend: 'collection',
                text: 'Export selected ... ',
                autoClose: true,
                buttons : [
                    { extend: 'copy',
                      text:'Copy to clipboard',
                      exportOptions: {
                            modifier: {
                                selected: true
                            }
                      }
                    },
                    { extend: 'csv',
                      text:'Export to CSV file',
                      exportOptions: {
                            modifier: {
                                selected: true
                            }
                      }
                    },
                    { extend: 'excel',
                      text:'Export to Excel file',
                      exportOptions: {
                            modifier: {
                                selected: true
                            }
                      }
                    },
                    { extend: 'pdfHtml5',
                        orientation: 'landscape',
                        pageSize: 'A3',
                        exportOptions : {
                            columns: ':visible',
                            modifier: {
                                selected: true
                            }
                      },
                      customize: function ( doc ) {
                            var wList = [];
                            for (i=0; i < table.columns(':visible').length; i++) { wList.push('*');}
                            doc.widths = wList;
                      }
                    },
                ]
            },
        ],
} );

function pledgeTableFooterCallback( row, data, start, end, display) {
    return {
    }
}

function pledgeTableSetupAjax( ajaxInfo, pldTableDivName ) {
    // console.log('pledgeTableSetupAjax> ajaxinfo: ', ajaxInfo);
    return {
        url: ajaxInfo.url,
        data: ajaxInfo.data,
        method:"POST",
        "dataSrc": "allProjects",
        beforeSend: function(xhr, settings) {
            var d = new Date();
            start = d.getTime();
            $("#loading-time").html('loading info ... ');
            xhr.setRequestHeader("X-CSRFToken", csrftoken)
            $('#'+pldTableDivName).hide();
            $('#no'+pldTableDivName).hide();
        },
        complete: function( json ) {
            // console.log('pledgeTableSetupAjax> json:', json);
            var stop = new Date().getTime();
            $("#loading-time").html('Loading took '+ (stop-start).toFixed(0) +' msec.');
            if ( (json.responseJSON !== undefined) && (json.responseJSON.allProjects.length > 0) ) {
                $('#'+pldTableDivName).show();
                $('#no'+pldTableDivName).hide();
            } else {
                $('#'+pldTableDivName).hide();
                $('#no'+pldTableDivName).show();
            }

        },
        error: function( data , textStatus /* , jqXHR */ ) {
            if (data.status !== 200) {
                console.log('error from API ');
                console.log('data:', data);
                var msg = 'Error from API call ... ';
                if (data.statusText !== 'OK') {
                    console.log(data.responseText);
                    msg = msg + data.responseText;
                }
                if (textStatus !== undefined) {
                    console.log('textStatus:', textStatus);
                    msg = msg + ' | ' + textStatus;
                }
                alert(msg);
            }
        },
    }
}

function pledgeTableSetupColDefs () {
    return [
        // add a first column with checkboxes
        { targets: 0,
          orderable: false,
          className: 'select-checkbox'
        },
        { targets:1,
          className: "project-description",
          "render": function ( data, type, row ) { return data+'<div id="project-desc"></div>'; }
        },
        { targets:2,
            className: "activity-description",
            "render": function ( data, type, row ) { return data+'<div id="activity-desc"></div>'; }
          },
          { targets:3,
            "render": function ( data, type, row ) { return data; }
          },
        {
          targets: 4,
          className: 'task-description left-border',
          "render": function ( data, type, row ) { return data+'<div id="task-desc"></div>'; }
        },
        {   // borders for this one too:
            targets: 11,
            className: 'left-border',
        },
        {
          targets: 21,
          className : 'left-border',
          "render": function ( data, type, row ) {
              return '<a href="'+$SCRIPT_ROOT+'/showPledge/'+data+'"> '+data+' </a>';
          },
          visible: false,
        },
        { // format the numbers to 2 decimals
          targets: [13,14,15,16], "render":
                function ( data, type, row ) {
                         if ( jQuery.isNumeric(data) ) {
                             return ' '+data.toFixed(2);
                         } else {
                             console.log("Invalid data "+data+" found in row: " + row)
                             return '-.0';
                         }
                }
        },
        { // hide year, pledge and task code columns (count starts at 0):
            targets: [18,21,22],
            visible: false,
        },
    ]
}

function instRespPledgeTableSetupColDefs () {
    return [
        // add a first column with checkboxes
        { targets : 0,
          orderable: false,
          className: 'select-checkbox'
        },
        { targets:1,
          className : "project-description",
          "render": function ( data, type, row ) { return data+'<div id="project-desc"></div>'; }
        },
        { targets:2,
          className : "activity-description",
          "render": function ( data, type, row ) { return data+'<div id="activity-desc"></div>'; }
        },
        { targets:3,
            "render": function ( data, type, row ) { return data; }
        },
        {
          targets : 4,
          className : 'task-description left-border',
          "render": function ( data, type, row ) { return data+'<div id="task-desc"></div>'; }
        },
        {   // borders for this one too:
            targets : 12,
            className : 'left-border',
            visible : false,
        },
        {
          targets : 21,
          visible : false,
          className : 'left-border',
          "render": function ( data, type, row ) {
              return '<a href="'+$SCRIPT_ROOT+'/showPledge/'+data+'"> '+data+' </a>';
          },
        },
        { // format the numbers to 2 decimals
            targets: [6,13,14,15,16], "render":
                function ( data, type, row ) {
                         if ( jQuery.isNumeric(data) ) {
                             return ' '+data.toFixed(2);
                         } else {
                             console.log("Invalid data "+data+" found in row: " + row)
                             return '-.0';
                         }
                }
        },
        { // hide user, pledge and task code columns (count starts at 0):
            targets: [11,20,21,22,23,24],
            visible: false,
        },
    ]
}

function pledgeTableSetupCreatedRow (row, data, index) {
    if (data[5] != '-') {
        var col = getColour(data[6]);
        $('td', row).eq(5).addClass(col);
        $('td', row).eq(6).addClass(col);
    }
    if (data[13] === 0.00) {
        var col = 'red';
        $('td', row).eq(13).addClass(col);
    }
    // console.log("editTask : "+ data[9]);
    if (data[10].trim != '') {
        $(row).addClass('canManage');
    }
    if (data[8] === 'InstResp') {
        $(row).addClass('instRespTask');
    }
}

function projPldTableSetup ( ajaxInfo, projTableName, pldTableDivName ) {

    // console.log('ajaxinfo: ', ajaxInfo);
    // console.log('pldTableDivName: ', pldTableDivName)

    return {
        "deferRender": true,
        stateSave: true,
        // "fixedHeader" : true,
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        // see: https://datatables.net/examples/basic_init/dom.html
        "dom": icmsEprDOM(),
        "footerCallback": function (row, data, start, end, display ) {
            return footerSummary( row, data, start, end, display, this.api(), projTableName, [5,13,14,15], [6,16], 4)
        },
        ajax: pledgeTableSetupAjax( ajaxInfo, pldTableDivName ),
        "columnDefs": pledgeTableSetupColDefs(),
        // add colour to the taks done related fields
        "createdRow": pledgeTableSetupCreatedRow,
        select : {  // select on the checkboxes:
            style:    'multi',
            selector: 'td:first-child',
            rows: {
                    _: "You have selected %d rows",
                    0: "Click a row to select it",
                    1: "Only 1 row selected"
                }
        }
    }
};

function instRespPldTableSetup ( ajaxInfo, projTableName, pldTableDivName ) {

    // console.log('ajaxinfo: ', ajaxInfo);
    // console.log('pldTableDivName: ', pldTableDivName)

    return {
        "deferRender": true,
        stateSave: true,
        // "fixedHeader" : true,
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        // see: https://datatables.net/examples/basic_init/dom.html
        "dom": icmsEprDOM(),
        "footerCallback": function (row, data, start, end, display ) {
            // note, the count here includes the "hidden" user column (11), so we have the cols 13,14,15 for hte pledges
            return footerSummary( row, data, start, end, display, this.api(), projTableName, [5,13,14,15], [6,16], 4)
        },
        ajax: pledgeTableSetupAjax( ajaxInfo, pldTableDivName ),
        "columnDefs": instRespPledgeTableSetupColDefs(),
        // add colour to the taks done related fields
        "createdRow": pledgeTableSetupCreatedRow,
        select : {  // select on the checkboxes:
            style:    'multi',
            selector: 'td:first-child',
            rows: {
                    _: "You have selected %d rows",
                    0: "Click a row to select it",
                    1: "Only 1 row selected"
                }
        }
    }
};



function addOnButtonsPledges ( canAdmin, table ) {

    var aob = [];
    if (canAdmin !== undefined && canAdmin === true) {
        aob.push(
                {
                text: "Approve selected",
                action: function ( e, dt, node, config ) {
                            approvePledges( table, table.buttons.exportData( {
                                modifier: { selected: true }
                            } ) );
                            dt.rows().deselect();
                        } // end function
                } // end button "Approve selected"
        )
    }
    aob.push([
            {
            text: "SendMail selected",
            action: function ( e, dt, node, config ) {
                        sendMail( table, table.buttons.exportData( {
                            modifier: { selected: true }
                        } ) );
                        dt.rows().deselect();
                    } // end function
            }, // end button "SendMail selected"
            //  document.getElementById("copyButton").addEventListener("click", function() {
            //    copyToClipboard(document.getElementById("copyTarget"));
            //  });
            {
            text: "Copy selected mail addr to clipboard",
            action: function ( e, dt, node, config ) {
                        getMailAddrs( table, table.buttons.exportData( {
                            modifier: { selected: true }
                        } ) );
                        dt.rows().deselect();
                    } // end function
            }, // end button "Copy selected mail addr to clipboard"
        ])
    return aob;
}


function addOnButtonsIRPledges ( canAdmin, table ) {

    var aob = [];
    if (canAdmin !== undefined && canAdmin === true) {
        aob.push(
                {
                text: "Approve selected",
                action: function ( e, dt, node, config ) {
                            approveIRPledges( table, table.buttons.exportData( {
                                modifier: { selected: true }
                            } ) );
                            dt.rows().deselect();
                        } // end function
                } // end button "Approve selected"
        )
    }
    aob.push([
            {
            text: "SendMail selected",
            action: function ( e, dt, node, config ) {
                        sendMail( table, table.buttons.exportData( {
                            modifier: { selected: true }
                        } ) );
                        dt.rows().deselect();
                    } // end function
            }, // end button "SendMail selected"
            //  document.getElementById("copyButton").addEventListener("click", function() {
            //    copyToClipboard(document.getElementById("copyTarget"));
            //  });
            {
            text: "Copy selected mail addr to clipboard",
            action: function ( e, dt, node, config ) {
                        getMailAddrs( table, table.buttons.exportData( {
                            modifier: { selected: true }
                        } ) );
                        dt.rows().deselect();
                    } // end function
            }, // end button "Copy selected mail addr to clipboard"
        ])
    return aob;
}


function approveIRPledges(table, selPledges) {

    // here we now have the full data of the selected rows. Iterate and call approvePlegdge ...
    if ( !selPledges ) {
        console.log("approveIRPledges> no pledges selected");
        return;
    }
    if ( !selPledges.body ) {
        console.log("approveIRPledges> empty list of selected pledges");
        return;
    }

    console.log( selPledges );

    var plCodes = [];
    var taskCodeColumn = 24;
    var instCodeColumn = 12;
    for (i=0; i<selPledges.body.length; i++) {
        plCodes.push( [selPledges.body[i][instCodeColumn], selPledges.body[i][taskCodeColumn]] );
    }

    var url = $SCRIPT_ROOT + "/approveSelectedIRPledges";
    var jqxhr = $.ajax( { 
                    url: url, 
                    data: { 'pledges': JSON.stringify(plCodes) }, 
                    method:'POST',
                 } )
                .done( function(retData, textStatus, jqXHR) {
                    if ( textStatus != "success" ) {
                            alert("Got error when approving selected pledges: " + retData['msg']);
                    }
                    alert("got: " + retData['msg'] + " status: "+ textStatus);
                    console.log('approveIRPledges got: ' + retData.msg);
                    console.log('approveIRPledges got: ' + retData.err);
                    console.log('approveIRPledges got: ' + retData.httpStats);
                    table.ajax.reload();
                    // document.location.reload();
                })
                .fail( function( data ) {
                    // alert( 'ERROR AJAX fail when approving selected pledges:\n' + data);
                    console.log(data);
                    console.log('approveIRPledges got msg: ' + data.responseJSON.msg);
                    console.log('approveIRPledges got err: ' + data.responseJSON.err);
                    console.log('approveIRPledges got stat: ' + data.responseJSON.httpStats);
                    alert( data.responseJSON.err );
                    // table.ajax.reload();
                    document.location.reload();
                });
}    