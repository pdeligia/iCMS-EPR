import socket
import os
import re

import sqlalchemy

from flask import render_template, redirect, request, url_for, flash, session, abort, current_app
from flask_login import login_user, logout_user, login_required, current_user

from config import prodHostList

from . import auth
from .. import db
from ..models import EprUser
from .forms import LoginForm, LoginAsForm

from ..ldapAuth import authenticate, getUserFromEmail

from ..main.Helpers import getPledgeYear

@auth.before_app_request
def before_request():
    if current_user.is_authenticated:
        pass

def next_is_valid(url):
   return True

@auth.route('/login', methods=['GET', 'POST'])
def login():

    if 'year' not in session:
        session['year'] = getPledgeYear()
        session.modified = True

    # current_app.logger.debug('login> running on %s ' % db.engine.url)

    # check if we are authenticated via SSO, if so ...
    req = request._get_current_object()

    # current_app.logger.debug( "login> got headers: %s " % req.headers )

    shibUser = req.headers.get('User', None)
    user = None
    if shibUser:
        # ... get the actual username and authenticate locally
        userEmail = shibUser

        #-ap: "hack": the new SSO infrastructure returns the username instead of the email, 
        # so we check if there is an '@' in the name, if not, we assume it's the username:
        if '@' in userEmail:
            username = getUserFromEmail(userEmail)
        else:
            username = shibUser

        if not username:
            msg = "login> no user found in LDAP for email %s - rolling back " % (userEmail,)
            current_app.logger.error( msg )
            flash(msg, 'error')
            # roll-back the session and retry:
            db.session.rollback()
        else:
            if username == 'andreasp': username = 'pfeiffer' # work around issues with my primary login.
            current_app.logger.info( "login> checking in shibbolet - auth user is: %s" % username )
            try:
                user = db.session.query(EprUser).filter(EprUser.username==username).first()
            except Exception as e:
                current_app.logger.error( "login> DB error when searching for user %s (got:%s) - roll-back and trying again ... " % (username, str(e) ) )
                # roll-back the session and retry:
                db.session.rollback()
            try:
                user = db.session.query(EprUser).filter_by(username=username).first()
            except Exception as e:
                current_app.logger.error( "login> DB error when searching for user %s (got:%s) - second try failed ... " % (username, str(e)) )
                # roll-back the session and retry:
                db.session.rollback()

        if user: # only allow if we found a user:
            current_app.logger.info( "login> found valid shibboleth user %s - logging in" % username )
            session['sso'] = True
            session.modified = True
            login_user(user, False)
            return redirect(request.args.get('next') or url_for('main.showMine'))
        else:
            msg = 'ERROR: no valid user for shibboleth login %s found in database ' % username
            current_app.logger.error( "login> %s" % msg )
            flash(msg, 'error')

    # for prodHost in prodHostList:
    #     if prodHost in socket.gethostname():
    #         flash("SSO login succeeded but did not return a valid username, please login with your CERN account/pwd")

    # come here for development if we don't have SSO ...
    form = LoginForm()
    user = None
    if form.validate_on_submit():
        try:
            user = db.session.query(EprUser).filter(EprUser.username == str(form.username.data)).first()
        except sqlalchemy.exc.DatabaseError as e:
            if "ORA-02396" in str(e): # ORA-02396: exceeded maximum idle time, please connect again
                # roll-back and then retry:
                db.session.rollback()
                user = db.session.query(EprUser).filter_by( username=str(form.username.data) ).first( )
            else:
                current_app.logger.warning("exception from DB when querying user %s : got %s  -- rollback and retry ... " % (str(form.username.data), str(e)))
                db.session.rollback()
                user = db.session.query(EprUser).filter_by(username=str(form.username.data)).first()
                current_app.logger.warning("... rollback and retry sucessful, user is %s " % user.username )

        # current_app.logger.debug( 'login(noShib)> user is %s (form.username=%s)' % (user, form.username.data) )
        # check if we're running in testing env, if so, login the test user
        localDBallowed = "EPR_LOGIN_USE_LOCAL" in os.environ
        localDBallowed = localDBallowed and "TESTING" in current_app.config and 'TEST_DATABASE_URL' in os.environ 
        current_app.logger.debug( 'login(noShib)> local login (no auth) allowed: %s ' % str(localDBallowed) )

        if user is not None and localDBallowed :
            current_app.logger.debug( "login-TESTING> found env set for testing - logging in %s" % user.username )
            session[ 'testing' ] = True
            session.modified = True
            login_user( user, False )
            current_app.logger.debug( "login-TESTING> user logged in, redirecting. req.args: %s -- next = %s user %s " % (str(request.args), request.args.get( 'next' ), user.username) )
            return redirect( request.args.get( 'next' ) or url_for( 'main.showMine' ) )

        if user is not None:
            if authenticate(form.username.data, form.password.data):
                login_user(user, form.remember_me.data)
        else:
            msg = 'User %s not found in DB ... ' % form.username.data
            current_app.logger.warning( msg )
            flash(msg)
            # current_app.logger.debug( '===> %s' % db.session.query(EprUser).all() )
        next = request.args.get('next')
        # next_is_valid should check if the user has valid
        # permission to access the `next` url
        if not next_is_valid(next):
           return abort(400)

        current_app.logger.info( "login> redirecting. req.args: %s -- next = %s user %s " % (str(request.args), request.args.get('next'), user) )
        return redirect(request.args.get('next') or url_for('main.showMine'))
    else:
        if form.errors:
            current_app.logger.warning( 'login(formErrors)> ERROR in form: %s' % str(form.errors) )

    flash('Invalid username or password.', 'error')
    return render_template('auth/login.html', form=form)


@auth.route('/logout')
@login_required
def logout():
    realHRid = session.get('REAL_CMS_ID', None)
    logout_user()
    flash('You have been logged out.')

    if realHRid:
        flash( 'trying to re-login your real HR id %s' % realHRid )
        user = db.session.query( EprUser ).filter( EprUser.hrId == realHRid ).one()
        # user = User( person=you )
        login_user( user )
        flash( 'You were logged back in as yourself, an admin!' )
        del session[ 'REAL_CMS_ID' ]
        session.modified = True

    if session.get('sso' ,False):
        ssoLogoutUrl = 'https://login-dev.cern.ch/adfs/ls/?wa=wsignout1.0'
        return redirect (ssoLogoutUrl)

    return redirect(url_for('main.index'))

@auth.route('/loginAs', methods=['GET','POST'])
@login_required
def loginAs():

    if not current_user.is_wizard():
        flash("Sorry, you are not allowed to use this function.")
        next = request.args.get('next')
        # next_is_valid should check if the user has valid
        # permission to access the `next` url
        if not next_is_valid(next):
           return abort(400)

        current_app.logger.info( "redirecting. next = %s user %s " % (request.args.get('next'), current_user) )

        return redirect(request.args.get('next') or url_for('main.showMine'))

    if 'year' not in session:
        session['year'] = getPledgeYear()
        session.modified = True

    form = LoginAsForm()

    if form.validate_on_submit():
        userSel = request.form.get('userSel',None) # current_user.id

        current_app.logger.debug( "request to loginAs user %s " % (userSel,) )

        try:
            #     #relyOnSearchFormat
            hrId = int(re.match(r'.*\(CERNid:\s*(\d+)\)\s*', userSel).group(1))
            current_app.logger.info( "loginAs> got hrId %i from %s" % (hrId, userSel) )
        except AttributeError:
            flash('User %s not found in DB - nothing done ... ' % userSel)
            return redirect(request.args.get('next') or url_for('main.showMine'))

        user = None
        try:
            user = db.session.query(EprUser).filter_by( hrId=hrId ).one()
        except sqlalchemy.orm.exc.NoResultFound:
            pass

        if user is not None:
            realHRid = current_user.hrId if 'REAL_CMS_ID' not in session else session['REAL_CMS_ID']
            logout_user()
            user = db.session.query(EprUser).filter_by( hrId=hrId ).one()
            login_user(user)
            session[ 'REAL_CMS_ID' ] = realHRid
            session.modified = True
            current_app.logger.info("real HR id is: %s" % realHRid)
            flash("real HR id is: %s" % realHRid )
        else:
            flash('User for %s not found in DB - nothing done ... ' % hrId)

        next = request.args.get('next')
        # next_is_valid should check if the user has valid
        # permission to access the `next` url
        if not next_is_valid(next):
           return abort(400)

        current_app.logger.info( "redirecting. next = %s user %s " % (request.args.get('next'), user) )
        return redirect(request.args.get('next') or url_for('main.showMine'))

    return render_template('auth/loginAs.html', form=form)


