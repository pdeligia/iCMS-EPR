from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField,  TextAreaField, BooleanField, SelectField, SubmitField, IntegerField, DecimalField
from wtforms.validators import DataRequired, Length, Optional


class LoginForm(FlaskForm):
    username = StringField('username', validators=[DataRequired(), Length(1, 64)])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Keep me logged in')
    submit = SubmitField('Log In')

class LoginAsForm(FlaskForm):
    username = StringField('username', validators=[Optional()])
    submit = SubmitField('Login As ... ')
