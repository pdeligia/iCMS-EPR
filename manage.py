#!/usr/bin/env python

#  Copyright (c) 2015-2021 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import sys
import os

sys.path.append( os.path.join(os.getcwd(), 'scripts') )

COV = None
if os.environ.get('ICMS_EPR_COVERAGE'):
    import coverage
    COV = coverage.coverage(branch=True, include='app/*')
    COV.start()

if os.path.exists('.env'):
    print('Importing environment from .env...')
    for line in open('.env'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]

from app import create_app, db as theDB
from app.models import EprUser, EprInstitute, Role, TimeLineInst, TimeLineUser

from flask_script import Manager, Shell
from flask_migrate import Migrate # , MigrateCommand

# select a configuration depending on where we run (as set in config):
from config import prodLevel
# set the default:
confLevel = os.getenv('ICMS_EPR_CONFIG') or 'default'
if prodLevel == 'prod': # ignore any ENV var for production:
    confLevel = 'production'

theApp = create_app(os.getenv('ICMS_EPR_CONFIG') or confLevel)
manager = Manager(theApp)
migrate = Migrate(theApp, theDB)

def setupEnv():
    import os, sys, string

    # for command-line completion :
    import rlcompleter
    rlcompleter.readline.parse_and_bind("tab: complete")

    try :
        # for history (across-sessions)
        import readline
        # limit history file to 10000 lines
        readline.set_history_length(10000)
        # read previous file (if existing)
        histfile = os.path.join(os.environ["HOME"], ".eprHist")
        try:
            readline.read_history_file(histfile)
        except IOError:
            print("cannot read history-file ", histfile)
        # register function to write history file at exit
        import atexit
        atexit.register(readline.write_history_file, histfile)
        del histfile
        # end history
    except :
        print("\nerror while accessing history-file %s \n" % histfile)


def make_shell_context():
    setupEnv()
    from app.models import Category, Project, CMSActivity, Task, Pledge, AllInstView, EprUser, EprInstitute, Manager
    return dict(app=theApp, db=theDB,
                User = EprUser,
                Institute = EprInstitute,
                Manager = Manager,
                Role = Role,
                Category = Category,
                Project = Project,
                CMSActivity = CMSActivity,
                Task = Task,
                Pledge = Pledge,
                AllInstView = AllInstView,
                TimeLineInst = TimeLineInst,
                TimeLineUser  =TimeLineUser
                )

manager.add_command("shell", Shell(make_context=make_shell_context))
# manager.add_command('db', MigrateCommand)


@manager.command
def test(coverage=False):
    """Run the unit tests."""
    if coverage and not os.environ.get('ICMS_COVERAGE'):
        import sys
        os.environ['ICMS_COVERAGE'] = '1'
        os.execvp(sys.executable, [sys.executable] + sys.argv)
    import unittest
    tests = unittest.TestLoader().discover('tests')
    print('unit-tests - not running for now')
    # unittest.TextTestRunner(verbosity=2).run(tests)
    if COV:
        COV.stop()
        COV.save()
        print('Coverage Summary:')
        COV.report()
        basedir = os.path.abspath(os.path.dirname(__file__))
        covdir = os.path.join(basedir, 'tmp/coverage')
        COV.html_report(directory=covdir)
        print('HTML version: file://%s/index.html' % covdir)
        COV.erase()


@manager.command
def profile(length=25, profile_dir=None):
    """Start the application under the code profiler."""
    from werkzeug.contrib.profiler import ProfilerMiddleware
    theApp.wsgi_app = ProfilerMiddleware(theApp.wsgi_app, restrictions=[length],
                                      profile_dir=profile_dir)
    theApp.run()


@manager.command
def updateShiftTasks():

    import importEPR
    importEPR.updateShiftTasks()


@manager.command
def updateInstUsers():

    import updateInstUsers
    updateInstUsers.updateAll()


@manager.command
def updateEpr():

    import updateEPR
    updateEPR.updateAll()

@manager.command
def fix2015Epr():

    import updateEPR
    updateEPR.fixImportErrors()

@manager.command
def updateMgrs():

    # db.create_all()
    import SetupManagers
    SetupManagers.setup()

@manager.command
def import2016EPR():

    import importNewPAT
    importNewPAT.importAll()

@manager.command
def update2016TrackerPAT():

    import update2016TrackerPAT
    update2016TrackerPAT.update2016TrackerPAT()

@manager.command
def update2016HCalPAT():

    import update2016HCalPAT
    update2016HCalPAT.update2016HCalPAT()

@manager.command
def importNewPPS2018():

    import importNewPPS2018
    importNewPPS2018.importNewPPS2018()


@manager.command
def updateDue():

    # db.create_all()
    import updateDue
    updateDue.updateDue()

@manager.command
def exportToJson():

    import exportAllToJson
    exportAllToJson.exportAll()

@manager.command
def importFromJson():

    import importAllFromJson
    importAllFromJson.importAll()

@manager.command
def updateInstView():
    """Run update tasks."""

    from flask_migrate import upgrade
    from app.models import Role, EprUser, EprInstitute, Project, CMSActivity, Task

    # db.create_all()

    import updateInstView
    updateInstView.updateAllInstView(year=2020)
    print('\n', '-' * 80, '\n')
    updateInstView.updateAllInstView(year=2021)

@manager.command
def updateShifts():

    import importShifts
    importShifts.importShifts(2020)

@manager.command
def updateMuonMgrs2020():

    import updateMuonMgrs2020
    updateMuonMgrs2020.updateManagers()

@manager.command
def importShifts():

    # db.create_all()

    import importShifts
    # importShifts.importShifts(2015)
    # importShifts.importShifts(2016)
    importShifts.importShifts(2017)

@manager.command
def updateInfo():

    # db.create_all()

    import importShifts
    # importShifts.importShifts(2015)
    # importShifts.importShifts(2016)
    importShifts.importShifts(2017)

    updateDue()
    updateInstView()


@manager.command
def deploy():
    """Run deployment tasks."""
    from flask_migrate import upgrade
    from app.models import Role

    if not os.path.exists('data-dev.sql'):
        # db.create_all()

        # create user roles
        Role.insert_roles()

    # migrate database to latest revision
    upgrade()

    if theApp.config['DEBUG']:
        import importUserInst
        importUserInst.importAll( )

        import importEPR
        importEPR.doAll()

        import SetupManagers
        SetupManagers.setup()

        import importShifts
        importShifts.importShifts()

        # updateDue()
        # updateInstView()

        # import SetupDummies
        # SetupDummies.setup()

        pass

if __name__ == '__main__':
    manager.run()
