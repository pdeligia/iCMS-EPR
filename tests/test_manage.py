
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import re
import json

try:
    from .BaseTestClass import *
except ImportError:
    from BaseTestClass import *

log = logging.getLogger( "Testing.test_manage" )
logging.basicConfig( stream=sys.stderr )
logging.getLogger( "Testing.test_manage" ).setLevel( logging.DEBUG )

class ManageTestCase ( BaseTestCase ):

    def doPage(self, endPoint, username, data, isIn, isNotIn, retStat = 200, showPage=False, adminOK=False, **kwargs):
        user = db.session.query(EprUser).filter_by( username=username ).one( )
        if not adminOK:
            self.assertFalse( user.is_wizard( ) )
        self.login( user.username )

        response = self.client.post( url_for( endpoint=endPoint, **kwargs ),
                                     data=data,
                                     follow_redirects=True )

        # log.info("got response status %s " % str(response.status) )

        if showPage: log.info( "got response data %s " % str( response.data.decode('utf-8') ) )

        self.assertEqual( response.status_code, retStat )

        for k in isIn:
            self.assertIn( k, getHtmlDoc( response ) )
        for k in isNotIn :
            self.assertNotIn( k, getHtmlDoc( response ) )

    def test_changeLvl3ForTask( self ):

        user = db.session.query(EprUser).filter_by( username='p1m1' ).one( )
        self.assertFalse( user.is_wizard( ) )

        self.login( user.username )

        taskT1 = db.session.query(Task).filter_by(code='t1').one()
        lvl3   = db.session.query(Level3Name).filter_by(id=taskT1.level3).one()
        lvl3One   = db.session.query(Level3Name).filter_by(name='lvl3-one').one()

        taskData = { 'name' : taskT1.name,
                     'description' : taskT1.description,
                     'neededWork' : taskT1.neededWork,
                     'activities': 7,  # has to be in the same project
                     'chgLvl3': True,
                     'level3': '%s : %s' % (lvl3One.id, lvl3One.name),
                     }

        # access the page and remove an observer
        response = self.client.post( url_for( 'manage.task', code=taskT1.code ),
                                     data = taskData,
                                     follow_redirects=True )

        # log.info("got response status %s " % str(response.status) )
        # log.info( "got response data %s " % str( response.data.decode('utf-8')[:30000] ) )

        self.assertEqual( response.status_code, 200 )
        self.assertNotIn( 'form-group  has-error', response.data.decode('utf-8')[:30000] )
        self.assertIn( 'task 1 (a1 p1)', response.data.decode('utf-8')[:30000] )
        self.assertIn( 'task info for ta1 successfully updated - new level3Name is lvl3-one (2).', response.data.decode('utf-8')[:30000] )

        # change it back to not break other tests ...

        taskData['level3'] = '%s : %s' % (lvl3.id, lvl3.name)
        response = self.client.post( url_for( 'manage.task', code='t1' ),
                                     data = taskData,
                                     follow_redirects=True )

        # log.info("got response status %s " % str(response.status) )
        # log.info( "got response data %s " % str( response.data.decode('utf-8')[:30000] ) )

        self.assertEqual( response.status_code, 200 )
        self.assertNotIn( 'form-group  has-error', response.data.decode('utf-8')[:30000] )
        self.assertIn( 'task 1 (a1 p1)', response.data.decode('utf-8')[:30000] )
        self.assertIn( 'task info for ta1 successfully updated - new level3Name is Unknown (1).', response.data.decode('utf-8')[:30000] )

        self.logout( )

    def test_changeActForTask( self ):

        user = db.session.query(EprUser).filter_by( username='p1m1' ).one( )
        self.assertFalse( user.is_wizard( ) )

        self.login( user.username )

        taskT1 = db.session.query(Task).filter_by(code='t1').one()
        lvl3   = db.session.query(Level3Name).filter_by(id=taskT1.level3).one()
        print('\n:::> lvl3:', lvl3)
        taskData = { 'name' : taskT1.name,
                     'description' : taskT1.description,
                     'neededWork' : taskT1.neededWork,
                     'activities': 7,  # has to be in the same project
                     'chgAct': True,
                     'level3': '%s : %s' % (lvl3.id, lvl3.name),
                     }

        # access the page and remove an observer
        response = self.client.post( url_for( 'manage.task', code=taskT1.code ),
                                     data = taskData,
                                     follow_redirects=True )

        # log.info("got response status %s " % str(response.status) )
        # log.info( "got response data %s " % str( response.data.decode('utf-8')[:30000] ) )

        self.assertEqual( response.status_code, 200 )
        self.assertNotIn( 'form-group  has-error', response.data.decode('utf-8')[:30000] )
        self.assertIn( 'task 1 (a1 p1)', response.data.decode('utf-8')[:30000] )
        self.assertIn( 'task info for ta1 successfully updated - new activity is ac4a.', response.data.decode('utf-8')[:30000] )

        # change it back to not break other tests ...

        taskData['activities'] = 1
        response = self.client.post( url_for( 'manage.task', code='t1' ),
                                     data = taskData,
                                     follow_redirects=True )

        # log.info("got response status %s " % str(response.status) )
        # log.info( "got response data %s " % str( response.data.decode('utf-8')[:30000] ) )

        self.assertEqual( response.status_code, 200 )
        self.assertNotIn( 'form-group  has-error', response.data.decode('utf-8')[:30000] )
        self.assertIn( 'task 1 (a1 p1)', response.data.decode('utf-8')[:30000] )
        self.assertIn( 'task info for ta1 successfully updated - new activity is ac1.', response.data.decode('utf-8')[:30000] )

        self.logout( )


    def test_proj_addTaskObs( self ) :

        data = { 'patType'  : 'task',
                 'patCode'  : 't1',
                 'userType' : 'observer',
                 'userList' : '9901,9902',  # string of comma-separated cmsIds
                 }
        self.doPage( endPoint='manage.taskMgr', username='p1m1',
                     data=data,
                     isIn = ['class="alert alert-warning"',
                             'task 1 (a1 p1) ',
                             'User(s)  user, two user, three added as new observer(s) to task ta1',
                             '<option value="9901">user, two</option>'
                             '<option value="9902">user, three</option>'
                             ],
                     isNotIn = [],
                     )


    def test_taskRemoveObs_PM( self ) :

        # add the user (9903) as observer to the task, so we can later remove it again
        data = { 'patType'  : 'task',
                 'patCode'  : 't1',
                 'userType' : 'observer',
                 'userList' : '9903',  # string of comma-separated cmsIds
                 }
        self.doPage( endPoint='manage.taskMgr', username='p1m1',
                     data=data,
                     isIn = ['task 1 (a1 p1) ',
                             'User(s)  user, four added as new observer(s) to task ta1',
                             '<option value="9901">user, two</option>'
                             '<option value="9902">user, three</option>'
                             '<option value="9903">user, four</option>'
                             ],
                     isNotIn = [],
                     )

        user = db.session.query(EprUser).filter_by( username='p1m1' ).one( )
        self.assertFalse( user.is_wizard( ) )

        self.login( user.username )

        taskT1 = db.session.query(Task).filter_by(code='t1').one()
        lvl3   = db.session.query(Level3Name).filter_by(id=taskT1.level3).one()
        taskData = { 'name' : taskT1.name,
                     'description' : taskT1.description,
                     'neededWork' : taskT1.neededWork,
                     'level3' : '1 : Unknown',
                     'activities': 1,
                     'removeObs': True,
                     'observers': [9903],
                     }


        # access the page and remove an observer
        response = self.client.post( url_for( 'manage.task', code='t1' ),
                                     data = taskData,
                                     follow_redirects=True )

        # log.info("got response status %s " % str(response.status) )
        # log.info( "got response data %s " % str( response.data.decode('utf-8') ) )

        self.assertEqual( response.status_code, 200 )

        self.assertNotIn( 'form-group  has-error', response.data.decode('utf-8')[:30000] )
        self.assertIn( 'task 1 (a1 p1)', response.data.decode('utf-8')[:30000] )

        # check that we really removed the guy ...
        self.assertIn( '<option value="9901">user, two</option>', response.data.decode('utf-8')[:30000] )
        self.assertIn( '<option value="9902">user, three</option>', response.data.decode('utf-8')[:30000] )
        self.assertIn( 'Successfully removed observer(s) user, four from ta1', response.data.decode('utf-8')[:30000] )

        self.assertNotIn( '<option value="9903">user, four</option>', response.data.decode('utf-8')[:30000] )

        self.logout( )

    def test_loginAsFail( self ) :

        self.doPage( endPoint='auth.loginAs',
                     username='p1m1',
                     data={ 'userSel' : 'foo (CERNid: 99000)' },
                     isIn = ['class="alert alert-warning"',
                             "Sorry, you are not allowed to use this function."],
                     isNotIn = []
                     )

    def test_loginAsFail1( self ) :

            self.doPage( endPoint='auth.loginAs',
                     username='pfeiffer',
                     adminOK=True,
                     data={ 'userSel' : 'foo (CERNid: 4242)' },
                     isIn=[ 'class="alert alert-warning"',
                            'User for 4242 not found in DB - nothing done ...',
                            'iCMS - EPR Pledge Overview', 'for Pfeiffer, Andreas '],
                     isNotIn=[ 'Error' ]
                     )

    def test_loginAs( self ) :
        self.doPage( endPoint='auth.loginAs',
                     username='pfeiffer',
                     adminOK=True,
                     data={ 'userSel' : 'foo (CERNid: 99000)' },
                     isIn=[ 'class="alert alert-warning"',
                            'real HR id is: 422405',
                            'iCMS - EPR Pledge Overview', 'for user, one', 'in %s' % testYear],
                     isNotIn=[ 'Error' ]
                     )

    def test_addInstitute( self ):

        i0 = {  'name' : 'InstAdded',
                'cernId' : 42,
                'country' : 'Europe',
                'code' : 'NewInst',
                'status' : 'Yes',
                'comment' : 'no comment',
            }

        self.doPage( endPoint='manage.addInstitute',
                     username='pfeiffer',
                     adminOK=True,
                     data=i0,
                     isIn = ['class="alert alert-warning"',
                             'Institute %s successfully added' % i0['name'] ],
                     isNotIn = ['Error']
                     )


    def test_addInstituteFail( self ):

        i0 = {  'name' : 'InstAdded',
                'cernId' : 42,
                'country' : 'Europe',
                'code' : 'NewInst',
                'status' : 'Yes',
                'comment' : 'no comment',
            }

        self.doPage( endPoint='manage.addInstitute', username='p1m1',
                     data=i0,
                     isIn = ['class="alert alert-warning"',
                             'Sorry you are not authorised for this action' ],
                     isNotIn = ['Error']
                     )



    def test_addEprUser( self ):

        u0 = { 'username' : 'username',
                'name' : 'Firstname I. Lastname',
                'hrId' : 42,
                'cmsId' : 42,
                'authorReq' :False,
                'status' : 'CMS',
                'isSusp' : False,
                'category' : 'Physicist',
                'comment' : 'no comment',
                'instCode' : 'CERN',
            }

        self.doPage( endPoint='manage.addUser',
                     username='pfeiffer',
                     adminOK=True,
                     data=u0,
                     isIn = ['class="alert alert-warning"',
                             'User %s successfully added' % u0['name'] ],
                     isNotIn = ['Error']
                     )


    def test_addEprUserFail( self ):

        u0 = { 'username' : 'username',
                'name' : 'Firstname I. Lastname',
                'hrId' : 42,
                'cmsId' : 42,
                'authorReq' :False,
                'status' : 'CMS',
                'isSusp' : False,
                'category' : 'Physicist',
                'comment' : 'no comment',
                'instCode' : 'CERN',
            }

        self.doPage( endPoint='manage.addUser', username='p1m1',
                     data=u0,
                     isIn = ['class="alert alert-warning"',
                             'Sorry you are not authorised for this action' ],
                     isNotIn = ['Error']
                     )

    def test_proj_projectMgrEgroup( self ) :
        data = { 'patType'  : 'project',
                 'patCode'  : 'p1',
                 'eGroup' : 'testPMEgroup',  # string of comma-separated cmsIds
                 }
        self.doPage( endPoint='manage.projectMgr', username='p1m1',
                     data=data,
                     isIn = ['class="alert alert-warning"',
                             'eGroup(s) testPMEgroup@cern.ch added to manager(s) of project p1'],
                     isNotIn = []
                     )

    def test_proj_projectMgrEgroupFail0( self ) :
        # the eGroup is already in for this proj
        data = { 'patType'  : 'project',
                 'patCode'  : 'p1',
                 'eGroup' : 'testPMEgroup',  # string of comma-separated cmsIds
                 }
        self.doPage( endPoint='manage.projectMgr', username='p1m1',
                     data=data,
                     isIn = ['class="alert alert-warning"',
                             'eGroup(s) testPMEgroup@cern.ch added to manager(s) of project p1'],
                     isNotIn = []
                     )

    def test_egroupProj( self ) :
        data = { 'project'  : 'pr1',
                 'patCode'  : 'p1',
                 }
        self.doPage( endPoint='manage.egroup', username='p1m1',
                     what='project',
                     data = data,
                     isIn=[ 'iCMS - EPR Management',
                            'Add an eGroup as manager to project %s' % data['project'],
                            '<input type="hidden" name="patCode" id="patCode" value="%s" />' % data['patCode'] ],
                     isNotIn=[ ],
                     # showPage=True,
                   )

    def test_egroupAct( self ) :
        data = { 'activity'  : 'ac4',
                 'patCode'  : 'a4',
                 }
        self.doPage( endPoint='manage.egroup', username='p2m1',
                     what='activity',
                     data = data,
                     isIn=[ 'iCMS - EPR Management',
                            'Add an eGroup as manager to activity %s' % data['activity'],
                            '<input type="hidden" name="patCode" id="patCode" value="%s" />' % data['patCode'] ],
                     isNotIn=[ ],
                     # showPage=True,
                   )

    def test_egroupTask( self ) :
        data = { 'task'    : 'ta4',
                 'patCode' : 't4',
                 }
        self.doPage( endPoint='manage.egroup', username='p2m1',
                     what='task',
                     data = data,
                     isIn=[ 'iCMS - EPR Management',
                            'Add an eGroup as manager to task %s' % data['task'],
                            '<input type="hidden" name="patCode" id="patCode" value="%s" />' % data['patCode'] ],
                     isNotIn=[ ],
                     # showPage=True,
                   )

    def test_mngAll(self):

        u1 = db.session.query(EprUser).filter_by(username='p1m2').one()
        self.assertFalse(u1.is_wizard())

        self.login(u1.username)

        # access the main view page
        response = self.client.get( url_for('manage.full'), follow_redirects=True )

        # log.info("got response status %s " % str(response.status) )
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )

        self.assertEqual(response.status_code, 200)

        self.assertIn( 'Manage Projects', response.data.decode('utf-8') )
        self.assertIn( 'Manage Activities', response.data.decode('utf-8') )
        self.assertIn( 'Manage Tasks', response.data.decode('utf-8') )

        self.logout()


    def test_indexAnonEprUser(self):

        # access the main view page
        response = self.client.get( url_for('manage.mine'), follow_redirects=True )

        # log.info("got response data %s " % str(response.data.decode('utf-8')) )

        self.assertEqual(response.status_code, 200)
        self.assertNotIn( 'Manage Activities', response.data.decode('utf-8'))

        self.assertNotIn( 'Manage Projects', response.data.decode('utf-8') )
        self.assertNotIn( 'Manage Activities', response.data.decode('utf-8') )
        self.assertNotIn( 'Manage Tasks', response.data.decode('utf-8') )

    def test_indexEprUser(self):

        u1 = db.session.query(EprUser).filter_by(username='user1').one()
        self.assertFalse(u1.is_wizard())

        self.login(u1.username)

        # access the main view page
        response = self.client.get( url_for('manage.mine'), follow_redirects=True )

        # log.info("got response status %s " % str(response.status) )
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )

        self.assertEqual(response.status_code, 200)

        self.assertNotIn( 'Projects you can manage', response.data.decode('utf-8') )
        self.assertNotIn( 'Activities you can manage', response.data.decode('utf-8') )
        self.assertNotIn( 'Tasks you can manage', response.data.decode('utf-8') )

        self.logout()

    def test_indexPM( self ) :

        user = db.session.query(EprUser).filter_by( username='p1m2' ).one( )
        self.assertFalse( user.is_wizard( ) )

        self.login(user.username)

        # access the main view page
        response = self.client.get( url_for( 'manage.index' ), follow_redirects=True )

        # log.info("got response status %s " % str(response.status) )
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )

        self.assertEqual( response.status_code, 200 )
        self.assertIn( '/manage/project/p1', response.data.decode('utf-8') )

        # project itself and list of activities for that project should be there,
        self.assertIn( 'Projects you can manage', response.data.decode('utf-8') )
        self.assertIn( 'Activities you can manage', response.data.decode('utf-8') )

        # ... but tasks from the project are not listed,
        # and that user does not manage other tasks
        self.assertNotIn( 'Tasks you can manage', response.data.decode('utf-8') )

        self.logout( )

    def test_projEprUser( self ) :

        user = db.session.query(EprUser).filter_by( username='user1' ).one( )
        self.assertFalse( user.is_wizard( ) )

        self.login( user.username )

        # access the page
        response = self.client.get( url_for( 'manage.project', code = 'p1' ), follow_redirects=True )

        # log.info("got response status %s " % str(response.status) )
        # log.info( "got response data %s " % str( response.data.decode('utf-8') ) )

        self.assertEqual( response.status_code, 200 )
        self.assertIn('You are not a manager for this project, code p1, sorry.', response.data.decode('utf-8'))

        self.logout( )

    def test_projPM_get( self ) :
        user = db.session.query(EprUser).filter_by( username='p1m1' ).one( )
        self.assertFalse( user.is_wizard( ) )

        self.login( user.username )

        # access the page
        response = self.client.get( url_for( 'manage.project', code='p1' ), follow_redirects=True )

        # log.info("got response status %s " % str(response.status) )
        # log.info( "got response data %s " % str( response.data.decode('utf-8') ) )

        self.assertEqual( response.status_code, 200 )
        self.assertIn( 'Project pr1', response.data.decode('utf-8') )
        self.assertIn( 'project 1', response.data.decode('utf-8'))

        self.assertNotIn( 'div class="alert alert-warning"', response.data.decode('utf-8') )

    def test_projNonExisting_PM( self ) :

        user = db.session.query(EprUser).filter_by( username='p1m1' ).one( )
        self.assertFalse( user.is_wizard( ) )

        self.login( user.username )

        response = self.client.get( url_for( 'manage.project', code='notExisting' ), follow_redirects=True )
        self.assertEqual( response.status_code, 200 )

        self.assertIn( 'div class="alert alert-danger"', response.data.decode('utf-8') )
        self.assertIn( 'iCMS - EPR Manage', response.data.decode('utf-8') )
        self.assertIn( 'can not find project for code notExisting', response.data.decode('utf-8') )

        self.logout( )

    def test_projNonExisting_Admin( self ) :
        user = db.session.query(EprUser).filter_by( username='pfeiffer' ).one( )
        self.assertTrue( user.is_wizard( ) )

        self.login( user.username )

        response = self.client.get( url_for( 'manage.project', code='notExisting' ), follow_redirects=True )
        self.assertEqual( response.status_code, 200 )

        self.assertIn( 'div class="alert alert-danger"', response.data.decode('utf-8') )
        self.assertIn( 'iCMS - EPR Manage', response.data.decode('utf-8') )
        self.assertIn( 'can not find project for code notExisting', response.data.decode('utf-8') )

        self.logout( )

    def test_projUpdate_PM( self ) :
        user = db.session.query(EprUser).filter_by( username='p1m1' ).one( )
        self.assertFalse( user.is_wizard( ) )

        self.login( user.username )

        # access the page and update the description
        response = self.client.post( url_for( 'manage.project', code='p1' ),
                                     data = { 'update' : True,
                                              'remove' : False,
                                              'name' : 'pr1',
                                              'description' : 'updated description for Project 1',
                                            },
                                     follow_redirects=True )

        # log.info("got response status %s " % str(response.status) )
        # log.info( "got response data %s " % str( response.data.decode('utf-8') ) )

        self.assertEqual( response.status_code, 200 )
        self.assertIn( 'div class="alert alert-warning"', response.data.decode('utf-8') )
        self.assertIn( '<h3 class="red">', response.data.decode('utf-8') )
        self.assertIn( 'Successfully updated description for pr1', response.data.decode('utf-8') )

        self.assertIn( 'Project pr1', response.data.decode('utf-8') )
        self.assertIn( 'updated descr', response.data.decode('utf-8') )

        self.logout()

    def test_projUpdateDesc_PM( self ) :
        user = db.session.query(EprUser).filter_by( username='p1m1' ).one( )
        self.assertFalse( user.is_wizard( ) )

        self.login( user.username )

        # access the page and update the description
        response = self.client.post( url_for( 'manage.project', code='p1' ),
                                     data = { 'update' : 'Update description',
                                              'name' : 'pr1',
                                              'remove' : None,
                                              'description' : 'updated descr for Project 1',
                                            },
                                     follow_redirects=True )

        # log.info("got response status %s " % str(response.status) )
        # log.info( "got response data %s " % str( response.data.decode('utf-8') ) )

        self.assertEqual( response.status_code, 200 )
        self.assertIn( 'div class="alert alert-warning"', response.data.decode('utf-8') )
        self.assertIn( '<h3 class="red">', response.data.decode('utf-8') )
        self.assertIn( 'Successfully updated description for pr1', response.data.decode('utf-8') )

        self.assertIn( 'Project pr1', response.data.decode('utf-8') )
        self.assertIn( 'updated descr', response.data.decode('utf-8') )

        self.logout()

    def test_projRemoveMgr_PM( self ) :
        user = db.session.query(EprUser).filter_by( username='p1m1' ).one( )
        self.assertFalse( user.is_wizard( ) )

        self.login( user.username )

        # access the page and remove a manager
        response = self.client.post( url_for( 'manage.project', code='p1' ),
                                     data = { 'remove' : True,
                                              'name' : 'pr1',
                                              'description' : 'updated descr for Project 1',
                                              'managers' : [ 9803 ],
                                            },
                                     follow_redirects=True )

        # log.info("got response status %s " % str(response.status) )
        # log.info( "got response data %s " % str( response.data.decode('utf-8') ) )

        self.assertEqual( response.status_code, 200 )

        self.assertNotIn( 'form-group  has-error', response.data.decode('utf-8') )
        self.assertIn( 'Project pr1', response.data.decode('utf-8') )
        # check that we really removed the guy ...
        self.assertNotIn( 'p1, m2 </option', response.data.decode('utf-8'))
        self.assertIn( 'Successfully removed manager(s) p1, m2 from pr1', response.data.decode('utf-8') )

        self.logout( )

    def test_projEprUsers( self ):

        user = db.session.query(EprUser).filter_by( username='p1m1' ).one( )
        self.assertFalse( user.is_wizard( ) )

        self.login( user.username )

        # access the page and remove a manager
        response = self.client.post( url_for( 'manage.managers', what='project' ),
                                     data = { 'project' : 'p1' },
                                     follow_redirects=True )

        # log.info("got response status %s " % str(response.status) )
        # log.info( "got response data %s " % str( response.data.decode('utf-8') ) )

        self.assertEqual( response.status_code, 200 )

        # Select users to be added as managers to {{ what }} {{ name }}:
        self.assertIn( 'Select users to be added as manager to', response.data.decode('utf-8').replace('\n','') )

    def test_proj_addProjectMgr( self ) :
        data = { 'patType'  : 'project',
                 'patCode'  : 'p1',
                 'userList' : '9900,9902',  # string of comma-separated cmsIds
                 }
        self.doPage( endPoint='manage.projectMgr', username='p1m1',
                     data=data,
                     isIn = ['class="alert alert-warning"',
                             'User(s)  user, one user, three added as new manager(s) to project pr1'],
                     isNotIn = []
                     )

    def test_proj_projectMgrFail0( self ) :
        # the users are already PMs for this project
        data = { 'patType'  : 'project',
                 'patCode'  : 'p1',
                 'userList' : '9900,9902',  # string of comma-separated cmsIds
                 }
        self.doPage( endPoint='manage.projectMgr', username='p1m1',
                     data=data,
                     isIn = ['class="alert alert-warning"',
                             'User user, one is already in list of manager(s) for pr1'],
                     isNotIn = []
                     )

    def test_proj_activityMgr( self ) :
        data = { 'patType'  : 'activity',
                 'patCode'  : 'a1',
                 'userList' : '9901',  # string of comma-separated cmsIds
                 }
        self.doPage( endPoint='manage.activityMgr', username='p1m1',
                     data=data,
                     isIn=[ 'class="alert alert-warning"',
                            'User(s)  user, two added as new manager(s) to activity ac1' ],
                     isNotIn=[ ]
                     )

    def test_proj_taskMgr( self ) :
        data = { 'patType'  : 'task',
                 'patCode'  : 't1',
                 'userList' : '9901',  # string of comma-separated cmsIds
                 }
        self.doPage( endPoint='manage.taskMgr', username='p1m1',
                     data=data,
                     isIn=[ 'class="alert alert-warning"',
                            'User(s)  user, two added as new manager(s) to task ta1' ],
                     isNotIn=[ ]
                     )

    def test_proj_projectMgrFail( self ) :
        data = { 'patType'  : 'project',
                 'patCode'  : 'foo',
                 'userList' : '9900,9902',  # string of comma-separated cmsIds
                 }
        self.doPage( endPoint='manage.projectMgr', username='p1m1',
                     data = {},
                     isIn = [ ],
                     isNotIn = [ ],
                     retStat = 401,
                     )

        self.doPage( endPoint='manage.projectMgr', username='p1m1',
                     data = data,
                     isIn = [ ],
                     isNotIn = [ ],
                     retStat = 401,
                     )

    def test_proj_activityMgrFail( self ) :
        data = { 'patType'  : 'activity',
                 'patCode'  : 'foo',
                 'userList' : '9901',  # string of comma-separated cmsIds
                 }
        self.doPage( endPoint='manage.activityMgr', username='p1m1',
                     data = {},
                     isIn = [  ],
                     isNotIn = [ ],
                     retStat = 401,
                     )

        self.doPage( endPoint='manage.activityMgr', username='p1m1',
                     data = data,
                     isIn = [  ],
                     isNotIn = [ ],
                     retStat = 401,
                     )

    def test_proj_taskMgrFail( self ) :
        data = { 'patType'  : 'task',
                 'patCode'  : 'foo',
                 'userList' : '9903',  # string of comma-separated cmsIds
                 }
        self.doPage( endPoint='manage.taskMgr', username='p1m1',
                     data = {},
                     isIn = [  ],
                     isNotIn = [ ],
                     retStat = 401,
                     )
        self.doPage( endPoint='manage.taskMgr', username='p1m1',
                     data = data,
                     isIn = [  ],
                     isNotIn = [ ],
                     retStat = 401,
                     )

    def test_proj_addTaskObsFail0( self ) :
        # the user is already observer for this project

        data = { 'patType'  : 'task',
                 'patCode'  : 't1',
                 'userType' : 'observer',
                 'userList' : '9901,9902',  # string of comma-separated cmsIds
                 }
        self.doPage( endPoint='manage.taskMgr', username='p1m1',
                     data=data,
                     isIn = ['class="alert alert-warning"',
                             'task 1 (a1 p1) ',
                             'User user, two is already in list of observer(s) for ta1'
                             ],
                     isNotIn = ['form-group  has-error'],
                     )

    def test_projChangeName_PM( self ) :
        user = db.session.query(EprUser).filter_by( username='p1m1' ).one( )
        self.assertFalse( user.is_wizard( ) )

        self.login( user.username )

        # access the page and update the description
        response = self.client.post( url_for( 'manage.project', code='p1' ),
                                     data = { 'updateName' : 'Update Name',
                                              'name' : 'proj1',
                                              'remove' : None,
                                              'description' : 'updated descr for Project 1',
                                            },
                                     follow_redirects=True )

        # log.info("got response status %s " % str(response.status) )
        # log.info( "got response data %s " % str( response.data.decode('utf-8') ) )

        self.assertEqual( response.status_code, 200 )
        self.assertIn( 'div class="alert alert-warning"', response.data.decode('utf-8') )
        self.assertIn( '<h3 class="red">', response.data.decode('utf-8') )
        self.assertIn( 'Successfully updated name for proj1', response.data.decode('utf-8') )

        self.assertNotIn( 'Project pr1', response.data.decode('utf-8') )
        self.assertNotIn( 'updated descr', response.data.decode('utf-8') )

        self.assertIn( 'Project proj1', response.data.decode('utf-8') )

        log.info('test_projChangeName_PM> name of proj with code p1 changed to proj1 ... ')

        # change it back to not break other tests ...

        # access the page and update the description
        response = self.client.post( url_for( 'manage.project', code='p1' ),
                                     data = { 'updateName' : 'Update Name',
                                              'name' : 'pr1',
                                              'remove' : None,
                                              'description' : 'updated descr for Project 1',
                                            },
                                     follow_redirects=True )

        # log.info("got response status %s " % str(response.status) )
        # log.info( "got response data %s " % str( response.data.decode('utf-8') ) )

        self.assertEqual( response.status_code, 200 )
        self.assertIn( 'div class="alert alert-warning"', response.data.decode('utf-8') )
        self.assertIn( '<h3 class="red">', response.data.decode('utf-8') )
        self.assertIn( 'Successfully updated name for pr1', response.data.decode('utf-8') )

        self.assertNotIn( 'Project proj1', response.data.decode('utf-8') )
        self.assertNotIn( 'updated descr', response.data.decode('utf-8') )

        self.assertIn( 'Project pr1', response.data.decode('utf-8') )

        log.info('test_projChangeName_PM> name of proj with code p1 changed BACK to pr1 ... ')

        self.logout()
