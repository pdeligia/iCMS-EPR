#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import socket

try:
    from .BaseTestClass import *
except ImportError:
    from BaseTestClass import *

log = logging.getLogger( "Testing.test_api" )
log.setLevel(logging.INFO)

class APITestCase( BaseTestCase ) :

    def setUp( self ) :
        BaseTestCase.setUp( self )
        self.defaultUser = 'tester'

    def test_getActLvl3(self):
        tstUser = db.session.query(EprUser).filter(EprUser.username == 'tester').one()
        self.login()
        log.info('test user is: %s ' % tstUser)

        resG, resP = self.callAndCheck(endPointName='api.actLvl3', code='a1',
                                data={},
                                jsonKey='', jsonLen=0,
                                showPage=True)

        # log.info('test_getActLvl3> got resG: %s' % str(resG))
        log.info('test_getActLvl3> got resP: %s' % str(resP))

        expectedRes = [ {'code': 'Unknown', 'name': 'Unknown'}, {'code': 'lvl3-one', 'name': 'lvl3-one'} ]
        self.assertListEqual( resG['data'], expectedRes )
        self.assertListEqual( resP['data'], expectedRes )

    def test_projectCounts( self ):
        pId = '1' # this is a key in the json dict, so it needs to be a string
        response = self.callAndCheckPost( endPointName='api.projectCounts',
                                          data = { 'proj': pId, 'year': self.selYear },
                                          jsonKey='data', jsonLen=0
                                         )
        # log.info( 'test_getObservers> len(response): %s' % len(response['mgr']) )
        log.info( 'test_projectCounts> response: %s' % response )
        log.info( 'test_projectCounts> response.data: %s' % response['data'] )
        self.assertEqual( 'pr1', response['data'][pId] )
        prName = response['data'][pId]
        self.assertDictEqual( {'nAuthors': 1, 'nCountries': 2, 'nCountriesAuth': 1, 'nInstAuth': 1, 'nInsts': 2, 'nUsers': 2}, response['data'][prName] )

    def test_projectUserInsts( self ):
        pId = '1' # this is a key in the json dict, so it needs to be a string
        response = self.callAndCheckPost( endPointName='api.projectUserInsts',
                                          data = { 'proj': pId, 'year': self.selYear },
                                          jsonKey=pId, jsonLen=0
                                         )
        # log.info( 'test_getObservers> len(response): %s' % len(response['mgr']) )
        log.info( 'test_projectUserInsts> response: %s' % response )
        log.info( 'test_projectUserInsts> response.pId: %s' % response[pId] )
        self.assertDictEqual( {'nAuthors': 1, 'nCountries': 2, 'nInsts': 3, 'nUsers': 3}, response[pId] )

    def test_projectAuthWork( self ) :
        pId = '1'  # this is a key in the json dict, so it needs to be a string
        response = self.callAndCheckGet( endPointName='api.projectAuthWork',
                                         proj=pId, year=self.selYear,
                                         data={ },
                                         jsonKey=pId, jsonLen=0
                                         )
        # log.info( 'test_getObservers> len(response): %s' % len(response['mgr']) )
        log.info( 'test_projectAuthWork> response: %s' % response )
        log.info( 'test_projectAuthWork> response.pId: %s' % response[ pId ] )
        self.assertEqual( '  5.6,   0.4,   0.4', response[ pId ][ 'all' ] )
        self.assertEqual( '  2.9,   0.0,   0.0', response[ pId ][ 'authors' ] )

    def test_splitPledgeFail( self ) :
        # without being PM, this should fail
        res = self.client.post( url_for( 'api.splitPledge' ),
                                data={ 'plCode' : '', 'newInst' : '', 'newInstWork' : 0. } )

        self.assertEqual( res.status_code, 403 )

        # log.info( 'test_deleteActFail 1> response: %s' % res.data )
        json_response = json.loads( res.data.decode( 'utf-8' ) )

        self.assertGreaterEqual( len( json_response[ 'status' ] ), 1 )
        self.assertEqual( json_response[ 'status' ], "ERROR" )
        self.assertIn( 'are not allowed to split pledges, sorry.', json_response[ 'msg' ] )

    def test_instAuthorsForProject( self ):
        projCode = 'COMP'
        res = self.callAndCheckPost( endPointName='api.instAuthorsForProject',
                                    data={ 'projCode' : projCode, 'year' : self.selYear},
                                    jsonKey='projCode', jsonLen=1, retCodeExp=200, followRedir=True )

        self.assertIn( 'projCode', res )
        self.assertIn( 'nAuthors', res )
        self.assertIn( 'projList', res )
        self.assertIn( 'instList', res )
        self.assertEqual( len(res['projList']), 1 )
        self.assertIn( projCode, res['projList'] )
        self.assertIn( projCode, res['projCode'] )
        self.assertIn( 'CERN', res['instList'] )

        self.assertDictEqual( {u'CERN': 5.0, u'ETHZ': 1.0, u'FNAL': 2.0}, res['nAuthors'][projCode] )

    def test_instAuthorsForProjectAll( self ):
        projCode = 'all'
        res = self.callAndCheckPost( endPointName='api.instAuthorsForProject',
                                    data={ 'projCode' : projCode, 'year' : self.selYear},
                                    jsonKey='projCode', jsonLen=1, retCodeExp=200, followRedir=True )

        self.assertIn( 'projCode', res )
        self.assertIn( 'nAuthors', res )
        self.assertIn( 'projList', res )
        self.assertIn( 'instList', res )
        self.assertEqual( len(res['projList']), 5 )
        self.assertIn( 'COMP', res['projList'] )
        self.assertIn( projCode, res['projCode'] )
        self.assertIn( 'CERN', res['instList'] )

        # log.info( 'test_instAuthorsForProjectAll> response: %s' % res )

        self.assertDictEqual( { u'HCAL': { u'CERN': 1.0 },
                                u'COMP': { u'CERN': 5.0, u'ETHZ': 1.0, u'FNAL': 2.0 },
                                # u'ECAL': {u'ETHZ': 1.0 },
                                u'PPD': { u'CERN': 5.0, 'ETHZ': 3.0 },
                                u'pr1': { u'FNAL' : 0.5 },
                                u'pr2': { u'FNAL': 0.5 },
                                },
                              res['nAuthors'] )

    def test_getShiftSubSystems(self):
        response = self.callAndCheckGet( endPointName='api.getShiftSubSystems',
                              data=None,
                              jsonKey='data', jsonLen=3 )
        # log.info( 'test_getShiftSubSystems> len(response): %s' % len(response['data']) )
        # log.info( 'test_getShiftSubSystems> response: %s' % response['data'] )
        self.assertEqual( len(response['data']), 3 )
        self.assertListEqual( response['data'],  [[u'pr1'], [u'pr2'], [u'pr3']] )

    def test_sumShiftsForTask( self ) :

        self.logout()
        self.login(username='shifter')

        response = self.callAndCheckGet( endPointName='api.sumShiftsForTask',
                                         data=None, taskCode = 'ts1',
                                         jsonKey='data', jsonLen=3 )
        # log.info( 'test_sumShiftsForTask> len(response): %s' % len( response[ 'data' ] ) )
        log.info( 'test_sumShiftsForTask> response: %s' % response[ 'data' ] )
        self.assertEqual( len(response['data']), 3 )
        self.assertEqual( response['data'][0],  4 )
        self.assertAlmostEqual( response['data'][1], 0.27692307692307694 )
        self.assertAlmostEqual( response['data'][2], 0.0 )

    def test_getAllManagers(self):
        response = self.callAndCheckGet( endPointName='api.getAllManagers',
                              data=None,
                              jsonKey='data', jsonLen=21 )
        # log.info( 'test_getAllManagers> len(response): %s' % len(response['data']) )
        # log.info( 'test_getAllManagers> response: %s' % response['data'] )
        found = False
        lookFor = db.session.query(EprUser).filter(EprUser.username == 'a1m1').one()
        for item in response['data']:
             if item['userId'] == lookFor.id and item['year'] == self.selYear : found = True
        self.assertEqual( found, True )

    def test_getManager(self):
        # get managers for one project, one act, and one task
        response = self.callAndCheckPost( endPointName='api.getManager',
                              data={ 'project' : 'p1', 'activity' : 'a1', 'task' : 't1'},
                              jsonKey='mgr', jsonLen=3 )
        # log.info( 'test_getManager> len(response): %s' % len(response['mgr']) )
        # log.info( 'test_getManager> response: %s' % response['mgr'] )
        self.assertListEqual( [u'p1, m1', u'p1, m2'], response['mgr']['project'] )
        self.assertListEqual( [u'a1, m1'], response['mgr']['activity'] )
        self.assertListEqual( [u't1, m1'], response['mgr']['task'] )

    def test_udpateManagerFail1(self):

        data = { 'itemType' : '',
                 'itemCode' : '',
                 'userid' : '' }

        # get managers for one project, one act, and one task
        response = self.callAndCheckPost( endPointName='api.updateManager',
                              data=data,
                              jsonKey=None, jsonLen=0 )
        # log.info( 'test_udpateManager> len(response): %s' % len(response['msg']) )
        # log.info( 'test_udpateManager> response: %s' % response['msg'] )
        self.assertIn( "Error: invalid values given for itemType:itemCode:userId '::'", response['msg'] )

    def test_udpateManagerFail2(self):

        data = { 'itemType' : 'activity',
                 'itemCode' : 'a3',
                 'userid' : 14 }

        # get managers for one project, one act, and one task
        response = self.callAndCheckPost( endPointName='api.updateManager',
                              data=data,
                              jsonKey=None, jsonLen=0 )
        # log.info( 'test_udpateManager> len(response): %s' % len(response['msg']) )
        # log.info( 'test_udpateManager> response: %s' % response['msg'] )
        self.assertIn( "Error: Sorry you are not allowed to update the manager info", response['msg'] )

    def test_404(self):
        response = self.client.get(
            '/wrong/url',
            headers=self.get_api_headers('name', 'password'))
        self.assertTrue(response.status_code == 404)
        json_response = json.loads(response.data.decode('utf-8'))
        self.assertTrue(json_response['error'] == 'not found')

    def test_actForProjId(self):
        resP, resG = self.callAndCheck( endPointName='api.get_activitesForProjectId', projId = 2,
                           data=None, jsonKey='activities', jsonLen=1 )
        # log.info( "got: %s " % str( resP ) )
        self.assertEqual( 3, len( resP['activities'] ) )
        self.assertEqual( 3, len( resG['activities'] ) )
        for res in [resP, resG]:
            for item in res['activities']:
                self.assertEqual(2, item['projectId'])
                self.assertEqual(self.selYear, item['year'])
                self.assertIn('activity', item['description'])

    def test_getAllUsers(self):
        self.callAndCheck( endPointName='api.getAllUsers', data=None, jsonKey='data', jsonLen=12 )

    def test_allProjects( self ) :
        self.callAndCheck( endPointName='api.allProjects', data=None, jsonKey='projects', jsonLen=1 )

    def test_projectTaskInfo( self ) :
        self.callAndCheck( endPointName='api.projectTaskInfo', data=None, jsonKey='projectTasks', jsonLen=1, code='p1' )
        self.callAndCheck( endPointName='api.projectTaskInfo', data={'initial':True}, jsonKey='projectTasks', jsonLen=1, code='p1' )

    def test_projectTaskInfoFail( self ) :
        self.callAndCheck( endPointName='api.projectTaskInfo', data=None, jsonKey='projectTasks', jsonLen=0, code='nop998' )
        self.callAndCheck( endPointName='api.projectTaskInfo', data={'initial':True}, jsonKey='projectTasks', jsonLen=0, code='nop999' )

    def test_projectActInfo( self ) :
        self.callAndCheck( endPointName='api.projectActInfo', data=None, jsonKey='projectActs', jsonLen=1, code='p1' )
        self.callAndCheck( endPointName='api.projectActInfo', data={'initial':True}, jsonKey='projectActs', jsonLen=1, code='p1' )

    def test_projectActInfoFail( self ) :
        self.callAndCheck( endPointName='api.projectActInfo', data=None, jsonKey='projectActs', jsonLen=0, code='nop998' )
        self.callAndCheck( endPointName='api.projectActInfo', data={'initial':True}, jsonKey='projectActs', jsonLen=0, code='nop999' )

    def test_projectSummary(self):
        self.callAndCheck( endPointName='api.projectSummary', data=None, jsonKey='projectSummary', jsonLen=1)
        self.callAndCheck( endPointName='api.projectSummary', data={'initial':True}, jsonKey='projectSummary', jsonLen=1)

    def test_projByName(self):
        self.callAndCheck( endPointName='api.projByName', data=None, jsonKey=None, jsonLen=1, name='pr1' )

    def test_projectInfo(self):
        self.callAndCheck( endPointName='api.projectInfo', data=None, jsonKey='allProjects', jsonLen=1 )

    def test_projActivities( self ) :
        self.callAndCheck( endPointName='api.projActivities', data=None, jsonKey='data', jsonLen=1, code='p1' )

    def test_getAllActivities( self ) :
        res = self.callAndCheckGet( endPointName='api.getAllActivities', data=None, jsonKey='data', jsonLen=1 )
        log.info('test_getAllActivities> got: %s' % str(res))
        self.assertEqual( len(res['data']), 8 )

    def test_getActivity( self ) :
        res = self.callAndCheckGet( endPointName='api.get_activity', data=None, jsonKey='', jsonLen=0, id=1 )
        log.info('test_getActivity> got: %s' % str(res))
        self.assertEqual( res[ 'name' ], 'ac1' )

    def test_getActivityByName( self ) :

        self.login()
        response = self.client.get( url_for( 'api.get_activityByName', name='' ) )
        self.assertEqual( response.status_code, 404 ) # no name given, should return 404
        self.logout()
        # log.info('test_getActivity> got (Fail): %s' % str(response))

        self.login()
        response = self.client.get( url_for( 'api.get_activityByName', name='fooo' ) )
        self.assertEqual( response.status_code, 200 ) # no name given, should return 404
        self.logout()
        log.info('test_getActivity> got (Fail): %s' % str(response))

        resOK = self.callAndCheck( endPointName='api.get_activityByName', data=None, jsonKey='', jsonLen=0, name='ac4' )
        log.info('test_getActivity> got (OK)  : %s' % str(resOK))

    def test_getActivityByTaskCode( self ) :
        res = self.callAndCheckPost( endPointName='api.activityByTaskCode',
                                     data = { 'code' : 't4' },
                                     jsonKey='', jsonLen=0 )
        log.info('test_getActivityByTaskCode> got: %s' % str(res))
        self.assertIn( res['activity']['name'], 'ac3' )

    def test_getActTasks( self ) :
        resG, resP = self.callAndCheck( endPointName='api.actTasks', lvl3='Unknown',
                                     data = { 'aCode': 'a1' },
                                     jsonKey='', jsonLen=0 )
        log.info('test_getActTasks> got: %s' % str(resG))

        self.assertListEqual( resG['data'], [{'code': 't1', 'name': 'ta1'}] )
        self.assertListEqual( resP['data'], [{'code': 't1', 'name': 'ta1'}] )

    def test_projectByTaskCode( self ) :
        self.callAndCheckPost( endPointName='api.projectByTaskCode', data={ 'code' : 't1' }, jsonKey='project', jsonLen=1 )

    def test_projectByTaskCodeFail( self ) :
        self.callAndCheckPost( endPointName='api.projectByTaskCode', data={ 'code' : 'notExisting' }, jsonKey='error', jsonLen=1 )

    def test_getAllTasks( self ) :
        res = self.callAndCheckGet( endPointName='api.getAllTasks', data=None, jsonKey='data', jsonLen=0 )
        log.info('test_getAllTasks> got: %s' % str(res))
        allTasks = db.session.query(Task).all()
        self.assertEqual( len(res['data']), len(allTasks) )

    def test_get_task( self ) :
        res = self.callAndCheckGet( endPointName='api.get_task', data=None, jsonKey='', jsonLen=0, id=1 )
        log.info('test_get_task> got: %s' % str(res))
        self.assertEqual( res['name'], 'ta1' )

    def test_get_taskByName( self ) :
        res = self.callAndCheckGet( endPointName='api.get_taskByName', data=None, jsonKey='', jsonLen=0, name='ta1' )
        log.info('test_get_taskByName> got: %s' % str(res))
        self.assertEqual( res['name'], 'ta1' )
        self.assertEqual( res['code'], 't1' )
        self.assertIn( 'task 1 (a1 p1)', res['description'] )

    def test_taskByCode( self ) :
        res = self.callAndCheckPost( endPointName='api.taskByCode',
                                    data = { 'code' : 't1' },
                                    jsonKey='', jsonLen=0 )
        log.info('test_taskByCode> got: %s' % str(res))
        self.assertEqual( res['task']['name'], 'ta1' )
        self.assertEqual( res['task']['code'], 't1' )
        self.assertIn( 'task 1 (a1 p1)', res['task']['description'] )

    def test_getAllPledges( self ) :
        res = self.callAndCheckGet( endPointName='api.getAllPledges', data=None, jsonKey='data', jsonLen=0 )
        log.info('test_getAllPledges> got: %s' % str(res))
        self.assertEqual( len(res['data']), 10 )

    def test_getPledge( self ) :
        res = self.callAndCheckGet( endPointName='api.getPledge', data=None, jsonKey='', jsonLen=0, id=1 )
        log.info('test_getPledge> got: %s' % str(res))
        self.assertEqual( res['code'], '1+5+1' )
        self.assertEqual( res['status'], 'new' )

    def test_getPledgeByName( self ) :
        res = self.callAndCheckGet( endPointName='api.getPledgeByName', data=None, jsonKey='', jsonLen=0, code='1:5:1' )
        log.info('test_getPledgeByName> got: %s' % str(res))
        self.assertEqual( res['status'], 'new' )
        self.assertEqual( res['code'], '1+5+1' )

    def test_pledgesForInst( self ) :
        res = self.callAndCheckPost( endPointName='api.pledgesForInst',
                                    data = { 'instCode' : 'CERN', 'year' : self.selYear },
                                    jsonKey='', jsonLen=0 )
        log.info('test_pledgesForInst> got: %s' % str(res))
        self.assertEqual( len( res['allProjects'] ), 3)
        self.assertIn( '1:5:1', res['allProjects'][0][-5] )
        self.assertIn( '3:5:1', res['allProjects'][2][-5] )

    def test_guestPledgesForInst( self ) :
        res = self.callAndCheckPost( endPointName='api.guestPledgesForInst',
                                    data = { 'instCode' : 'FNAL', 'year' : self.selYear },
                                    jsonKey='', jsonLen=0 )
        log.info('test_guestPledgesForInst> got: %s' % str(res))
        self.assertEqual( len(res['guestPledges']), 1 )
        self.assertIn( 't4', res['guestPledges'][0][-1] )
        self.assertIn( 'a3', res['guestPledges'][0][-2] )
        self.assertIn( '4:7:2', res['guestPledges'][0][-5] )

    def test_pledgesForTaskCodes( self ) :
        res = self.callAndCheckPost( endPointName='api.pledgesForTaskCodes',
                                    data = { 'taskCodes' : ['t1', 't2'], 'year' : self.selYear },
                                    jsonKey='', jsonLen=0 )
        log.info('test_pledgesForTaskCodes> got: %s' % str(res))
        self.assertEqual( len( res['allProjects'] ), 3 )
        self.assertIn( '1+5+1', res['allProjects'][0][-1] )
        self.assertIn( '1+6+2', res['allProjects'][1][-1] )

    def test_pledgesForPledgeCodes( self ) :

        plCodes = [ x.code for x in db.session.query(Pledge).filter_by(year=self.selYear).all() ]
        log.info( 'test_pledgesForPledgeCodes> found %d pledges: %s ' % (len(plCodes), ','.join( plCodes ) ) )

        res = self.callAndCheckPost( endPointName='api.pledgesForPledgeCodes',
                                    data = { 'plCodes' : ','.join( plCodes ),
                                             'year' : self.selYear },
                                    jsonKey='', jsonLen=0 )

        log.info('test_pledgesForPledgeCodes> got: %s' % str(res))
        self.assertEqual( len( res['allProjects'] ), 10 )
        self.assertIn( 'ta1', res['allProjects'][0][4] )
        self.assertEqual( 12., res['allProjects'][0][5] )
        self.assertIn( 'ta2', res['allProjects'][4][4] )
        self.assertEqual( 9., res['allProjects'][4][5] )


    def test_pledgeInfoForPledgeCodes( self ) :
        # availablePlCodes (2017-10-16):
        # '4+7+2', '7+7+3', '3+5+1', '2+6+2', '1+6+2', '2+5+1', '1+5+1', '4+7+3', '1+12+3', '3+7+3'
        res = self.callAndCheckPost( endPointName='api.pledgeInfoForPledgeCodes',
                                    data = { 'plCodes' : ['4+7+2,1+5+1'], 'year' : self.selYear },
                                    jsonKey='', jsonLen=0 )
        log.info('pledgeInfoForPledgeCodes> got: %s' % str(res))
        self.assertEqual( len( res['plInfo'] ), 2 )

        # pl1 = {u'status': u'new', u'code': u'1+5+1', u'workTimeAcc': 0.0, u'workTimePld': 1.1, u'workTimeDone': 0.0, u'instId': 1, u'userId': 5, u'workedSoFar': 0.0, u'taskId': 1, u'year': self.selYear, u'id': 1, u'isGuest': False},
        # pl2 = {u'status': u'new', u'code': u'4+7+2', u'workTimeAcc': 0.0, u'workTimePld': 1.0, u'workTimeDone': 0.0, u'instId': 2, u'userId': 7, u'workedSoFar': 0.0, u'taskId': 4, u'year': self.selYear, u'id': 9, u'isGuest': True}
        self.assertIn( u'1+5+1' , [ res['plInfo'][i]['code'] for i in [0,1] ] )
        self.assertIn( u'4+7+2' , [ res['plInfo'][i]['code'] for i in [0,1]  ] )

    def test_pledgeInfoForPledgeCodesFail( self ) :
        res = self.callAndCheckPost( endPointName='api.pledgeInfoForPledgeCodes',
                                    data = { 'year' : self.selYear },
                                    jsonKey='', jsonLen=0 )
        log.info('pledgeInfoForPledgeCodes> got: %s' % str(res))
        self.assertIn( 'error', res )
        self.assertEqual( 'pledgeInfoForPledgeCodes> no plCodes found in request ', res['error'] )

    def test_mailForPledges( self ) :
        res = self.callAndCheckPost( endPointName='api.mailForPledges',
                                    data = { 'pledges' : ['t1', 't2'], 'year' : self.selYear },
                                    jsonKey='', jsonLen=0 )
        log.info('test_mailForPledges> got: %s' % str(res))
        self.assertEqual( len( res['mails'] ), 1 )
        self.assertIn( 'not.yet.implem', res['mails'][0] )

    def test_countryInfo( self ) :
        resP, resG = self.callAndCheck( endPointName='api.countryInfo', data=None, jsonKey='country', jsonLen=1, name='SWITZERLAND' )
        self.assertGreaterEqual( len( resP[ 'data' ] ), 1 )
        self.assertGreaterEqual( len( resG[ 'data' ] ), 1 )

    def test_countryOverview( self ) :
        resP, resG = self.callAndCheck( endPointName='api.countryOverview', data=None, jsonKey='country', jsonLen=1 )
        self.assertGreaterEqual( len( resP[ 'data' ] ), 1 )
        self.assertGreaterEqual( len( resG[ 'data' ] ), 1 )

    def test_allInstitutes(self):
        self.callAndCheck( endPointName='api.allInstitutes', data=None, jsonKey='institutes', jsonLen=1 )

    def test_searchInstCode(self):
        self.callAndCheck( endPointName='api.searchInstCode', data={"term":'CERN'}, jsonKey=None, jsonLen=1 )

    def test_searchInst( self ) :
        self.callAndCheck( endPointName='api.searchInst', data={"term":'CERN'}, jsonKey=None, jsonLen=1 )

    # search also with a name which is different to the code
    def test_searchInst_name( self ) :
        self.callAndCheck( endPointName='api.searchInst', data={ "term" : 'ETH Zuri' }, jsonKey=None, jsonLen=1 )

    def test_searchInstCode_name( self ) :
        self.callAndCheck( endPointName='api.searchInstCode', data={ "term" : 'ETH Zuri' }, jsonKey=None, jsonLen=1 )

    def test_instByName( self ) :
        self.callAndCheck( endPointName='api.instByName', data=None, jsonKey=None, jsonLen=1, name='FNAL' )

    def test_instByCode( self ) :
        self.callAndCheck( endPointName='api.instByCode', data=None, jsonKey=None, jsonLen=1, codeName='FNAL - FNAL' )

    def test_instituteInfo( self ) :
        # no GET defined for this
        res = self.callAndCheckPost( endPointName='api.instituteInfo', data=None, jsonKey='institute', jsonLen=1, code='FNAL' )
        # log.info( "got: %s " % str(res) )
        # ensure we got back the correct numbers: some authors and the numbers match ...
        self.assertGreater( int( res['summary']['actAuthors'] ), 0 )
        self.assertEqual( float( res[ 'summary' ][ 'Expected' ] ), self.monthsPerAuthor * float( res[ 'summary' ][ 'actAuthors' ] ) )

        self.assertGreater( len(res['data']), 0, "no user found for institute ???")

    def test_instituteOverview( self ) :
        self.callAndCheck( endPointName='api.instituteOverview', data=None, jsonKey='institute', jsonLen=1, code='FNAL' )

    def test_projSumm_content(self):

        self.login( )

        response = self.client.post( url_for( 'api.projectSummary' ) )

        self.assertTrue( response.status_code == 200 )
        json_response = json.loads( response.data.decode( 'utf-8' ) )

        # make sure we get something back for all needed params:
        keyList = ['checkbox','pName','neededWork','sumDone','pctDone',
                   'sumPledged','pctPld','sumAcc','pctAcc',
                   'nPlNew', 'wtpNew', 'pctNew',
                   'nPlRej', 'wtpRej', 'pctRej',
                   ]
        res = json_response['projectSummary'][0]

        self.assertListEqual( sorted( res.keys() ), sorted( keyList ) )


    # from shift.py
    def test_shiftInfo(self):
        # no GET ...
        res = self.callAndCheckPost( endPointName="api.shiftInfo",
                                     jsonKey='shifts', jsonLen=0, # no shifts yet in db ...
                                     data=None, subSys='all')
        # log.info( "got: %s " % str(res) )


    def test_shiftInfoUser(self):
        # no GET ...
        res = self.callAndCheckPost( endPointName="api.shiftInfoUser", username='user2',
                                     jsonKey='shifts', jsonLen=0, # no shifts yet in db ...
                                     data=None, subSys='all')
        # log.info( "got: %s " % str(res) )

    # from users.py
    def test_searchUsers_username(self):
        res = self.callAndCheckGet( endPointName='api.searchUsers',
                                    data = { 'term' : 'user1' },
                                    jsonKey=None, jsonLen=1 )
        # log.info( "got: %s " % str( res ) )
        self.assertIn( 'user, one - CERN (CERNid: 99000)', res)

    def test_searchUsers_name( self ) :
        res = self.callAndCheckGet( endPointName='api.searchUsers',
                                    data = { 'term' : 'user, one' },
                                    jsonKey = None, jsonLen = 1 )
        # log.info( "got: %s " % str( res ) )
        self.assertIn( 'user, one - CERN (CERNid: 99000)', res )

    def test_getUsers( self ) :
        resG, resP = self.callAndCheck( endPointName='api.getUsers', data = {},
                                    jsonKey = 'data', jsonLen = 1 )
        # log.info( "test_get_user> got: \nGET: %s \nPOST: %s " % (str( resG ), str(resP)) )

        self.assertGreaterEqual( len( resG['data'] ), len( resP['data'] ) )
        self.assertGreaterEqual( len( resG['data'] ), 13 )
        self.assertGreaterEqual( len( resP['data'] ), 13 )

    def test_get_user( self ) :

        resG, resP = self.callAndCheck( endPointName='api.get_user', data = {},
                                      jsonKey = None, jsonLen = 1,
                                      id=3)
        # log.info( "test_get_user> got: GET: %s POST: %s " % (str( resG ), str(resP)) )
        self.assertIn( 'Tester, Master', resG[ 'name' ] )
        self.assertIn( 'Tester, Master', resP[ 'name' ] )

    def test_get_userByCmsId( self ) :

        resG, resP = self.callAndCheck( endPointName='api.get_userByCmsId', data = {},
                                      jsonKey = None, jsonLen = 1,
                                      cmsId=9900)
        # log.info( "test_get_userByCmsId> got: GET: %s POST: %s " % (str( resG ), str(resP)) )
        self.assertIn( 'user, one', resG['user'][ 'name' ] )
        self.assertIn( 'user, one', resP['user'][ 'name' ] )
        self.assertIn( 'CERN', resG['inst'][ 'code' ] )
        self.assertIn( 'CERN', resP['inst'][ 'code' ] )


    def test_searchUsers_fail( self ) :
        resG = self.callAndCheckGet( endPointName='api.searchUsers',
                                         data={ 'term' : 'Not, existing' },
                                         jsonKey=None, jsonLen=0 )
        # log.info( "got: %s " % str( res ) )
        self.assertEqual( len( resG ), len( [ ] ) )

    def test_searchUsers_fail2( self ) :
        res = self.callAndCheckGet( endPointName='api.searchUsers',
                                    data={},
                                    jsonKey=None, jsonLen=0 )
        # log.info( "got: %s " % str( res ) )
        self.assertEqual( len( res ), len( [ ] ) )

    def test_userByName( self ) :
        res = self.callAndCheckGet( endPointName='api.userByName',
                                        data = None,
                                        jsonKey='user', jsonLen=1,
                                        name='user, two - CERN (CERNid: 99001)')
        # log.info( "got: %s " % str( res ) )
        self.assertIn( 'user, two', res['user']['name'] )
        self.assertEqual( res[ 'user' ][ 'hrId' ], 99001 )

    def test_userByName_fail( self ) :
        res = self.callAndCheckGet( endPointName='api.userByName',
                                        data = None,
                                        jsonKey=None, jsonLen=0,
                                        name='Not, existing - UN (CERNid: -1)')
        # log.info( "got: %s " % str( res ) )
        self.assertIn( 'illegal search string', res['error'] )

    def test_userByHrId( self ) :
        res = self.callAndCheckGet( endPointName='api.getUserByHrId',
                                        data = None,
                                        jsonKey='user', jsonLen=1,
                                        hrId=99001 )
        # log.info( "got: %s " % str( res ) )
        self.assertIn( 'user, two', res['user']['name'] )
        self.assertEqual( res[ 'user' ][ 'hrId' ], 99001 )

    def test_searchUsersLdap( self ):

        res = self.callAndCheckGet( endPointName='api.searchUsersLdap',
                                        data = { 'term' : '422405' },
                                        jsonKey='', jsonLen=0 )
        log.info( "test_searchUsersLdap> got: %s " % str( res ) )
        self.assertIn( 'pfeiffer', res[0] )
        self.assertIn( '422405', res[0] )

    def test_ldapUserByHrId( self ):

        res = self.callAndCheckGet( endPointName='api.ldapUserByHrId',
                                        data = None,
                                        jsonKey='', jsonLen=0,
                                        hrId = 422405 )
        log.info( "test_ldapUserByHrId> got: %s " % str( res ) )
        self.assertIn( 'Pfeiffer, Andreas', res['user']['name'] )
        self.assertEqual( res['user'][ 'hrId' ], ['422405'] )

    def test_ldapUserByDN( self ):
        dn = 'CN=%s,OU=Users,OU=Organic Units,DC=cern,DC=ch' % 'pfeiffer'
        res = self.callAndCheckGet( endPointName='api.ldapUserByDN',
                                        data = None,
                                        jsonKey='', jsonLen=0,
                                        dn = dn )
        log.info( "test_ldapUserByHrId> got: %s " % str( res ) )
        self.assertIn( 'Pfeiffer, Andreas', res['user']['name'] )
        self.assertEqual( res['user'][ 'hrId' ], ['422405'] )

    def test_getShiftsForUser(self):
        res = self.callAndCheckGet( endPointName='api.getShiftsForUser',
                                     data=None,
                                     jsonKey='summary', jsonLen=1,
                                     name='Tester, Shifter' )
        # log.info( "got: %s " % str(res) )
        self.assertGreaterEqual( len( res[ 'shifts' ] ), 1 )

    def test_pledgesForTask(self):
        res = self.callAndCheckPost( endPointName='api.pledgesForTask',
                                     data= { 'code' : 't1' },
                                     jsonKey='data', jsonLen=2)
        # log.info( "got: %s " % str(res) )
        self.assertGreaterEqual( len( res[ 'data' ] ), 2 )
        # ensure
        self.assertIn( '<a href="/showUser/user1"> ', str( res[ 'data' ][ 1 ][ 0 ] ) )
        self.assertIn( '<a href="/showInstitute/CERN"', str( res[ 'data' ][ 1 ][ 1 ] ) )

    def test_userPledges( self ) :
        res = self.callAndCheckPost( endPointName='api.userPledges',
                                     data={ 'username' : 'user1' },
                                     jsonKey='allProjects', jsonLen=1 )
        # log.info( "got: %s " % str(res) )
        self.assertGreater( len( res[ 'allProjects' ][ 0 ] ), 0 )  # make sure we have at least one entry in the list[0] of lists

    def test_pmEmails(self):
        res = self.callAndCheckPost( endPointName='api.getPMemails',
                                     data={'project' : 'p2'},
                                     jsonKey='pmEmails', jsonLen=0)
        # log.info( "got: %s " % str(res) )
        #-toDo: somehow this does not work properly in the gitlab-CI ... :(
        # self.assertIn( 'Andreas.Pfeiffer@cern.ch', res['pmEmails'] )

    def test_deleteTaskNoAuth( self ) :
        # without being PM, this should fail

        res = self.client.post( url_for( 'api.deleteTask' ),
                                data={ 'year' : str(self.selYear), 'target' : None, 'taskCode' : 'del0' } )

        self.assertEqual( res.status_code, 403 )

        # log.info( 'callAndCheckPostNoLogin 1> response: %s' % response.data )
        json_response = json.loads( res.data.decode( 'utf-8' ) )

        self.assertGreaterEqual( len( json_response[ 'status' ] ), 1 )

        log.info('status: %s' % json_response['status'])
        self.assertIn( 'are not a manager for this task, sorry', json_response[ 'status' ] )

    def test_deleteActNoAuth( self ) :
        # without being PM, this should fail

        res = self.client.post( url_for( 'api.deleteActivity' ),
                                data={ 'year' : str(self.selYear), 'target' : None, 'actCode' : 'ad0' } )

        self.assertEqual( res.status_code, 403 )

        # log.info( 'callAndCheckPostNoLogin 1> response: %s' % response.data )
        json_response = json.loads( res.data.decode( 'utf-8' ) )

        self.assertGreaterEqual( len( json_response[ 'status' ] ), 1 )

        log.info('status: %s' % json_response['status'])
        self.assertIn( 'are not a manager for this activity, sorry', json_response[ 'status' ] )

    def test_manageInstUsers(self):

        # list of users with suspended and author flags:
        userData = 'susp-9900=yes&susp-9901=no&auth-9900=no&auth-9901=yes'

        res = self.callAndCheckPost( endPointName='api.manageInstUsers',
                                     data = { 'year' : str(self.selYear), 'data' : userData, 'instCode' : 'CERN' },
                                     jsonKey='status', jsonLen=1 )

        # log.info( "got: (type: %s) %s " % (type(res), str(res)) )

        log.info( 'status: %s' % res['status'] )
        self.assertGreater( len( res[ 'status' ] ), 0 )  # make sure we have at least one entry in the list[0] of lists
        self.assertIn( 'successfully updated info for', res['status'])

    def test_manageProjTasks(self):

        patData = 'taskStat-t1=active&taskStat-t2=deleted&taskLock-t3=no&taskLock-t4=yes'

        res = self.callAndCheckPost( endPointName='api.manageProjTasks',
                                     data = { 'year' : str(self.selYear), 'data' : patData, 'projCode' : 'p1' },
                                     jsonKey='status', jsonLen=1 )

        # log.info( "got: (type: %s) %s " % (type(res), str(res)) )

        log.info( 'status: %s' % res['status'] )
        self.assertGreater( len( res[ 'status' ] ), 0 )  # make sure we have at least one entry in the list[0] of lists
        self.assertIn( 'successfully updated info for', res[ 'status' ] )

    def test_getShiftTaskMap(self):
        res = self.callAndCheckGet( endPointName='api.getShiftTaskMap',
                                     data=None,
                                     jsonKey='result', jsonLen=1,
                                     name='Tester, Shifter' )
        log.info( "test_getShiftTaskMap> got: %s " % str(res) )
        # check we can not really manage anything:
        self.assertEqual( u'', res['data'][0]['manageButton'] )
        self.assertNotIn( 'remove', res['data'][0]['shiftMapInfo'].lower() )


    def test_getShiftEntities(self):
        res = self.callAndCheckPost( endPointName='api.getShiftEntities',
                                     data={ 'year' : self.selYear, 'subSystems' : ['pr1', 'pr2']},
                                     jsonKey='result', jsonLen=1,
                                     name='Tester, Shifter' )
        log.info( "test_getShiftTaskMap> got: %s " % str(res) )
        self.assertIn( 'Sorry you are not allowed to manage shifts', res[ 'msg' ] )


    def test_changeUserInst ( self ):
        res = self.callAndCheckPost( endPointName='api.changeUserInst',
                                    data={ 'instCode':'DESY', 'cmsId' : 9903, 'year' : self.selYear},
                                    jsonKey='status', jsonLen=1, retCodeExp=200, followRedir=True )
        # log.info ('data: %s' % str(res [ 'data' ]) )
        self.assertIn( 'error', res['status'] )
        self.assertIn( 'Sorry you are not allowed to change institute', res['msg'] )

    def test_getObservers(self):
        # get managers for one task
        response = self.callAndCheckPost( endPointName='api.getObservers', task='t1',
                              data={},
                              jsonKey='obs', jsonLen=1 )
        # log.info( 'test_getObservers> len(response): %s' % len(response['mgr']) )
        log.info( 'test_getObservers> response: %s' % response['obs'] )
        self.assertListEqual( [u't1, obs1'], response['obs']['names'] )
        self.assertEqual( 1, len( response[ 'obs' ][ 'obsFull' ]) )

    def test_projCeilings( self ):

        response = self.callAndCheckPost( endPointName='api.projCeilings',
                              data = { 'projects' : json.dumps( [ {'p1' : {} }, { 'p2': {} } ] ) },
                              jsonKey='data', jsonLen=0 )
        # log.info( 'test_projCeilings> len(response): %s' % len(response['mgr']) )
        log.info( 'test_projCeilings> response: %s' % response['data'] )

    def test_updateCeilingsFail( self ):

        # only allowed for admins, so we should get a 401
        response = self.callAndCheckPost( endPointName='api.updateCeilings',
                                          data={},
                                          jsonKey='status', jsonLen=0,
                                          expectedHtmlCode=401,
                                          # showPage = True
                                          )

        print( '===>>> here: ', str(response) )

        log.info( 'test_updateCeilingsFail 1> response: %s' % str(response) )
        log.info( 'test_updateCeilingsFail 2> response: %s' % response['status'] )
        self.assertEqual( response[ 'status' ], "ERROR" )

    def test_projStats( self ):

        response = self.callAndCheckGet( endPointName='api.projStats',
                              data = {'proj' : -1 , 'year': self.selYear },
                              jsonKey='data', jsonLen=1 )
        # log.info( 'test_projStats> len(response): %s' % len(response['mgr']) )
        log.info( 'test_projStats> response: %s' % response['data'] )
