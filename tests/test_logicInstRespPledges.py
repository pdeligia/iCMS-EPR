#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import json
import re

try:
    from .BaseTestClass import *
except ImportError:
    from BaseTestClass import *

from sqlalchemy.exc import IntegrityError

from app.main.Helpers import getFavIRTPledgeCodes, getFavPledgeCodes, getFavTaskCodes

from app import db

logging.basicConfig( stream=sys.stderr )
log = logging.getLogger( "Testing.test_logic" )
log.setLevel( logging.DEBUG )

def addAndCommit( what ):

    db.session.add( what )
    try:
        db.session.commit()
    except IntegrityError:
        db.session.rollback()

    return

def getInstRespTask1Data(tName='InstResp-1', tDesc='inst resp task 1 (a5/p1) new', need=42):
    return { "name" : tName,
             "description" : tDesc,
             "activityId" : 1,
             "pctAtCERN" : 10,
             "tType" : 'InstResp',
             "shiftTypeId" : '',
             "comment" : "no comment",
             "locked" : 'false',
             "kind" : 'CORE',
             'neededWork': need,
             'contactName' : 'ethUser, five (CERNid: 97005)',
             'level3': '1 : Unknown',
             }

def getInstRespPledge1AdminData(instCode, userList, tId=None, tName=None, selYear=None):

    if not tId and not tName:
        log.error( "error: neither task id nor task name given ... " )
        return None

    if not tId:
        tId = db.session.query(Task).filter(Task.name==tName).filter(Task.year==selYear).one().id

    return { 'year'        : selYear,
             'instCode'    : instCode,
             'userIdList'  : ','.join([str(x) for x in userList]),
             'taskId'      : tId,
             'workPledged' : 15.,
             'contactName' : 'ethUser, five (CERNid: 97005)',
             }

class InstRespPledgesTestCase( BaseTestCase ) :

    def getEprUser(self, name):
        u1 = db.session.query(EprUser).filter_by(username=name).one()
        self.assertIsNotNone(u1)
        # self.assertTrue(u1.can(Permission.READ))
        return u1

    def test_aacreateInstRespTask(self, tName='InstResp-1', tDesc='inst resp task 1 (a5/p1) new', need=100):

        p1m1 = self.getEprUser('p1m1')
        # check specifics for act-admin
        self.assertTrue(p1m1.canManage('project', 'p1', year=self.selYear))
        self.assertTrue(p1m1.canManage('activity', 'a5', year=self.selYear))

        # admin creates InstResp task ...
        response = self.login(p1m1.username)
        self.assertEqual(response.status_code, 200)
        response = self.client.post( url_for('main.newTask', actCode='a5'),
                                     data = getInstRespTask1Data(tName, tDesc, need),
                                     follow_redirects=True )
        self.assertEqual(response.status_code, 200)
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertIn('A new task was successfully created.', response.data.decode('utf-8')[:20000])
        # self.assertIn('iCMS - EPR New Task  for', response.data.decode('utf-8'))
        self.logout()

        return

    def test_bbnewInstRespPldPage( self ):
        cernLdr = self.getEprUser( 'cernLdr' )
        self.assertTrue( cernLdr.canManage( 'institute', 'CERN', year=self.selYear ) )

        # team leader creates instResp pledge for this task ...
        response = self.login( cernLdr.username )
        self.assertEqual( response.status_code, 200 )

        response = self.client.get( url_for( 'main.newInstRespPledge', instCode='CERN'),
                                     follow_redirects=True )
        self.assertEqual( response.status_code, 200 )

        result = response.data.decode('utf-8')[:30000]
        # log.info("got response data %s " % result )
        self.assertNotIn( 'class="alert alert-"', result )
        self.assertIn( 'Creating New Pledges for Institute Responsibility task for CERN in %d' % self.selYear, result)
        self.logout()


    def test_bbnewInstRespPldPageFail0( self ):
        cernLdr = self.getEprUser( 'cernLdr' )
        self.assertTrue( cernLdr.canManage( 'institute', 'CERN', year=self.selYear ) )

        # team leader creates instResp pledge for this task ...
        response = self.login( cernLdr.username )
        self.assertEqual( response.status_code, 200 )

        response = self.client.get( url_for( 'main.newInstRespPledge', instCode='ETHZ'),
                                     follow_redirects=True )
        self.assertEqual( response.status_code, 200 )

        result = response.data.decode('utf-8')[:30000]
        # log.info("got response data %s " % result )
        self.assertNotIn( 'class="alert alert-"', result )
        self.assertIn( 'Sorry, you (cernLdr) are not allowed to create Institute Responsibility pledges for ETHZ in %d' % self.selYear, result)

        self.logout()

    def test_bbnewInstRespPldPageFail1( self ):
        cernUser1 = self.getEprUser( 'user1' )
        self.assertFalse( cernUser1.canManage( 'institute', 'CERN', year=self.selYear ) )

        # team leader creates instResp pledge for this task ...
        response = self.login( cernUser1.username )
        self.assertEqual( response.status_code, 200 )

        response = self.client.get( url_for( 'main.newInstRespPledge', instCode='CERN'),
                                     follow_redirects=True )
        self.assertEqual( response.status_code, 200 )

        result = response.data.decode('utf-8')[:30000]
        # log.info("got response data %s " % result )
        self.assertIn( 'Sorry, you (user1) are not allowed to create Institute Responsibility pledges for CERN in %d' % testYear, result)
        self.logout()

    def test_bbnewInstRespPledge( self ):

        # create a new pledge for an InstResp task
        ethLdr = self.getEprUser( 'ethLdr' )
        self.assertTrue( ethLdr.canManage( 'institute', 'ETHZ', year=self.selYear ) )

        userList = [ 9700, 9701, 9702, 9703 ]
        taskName = 'InstResp-1'
        taskData = getInstRespPledge1AdminData(instCode='ETHZ', userList=userList, tName=taskName, selYear=self.selYear)
        self.assertIsNotNone( taskData )

        # team leader creates instResp pledge for this task ...
        response = self.login( ethLdr.username )
        self.assertEqual( response.status_code, 200 )
        response = self.client.post( url_for( 'api.createInstRespPledge'),
                                     data=taskData,
                                     follow_redirects=True )
        self.assertEqual( response.status_code, 200 )

        result = json.loads(response.data.decode('utf-8'))
        # log.info("got response data %s " % result )
        self.assertIn( 'OK', result['status'])
        self.assertIn( 'successfully created 4 pledges (3.6 months each) for ETHZ for InstResp task', result['msg'])

        irTask = db.session.query(Task).filter(Task.name==taskName).filter(Task.year==self.selYear).one()
        irtPlCodes = [ x[0] for x in db.session.query(Pledge.code).filter(Pledge.taskId==irTask.id).filter(Pledge.status!='rejected').all() ]
        contact = db.session.query(EprUser).filter(EprUser.hrId==97005).one()
        favTask = getFavTaskCodes(contact, self.selYear)
        favPld = getFavPledgeCodes(contact, self.selYear)
        irtPld = getFavIRTPledgeCodes(contact, self.selYear)
        self.assertEqual( [irTask.code], favTask)
        self.assertEqual( irtPlCodes, irtPld)
        self.assertEqual( [], favPld)


    def test_editInstRespPledge( self ):

        p1m1 = self.getEprUser( 'p1m1' )
        # check specifics for manager
        self.assertTrue( p1m1.canManage( 'project', 'p1', year=self.selYear ) )
        self.assertTrue( p1m1.canManage( 'activity', 'a5', year=self.selYear ) )

        task = db.session.query(Task).filter(Task.name == 'InstResp-1').one()
        self.assertIsNotNone( task )
        pledges = db.session.query(Pledge).filter(Pledge.taskId == task.id).all()
        self.assertIsNot( pledges, [] )

        formData = { 'workTimeAcc': 15.,
                    'workTimeDone': 15.,
                    'workedSoFar': 15.,
                    'status': 'accepted',
                     }

        # project/act/task manager to edit the task
        response = self.login( p1m1.username )
        self.assertEqual( response.status_code, 200 )
        response = self.client.post( url_for( 'main.editInstRespPledge', taskId=task.id, instCode='ETHZ'),
                                     data=formData,
                                     follow_redirects=True )
        self.assertEqual( response.status_code, 200 )

        result = response.data.decode('utf-8')[:35000]
        # log.info("got response data %s " % result )
        self.assertNotIn( 'Sorry, you are not allowed to edit pledges for InstResp task InstResp-1 for inst', result)
        self.assertIn( 'Successfully updated 4 (of 4) pledges for InstResp task InstResp-1 (%d) and inst ETHZ in %d' % (task.id, self.selYear), result)

    def test_yyrejectInstRespPledgeFail( self ):

        ethLdr = self.getEprUser( 'ethLdr' )
        # check specifics for manager
        self.assertFalse( ethLdr.canManage( 'project', 'p1', year=self.selYear ) )
        self.assertFalse( ethLdr.canManage( 'activity', 'a5', year=self.selYear ) )
        self.assertTrue( ethLdr.canManage( 'institute', 'ETHZ', year=self.selYear ) )

        task = db.session.query(Task).filter(Task.name == 'InstResp-1').one()
        self.assertIsNotNone( task )
        pledges = db.session.query(Pledge).filter(Pledge.taskId == task.id).all()
        self.assertIsNot( pledges, [] )

        formData = { 'reason': 'testing rejection of InstResp pledge' }

        # team leader should not be allowed to do this ...
        response = self.login( ethLdr.username )
        self.assertEqual( response.status_code, 200 )
        response = self.client.post( url_for( 'main.rejectInstRespPledge', taskId=task.id, instCode='ETHZ'),
                                     data=formData,
                                     follow_redirects=True )
        self.assertEqual( response.status_code, 200 )

        result = response.data.decode('utf-8')[:35000]
        # log.info("got response data %s " % result )
        self.assertIn( 'Sorry, you are not allowed to reject pledges for InstResp task InstResp-1 for inst ETHZ', result)

    def test_zzrejectInstRespPledge( self ):

        p1m1 = self.getEprUser( 'p1m1' )
        # check specifics for manager
        self.assertTrue( p1m1.canManage( 'project', 'p1', year=self.selYear ) )
        self.assertTrue( p1m1.canManage( 'activity', 'a5', year=self.selYear ) )

        task = db.session.query(Task).filter(Task.name == 'InstResp-1').one()
        self.assertIsNotNone( task )
        pledges = db.session.query(Pledge).filter(Pledge.taskId == task.id).all()
        self.assertIsNot( pledges, [] )

        formData = { 'reason': 'testing rejection of InstResp pledge' }

        # project/act/task manager to edit the task
        response = self.login( p1m1.username )
        self.assertEqual( response.status_code, 200 )
        response = self.client.post( url_for( 'main.rejectInstRespPledge', taskId=task.id, instCode='ETHZ'),
                                     data=formData,
                                     follow_redirects=True )
        self.assertEqual( response.status_code, 200 )

        result = response.data.decode('utf-8')[:35000]
        # log.info("got response data %s " % result )
        self.assertNotIn( 'Sorry, you are not allowed to reject pledges for InstResp task InstResp-1 for inst CERN', result)
        self.assertIn( 'Rejected %d pledges for InstResp task InstResp-1 (%d) and inst ETHZ in %d' % (len(pledges), task.id, self.selYear), result)


    def test_editInstRespPledgeFail0( self ):

        p2m1 = self.getEprUser( 'p2m1' )
        # check specifics for manager
        self.assertFalse( p2m1.canManage( 'project', 'p1', year=self.selYear ) )
        self.assertFalse( p2m1.canManage( 'activity', 'a5', year=self.selYear ) )

        task = db.session.query(Task).filter(Task.name == 'InstResp-1').one()
        self.assertIsNotNone( task )
        pledges = db.session.query(Pledge).filter(Pledge.taskId == task.id).all()
        self.assertIsNot( pledges, [] )

        formData = { 'workTimeAcc': 15.,
                    'workTimeDone': 15.,
                    'workedSoFar': 15.,
                    'status': 'accepted',
                     }

        response = self.login( p2m1.username )
        self.assertEqual( response.status_code, 200 )

        # manager is not a manager for _this_ project, should fail
        response = self.client.post( url_for( 'main.editInstRespPledge', taskId=task.id, instCode='ETHZ'),
                                     data=formData,
                                     follow_redirects=True )
        self.assertEqual( response.status_code, 200 )

        result = response.data.decode('utf-8')[:35000]
        # log.info("got response data %s " % result )
        self.assertIn( 'Sorry, you are not allowed to edit pledges for InstResp task InstResp-1 for inst ETHZ', result)

    def test_createTaskFail0( self, tName='InstResp-1', tDesc='inst resp task 1 (a5/p1) new', need=77 ) :
        # create a new task which already exists in the DB
        p1m1 = self.getEprUser( 'p1m1' )
        # check specifics for act-admin
        self.assertTrue( p1m1.canManage( 'project', 'p1', year=self.selYear ) )
        self.assertTrue( p1m1.canManage( 'activity', 'a5', year=self.selYear ) )

        taskData = getInstRespTask1Data( tName, tDesc, need )
        taskData['name'] = 'InstResp-1'

        # admin creates task ...
        response = self.login( p1m1.username )
        self.assertEqual( response.status_code, 200 )
        response = self.client.post( url_for( 'main.newTask', actCode='a5' ),
                                     data=taskData,
                                     follow_redirects=True )
        self.assertEqual( response.status_code, 200 )
        result = response.data.decode( 'utf-8' )[:35000]
        # log.info("got response data %s " % str(result) )
        self.assertIn( 'ERROR - Integrity error from database, the name of the task is already used in this activity.', result )
        # self.assertIn('iCMS - EPR New Task  for', result)

        return

    def test_newInstRespPledgeFail1( self ):

        # create a new pledge for an InstResp task
        ethLdr = self.getEprUser( 'ethLdr' )
        self.assertTrue( ethLdr.canManage( 'institute', 'ETHZ', year=self.selYear ) )

        # empty user list should fail:
        taskData = getInstRespPledge1AdminData(instCode='ETHZ', userList=[], tName='InstResp-1', selYear=self.selYear)
        self.assertIsNotNone( taskData )

        # team leader creates instResp pledge for this task ...
        response = self.login( ethLdr.username )
        self.assertEqual( response.status_code, 200 )

        response = self.client.post( url_for( 'api.createInstRespPledge'),
                                     data=taskData,
                                     follow_redirects=True )
        self.assertEqual( response.status_code, 400 )

        result = json.loads(response.data.decode('utf-8'))
        # log.info("got response data %s " % str(response.data.decode('utf-8')) )
        self.assertIn( 'ERROR', result['status'])
        self.assertIn( 'ERROR: no users specified', result['msg'])

    def test_newInstRespPledgeFail2( self ):

        # create a new pledge for an InstResp task
        ethUser4 = self.getEprUser( 'userEth4' )
        self.assertFalse( ethUser4.canManage( 'institute', 'ETHZ', year=self.selYear ) )

        userList = [ 9700, 9701, 9702 ]
        taskData = getInstRespPledge1AdminData(instCode='ETHZ', userList=userList, tName='InstResp-1', selYear=self.selYear)
        self.assertIsNotNone( taskData )

        # user not a team leader, creating a pledge should fail:
        response = self.login( ethUser4.username )
        self.assertEqual( response.status_code, 200 )

        response = self.client.post( url_for( 'api.createInstRespPledge'),
                                     data=taskData,
                                     follow_redirects=True )
        self.assertEqual( response.status_code, 400 )

        result = json.loads(response.data.decode('utf-8'))
        log.info("got response data %s " % str(result) )
        self.assertIn( 'ERROR', result['status'])
        self.assertIn( 'Sorry, you (userEth4) are not allowed to create Institute Responsibility pledges for ETHZ in %d' % self.selYear, result['msg'])

    def test_newInstRespPledgeFail3( self ):

        # create a new pledge for an InstResp task
        cernLdr = self.getEprUser( 'cernLdr' )
        self.assertFalse( cernLdr.canManage( 'institute', 'ETHZ', year=self.selYear ) )

        # a team leader can't make pledges for other teams -- should fail:
        taskData = getInstRespPledge1AdminData(instCode='ETHZ', userList=[cernLdr.cmsId], tName='InstResp-1', selYear=self.selYear)
        self.assertIsNotNone( taskData )

        # team leader creates instResp pledge for this task ...
        response = self.login( cernLdr.username )
        self.assertEqual( response.status_code, 200 )

        response = self.client.post( url_for( 'api.createInstRespPledge'),
                                     data=taskData,
                                     follow_redirects=True )
        self.assertEqual( response.status_code, 400 )

        result = json.loads(response.data.decode('utf-8'))

        log.info("got response data %s " % str(result) )
        self.assertIn( 'ERROR', result['status'])
        self.assertIn( 'Sorry, you (cernLdr) are not allowed to create Institute Responsibility pledges for ETHZ in %d' % self.selYear, result['msg'])
