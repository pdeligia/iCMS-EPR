
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import unittest
from flask import current_app
from app import create_app

from app.models import Task, Pledge
from app.main.views import setupYearForm, checkPledgeApproval, checkPledgeDone
from app.main.Helpers import getInstituteInfo, getShiftTaskSummary, userCanManageTask

from .BaseTestClass import *

class HelpersTestCase( BaseTestCase ):

    def test_setupYearForm(self):
        form, selYear = setupYearForm()
        # current_app.logger.info('test_checkPledgeApproval> got form: %s' % str(form) )
        self.assertEqual(selYear, 2016)

    def test_checkPledgeApproval(self):
        task = db.session.query(Task).filter_by(code='t2').one() # 9 months needed
        plCode = '%d+5+1' % task.id
        pld  = db.session.query(Pledge).filter_by(code=plCode).one()
        result = checkPledgeApproval( workTimePld=1, workTimeAcc=1, workedSoFar=0, workTimeDone=1,
                                      task = task,
                                      plPrevJson = pld.to_json() )
        # current_app.logger.info('test_checkPledgeApproval> got result: %s' % str(result) )
        self.assertTrue( result[0] )

        result = checkPledgeApproval( workTimePld=13, workTimeAcc=1, workedSoFar=0, workTimeDone=1,
                                      task = task,
                                      plPrevJson = pld.to_json() )
        # current_app.logger.info('test_checkPledgeApproval> got result: %s' % str(result) )
        self.assertFalse( result[0] )

        result = checkPledgeApproval( workTimePld=12, workTimeAcc=1, workedSoFar=0, workTimeDone=1,
                                      task = task,
                                      plPrevJson = pld.to_json() )
        # current_app.logger.info('test_checkPledgeApproval> got result: %s' % str(result) )
        self.assertFalse( result[0] )

        result = checkPledgeApproval( workTimePld=1, workTimeAcc=1, workedSoFar=2, workTimeDone=1,
                                      task = task,
                                      plPrevJson = pld.to_json() )
        # current_app.logger.info('test_checkPledgeApproval> got result: %s' % str(result) )
        self.assertFalse( result[0] )

        result = checkPledgeApproval( workTimePld=2, workTimeAcc=1, workedSoFar=0, workTimeDone=3,
                                      task = task,
                                      plPrevJson = pld.to_json() )
        # current_app.logger.info('test_checkPledgeApproval> got result: %s' % str(result) )
        self.assertFalse( result[0] )

    def test_checkPledgeDone(self):
        task = db.session.query(Task).filter_by(code='t2').one() # 9 months needed
        plCode = '%d+5+1' % task.id
        pld  = db.session.query(Pledge).filter_by(code=plCode).one()
        result = checkPledgeDone( workTimePld=1, workTimeAcc=1, workedSoFar=0, workTimeDone=1,
                                      task = task,
                                      plPrevJson = pld.to_json() )
        # current_app.logger.info('checkPledgeDone> got result: %s' % str(result) )
        self.assertTrue( result[0] )

        result = checkPledgeDone( workTimePld=13, workTimeAcc=1, workedSoFar=0, workTimeDone=1,
                                      task = task,
                                      plPrevJson = pld.to_json() )
        # current_app.logger.info('checkPledgeDone> got result: %s' % str(result) )
        self.assertFalse( result[0] )

        result = checkPledgeDone( workTimePld=12, workTimeAcc=1, workedSoFar=0, workTimeDone=1,
                                      task = task,
                                      plPrevJson = pld.to_json() )
        # current_app.logger.info('checkPledgeDone> got result: %s' % str(result) )
        self.assertFalse( result[0] )

        result = checkPledgeDone( workTimePld=1, workTimeAcc=1, workedSoFar=2, workTimeDone=1,
                                      task = task,
                                      plPrevJson = pld.to_json() )
        # current_app.logger.info('checkPledgeDone> got result: %s' % str(result) )
        self.assertFalse( result[0] )

        result = checkPledgeDone( workTimePld=2, workTimeAcc=1, workedSoFar=0, workTimeDone=3,
                                      task = task,
                                      plPrevJson = pld.to_json() )
        # current_app.logger.info('checkPledgeDone> got result: %s' % str(result) )
        self.assertFalse( result[0] )

    def test_showCountry(self):

        self.login( )

        endPointName = 'main.showCountry'
        response = self.client.post( url_for( endPointName, name='foo' ) )
        # current_app.logger.info('test_showCountry> got result: %s' % str(response) )
        self.assertEqual( response.status_code, 200 )


    def test_showUser(self):

        self.login( )

        endPointName = 'main.showUser'
        data = { 'username' : 'p1m1' }
        response = self.client.get( url_for( endPointName, username=None ), data=data )
        # current_app.logger.info('test_showUser> got result: %s' % str(response) )
        self.assertEqual( response.status_code, 302 )

    def test_createNewPledge(self):

        self.login( )

        endPointName = 'main.newPledge'
        data = { 'username' : 'p1m1' }
        response = self.client.get( url_for( endPointName, taskCode='notExistingTask' ), data=data )
        current_app.logger.info('test_showUser> got result: %s' % str(response) )
        self.assertEqual( response.status_code, 302 )

        endPointName = 'main.newPledge'
        data = { 'username' : 'p1m1' }
        response = self.client.get( url_for( endPointName, taskCode='ts1' ), data=data )
        current_app.logger.info('test_showUser> got result: %s' % str(response) )
        self.assertEqual( response.status_code, 302 )

    def test_getInstituteInfo(self):

        response = getInstituteInfo( code='foo', year=1970 )
        current_app.logger.info('test_getInstituteInfo> got result: %s' % str(response) )
        self.assertEqual( response[0], None )
        self.assertEqual( response[2]['sumEprDue'], -1 )

    def test_getShiftTaskSummary(self):

        response = getShiftTaskSummary( taskCode='ts1', selYear=self.selYear)

        current_app.logger.info('test_getShiftTaskSummary> got result: %s' % str(response) )
        # self.assertEqual( (5, 0.3230769230769231, 0.0), response )
        # self.assertEqual( (2, 0.13846153846153847, 0.0), response )
        self.assertEqual((4, 0.27692307692307694, 0.0), response)
        # self.assertEqual((3, 0.18461538461538463, 0.046153846153846156), response)

    def test_userCanManageTask(self):

        response = userCanManageTask(user=db.session.query(EprUser).filter_by(username='p1m1').one(), tCode='ts1', year=self.selYear)
        current_app.logger.info('test_userCanManageTask> got result: %s' % str(response) )
        self.assertFalse( response )

        response = userCanManageTask(user=db.session.query(EprUser).filter_by(username='pfeiffer').one(), tCode='ts1', year=self.selYear)
        current_app.logger.info('test_userCanManageTask> got result: %s' % str(response) )
        self.assertTrue( response )

