
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import logging

import sys
import os
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from base64 import b64encode
from unittest import TestCase

# here for all the inheriting classes ...
import json
import pytest
from flask import url_for
from flask_login import current_user

from app.models import *

from app.main.Helpers import getRequiredWork, getPledgeYear

from app import create_app, db

log = logging.getLogger( "Testing.test_manage" )
logging.basicConfig( stream=sys.stderr )
logging.getLogger( "Testing.test_manage" ).setLevel( logging.DEBUG )

testYear = 2022

def getHtmlDoc( respData ):
    htmlDoc = respData.data.decode( 'utf-8' )
    scriptStart = htmlDoc.find( 'function flask_moment_render(' )
    if scriptStart > -1:
        return htmlDoc[:scriptStart]
    else:
        return htmlDoc

class BaseTestCase( TestCase ) :

    def setUp(self):
        self.defaultUser = 'tester'

        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        # self.app_context = self.app.test_request_context()
        self.app_context.push()
        self.client = self.app.test_client()

        self.selYear = getPledgeYear(defaultYear=testYear)
        self.monthsPerAuthor = getRequiredWork( self.selYear )

        self.assertEquals( self.selYear, testYear )
        self.assertNotEqual( self.monthsPerAuthor, 0 )

        log.info( 'test case %s set up ... ' % type(self).__name__ )

        self.login()

    def tearDown(self):
        db.session.remove()
        # db.drop_all(app=self.app)
        db.get_engine( self.app ).dispose( )
        # print db.engine.pool.status()
        if self.app_context is not None:
            self.app_context.pop()

    def get_api_headers(self, username, password):
        return {
            'Authorization': 'Basic ' + b64encode(
                (username + ':' + password).encode('utf-8')).decode('utf-8'),
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }

    def login(self, username=None, password='fffff'):

        if username is None:
            username = self.defaultUser

        response = self.client.post( url_for('auth.login'),
                                 data=dict(
                                    username=username,
                                    password=password
                                 ),
                                 follow_redirects=True
                                )
        # log.info( 'BaseTestCase::login> user: %s response: %s' % (username, str(response.data)) )
        self.assertIn( response.status_code, [200] )

        # ensure we did not get redirectec to the login page again:
        self.assertNotIn( str(response.data.decode( 'utf-8' )), '<title>iCMS-EPR - Login</title>' )

        return response

    def logout(self):
        db.session.flush()
        try:
            return self.client.get('/auth/logout')
        except RuntimeError as e:
            if "<class 'flask.testing.FlaskClient'> does not support redirect to external targets" in str(e):
                return
            else:
                raise e

    def getUser(self, name):
        u1 = db.session.query(EprUser).filter_by(username=name).one()
        self.assertIsNotNone(u1)
        # self.assertTrue(u1.can(Permission.READ))
        return u1

    def callAndCheck( self, endPointName, jsonKey, jsonLen, data, **kwargs ) :
        resG = self.callAndCheckGet( endPointName=endPointName, jsonKey=jsonKey, jsonLen=jsonLen, data=data,
                                     **kwargs )
        resP = self.callAndCheckPost( endPointName=endPointName, jsonKey=jsonKey, jsonLen=jsonLen, data=data,
                                      **kwargs )
        return resG, resP

    def callAndCheckPost( self, endPointName, jsonKey, jsonLen, data, showPage=False, expectedHtmlCode=200, **kwargs ) :
        self.login( )

        if data :
            response = self.client.post( url_for( endPointName, **kwargs ), data=data, follow_redirects=True )
        else :
            response = self.client.post( url_for( endPointName, **kwargs ), follow_redirects=True )

        self.assertEqual( expectedHtmlCode, response.status_code )

        if showPage: log.info( 'callAndCheckPost 1> response: %s' % response.data )

        self.assertNotIn( str(response.data.decode( 'utf-8' )), '<title>iCMS-EPR - Login</title>' )

        json_response = json.loads( response.data.decode( 'utf-8' ) )
        if jsonKey :
            self.assertGreaterEqual( len( json_response[ jsonKey ] ), jsonLen )
        else :  # only a list is returned
            self.assertGreaterEqual( len( json_response ), jsonLen )

        # now try again w/o being logged in ... this should fail
        self.logout( )

        if data :
            response = self.client.post( url_for( endPointName, **kwargs ), data=data, follow_redirects=True )
        else :
            response = self.client.post( url_for( endPointName, **kwargs ), follow_redirects=True )
        # log.info( 'callAndCheckPost 2> response: %s' % response.data )
        # log.info( 'callAndCheckPost 3> json len of data: %s ' % len(json_response[ 'projects' ]) )

        # if we expect a "no permission" for the page, we should now check that we're redirected to the login page
        if expectedHtmlCode == 401:
            self.assertIn( '<title>iCMS-EPR - Login</title>', response.data.decode('utf-8') )
        else:
            self.assertIn( response.status_code, [expectedHtmlCode, 302] )
        # self.assertFalse( response.status_code == expectedHtmlCode, msg='Anonymous access to page should not be possible.' )

        return json_response

    def callAndCheckGet( self, endPointName, jsonKey, jsonLen, data, expectedHtmlCode=200, **kwargs ) :
        self.login( )

        # log.info('callAndCheckGet> going to call %s with data %s ' % (endPointName, data) )
        if data :
            response = self.client.get( url_for( endPointName, **kwargs ),
                                        data=data )
        else :
            response = self.client.get( url_for( endPointName, **kwargs ) )

        self.assertEqual( response.status_code, expectedHtmlCode )

        # log.info( 'callAndCheckGet 1> response: %s' % response.data )
        json_response = json.loads( response.data.decode( 'utf-8' ) )
        if jsonKey :
            self.assertGreaterEqual( len( json_response[ jsonKey ] ), jsonLen )
        else :  # only a list is returned
            self.assertGreaterEqual( len( json_response ), jsonLen )

        # now try again w/o being logged in ... should fail
        self.logout( )

        if data :
            response = self.client.get( url_for( endPointName, **kwargs ), data=data, follow_redirects=True )
        else :
            response = self.client.get( url_for( endPointName, **kwargs ), follow_redirects=True )
        # log.info( 'callAndCheckGet 2> response: %s' % response.data )
        # log.info( 'callAndCheckGet 3> json len of data: %s ' % len(json_response[ 'projects' ]) )

        self.assertIn( response.status_code, [expectedHtmlCode, 302] )

        return json_response
